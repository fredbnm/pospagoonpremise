﻿using Meep.Pos.Domain.Core.Models;
using Meep.Pos.Infra.Data.Core.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Meep.Pos.Infra.Data.Core
{
    public class CacheRepository<TEntity> : ICacheRepository<TEntity> where TEntity : BaseModel
    {
        private IMemoryCache _memoryCache;
        private MemoryCacheEntryOptions _memoryOptions;
        private string _key;

        public CacheRepository(IMemoryCache memoryCache, MemoryCacheEntryOptions memoryOptions, string key)
        {
            _memoryCache = memoryCache;
            _memoryOptions = memoryOptions;
            _key = key;
        }

        public bool Delete(Guid id)
        {
            var items = new List<TEntity>();
            if (_memoryCache.TryGetValue(_key, out items))
            {
                var item = items.FirstOrDefault(p => p.Id == id);
                var removed = items.Remove(item);

                _memoryCache.Set(_key, items);
                return removed;
            }

            return false;
        }

        public TEntity Get(Guid id)
        {
            var items = new List<TEntity>();
            if (_memoryCache.TryGetValue(_key, out items))
            {
                var item = items.FirstOrDefault(i => i.Id == id);

                return item;
            }

            return null;
        }

        public IList<TEntity> GetAll()
        {
            var items = new List<TEntity>();
            if (_memoryCache.TryGetValue(_key, out items))
                return items;
            else
                return new List<TEntity>();
        }

        public TEntity Insert(TEntity entity)
        {
            var items = _memoryCache.Get<List<TEntity>>(_key);

            if (items == null)
                items = new List<TEntity>();

            items.Add(entity);

            _memoryCache.Set(_key, items, _memoryOptions);

            return entity;
        }

        public TEntity Update(TEntity entity)
        {
            return null;
        }
    }
}
