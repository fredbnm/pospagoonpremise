﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Meep.Pos.Infra.Data.Repository.Core.Interfaces
{
    public interface IRepository<TEntity>
    {
        TEntity Insert(TEntity item);
        bool Remove(Guid id);
        TEntity Get(Guid id);
        IEnumerable<TEntity> GetAll();
        TEntity Update(TEntity item);
        IEnumerable<TEntity> GetByFilter(Func<TEntity, bool> filter);
    }
}
