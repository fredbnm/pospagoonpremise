﻿using Meep.Pos.Domain.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Infra.Data.Core.Interfaces
{
    public interface ICacheRepository<TEntity>
    {
        TEntity Get(Guid id);
        IList<TEntity> GetAll();
        TEntity Insert(TEntity entity);
        //TEntity Update(TEntity entity);
        bool Delete(Guid id);
    }
}
