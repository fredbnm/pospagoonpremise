﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Infra.Data.Core.Interfaces
{
    public interface IConnectionProvider
    {
        IMongoCollection<TEntity> GetCollection<TEntity>();
    }
}
