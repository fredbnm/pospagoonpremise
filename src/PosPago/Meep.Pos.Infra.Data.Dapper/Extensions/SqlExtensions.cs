﻿using Flepper.QueryBuilder;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Infra.Data.Dapper.Extensions
{
    public static class SqlExtensions
    {
        public static QueryResult MySqlAdapter(this QueryResult query)
        {
            query.Query.Replace("[", "").Replace("]", "");
            return query;
        }

        public static string MySqlAdapter(this string query)
        {
            query = query.Replace("[", "").Replace("]", "");
            return query;
        }
    }
}
