﻿using System;
using System.Collections.Generic;
using System.Text;
using Dapper;
using System.Data;
using System.Linq;
using Flepper.QueryBuilder;
using Meep.Pos.Infra.Data.Dapper.Extensions;
using System.Linq.Expressions;
using Meep.Pos.Infra.Data.Repository.Core.Interfaces;
using Meep.Pos.Domain.Core.Models;

namespace Meep.Pos.Infra.Data.Dapper.Repository
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        protected IDbConnection Db { get; private set; }

        protected virtual string TableName
        {
            get
            {
                return typeof(TEntity).Name;
            }
        }

        protected Repository()
        {
        }

        public Repository(IDbConnection db)
        {
            Db = db;
        }

        public virtual TEntity Get(Guid id)
        {
            var sql = FlepperQueryBuilder.Select().From(TableName).Where("Id").EqualTo(id.ToString()).BuildWithParameters();

            return Db.QueryFirstOrDefault<TEntity>(sql.Query.MySqlAdapter(), sql.Parameters);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            var sql = FlepperQueryBuilder.Select().From(TableName).Build();

            return Db.Query<TEntity>(sql.MySqlAdapter());
        }

        public virtual IEnumerable<TEntity> GetByFilter(Func<TEntity, bool> filter)
        {
            var objetos = GetAll();

            return objetos.Where(filter);
        }

        public virtual TEntity Insert(TEntity item)
        {
            var columns = GetEntityColumns(item);
            var entitiValues = GetEntityValues(item);

            var sql = FlepperQueryBuilder.Insert().Into(TableName)
                .Columns(columns)
                .Values(entitiValues)
                .BuildWithParameters();

            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return item;
        }

        public virtual bool Remove(Guid id)
        {
            var sql = FlepperQueryBuilder.Delete().From(TableName).Where("Id").EqualTo(id.ToString()).BuildWithParameters();

            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return true;
        }

        public virtual TEntity Update(TEntity item)
        {
            var columns = GetEntityColumns(item);
            var values = GetEntityValues(item);
            string id = string.Empty;

            var sql = FlepperQueryBuilder.Update(TableName);

            for (int i = 0; i < columns.Length; i++)
            {
                if (columns[i].Column.Replace("[","").Replace("]","") != "Id")
                    if (values[i].GetType().IsEnum)
                        sql.Set(columns[i], (int)values[i]);
                    else
                        sql.Set(columns[i], values[i].ToString());
                else
                    id = values[i].ToString();
            }
            var query = sql.BuildWithParameters();

            Db.Query(query.Query.MySqlAdapter() + $" where Id = '{id}'", query.Parameters);

            return item;
        }

        private SqlColumn[] GetEntityColumns(object obj)
        {
            Type type = typeof(TEntity);

            var columns = (from p in type.GetProperties()
                           where p.CanRead
                           select p).ToList();

            var columnsName = (from p in type.GetProperties()
                               where p.CanRead
                               select p.Name).ToList();

            var values = (from p in type.GetProperties()
                          where p.CanRead
                          select p.GetValue(obj)).ToList();

            var sqlColumns = new List<SqlColumn>();

            for (int i = 0; i < columns.Count; i++)
            {
                if (columns[i].PropertyType.IsSubclassOf(typeof(BaseModel)))
                {
                    if (values[i] != null)
                        sqlColumns.Add(columnsName[i] + "Id");
                }
                else
                {
                    if (values[i] != null)
                        sqlColumns.Add(columnsName[i]);
                }
            }

            return sqlColumns.ToArray();

        }

        private object[] GetEntityValues(object obj)
        {
            Type type = typeof(TEntity);
            var columns = (from p in type.GetProperties()
                           where p.CanRead
                           select p).ToList();

            var values = (from p in type.GetProperties()
                          where p.CanRead
                          select p.GetValue(obj)).ToList();


            for (int i = 0; i < columns.Count; i++)
            {
                if (columns[i].PropertyType.IsSubclassOf(typeof(BaseModel)))
                {
                    if ((BaseModel)values[i] != null)
                        values[i] = ((BaseModel)values[i]).Id;
                }

                if (values[i] == null)
                {
                    values.Remove(values[i]);
                    columns.Remove(columns[i]);
                }
            }

            return values.ToArray();
        }
    }
}
