-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 192.168.33.247    Database: meep_pos
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

USE meep_pos;
--
-- Table structure for table `appuser`
--

DROP TABLE IF EXISTS `appuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appuser` (
  `Id` char(36) NOT NULL,
  `Name` varchar(256) NOT NULL,
  `Cpf` varchar(14) NOT NULL,
  `Sex` int(11) NOT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Login` varchar(50) NOT NULL,
  `Password` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Login` (`Login`),
  CONSTRAINT `appuser_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `person` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attendant`
--

DROP TABLE IF EXISTS `attendant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendant` (
  `Id` char(36) NOT NULL,
  `Name` varchar(256) NOT NULL,
  `Cpf` varchar(14) NOT NULL,
  `Sex` int(11) NOT NULL,
  `PinCode` char(4) NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `attendant_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `person` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cashier`
--

DROP TABLE IF EXISTS `cashier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cashier` (
  `Id` char(36) NOT NULL,
  `DateOpened` datetime NOT NULL,
  `DateClosed` datetime DEFAULT NULL,
  `Total` decimal(9,2) DEFAULT NULL,
  `InitialValue` decimal(9,2) NOT NULL,
  `CashierOpenerId` char(36) NOT NULL,
  `CashierCloserId` char(36) DEFAULT NULL,
  `IsOpen` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `CashierOpenerId` (`CashierOpenerId`),
  KEY `CashierCloserId` (`CashierCloserId`),
  CONSTRAINT `cashier_ibfk_1` FOREIGN KEY (`CashierOpenerId`) REFERENCES `person` (`Id`),
  CONSTRAINT `cashier_ibfk_2` FOREIGN KEY (`CashierCloserId`) REFERENCES `person` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `Id` char(36) NOT NULL,
  `Name` varchar(256) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configuration`
--

DROP TABLE IF EXISTS `configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration` (
  `Id` char(36) NOT NULL,
  `MeepType` int(11) NOT NULL,
  `CreationDate` datetime NOT NULL,
  `Integration` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configurationendpoint`
--

DROP TABLE IF EXISTS `configurationendpoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configurationendpoint` (
  `Id` char(36) NOT NULL,
  `Endpoint` varchar(150) DEFAULT NULL,
  `Flow` int(11) DEFAULT NULL,
  `Pipeline` int(11) DEFAULT NULL,
  `EndpointType` int(11) DEFAULT NULL,
  `ContinueOnError` tinyint(1) DEFAULT NULL,
  `ConfigurationId` char(36) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `ConfigurationId` (`ConfigurationId`),
  CONSTRAINT `configurationendpoint_ibfk_1` FOREIGN KEY (`ConfigurationId`) REFERENCES `configuration` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device`
--

DROP TABLE IF EXISTS `device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device` (
  `Id` char(36) NOT NULL,
  `Fingerprint` varchar(100) NOT NULL,
  `UserAgent` varchar(200) NOT NULL,
  `Resolution` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `deviceattendant`
--

DROP TABLE IF EXISTS `deviceattendant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deviceattendant` (
  `Id` char(36) NOT NULL,
  `DeviceId` char(36) NOT NULL,
  `AttendantId` char(36) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `discount`
--

DROP TABLE IF EXISTS `discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discount` (
  `Id` char(36) NOT NULL,
  `Description` varchar(256) NOT NULL,
  `Value` decimal(9,2) NOT NULL,
  `CalcType` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `Id` char(36) NOT NULL,
  `Name` varchar(256) NOT NULL,
  `Cnpj` varchar(256) NOT NULL,
  `Address` varchar(256) NOT NULL,
  `Country` varchar(256) NOT NULL,
  `State` varchar(256) NOT NULL,
  `City` varchar(256) NOT NULL,
  `ZipCode` varchar(256) NOT NULL,
  `MeepLocationId` char(36) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `orderdiscount`
--

DROP TABLE IF EXISTS `orderdiscount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderdiscount` (
  `Id` char(36) NOT NULL,
  `DiscountId` char(36) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `DiscountId` (`DiscountId`),
  CONSTRAINT `orderdiscount_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `orderitem` (`Id`),
  CONSTRAINT `orderdiscount_ibfk_2` FOREIGN KEY (`DiscountId`) REFERENCES `discount` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `orderitem`
--

DROP TABLE IF EXISTS `orderitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderitem` (
  `Id` char(36) NOT NULL,
  `DateOrder` datetime NOT NULL,
  `Value` decimal(9,2) NOT NULL,
  `Total` decimal(9,2) DEFAULT NULL,
  `OrderId` char(36) NOT NULL,
  `AttendantId` char(36) DEFAULT NULL,
  `Count` decimal(5,3) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `OrderId` (`OrderId`),
  CONSTRAINT `orderitem_ibfk_1` FOREIGN KEY (`OrderId`) REFERENCES `orders` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `orderproduct`
--

DROP TABLE IF EXISTS `orderproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderproduct` (
  `Id` char(36) NOT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `ProductId` char(36) NOT NULL,
  `DateFinalized` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `ProductId` (`ProductId`),
  CONSTRAINT `orderproduct_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `orderitem` (`Id`),
  CONSTRAINT `orderproduct_ibfk_2` FOREIGN KEY (`ProductId`) REFERENCES `product` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `Id` char(36) NOT NULL,
  `DateOpened` datetime NOT NULL,
  `DateClosed` datetime DEFAULT NULL,
  `Value` decimal(9,2) DEFAULT NULL,
  `TicketId` char(36) NOT NULL,
  `AttendantId` char(36) NOT NULL,
  `CashierId` char(36) NOT NULL,
  `IsOpen` tinyint(1) DEFAULT NULL,
  `CustomerCpf` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `TicketId` (`TicketId`),
  KEY `AttendantId` (`AttendantId`),
  KEY `CashierId` (`CashierId`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`TicketId`) REFERENCES `ticket` (`Id`),
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`AttendantId`) REFERENCES `attendant` (`Id`),
  CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`CashierId`) REFERENCES `cashier` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ordertax`
--

DROP TABLE IF EXISTS `ordertax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordertax` (
  `Id` char(36) NOT NULL,
  `TaxId` char(36) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `TaxId` (`TaxId`),
  CONSTRAINT `ordertax_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `orderitem` (`Id`),
  CONSTRAINT `ordertax_ibfk_2` FOREIGN KEY (`TaxId`) REFERENCES `tax` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `Id` char(36) NOT NULL,
  `Date` datetime NOT NULL,
  `Value` decimal(9,2) NOT NULL,
  `PaymentState` int(11) NOT NULL,
  `PaymentType` int(11) NOT NULL,
  `AttendantId` char(36) NOT NULL,
  `OrderId` char(36) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `AttendantId` (`AttendantId`),
  KEY `OrderId` (`OrderId`),
  CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`AttendantId`) REFERENCES `attendant` (`Id`),
  CONSTRAINT `payment_ibfk_2` FOREIGN KEY (`OrderId`) REFERENCES `orders` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `Id` char(36) NOT NULL,
  `Name` varchar(256) NOT NULL,
  `Cpf` varchar(14) NOT NULL,
  `Sex` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `Id` char(36) NOT NULL,
  `Name` varchar(256) NOT NULL,
  `Price` decimal(9,2) NOT NULL,
  `CategoryId` char(36) NOT NULL,
  `Unit` int(11) DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT NULL,
  `IsProduction` tinyint(1) DEFAULT NULL,
  `ProductionTime` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `CategoryId` (`CategoryId`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`CategoryId`) REFERENCES `category` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tax`
--

DROP TABLE IF EXISTS `tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax` (
  `Id` char(36) NOT NULL,
  `Description` varchar(256) NOT NULL,
  `Value` decimal(9,2) NOT NULL,
  `CalcType` int(11) NOT NULL,
  `Required` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket` (
  `Id` char(36) NOT NULL,
  `Number` varchar(256) NOT NULL,
  `Type` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-01 16:48:09