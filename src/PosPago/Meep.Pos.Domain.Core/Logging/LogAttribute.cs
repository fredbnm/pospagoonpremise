﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Logging
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public class LogAttribute : Attribute
    {

    }
}
