﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Enumerators
{
    /// <summary>
    /// Seguindo a ordem do portal meep
    /// </summary>
    public enum PaymentTypeEnum
    {
        Money = 3,
        Credit = 1,
        Debit = 2
    }
}
