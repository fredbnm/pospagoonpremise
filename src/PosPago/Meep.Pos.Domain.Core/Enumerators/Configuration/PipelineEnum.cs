﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Enumerators.Configuration
{
    public enum PipelineEnum
    {
        Pre = 1,
        Pos = 2
    }
}
