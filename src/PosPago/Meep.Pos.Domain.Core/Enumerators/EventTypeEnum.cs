﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Enumerators
{
    public enum EventTypeEnum
    {
        Delete = 0,
        ProductionLine = 1,
        Device = 2,
        ProductExclude = 3
    }
}
