﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Enumerators
{
    public enum PriceCalcTypeEnum
    {
        Value = 1,
        Percent = 2
    }
}
