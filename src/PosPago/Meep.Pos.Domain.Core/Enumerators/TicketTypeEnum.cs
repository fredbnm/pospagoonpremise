﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Enumerators
{
    public enum TicketTypeEnum
    {
        Constant = 1,
        Express = 2
    }
}
