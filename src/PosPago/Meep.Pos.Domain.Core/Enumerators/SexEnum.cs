﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Enumerators
{
    public enum SexEnum
    {
        M = 1,
        F = 2
    }
}
