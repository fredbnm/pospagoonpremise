﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Enumerators
{
    public enum MeepTypeEnum
    {
        Pre = 1,
        Pos = 2,
        Both = 3
    }
}
