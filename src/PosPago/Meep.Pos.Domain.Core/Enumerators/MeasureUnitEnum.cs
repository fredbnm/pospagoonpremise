﻿namespace Meep.Pos.Domain.Core.Enumerators
{
    public enum MeasureUnitEnum
    {
        Unit = 1,
        Kg = 2
    }
}
