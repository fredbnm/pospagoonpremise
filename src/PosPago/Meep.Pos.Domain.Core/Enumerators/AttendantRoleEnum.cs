﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Enumerators
{
    public enum AttendantRoleEnum
    {
        Waiter = 1,
        Cashier = 2,
        Manager = 3
    }
}
