﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Enumerators
{
    public enum PaymentStateEnum
    {
        Partial = 1,
        Final = 2
    }
}
