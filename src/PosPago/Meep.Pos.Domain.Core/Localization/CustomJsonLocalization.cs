﻿using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Localization
{
    public class CustomJsonLocalization : ICustomJsonLocalization
    {
        private IStringLocalizer<CustomJsonLocalization> _localizer;
        public CustomJsonLocalization(IStringLocalizer<CustomJsonLocalization> localizer)
        {
            _localizer = localizer;
        }

        public string GetResource(string key)
        {
            try
            {
                var resource = _localizer.GetString(key);
                return resource;
            }
            catch
            {
                return null;
            }
        }

        public string GetResource(string key, params object[] args)
        {
            try
            {
                return string.Format(_localizer.GetString(key), args);
            }
            catch
            {
                return null;
            }
        }
    }
}
