﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Localization
{
    public interface ICustomJsonLocalization
    {
        string GetResource(string key);
        string GetResource(string key, params object[] args);
    }
}
