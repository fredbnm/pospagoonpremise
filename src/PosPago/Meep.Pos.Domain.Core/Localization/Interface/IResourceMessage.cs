﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Localization.Interface
{
    public interface IResourceMessage
    {
        string Code { get; }
        string Message { get; }
        object[] Args { get; }
        ResourceMessage Instance(string code, string message);
        ResourceMessage Instance(string code, string message, params object[] args);
    }
}
