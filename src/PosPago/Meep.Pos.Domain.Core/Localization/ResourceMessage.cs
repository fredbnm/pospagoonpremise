﻿using Meep.Pos.Domain.Core.Localization.Interface;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Localization
{
    public class ResourceMessage : IResourceMessage
    {
        private string _code;
        private string _message;
        private object[] _args;

        private ICustomJsonLocalization _resource;

        public ResourceMessage(ICustomJsonLocalization resource)
        {
            _resource = resource;
        }

        public ResourceMessage Instance(string code, string message)
        {
            this._code = code;
            this._message = _resource.GetResource(code) ?? message;

            return this;
        }

        public ResourceMessage Instance(string code, string message, params object[] args)
        {
            this._code = code;
            this._message = _resource.GetResource(code, args) ?? message;
            this._args = args;

            return this;
        }
        public string Code { get { return _code; } }

        public string Message { get { return _message; } }

        public object[] Args { get { return _args; } }
    }
}
