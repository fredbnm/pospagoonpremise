﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Meep.Pos.Domain.Core.Util
{
    public static class RandomGenerator
    {
        private static readonly Random _random = new Random();

        public static int Int(int min, int max)
        {
            return _random.Next(min,max);
        }

        public static string Password(int length = 10)
        {
            return new Faker<string>()
                .CustomInstantiator(f => f.Internet.Password(length));
        }

        public static string Ticket()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string numbers = "0123456789";

            var random = new Random();

            var prefix = new string(Enumerable.Repeat(chars, 2).Select(s => s[random.Next(2)]).ToArray());

            var passwd = new string(Enumerable.Repeat(numbers, 4).Select(s => s[random.Next(4)]).ToArray());

            return prefix + passwd;
        }
    }
}
