﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Meep.Pos.Domain.Core.Models;

//namespace Meep.Pos.Domain.Core.Repository
//{
//    public class Repository<TEntity> : IRepository<TEntity> 
//        where TEntity : BaseModel
//    {
//        IList<TEntity> _items;

//        public Repository()
//        {
//            _items = new List<TEntity>();
//        }

//        public virtual TEntity Insert(TEntity item)
//        {
//            _items.Add(item);
//            return item;
//        }

//        public virtual bool Remove(Guid id)
//        {
//            var item = _items.FirstOrDefault(p => p.Id == id);
//            return _items.Remove(item);
//        }

//        public virtual TEntity Get(Guid id)
//        {
//            return _items.FirstOrDefault(p => p.Id == id);
//        }

//        public virtual IEnumerable<TEntity> GetAll()
//        {
//            return _items;
//        }

//        public virtual TEntity Update(TEntity item)
//        {
//           
//            var itemToUpdate = _items.FirstOrDefault(i => i.Id == item.Id);
//            itemToUpdate = item;

//            return itemToUpdate;

//        }

//        public virtual IEnumerable<TEntity> GetByFilter(Func<TEntity, bool> filter)
//        {
//            return _items.Where(filter);
//        }
//    }
//}
