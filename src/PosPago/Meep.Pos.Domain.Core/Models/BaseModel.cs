﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Models
{
    public class BaseModel
    {
        public BaseModel()
        {
            _id = Guid.NewGuid();
        }

        #region [ Properties ]
        private Guid _id;
        private bool _hasChanged = false;

        public Guid Id
        {
            get { return _id; }
            private set {
                if (!_hasChanged)
                {
                    _id = value == Guid.Empty ? _id : value;
                    _hasChanged = true;
                }
            }
        }
        #endregion
    }
}
