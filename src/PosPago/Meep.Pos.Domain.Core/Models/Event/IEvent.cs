﻿using Meep.Pos.Domain.Core.Enumerators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Models.Event
{
    public interface IEvent
    {
        Guid Id { get; }
        Guid Identifier { get; }
        EventTypeEnum EventType { get; set; }
        DateTime TimeStamp { get; set; }
        string Message { get; set; }
        bool IsRead { get; set; }
    }
}
