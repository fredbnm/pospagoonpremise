﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Constants
{
    public partial struct DomainConstants
    {
        public struct SuccessCode
        {
            public struct Product
            {
                public struct Inserted
                {
                    public const string CODE = "PS001";
                    public const string MESSAGE = "Produto criado com sucesso.";
                }

                public struct Updated
                {
                    public const string CODE = "PS002";
                    public const string MESSAGE = "Produto alterado com sucesso.";
                }

                public struct Removed
                {
                    public const string CODE = "PS003";
                    public const string MESSAGE = "Produto removido com sucesso.";
                }
            }

            public struct Attendant
            {
                public struct Inserted
                {
                    public const string CODE = "AS001";
                    public const string MESSAGE = "Atendente criado com sucesso.";
                }

                public struct Updated
                {
                    public const string CODE = "AS002";
                    public const string MESSAGE = "Atendente alterado com sucesso.";
                }

                public struct Removed
                {
                    public const string CODE = "AS003";
                    public const string MESSAGE = "Atendente removido com sucesso.";
                }

                public struct Pin_Changed
                {
                    public const string CODE = "AS004";
                    public const string MESSAGE = "Novo PIN gerado: {0}";
                }
            }

            public struct Category
            {
                public struct Inserted
                {
                    public const string CODE = "CTS001";
                    public const string MESSAGE = "Categoria criada com sucesso.";
                }

                public struct Updated
                {
                    public const string CODE = "CTS002";
                    public const string MESSAGE = "Categoria alterada com sucesso.";
                }

                public struct Removed
                {
                    public const string CODE = "CTS003";
                    public const string MESSAGE = "Categoria removida com sucesso.";
                }
            }

            public struct Comission
            {
                public struct Inserted
                {
                    public const string CODE = "COS001";
                    public const string MESSAGE = "Comissão criada com sucesso.";
                }

                public struct Updated
                {
                    public const string CODE = "COS002";
                    public const string MESSAGE = "Comissão alterada com sucesso.";
                }

                public struct Removed
                {
                    public const string CODE = "COS003";
                    public const string MESSAGE = "Comissão removida com sucesso.";
                }
            }

            public struct Discount
            {
                public struct Inserted
                {
                    public const string CODE = "DS001";
                    public const string MESSAGE = "Desconto criado com sucesso.";
                }

                public struct Updated
                {
                    public const string CODE = "DS002";
                    public const string MESSAGE = "Desconto alterado com sucesso.";
                }

                public struct Removed
                {
                    public const string CODE = "DS003";
                    public const string MESSAGE = "Desconto removido com sucesso.";
                }
            }

            public struct Location
            {
                public struct Inserted
                {
                    public const string CODE = "LS001";
                    public const string MESSAGE = "Local criado com sucesso.";
                }

                public struct Updated
                {
                    public const string CODE = "LS002";
                    public const string MESSAGE = "Local alterado com sucesso.";
                }

                public struct Removed
                {
                    public const string CODE = "LS003";
                    public const string MESSAGE = "Local removido com sucesso.";
                }
            }

            public struct Tax
            {
                public struct Inserted
                {
                    public const string CODE = "TS001";
                    public const string MESSAGE = "Taxa criada com sucesso.";
                }

                public struct Updated
                {
                    public const string CODE = "TS002";
                    public const string MESSAGE = "Taxa alterada com sucesso.";
                }

                public struct Removed
                {
                    public const string CODE = "TS003";
                    public const string MESSAGE = "Taxa removida com sucesso.";
                }
            }

            public struct Ticket
            {
                public struct Inserted
                {
                    public const string CODE = "TTS001";
                    public const string MESSAGE = "Ticket criado com sucesso.";
                }

                public struct Updated
                {
                    public const string CODE = "TTS002";
                    public const string MESSAGE = "Ticket alterado com sucesso.";
                }

                public struct Removed
                {
                    public const string CODE = "TTS003";
                    public const string MESSAGE = "Ticket removido com sucesso.";
                }
            }

            public struct User
            {
                public struct Inserted
                {
                    public const string CODE = "US001";
                    public const string MESSAGE = "Usuário criado com sucesso.";
                }

                public struct Updated
                {
                    public const string CODE = "US002";
                    public const string MESSAGE = "Usuário alterado com sucesso.";
                }

                public struct Removed
                {
                    public const string CODE = "US003";
                    public const string MESSAGE = "Usuário removido com sucesso.";
                }

                public struct Password_Changed
                {
                    public const string CODE = "US004";
                    public const string MESSAGE = "Senha alterada com sucesso.";
                }

                public struct Email_Changed
                {
                    public const string CODE = "US005";
                    public const string MESSAGE = "Email alterado com sucesso.";
                }
            }

            public struct Order
            {
                public struct Add_Item
                {
                    public const string CODE = "OS001";
                    public const string MESSAGE = "Item adicionado com sucesso.";
                }

                public struct Remove_Item
                {
                    public const string CODE = "OS002";
                    public const string MESSAGE = "Item removido com sucesso.";
                }

                public struct Add_Payment
                {
                    public const string CODE = "OS003";
                    public const string MESSAGE = "Pagamento efetuado com sucesso.";
                }

                public struct Payment_Status_Saved
                {
                    public const string CODE = "OS004";
                    public const string MESSAGE = "Status do pagamento salvo com sucesso.";
                }

                public struct Customer_Cpf_Attached
                {
                    public const string CODE = "OS005";
                    public const string MESSAGE = "O CPF do cliente foi adicionado com êxito.";
                }
            }

            public struct Cashier
            {
                public struct Open
                {
                    public const string CODE = "CS001";
                    public const string MESSAGE = "Caixa aberto com sucesso.";
                }

                public struct Close
                {
                    public const string CODE = "CS002";
                    public const string MESSAGE = "Caixa fechado com sucesso.";
                }

                public struct Open_Order
                {
                    public const string CODE = "CS003";
                    public const string MESSAGE = "Conta aberta com sucesso.";
                }

                public struct Close_Order
                {
                    public const string CODE = "CS004";
                    public const string MESSAGE = "Conta fechada com sucesso.";
                }
            }
        }
    }
}
