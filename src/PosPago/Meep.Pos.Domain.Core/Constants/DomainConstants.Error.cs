﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Constants
{
    public partial struct DomainConstants
    {
        public struct ErrorCode
        {
            public struct Product
            {
                public struct Doesnt_Exists
                {
                    public const string CODE = "PE001";
                    public const string MESSAGE = "O Produto informado não existe na base de dados.";
                }
                public struct Category_Doesnt_Exists
                {
                    public const string CODE = "PE002";
                    public const string MESSAGE = "É necessário informar a categoria para a criação do produto.";
                }
                
                public struct Already_Exists
                {
                    public const string CODE = "PE003";
                    public const string MESSAGE = "O produto {0} já existe na base de dados.";
                }
                
                public struct Id_Required
                {
                    public const string CODE = "PE004";
                    public const string MESSAGE = "O Id é obrigatório para executar esta ação.";
                }
            }

            public struct Cashier
            {
                public struct Already_Opened
                {
                    public const string CODE = "CE001";
                    public const string MESSAGE = "O Caixa selecionado já encontra-se aberto.";
                }

                public struct Attendant_Required
                {
                    public const string CODE = "CE002";
                    public const string MESSAGE = "É obrigatório informar um usuário responsável para executar esta ação do caixa.";
                }
                
                public struct Close_Orders
                {
                    public const string CODE = "CE003";
                    public const string MESSAGE = "É necessário finalizar todas as mesas/comandas para fechar o caixa.";
                }

                public struct Must_Be_Opened
                {
                    public const string CODE = "CE004";
                    public const string MESSAGE = "O Caixa deve estar aberto para executar esta ação.";
                }

                public struct Doesnt_Exists
                {
                    public const string CODE = "CE005";
                    public const string MESSAGE = "Caixa informado não existe na base de dados.";
                }
            }
            public struct Discount
            {
                public struct Percentage
                {
                    public const string CODE = "DE001";
                    public const string MESSAGE = "Descontos do tipo porcentagem devem ser maior ou igual a 0 e menor ou igual a 100.";
                }

                public struct Already_Exists
                {
                    public const string CODE = "DE002";
                    public const string MESSAGE = "O desconto {0} já existe na base de dados.";

                }

                public struct Doesnt_Exists
                {
                    public const string CODE = "DE003";
                    public const string MESSAGE = "Desconto informado não existe na base de dados.";
                }

                public struct Id_Required
                {
                    public const string CODE = "DE004";
                    public const string MESSAGE = "O Id do desconto é obrigatório.";
                }
            }
            public struct Order
            {
                public struct Item_Doesnt_Exists
                {
                    public const string CODE = "OE001";
                    public const string MESSAGE = "Item informado não existe na base de dados.";
                }

                public struct Payment_Without_Items
                {
                    public const string CODE = "OE002";
                    public const string MESSAGE = "Não existem pedidos lançados para efetuar pagamento.";
                }

                public struct Already_Closed
                {
                    public const string CODE = "OE003";
                    public const string MESSAGE = "Conta informada já está fechada.";
                }
                public struct Already_Opened
                {
                    public const string CODE = "OE004";
                    public const string MESSAGE = "Conta informada já está aberta.";
                }
                public struct Cannot_Close_Order
                {
                    public const string CODE = "OE005";
                    public const string MESSAGE = "Não é possível fechar a conta, pois faltam R${0} a ser pago.";

                }
                public struct Doesnt_Exists
                {
                    public const string CODE = "OE006";
                    public const string MESSAGE = "Order não existe na base de dados.";
                }

                public struct Cannot_Add_Discount
                {
                    public const string CODE = "OE007";
                    public const string MESSAGE = "O desconto não pode ser aplicado, pois o desconto aplicado é maior que o valor da conta.";
                }

                public struct Order_Product_Finalized
                {
                    public const string CODE = "OE008";
                    public const string MESSAGE = "Este produto já foi finalizado.";
                }

            }
            public struct Tax
            {
                public struct Percentage
                {
                    public const string CODE = "TE001";
                    public const string MESSAGE = "Taxas do tipo porcentagem devem ser maior ou igual a 0 e menor ou igual a 100.";
                }

                public struct Already_Exists
                {
                    public const string CODE = "TE002";
                    public const string MESSAGE = "A taxa {0} já existe na base de dados.";

                }

                public struct Doesnt_Exists
                {
                    public const string CODE = "TE003";
                    public const string MESSAGE = "Taxa informada não existe na base de dados.";
                }

                public struct Id_Required
                {
                    public const string CODE = "TE004";
                    public const string MESSAGE = "O Id da taxa é obrigatório.";
                }
            }
            
            public struct Attendant
            {
                public struct Already_Exists
                {
                    public const string CODE = "AE001";
                    public const string MESSAGE = "Já existe um atendente cadastrado com o CPF informado.";
                }

                public struct Doesnt_Exists
                {
                    public const string CODE = "AE002";
                    public const string MESSAGE = "Atendente informado não existe na base de dados.";
                }
                
                public struct Id_Required
                {
                    public const string CODE = "AE003";
                    public const string MESSAGE = "O Id do atendente é obrigatório.";                    
                }

                public struct Required_For_Payment
                {
                    public const string CODE = "AE004";
                    public const string MESSAGE = "É obrigatório informar um atendente para gerar um pagamento";
                }

            }

            public struct Category
            {
                public struct Already_Exists
                {
                    public const string CODE = "CTE001";
                    public const string MESSAGE = "Já existe uma categoria cadastrada com o nome informado.";
                }

                public struct Doesnt_Exists
                {
                    public const string CODE = "CTE002";
                    public const string MESSAGE = "Categoria informada não existe na base de dados.";
                }

                public struct Id_Required
                {
                    public const string CODE = "CTE003";
                    public const string MESSAGE = "O Id da categoria é obrigatório para executar esta ação.";
                }
            }
            public struct Comission
            {
                public struct Already_Exists
                {
                    public const string CODE = "CME001";
                    public const string MESSAGE = "Já existe uma comissão cadastrada com a descrição informada.";
                }

                public struct Doesnt_Exists
                {
                    public const string CODE = "CME002";
                    public const string MESSAGE = "Comissão informada não existe na base de dados.";
                }

                public struct Id_Required
                {
                    public const string CODE = "CME003";
                    public const string MESSAGE = "O Id da comissão é obrigatório para executar esta ação.";
                }
            }

            public struct Location
            {
                public struct Already_Exists
                {
                    public const string CODE = "LE001";
                    public const string MESSAGE = "Já existe um local cadastrado com o Cep informado.";
                }

                public struct Doesnt_Exists
                {
                    public const string CODE = "LE002";
                    public const string MESSAGE = "Local informado não existe na base de dados.";
                }
                public struct Id_Required
                {
                    public const string CODE = "LE003";
                    public const string MESSAGE = "O Id do local é obrigatório.";
                }
                
            }

            public struct Ticket
            {
                public struct Already_Exists
                {
                    public const string CODE = "TTE001";
                    public const string MESSAGE = "Já existe um ticket cadastrado com este número informado.";
                }

                public struct Doesnt_Exists
                {
                    public const string CODE = "TTE002";
                    public const string MESSAGE = "Ticket informado não existe na base de dados.";
                }
                public struct Id_Required
                {
                    public const string CODE = "TTE003";
                    public const string MESSAGE = "O Id do ticket é obrigatório.";
                }

            }

            public struct User
            {
                public struct Already_Exists
                {
                    public const string CODE = "UE001";
                    public const string MESSAGE = "Já existe um usuário cadastrado com este login.";
                }

                public struct Doesnt_Exists
                {
                    public const string CODE = "UE002";
                    public const string MESSAGE = "Usuário informado não existe na base de dados.";
                }
                public struct Id_Required
                {
                    public const string CODE = "UE003";
                    public const string MESSAGE = "O Id do usuário é obrigatório.";
                }
                
                public struct Wrong_Login
                {
                    public const string CODE = "UE004";
                    public const string MESSAGE = "Login incorreto.";
                }

                public struct Wrong_Password
                {
                    public const string CODE = "UE005";
                    public const string MESSAGE = "Senha incorreta.";
                }
                
                public struct Wrong_Email
                {
                    public const string CODE = "UE006";
                    public const string MESSAGE = "Email incorreto.";
                }

                public struct New_Password_Same_As_Old
                {
                    public const string CODE = "UE007";
                    public const string MESSAGE = "A nova senha não pode ser igual à senha anterior.";
                }

                public struct New_Email_Already_Exists
                {
                    public const string CODE = "UE008";
                    public const string MESSAGE = "Já existe um usuário cadastrado com o novo email informado.";
                }
            }

            public struct Device
            {
                public struct Doesnt_Exists
                {
                    public const string CODE = "DEE001";
                    public const string MESSAGE = "Dispositivo informado não existe.";
                }

                public struct Not_Authorized
                {
                    public const string CODE = "DEE002";
                    public const string MESSAGE = "Usuário não está autorizado a acessar a aplicação utilizando este dispositivo. Contate o administrador do sistema para solicitar a liberação.";
                }
            }

            public struct Custom
            {

                public struct Not_Treated
                {
                    public const string CODE = "CUE001";
                    public const string MESSAGE = "Ocorreu um erro não tratado na aplicação: {0}";
                }

                public struct Unauthorized_Acess
                {
                    public const string CODE = "CUE002";
                    public const string MESSAGE = "O usuário não está autorizado nesta aplicação";
                }

                public struct Null_Reference
                {
                    public const string CODE = "CUE003";
                    public const string MESSAGE = "Objeto solicitado não existe.";
                }

                public struct Model_Not_Valid
                {
                    public const string CODE = "CUE004";
                    public const string MESSAGE = "O JSON enviado na requisição não é válido. \n Erros: {0}";
                }


                public struct Database
                {
                    public const string CODE = "CUE005";
                    public const string MESSAGE = "Ocorreu um erro de conexão com o banco de dados.";
                }

            }
        }
    }
}
