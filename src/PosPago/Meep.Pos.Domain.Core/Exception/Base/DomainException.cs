﻿namespace Meep.Pos.Domain.Core.Exception
{
    public class DomainException : System.Exception
    {
        private string _errorCode;
        private int? _statusCode;
        public DomainException() : base()
        {       
        }

        public DomainException(string message) : base(message)
        {
        }

        public DomainException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
        
        public DomainException (string message, string errorCode) : base(message)
        {
            _errorCode = errorCode;
        }
        public DomainException(string message, string errorCode, int statusCode) : base(message)
        {
            _errorCode = errorCode;
            _statusCode = statusCode;
        }

        public string ErrorCode { get { return _errorCode; } }
        public int? StatusCode { get { return _statusCode; } }
    }
}
