﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Location
{
    public class LocationIdRequiredException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Location.Id_Required.CODE;
        private const string _message = DomainConstants.ErrorCode.Location.Id_Required.MESSAGE;

        public LocationIdRequiredException() : base(_message, _code)
        {

        }
    }
}