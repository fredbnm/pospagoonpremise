﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Location
{
    public class LocationAlreadyExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Location.Already_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Location.Already_Exists.MESSAGE;

        public LocationAlreadyExistsException() : base(_message, _code)
        {

        }
    }
}