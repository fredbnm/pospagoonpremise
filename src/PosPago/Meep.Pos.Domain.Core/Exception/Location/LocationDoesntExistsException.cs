﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Location
{
    public class LocationDoesntExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Location.Doesnt_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Location.Doesnt_Exists.MESSAGE;

        public LocationDoesntExistsException() : base(_message, _code)
        {

        }
    }
}