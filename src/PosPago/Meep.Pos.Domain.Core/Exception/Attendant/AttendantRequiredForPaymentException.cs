﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Attendant
{
    public class AttendantRequiredForPaymentException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Attendant.Required_For_Payment.CODE;
        private const string _message = DomainConstants.ErrorCode.Attendant.Required_For_Payment.MESSAGE;

        public AttendantRequiredForPaymentException() : base(_message, _code)
        {

        }
    }
}