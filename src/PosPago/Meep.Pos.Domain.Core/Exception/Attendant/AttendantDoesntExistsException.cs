﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Attendant
{
    public class AttendantDoesntExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Attendant.Doesnt_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Attendant.Doesnt_Exists.MESSAGE;

        public AttendantDoesntExistsException() : base(_message, _code)
        {

        }
    }
}