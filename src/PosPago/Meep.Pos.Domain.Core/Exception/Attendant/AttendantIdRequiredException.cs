﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Attendant
{
    public class AttendantIdRequiredException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Attendant.Id_Required.CODE;
        private const string _message = DomainConstants.ErrorCode.Attendant.Id_Required.MESSAGE;

        public AttendantIdRequiredException() : base(_message, _code)
        {

        }
    }
}