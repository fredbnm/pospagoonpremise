﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Attendant
{
    public class AttendantAlreadyExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Attendant.Already_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Attendant.Already_Exists.MESSAGE;

        public AttendantAlreadyExistsException() : base(_message, _code)
        {

        }
    }
}