﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.User
{
    public class UserWrongLoginException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.User.Wrong_Login.CODE;
        private const string _message = DomainConstants.ErrorCode.User.Wrong_Login.MESSAGE;

        public UserWrongLoginException() : base(_message,_code)
        {

        }
    }
}
