﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.User
{
    public class UserIdRequiredException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.User.Id_Required.CODE;
        private const string _message = DomainConstants.ErrorCode.User.Id_Required.MESSAGE;

        public UserIdRequiredException() : base(_message,_code)
        {

        }
    }
}
