﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.User
{
    public class UserWrongEmailException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.User.Wrong_Email.CODE;
        private const string _message = DomainConstants.ErrorCode.User.Wrong_Email.MESSAGE;

        public UserWrongEmailException() : base(_message,_code)
        {

        }
    }
}
