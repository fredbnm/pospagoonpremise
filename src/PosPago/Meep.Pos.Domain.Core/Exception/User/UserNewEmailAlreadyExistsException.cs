﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.User
{
    public class UserNewEmailAlreadyExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.User.New_Email_Already_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.User.New_Email_Already_Exists.MESSAGE;

        public UserNewEmailAlreadyExistsException() : base(_message,_code)
        {

        }
    }
}
