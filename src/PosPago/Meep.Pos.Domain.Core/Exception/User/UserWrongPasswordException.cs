﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.User
{
    public class UserWrongPasswordException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.User.Wrong_Password.CODE;
        private const string _message = DomainConstants.ErrorCode.User.Wrong_Password.MESSAGE;

        public UserWrongPasswordException() : base(_message, _code)
        {

        }
    }
}
