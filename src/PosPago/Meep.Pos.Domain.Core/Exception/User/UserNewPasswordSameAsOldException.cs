﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.User
{
    public class UserNewPasswordSameAsOldException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.User.New_Password_Same_As_Old.CODE;
        private const string _message = DomainConstants.ErrorCode.User.New_Password_Same_As_Old.MESSAGE;

        public UserNewPasswordSameAsOldException() : base(_message,_code)
        {

        }
    }
}
