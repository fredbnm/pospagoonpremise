﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.User
{
    public class UserDoesntExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.User.Doesnt_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.User.Doesnt_Exists.MESSAGE;

        public UserDoesntExistsException() : base(_message,_code)
        {

        }
    }
}
