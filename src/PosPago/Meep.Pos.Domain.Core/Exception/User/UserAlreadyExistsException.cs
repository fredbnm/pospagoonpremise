﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.User
{
    public class UserAlreadyExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.User.Already_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.User.Already_Exists.MESSAGE;

        public UserAlreadyExistsException() : base(_message,_code)
        {

        }
    }
}
