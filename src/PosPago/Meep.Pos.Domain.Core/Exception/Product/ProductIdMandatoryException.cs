﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Product
{
    public class ProductIdRequiredException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Product.Id_Required.CODE;
        private const string _message = DomainConstants.ErrorCode.Product.Id_Required.MESSAGE;

        public ProductIdRequiredException() : base(_message, _code)
        {

        }
    }
}
