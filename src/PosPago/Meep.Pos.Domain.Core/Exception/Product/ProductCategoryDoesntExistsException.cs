﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Product
{
    public class ProductCategoryDoesntExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Product.Category_Doesnt_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Product.Category_Doesnt_Exists.MESSAGE;

        public ProductCategoryDoesntExistsException() : base(_message, _code)
        {

        }
    }
}
