﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Product
{
    public class ProductAlreadyExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Product.Already_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Product.Already_Exists.MESSAGE;

        public ProductAlreadyExistsException(string name) : base(string.Format(_message, name), _code)
        {

        }
    }
}
