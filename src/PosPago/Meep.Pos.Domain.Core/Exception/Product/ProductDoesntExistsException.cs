﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Product
{
    public class ProductDoesntExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Product.Doesnt_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Product.Doesnt_Exists.MESSAGE;
        public ProductDoesntExistsException() : base(_message,_code)
        {

        }
    }
}
