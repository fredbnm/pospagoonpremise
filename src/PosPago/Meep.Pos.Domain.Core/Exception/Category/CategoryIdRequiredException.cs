﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Category
{
    public class CategoryIdRequiredException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Category.Id_Required.CODE;
        private const string _message = DomainConstants.ErrorCode.Category.Id_Required.MESSAGE;

        public CategoryIdRequiredException() : base(_message, _code)
        {

        }
    }
}