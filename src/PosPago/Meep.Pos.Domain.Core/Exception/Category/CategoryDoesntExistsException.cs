﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Category
{
    public class CategoryDoesntExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Category.Doesnt_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Category.Doesnt_Exists.MESSAGE;

        public CategoryDoesntExistsException() : base(_message, _code)
        {

        }
    }
}