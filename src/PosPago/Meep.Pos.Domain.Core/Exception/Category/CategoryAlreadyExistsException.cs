﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Category
{
    public class CategoryAlreadyExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Category.Already_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Category.Already_Exists.MESSAGE;

        public CategoryAlreadyExistsException() : base(_message, _code)
        {

        }
    }
}