﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Device
{
    public class DeviceNotAuthorizedException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Device.Not_Authorized.CODE;
        private const string _message = DomainConstants.ErrorCode.Device.Not_Authorized.MESSAGE;

        public DeviceNotAuthorizedException() : base(_message, _code, 401)
        {

        }
    }
}
