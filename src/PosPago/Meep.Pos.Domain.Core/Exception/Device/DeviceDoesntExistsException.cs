﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Device
{
    public class DeviceDoesntExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Device.Doesnt_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Device.Doesnt_Exists.MESSAGE;

        public DeviceDoesntExistsException() : base(_message, _code)
        {

        }
    }
}
