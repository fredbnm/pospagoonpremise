﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Cashier
{
    public class CashierCloseOrdersException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Cashier.Close_Orders.CODE;
        private const string _message = DomainConstants.ErrorCode.Cashier.Close_Orders.MESSAGE;

        public CashierCloseOrdersException() : base(_message, _code)
        {

        }
    }
}