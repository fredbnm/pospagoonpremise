﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Cashier
{
    public class CashierMustBeOpenedException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Cashier.Must_Be_Opened.CODE;
        private const string _message = DomainConstants.ErrorCode.Cashier.Must_Be_Opened.MESSAGE;

        public CashierMustBeOpenedException() : base(_message, _code)
        {

        }
    }
}