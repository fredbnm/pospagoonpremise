﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Cashier
{
    public class CashierDoesntExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Cashier.Doesnt_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Cashier.Doesnt_Exists.MESSAGE;

        public CashierDoesntExistsException() : base(_message, _code)
        {

        }
    }
}