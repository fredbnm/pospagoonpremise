﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Cashier
{
    public class CashierUserRequiredException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Cashier.Attendant_Required.CODE;
        private const string _message = DomainConstants.ErrorCode.Cashier.Attendant_Required.MESSAGE;

        public CashierUserRequiredException() : base(_message, _code)
        {

        }
    }
}
