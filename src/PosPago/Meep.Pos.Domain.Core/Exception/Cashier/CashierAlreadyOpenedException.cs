﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Cashier
{
    public class CashierAlreadyOpenedException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Cashier.Already_Opened.CODE;
        private const string _message = DomainConstants.ErrorCode.Cashier.Already_Opened.MESSAGE;

        public CashierAlreadyOpenedException() : base(_message, _code)
        {

        }
    }
}
