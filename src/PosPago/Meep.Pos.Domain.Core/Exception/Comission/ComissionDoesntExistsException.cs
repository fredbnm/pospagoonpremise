﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Comission
{
    public class ComissionDoesntExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Comission.Doesnt_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Comission.Doesnt_Exists.MESSAGE;

        public ComissionDoesntExistsException() : base(_message, _code)
        {

        }
    }
}