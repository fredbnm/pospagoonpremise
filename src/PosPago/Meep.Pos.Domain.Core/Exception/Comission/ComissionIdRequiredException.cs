﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Comission
{
    public class ComissionIdRequiredException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Comission.Id_Required.CODE;
        private const string _message = DomainConstants.ErrorCode.Comission.Id_Required.MESSAGE;

        public ComissionIdRequiredException() : base(_message, _code)
        {

        }
    }
}