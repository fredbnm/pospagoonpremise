﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Comission
{
    public class ComissionAlreadyExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Comission.Already_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Comission.Already_Exists.MESSAGE;

        public ComissionAlreadyExistsException() : base(_message, _code)
        {

        }
    }
}