﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Tax
{
    public class TaxIdRequiredException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Tax.Id_Required.CODE;
        private const string _message = DomainConstants.ErrorCode.Tax.Id_Required.MESSAGE;

        public TaxIdRequiredException() : base(_message, _code)
        {

        }
    }
}
