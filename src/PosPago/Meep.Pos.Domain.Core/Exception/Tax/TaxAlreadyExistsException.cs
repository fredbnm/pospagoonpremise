﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Tax
{
    public class TaxAlreadyExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Tax.Already_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Tax.Already_Exists.MESSAGE;

        public TaxAlreadyExistsException(string name) : base(string.Format(_message, name), _code)
        {

        }
    }
}
