﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Tax
{
    public class TaxPercentageException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Tax.Percentage.CODE;
        private const string _message = DomainConstants.ErrorCode.Tax.Percentage.MESSAGE;

        public TaxPercentageException() : base(_message, _code)
        {

        }
    }
}
