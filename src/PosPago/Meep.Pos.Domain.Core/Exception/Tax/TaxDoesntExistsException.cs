﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Tax
{
    public class TaxDoesntExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Tax.Doesnt_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Tax.Doesnt_Exists.MESSAGE;

        public TaxDoesntExistsException() : base(_message, _code)
        {

        }
    }
}
