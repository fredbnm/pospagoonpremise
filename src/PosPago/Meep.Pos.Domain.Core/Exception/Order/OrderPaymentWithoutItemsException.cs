﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Order
{
    public class OrderPaymentWithoutItemsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Order.Payment_Without_Items.CODE;
        private const string _message = DomainConstants.ErrorCode.Order.Payment_Without_Items.MESSAGE;

        public OrderPaymentWithoutItemsException() : base(_message, _code)
        {

        }
    }
}