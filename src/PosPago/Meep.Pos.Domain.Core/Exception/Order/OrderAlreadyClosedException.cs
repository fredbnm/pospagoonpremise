﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Order
{
    public class OrderAlreadyClosedException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Order.Already_Closed.CODE;
        private const string _message = DomainConstants.ErrorCode.Order.Already_Closed.MESSAGE;

        public OrderAlreadyClosedException() : base(_message, _code)
        {

        }
    }
}