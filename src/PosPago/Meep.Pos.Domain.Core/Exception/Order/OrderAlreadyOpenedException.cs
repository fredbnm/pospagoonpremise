﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Order
{
    public class OrderAlreadyOpenedException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Order.Already_Opened.CODE;
        private const string _message = DomainConstants.ErrorCode.Order.Already_Opened.MESSAGE;

        public OrderAlreadyOpenedException() : base(_message, _code)
        {

        }
    }
}