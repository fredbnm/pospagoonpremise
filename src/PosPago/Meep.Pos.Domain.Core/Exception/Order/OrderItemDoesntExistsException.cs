﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Order
{
    public class OrderItemDoesntExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Order.Item_Doesnt_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Order.Item_Doesnt_Exists.MESSAGE;

        public OrderItemDoesntExistsException() : base(_message, _code)
        {

        }
    }
}