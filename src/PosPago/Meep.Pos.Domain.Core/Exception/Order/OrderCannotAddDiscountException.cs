﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Order
{
    public class OrderCannotAddDiscountException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Order.Cannot_Add_Discount.CODE;
        private const string _message = DomainConstants.ErrorCode.Order.Cannot_Add_Discount.MESSAGE;
        public OrderCannotAddDiscountException() : base(_message, _code)
        {

        }
    }
}
