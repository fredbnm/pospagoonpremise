﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Order
{
    public class OrderProductAlreadyFinalizedException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Order.Order_Product_Finalized.CODE;
        private const string _message = DomainConstants.ErrorCode.Order.Order_Product_Finalized.MESSAGE;

        public OrderProductAlreadyFinalizedException() : base(_message, _code)
        {

        }
    }
}