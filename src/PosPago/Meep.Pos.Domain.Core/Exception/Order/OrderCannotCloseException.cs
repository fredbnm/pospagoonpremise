﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Order
{
    public class OrderCannotCloseException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Order.Cannot_Close_Order.CODE;
        private const string _message = DomainConstants.ErrorCode.Order.Cannot_Close_Order.MESSAGE;

        public OrderCannotCloseException(decimal value) : base(string.Format(_message, value.ToString()), _code)
        {

        }
    }
}