﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Order
{
    class OrderDoesntExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Order.Doesnt_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Order.Doesnt_Exists.MESSAGE;

        public OrderDoesntExistsException() : base(_message, _code)
        {

        }
    }
}