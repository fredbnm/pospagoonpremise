﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Custom
{
    public class CustomModelNotValidException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Custom.Model_Not_Valid.CODE;
        private const string _message = DomainConstants.ErrorCode.Custom.Model_Not_Valid.MESSAGE;

        public CustomModelNotValidException(string fields) : base(string.Format(_message, fields), _code)
        {

        }
    }
}