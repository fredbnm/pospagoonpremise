﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Discount
{
    public class DiscountDoesntExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Discount.Doesnt_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Discount.Doesnt_Exists.MESSAGE;

        public DiscountDoesntExistsException() : base(_message, _code)
        {

        }
    }
}
