﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Discount
{
    public class DiscountIdRequiredException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Discount.Id_Required.CODE;
        private const string _message = DomainConstants.ErrorCode.Discount.Id_Required.MESSAGE;

        public DiscountIdRequiredException() : base(_message, _code)
        {

        }
    }
}
