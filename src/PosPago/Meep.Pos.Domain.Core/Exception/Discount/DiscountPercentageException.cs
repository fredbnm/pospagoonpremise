﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Discount
{
    public class DiscountPercentageException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Discount.Percentage.CODE;
        private const string _message = DomainConstants.ErrorCode.Discount.Percentage.MESSAGE;

        public DiscountPercentageException() : base(_message, _code)
        {

        }
    }
}