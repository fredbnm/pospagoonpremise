﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Discount
{
    public class DiscountAlreadyExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Discount.Already_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Discount.Already_Exists.MESSAGE;

        public DiscountAlreadyExistsException(string name) : base(string.Format(_message,name), _code)
        {

        }
    }
}
