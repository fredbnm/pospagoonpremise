﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Ticket
{
    public class TicketDoesntExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Ticket.Doesnt_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Ticket.Doesnt_Exists.MESSAGE;

        public TicketDoesntExistsException() : base(_message, _code)
        {

        }
    }
}