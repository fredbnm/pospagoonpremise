﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Ticket
{
    public class TicketIdRequiredException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Ticket.Id_Required.CODE;
        private const string _message = DomainConstants.ErrorCode.Ticket.Id_Required.MESSAGE;

        public TicketIdRequiredException() : base(_message, _code)
        {

        }
    }
}