﻿using Meep.Pos.Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Core.Exception.Ticket
{
    public class TicketAlreadyExistsException : DomainException
    {
        private const string _code = DomainConstants.ErrorCode.Ticket.Already_Exists.CODE;
        private const string _message = DomainConstants.ErrorCode.Ticket.Already_Exists.MESSAGE;

        public TicketAlreadyExistsException() : base(_message, _code)
        {

        }
    }
}