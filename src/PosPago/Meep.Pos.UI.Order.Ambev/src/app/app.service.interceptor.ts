import { LoginDeviceModel } from './models/loginDevice.model';
import { LoginService } from './components/login/login.service';
import swal from 'sweetalert2';
import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Router } from '@angular/router';
declare var Fingerprint2: any;

@Injectable()
export class AppServiceInterceptor implements HttpInterceptor {
    constructor(private router : Router){
    }

  private fingerPrint = new Fingerprint2();

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).do(evt => {
            if (evt instanceof HttpResponse) {
                var retornoSever = evt.body;

                if (!retornoSever) retornoSever = {};

                if (retornoSever.hasErrors === true) {
                    switch(evt.status)  {
                        case 500: 
                        case 400:
                            swal('Erro', retornoSever.message, 'error');
                            break;
                        case 401:
                            swal('Atenção', 'Sua sessão está expirada. Por favor, efetue o login novamente', 'warning').then(() => {
                            this 
                                // TODO: Implement 
                            });
                            break;
                        default:
                        swal('Erro', 'Ocorreu um erro ao tentar efetuar a requisição. Tente novamente mais tarde.', 'error');
                            break;
                    }                    
                }
            }
          }).catch(response => {
            if (response instanceof HttpErrorResponse) {

                try{
                    var retornoSever = JSON.parse(response.error);
                }
                catch(Error){
                    swal('Erro', 'Ocorreu um erro ao tentar efetuar a requisição. Tente novamente mais tarde.', 'error');
                }

                if (!retornoSever) {
                    
                    retornoSever = {};
                }
                
                if (retornoSever.hasErrors && retornoSever.hasErrors === true) {
                    if (response.status)  {
                            swal('Erro', retornoSever.message, 'error');
                    } else {
                        swal('Erro', 'Ocorreu um erro ao tentar efetuar a requisição. Tente novamente mais tarde.', 'error');
                            
                    }                    
                } else if (response.status == 401){

                    // this.fingerPrint.get((result, components) => {

                    //     var login = new LoginDeviceModel();
                    //     login.browserFingerprint = result;
                    //     login.pinCode = localStorage.getItem("pinCode");

                    //     this.deviceService.deviceLogin(login).then(device => {

                    //         localStorage.setItem("token", device.access_token);

                    //     });
                    // });
                    swal({title: "Erro", type: "error", text: "Usuário não está autorizado. Redirecionando para a tela de login.", allowOutsideClick: false})
                    .then(() => this.router.navigate(['..']));
                }
                else
                    swal('Erro', 'Ocorreu um erro ao tentar efetuar a requisição. Tente novamente mais tarde.', 'error');
            }
    
            return Observable.throw(response);
          });
        
    }

}