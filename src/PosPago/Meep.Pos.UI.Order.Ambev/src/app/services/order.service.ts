import { UserInfo } from './../models/order/userInfo.model';
import { async } from '@angular/core/testing';
import { OrderItemModel } from './../models/orderItem.model';
import { PaymentStatusModel } from './../models/paymentStatus.model';
import { PaymentModel } from './../models/order/payment.model';
import { OrderModel, OrderTax } from './../models/order.model';
import { AppService } from './../app.service';

import { HttpClient } from '@angular/common/http';
import { HttpModule, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpErrorResponse } from "@angular/common/http";

@Injectable()
export class OrderService extends AppService<OrderModel> {

  constructor(http: HttpClient) {
    super(http); 
  }

  method : string = 'order';

  async getAll(): Promise<OrderModel[]>{

    var orders = await this.getAllAsync(this.method);
    
    return <OrderModel[]>orders;
  }
  



  async get(id : string) : Promise<OrderModel>{
    var result = await this.getAsync(this.method+'/'+id);

    return <OrderModel>result;
  }

  async pay(orderId : string, model : PaymentModel) : Promise<OrderModel>{
    var result = await this.postAsync(this.method + '/' + orderId + '/pay', model);

    return <OrderModel>result;
  }

  async closeOrder(orderId :string) : Promise<OrderModel>{
      var result = await this.putAsync('cashier/current/order/'+orderId+'/close');

      return <OrderModel>result;
  }

  async save(product : OrderModel) : Promise<OrderModel>{

    var result = await this.postAsync(this.method,product);

    return <OrderModel>result;
  }

  async update(product : OrderModel) : Promise<OrderModel>{
    
    var result = await this.putAsync(this.method,product);

    return <OrderModel>result;
  }
  
  async delete(id : string) : Promise<string>{
    var result = await this.deleteAsync(this.method+'/'+id);

    return result;
  }

  async getPaymentStatus(orderId : string) : Promise<PaymentStatusModel>{
    var result = await this.getAsync(this.method + '/' + orderId + '/payment/status');

    return <PaymentStatusModel>result;
  }

  getPaymentStatusEndPoint(orderId : string) : string{
      var endpoint = this.methodUrl(this.method + '/' + orderId + '/payment/status');

      return endpoint
  }

  async deleteStatus(orderId : string) : Promise<string>{
      var result = await this.deleteAsync(this.method + '/' + orderId + '/payment/status');
    
      return result;
  }

  async removeItem(orderId : string, orderItem : OrderItemModel) : Promise<OrderModel>{
      var result = await this.postAsync(this.method + '/' + orderId + '/remove', orderItem);

      return <OrderModel>result;
  }

  async getCpfUser(orderId: string, cpf: string) : Promise<UserInfo> {
    var result = await this.getAsync(this.method + '/' + orderId + '/customer/' + cpf);
    return <UserInfo>result;    
  }

  async addCustomerToOrder(orderId : string, model : UserInfo) : Promise<string>{
    var result = await this.postAsync(`order/${orderId}/customer`, model);

    return <string>result;
  }

}
