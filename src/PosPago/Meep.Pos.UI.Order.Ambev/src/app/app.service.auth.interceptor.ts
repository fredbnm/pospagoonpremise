import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
 
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  
    let token = localStorage.getItem("token");

    if (token) {
        // Clone the request to add the new header.
        const authReq = req.clone({
          headers: req.headers.set('Authorization', 'bearer ' + token)
              .set('Content-Type', 'application/json')
              .set('Accept', 'application/json')
            });
     
        // Pass on the cloned request instead of the original request.
        return next.handle(authReq);
    }
    
    return next.handle(req);
  }
}