export default class Utils{

    static roundTo(n, digits) {
        if (digits === undefined) {
            digits = 0;
        }

        var multiplicator = Math.pow(10, digits);
        n = parseFloat((n * multiplicator).toFixed(11));
        var test =(Math.round(n) / multiplicator);
        return +(test.toFixed(digits));
    }

    static identifyDevice() : string {
        if(navigator.userAgent.match(/Android/i))
            return "Android";

        if (navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i))
            return "Iphone";

        if (navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i))
            return "iPad";
        
        if (navigator.userAgent.match(/BlackBerry/i))
            return "BlackBerry";
        
        if (navigator.userAgent.match(/Windows Phone/i))
            return "Windows Phone";
        
        if (navigator.userAgent.match(/Windows/i))
            return "Windows";

        if (navigator.userAgent.match(/Windows/i))
            return "Windows";

        if (navigator.userAgent.match(/Mac/i))
            return "Mac";
        
        if (navigator.userAgent.match(/Linux/i))
            return "Linux";

        return "PC";
    }

    static identifyBrowser() : string{
        if (navigator.userAgent.match(/Firefox/i)
            && !navigator.userAgent.match(/Seamonkey/i))
            return "Firefox";

        if (navigator.userAgent.match(/Chrome/i)
            && !navigator.userAgent.match(/Chromium/i)
            && !navigator.userAgent.match(/Edge/i))
            return "Chrome";
        
        if (navigator.userAgent.match(/Safari/i)
            && !navigator.userAgent.match(/Chrome/i)
            && !navigator.userAgent.match(/Chromium/i))
            return "Safari";
        
        if (navigator.userAgent.match(/OPR/i)
            || navigator.userAgent.match(/Opera/i))
            return "Opera";

        if (navigator.userAgent.match(/Edge/i))
            return "Edge";

        if (navigator.userAgent.match(/MSIE/i)
            || navigator.userAgent.match(/IEMobile/i))
            return "Internet Explorer";
    }
}
