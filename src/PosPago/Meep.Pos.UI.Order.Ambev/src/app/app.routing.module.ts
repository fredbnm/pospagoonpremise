import { UserInfoComponent } from './components/user-info/user-info.component';
import { Page404Component } from './page404/page404.component';
import { ResumeComponent } from './components/resume/resume.component';
import { HomeComponent } from './components/home/home.component';
import { NgModule, Component, ModuleWithProviders } from '@angular/core';
import { OrderOpenComponent } from './components/order-open/order-open.component';
import { LoginComponent } from './components/login/login.component';

import { Routes, RouterModule } from '@angular/router';
const appRoutes: Routes = [
    {
        path: '',
        component: LoginComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path:'comandar',
        component: OrderOpenComponent
    },
    {
        path: 'home',
        component: HomeComponent,
        
    },
    {
        path:'resumo',
        component: ResumeComponent
    },    
  
    {
        path:'resumo/:id',
        component: ResumeComponent
    },    
    {
        path:'resumo/total',
        component: ResumeComponent
    },
    {
        path:'registro-usuario',
        component: UserInfoComponent
    },
    {
        path: '**',
        component: Page404Component
    },
    
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    // Exportar modulo rota
    exports: [RouterModule]
})
export class AppRoutingModule { }