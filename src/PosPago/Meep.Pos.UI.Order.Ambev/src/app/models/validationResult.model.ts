export class ValidationResult<TModel>{
    hasErrors : boolean;
    message : string;
    value: TModel;
}