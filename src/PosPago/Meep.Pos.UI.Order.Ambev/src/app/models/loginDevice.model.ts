export class LoginDeviceModel{
    pinCode : string;
    browserFingerprint : string;
    resolution : string;
    userAgent : string;
    orderType : OrderTypeEnum;
}

export enum OrderTypeEnum{
    classic = 1,
    ambev = 2
}