export class CashierModel{
    id : string;
    initialValue :number;
    cashierOpenerId:string;
    cashierCloserId: string;
    cashierOpenerName:string;
    cashierCloserName: string;
    ordersId: string;
    total: number;
    isOpen: boolean;
    dateClosed: string
}