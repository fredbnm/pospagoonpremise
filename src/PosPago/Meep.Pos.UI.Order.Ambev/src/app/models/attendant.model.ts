export class AttendantModel{
    id : string;
    name : string;
    cpf : string;
    sex : string;
    pinCode : number;
}