import { DiscountModel } from './discounts.model';
import { ProductModel } from './product.model';
import Utils from '../util/utils';
export class OrderItemModel {
        itemId: string;
        product = new ProductModel();
        discount = new DiscountModel();
        quantity: number;
        description: string;
        attendantId: string = localStorage.getItem("attendantId");
        get total () {
            var total : number = 0.00;
            var productPrice : number = 0.00;
            var discountValue : number = 0.00;

            if (this.product.id)
                productPrice = Utils.roundTo(this.product.price * this.quantity,2);
            
            if (this.discount.id)
                discountValue = Utils.roundTo(this.discount.value * this.quantity,2);
            
            total = productPrice - discountValue;

            return total;//Math.round(parseFloat((this.product.price * this.quantity).toPrecision(2)));//parseFloat(Math.round(this.product.price * this.quantity).toFixed(2));
        }
       
}