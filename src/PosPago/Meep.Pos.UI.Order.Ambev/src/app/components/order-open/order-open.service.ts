import { OrderItemModel } from './../../models/orderItem.model';
import { async } from '@angular/core/testing';
import { OrderModel } from './../../models/order.model';
import { AppService } from './../../app.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OrderOpenModel } from '../../models/order/orderOpen.model';

@Injectable()
export class OrderOpenService extends AppService<OrderModel> {


  constructor(http: HttpClient) {
    super(http);
  }

  method: string = 'attendant';

  async openOrder(model : OrderOpenModel): Promise<OrderModel> {
    var order = await this.putAsync('cashier/current/order', model);
    return <OrderModel>order;
  }

  async openOrderExpress(model: OrderOpenModel): Promise<OrderModel>{
    var order = await this.postAsync('cashier/current/order/express', model);
    return <OrderModel>order
  }

}
