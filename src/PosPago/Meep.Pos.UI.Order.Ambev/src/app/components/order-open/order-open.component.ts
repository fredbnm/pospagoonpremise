import { AttendantModel } from './../../models/attendant.model';
import { OrderOpenService } from './order-open.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrderOpenModel } from '../../models/order/orderOpen.model';

@Component({
  selector: 'app-order-open',
  templateUrl: './order-open.component.html',
  styleUrls: ['./order-open.component.css']
})
export class OrderOpenComponent implements OnInit {
  attendantName: string = localStorage.getItem("attendantName");
  private attendantId: string = localStorage.getItem("attendantId");
  private _ticketNumber : number[] = []; 
  private _attendantModel : AttendantModel;
  
  get attendantNameMtds() {
    return this._attendantModel.name;
  }

  get ticketNumber() : string {
    var code = this._ticketNumber.toString().replace(/,/g,'');
    return code;
  }

  constructor(private router: Router, private orderOpenService : OrderOpenService) { }

  ngOnInit() {
    
    this._attendantModel = new AttendantModel();
    // Preencher as propriedades vindas do local storage
    this._attendantModel.name = localStorage.getItem("userName");
  }

  fillNumber(number) {
      this._ticketNumber.push(number);
  }
  
  removeNumber(){
    this._ticketNumber.pop();
  }



  sellOrder(){
    var model = new OrderOpenModel();
    model.attendantId = this.attendantId;
    //model.ticketNumber = this.ticketNumber;
    
    this.orderOpenService.openOrderExpress(model).then(o => {
      localStorage.setItem("orderId", o.id)
      localStorage.setItem("ticketNumber", o.ticketNumber);
      this.router.navigate(['/home']);
    })
    
  }
}
