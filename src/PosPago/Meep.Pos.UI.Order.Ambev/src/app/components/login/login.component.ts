import swal from 'sweetalert2';
import { LoginDeviceModel, OrderTypeEnum } from './../../models/loginDevice.model';
import { LoginService } from './login.service';
import { AttendantModel } from './../../models/attendant.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Utils from '../../util/utils';
declare var Fingerprint2: any;
declare var MobileDetect : any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  private _pinCode: number[] = [];
  private fingerPrint = new Fingerprint2();

  get pinCode(): string {
    var code = this._pinCode.toString().replace(/,/g, '');
    return code;
  }

  constructor(private router: Router,
    private loginService: LoginService) {
      //localStorage.clear();
      localStorage.removeItem("token");
      localStorage.removeItem("attendantId");
      localStorage.removeItem("orderId");
      localStorage.removeItem("pincode");
      localStorage.removeItem("attendantName");
  }

  saveLocalStorage(attendantId, token, attendantName) {
    localStorage.setItem("token", token);
    localStorage.setItem("attendantId", attendantId);
    localStorage.setItem("pincode", this.pinCode);
    localStorage.setItem("attendantName",attendantName);
  }
  ngOnInit() {
  }

  fillPin(number) {
    if (this._pinCode.length < 4)
      this._pinCode.push(number);
  }

  removePin() {
    this._pinCode.pop();
  }

  signIn() {
    this.fingerPrint.get((result, components) => {
      var hash = result;
      var loginDevice = new LoginDeviceModel();
      loginDevice.browserFingerprint = hash;
      loginDevice.pinCode = this.pinCode;
      loginDevice.resolution = components[5].value[0] + 'x'+  components[5].value[1];
      loginDevice.userAgent = Utils.identifyDevice() + " " + Utils.identifyBrowser() + " " + loginDevice.resolution;//components[0].value;
      loginDevice.orderType = OrderTypeEnum.ambev;
      
      this.loginService.deviceLogin(loginDevice)
        .then(l => {
          this.saveLocalStorage(l.attendantId, l.access_token, l.attendantName);

          this.router.navigate(['/comandar']);
        });

    });
  }

  openOptions(){
    swal({
      title: 'Configurações',
      input: 'checkbox',
      inputValue: 1,
      inputPlaceholder: " Desativar Pinpad.",
      confirmButtonText:
        'Confirmar',
      onOpen: (result) => {
        var checkboxes = result.querySelectorAll("input[type='checkbox']");
        (<HTMLInputElement>checkboxes[0]).checked = localStorage.getItem("pinpad") === "1";
      },
      inputValidator: (result) => {
        return new Promise<void>((resolve, reject) => {
            resolve();
      })
    }
    }).then((result) => {
      localStorage.setItem("pinpad", result);
      swal({
        type: 'success',
        text: 'Configurações atualizadas.'
      });
    });
  }


}
