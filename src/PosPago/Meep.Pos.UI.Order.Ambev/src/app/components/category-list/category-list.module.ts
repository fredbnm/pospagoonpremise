import { MatTabsModule } from '@angular/material';
import { CategoryListComponent } from './category-list.component';
import { AppRoutingModule } from './../../app.routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    MatTabsModule,
    BrowserAnimationsModule
  ],
  exports: [
    CategoryListComponent
  ],
  declarations: [
    CategoryListComponent
  ]
})
export class CategoryListModel { }
