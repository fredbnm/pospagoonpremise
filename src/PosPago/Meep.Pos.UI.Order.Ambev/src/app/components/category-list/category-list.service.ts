import { OrderItemModel } from './../../models/orderItem.model';
import { AppService } from './../../app.service';
import { OrderModel } from './../../models/order.model';
import { HttpClient } from '@angular/common/http';
import { HttpModule, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpErrorResponse } from "@angular/common/http";

@Injectable()
export class CategoryListService extends AppService<OrderModel> {

  constructor(http: HttpClient) {
    super(http); 
  }

  method : string = 'order';

  async getAll(): Promise<OrderModel[]>{

    var products = await this.getAllAsync(this.method);
    
    return <OrderModel[]>products;
  }

  async get(id : string) : Promise<OrderModel>{
    var result = await this.getAsync(this.method+'/'+id);

    return <OrderModel>result;
  }

  async addItems(orderId, orderItems : OrderItemModel[]) : Promise<OrderModel>{

    var result = await this.postAsync(this.method + '/' + orderId + '/add/list',orderItems);

    return <OrderModel>result;
  }

  async update(product : OrderModel) : Promise<OrderModel>{
    
    var result = await this.putAsync(this.method,product);

    return <OrderModel>result;
  }
  
  async delete(id : string) : Promise<string>{
    var result = await this.deleteAsync(this.method+'/'+id);

    return result;
  }
}
