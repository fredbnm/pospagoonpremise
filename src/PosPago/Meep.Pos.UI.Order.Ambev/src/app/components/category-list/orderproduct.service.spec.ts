import { TestBed, inject } from '@angular/core/testing';

import { OrderProductService } from './orderproduct.service';

describe('OrderproductService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrderProductService]
    });
  });

  it('should be created', inject([OrderProductService], (service: OrderProductService) => {
    expect(service).toBeTruthy();
  }));
});
