import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { OrderProductService } from './../category-list/orderproduct.service';
import { OrderItemModel } from './../../models/orderItem.model';
import { ProductModel } from './../../models/product.model';
import { Component, OnInit } from '@angular/core';
import { ResumeComponent } from './../resume/resume.component';
import Utils from '../../util/utils';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  orderNumber : string = localStorage.getItem("ticketNumber"); 
  orderItems : OrderItemModel [] = [];
  constructor(private orderProductService : OrderProductService,
    private router: Router) { }

  ngOnInit() {

    this.orderProductService.emitProduct.subscribe(
      orderItem => this.orderItems.push(orderItem)
    );

    this.orderProductService.emitClearItems.subscribe(
      clear =>{
        if (clear)
        {
          this.orderItems = [];
        }
      });
    
    this.orderProductService.emitRemoveProduct.subscribe(
      item => {
        var index = this.orderItems.indexOf(item);
        this.orderItems.splice(index,1);
      }
    )

    this.orderProductService.clearOrderItems();

  }

  
  addQuantity(orderItem: OrderItemModel) {
    orderItem.quantity = orderItem.quantity + 1;
  }

  addDescription(orderItem: OrderItemModel) {    
      swal({
        title: 'Insira a observação',
        input: "textarea",
        showCancelButton: true,
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar',
        allowOutsideClick: false,
        onOpen: (result) => {
          var text = result.querySelectorAll(".swal2-textarea");
          (<HTMLInputElement>text[0]).value = orderItem.description ? orderItem.description : '';
        },
        preConfirm: observation => {
          return new Promise((resolve, reject) => {
            if (observation === '' )
              reject("Favor entrar com uma observação válida.")
            else{
              orderItem.description = observation;
              this.orderProductService.updateProduct(orderItem);
              resolve();
            }
          });
        }
      });
    
  }

  removeQuantity(orderItem: OrderItemModel) {
    if (orderItem.quantity > 1)
    {
      orderItem.quantity = orderItem.quantity - 1;
    }
  }

  get desconto(){
    var total = 0.00;
    this.orderItems.forEach(item => {
      if (item.discount.id)
        total +=item.total;
    }); 

    return Utils.roundTo(total,2);
  }

  get subtotal(){
    var total = 0.00;
    this.orderItems.forEach(item => {
      if (item.product.id)
        total +=item.total;
    }); 

    return Utils.roundTo(total,2);
  }

  get total(){
    var total = 0.00;
    this.orderItems.forEach(item => {
      total +=item.total;
    }); 

    return Utils.roundTo(total,2);
  }

  removeItem(orderItem: OrderItemModel) {
    //swal('Deseja excluir esse produto?')
    this.orderProductService.removeProduct(orderItem);
  }

  orderResume(){
    this.router.navigate(['resumo']);
  }

  orderResumeTotal(){
    if(this.orderItems == [] || this.orderItems == undefined) {
      swal('Você precisa adicionar itens ao pedido');
    }
    this.router.navigate(['resumo/total']);
  }
}
