﻿using Meep.Pos.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Meep.Pos.Infra.Data.Repository;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Meep.Pos.Domain.Core.Localization;
using Meep.Pos.Domain.Core.Localization.Interface;
using Meep.Pos.Service.Mapping;
using Meep.Pos.Infra.Data.Core.Interfaces;
using Meep.Pos.Infra.Data.Mongo.ConnectionProvider;
using Meep.Pos.Infra.Data.Mongo.Repository.Interface;
using Meep.Pos.Infra.Data.Mongo.Repository;
using Microsoft.Extensions.Configuration;
using System.Data;
using MySql.Data.MySqlClient;
using Meep.Pos.Domain.Command.Interface;
using Meep.Pos.Domain.Command.Base;
using Meep.Pos.Domain.Command.Production;
using Meep.Pos.Domain.Command.Command.Device;
using Meep.Pos.Domain.Command.Interface.Saga;

namespace Meep.Pos.Infra.CrossCutting.IoC
{
    public class NativeInjectorBootstrapper
    {
        public static void RegisterServices(IServiceCollection services, IConfiguration appConfiguration)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // AutoMapper
            services.AddSingleton(Mapper.Configuration);
            services.AddScoped<IMapper>(
                sp => AutoMapperConfig.RegisterMappings().CreateMapper());
           

            // Services
            services.AddScoped<IAttendantService, AttendantService>();
            services.AddScoped<ICashierService, CashierService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IDiscountService, DiscountService>();
            services.AddScoped<ILocationService, LocationService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<ITaxService, TaxService>();
            services.AddScoped<ITicketService, TicketService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IDeviceAttendantService, DeviceAttendantService>();
            services.AddScoped<IProductionService, ProductionService>();
            services.AddScoped<IEventService, EventService>();
            services.AddScoped<IConfigurationService, ConfigurationService>();
            services.AddScoped<IIntegrationService, IntegrationService>();
            services.AddScoped<IComissionService, ComissionService>();

            // Infra - Data
            services.AddScoped<IAttendantRepository, AttendantRepository>();
            services.AddScoped<ICashierRepository, CashierRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IDiscountRepository, DiscountRepository>();
            services.AddScoped<ILocationRepository, LocationRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<ITaxRepository, TaxRepository>();
            services.AddScoped<ITicketRepository, TicketRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IDeviceAttendantRepository, DeviceAttendantRepository>();
            services.AddScoped<IDeviceRepository, DeviceRepository>();
            services.AddScoped<IProductionRepository, ProductionRepository>();
            services.AddScoped<IEventRepository, EventRepository>();
            services.AddScoped<IConfigurationRepository, ConfigurationRepository>();
            services.AddScoped<IComissionRepository, ComissionRepository>();

            // Command
            services.AddScoped<ICommandHandler, CommandHandler>();
            services.AddScoped<IProductionSaga, ProductionSaga>();
            services.AddScoped<IDeviceSaga, DeviceSaga>();

            // Core
            services.AddSingleton<ICustomJsonLocalization, CustomJsonLocalization>();

            //Data
            //services.AddScoped<IRepository, Repository>();
            
            services.AddScoped<IConnectionProvider, MongoDbConnectionProvider>();
            services.AddScoped<IMongoDbRepository, MongoDbRepository>();
            services.AddScoped<IDbConnection>((ctx) =>
            {
                return new MySqlConnection(appConfiguration.GetConnectionString("DefaultConnection"));
                //);
            });
            services.AddScoped<IResourceMessage, ResourceMessage>();
        }
    }
}
