echo "Buildando as aplicações"

rem dotnet publish ./Meep.Pos.API/Meep.Pos.API.csproj -c Release -o ./obj/Docker/publish

rem set curr_dir=%cd%
echo %curr_dir%

rem cd ./Meep.Pos.UI.SiteAdm
rem ng build -prod -aot
rem cd %curr_dir%

rem cd ./Meep.Pos.UI.Order
rem ng build -prod -aot
rem cd %curr_dir%

echo "Definindo variáveis ambiente"

SET API_IP=192.168.32.108
SET ORDER_PORT=5002
SET API_PORT=5000
SET PORTAL_PORT=5001
SET POS_DATABASE_CONNECTION_STRING=server=localhost;port=3306;uid=root;pwd=meepMySql;database=meep_pos

echo "Gerando imagens docker"

docker-compose -f docker-compose.piloto.yml build