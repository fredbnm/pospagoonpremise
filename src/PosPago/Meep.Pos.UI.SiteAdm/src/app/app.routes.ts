import { ProductsComponent } from './components/cruds/products/products.component';
import {Routes, RouterModule } from '@angular/router';
import { LoginComponent } from "./components/login/login.component";
import { Error404Component } from "./components/pages/error404/error404.component";

const routes: Routes = [
    // Login
    {
        path: 'login',
        component: LoginComponent
    },
    // products
    {
        path: 'produtos',
        component: ProductsComponent
    },



    // Error 404
    {
        path: '**',
        component: Error404Component
    }
];

export const RoutingModule = RouterModule.forRoot(routes); 