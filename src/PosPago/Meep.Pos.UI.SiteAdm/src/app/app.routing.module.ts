import { ConfigurationComponent } from './components/configuration/configuration.component';
import { CreateTicketsComponent } from './components/forms/create-tickets/create-tickets.component';
import { DiscountSingleComponent } from './components/cruds/discount/discount-single/discount-single.component';
import { DiscountComponent } from './components/cruds/discount/discount.component';
import { TicketSingleComponent } from './components/pages/map-order/ticket-single/ticket-single.component';
import { TaxSingleComponent } from './components/cruds/tax/tax-single/tax-single.component';
import { TaxComponent } from './components/cruds/tax/tax.component';

import { DeviceManagerComponent } from './components/pages/device-manager/device-manager.component';
import { MapOrderComponent } from './components/pages/map-order/map-order.component';
import { CashierDetailComponent } from './components/report/report-cashier/cashier-detail/cashier-detail.component';
import { ReportCashierComponent } from './components/report/report-cashier/report-cashier.component';
import { UserSingleComponent } from './components/cruds/user/user-single/user-single.component';
import { UserComponent } from './components/cruds/user/user.component';
import { CashierComponent } from './components/pages/cashier/cashier.component';
import { AttendantSingleComponent } from './components/cruds/attendant/attendant-single/attendant-single.component';
import { AttendantComponent } from './components/cruds/attendant/attendant.component';
import { AuthGuard } from './guards/auth.guard';
import { NgModule, Component, ModuleWithProviders } from '@angular/core';


import { ProductsComponent } from './components/cruds/products/products.component';
import { ProductsSingleComponent } from './components/cruds/products/products-single/products-single.component';
import { CategoryComponent } from './components/cruds/category/category.component';
import { CategorySingleComponent } from './components/cruds/category/category-single/category-single.component';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from "./components/login/login.component";
import { Error404Component } from "./components/pages/error404/error404.component";

import { HomeComponent } from './components/pages/home/home.component';
import { SettingsComponent } from './components/settings/settings.component';
import { LoginMeepComponent } from './components/forms/login-meep/login-meep.component';
import { UserCreateComponent } from './components/forms/user-create/user-create.component';
import { MessageThanksComponent } from './components/forms/message-thanks/message-thanks.component';
import { ProductionComponent } from './components/pages/production/production.component';

import { ComissionComponent } from './components/cruds/comission/comission.component';
import { ComissionSingleComponent } from './components/cruds/comission/comission-single/comission-single.component';

const appRoutes: Routes = [
   
    {
        path: 'login-meep',
        component: LoginMeepComponent
    },
     {
         path: 'criar-usuario',
         component: UserCreateComponent
     },
     {
         path: 'obrigado',
         component: MessageThanksComponent
     },  
    {
        path: '',
        component: LoginComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'mesas',
        component: CreateTicketsComponent
    },
    {
        path: 'linha-producao',
        component: ProductionComponent,
        canActivate: [AuthGuard]
    }, 
  
    {
        path: 'home',
        component: HomeComponent,
        data: { title: 'Home' },
        canActivate: [AuthGuard],
        children: [
            {
                path: 'produtos',
                component: ProductsComponent,
                canActivate: [AuthGuard],
                children: []
            },
            {
                path: 'produtos/:id',
                component: ProductsSingleComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'produtos/novo',
                component: ProductsSingleComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'categorias',
                component: CategoryComponent,
                canActivate: [AuthGuard],
                children: []
            },
            {
                path: 'categorias/novo',
                component: CategorySingleComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'categorias/:id',
                component: CategorySingleComponent,
                canActivate: [AuthGuard]
            },     
            {
                path: 'gerenciador-dispositivos',
                component: DeviceManagerComponent,
                canActivate: [AuthGuard],
                children: []
            },       
            {
                path: 'atendente',
                component: AttendantComponent,
                canActivate: [AuthGuard],
                children: []
            },
            {
                path: 'atendente/novo',
                component: AttendantSingleComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'atendente/:id',
                component: AttendantSingleComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'caixa',
                component: CashierComponent,
                canActivate: [AuthGuard],
                children: []
            },
            {
                path: 'usuario',
                component: UserComponent,
                canActivate: [AuthGuard],
                children: []
            },
            {
                path: 'usuario/novo',
                component: UserSingleComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'usuario/:id',
                component: UserSingleComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'taxa',
                component: TaxComponent,
                canActivate: [AuthGuard],
                children: []
            },
            {
                path: 'taxa/novo',
                component: TaxSingleComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'taxa/:id',
                component: TaxSingleComponent ,
                canActivate: [AuthGuard]
            },
            {
                path: 'desconto',
                component: DiscountComponent,
                canActivate: [AuthGuard],
                children: []
            },
            {
                path: 'desconto/novo',
                component: DiscountSingleComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'desconto/:id',
                component: DiscountSingleComponent ,
                canActivate: [AuthGuard]
            },
            {
                path: 'comissão',
                component: ComissionComponent,
                canActivate: [AuthGuard],
                children: []
            },
            {
                path: 'comissão/novo',
                component: ComissionSingleComponent,
                canActivate: [AuthGuard],
                children: []
            },
            {
                path: 'comissão/:id',
                component: ComissionSingleComponent,
                canActivate: [AuthGuard],
                children: []
            },
            {
                path: 'relatorio-caixa',
                component: ReportCashierComponent,
                canActivate: [AuthGuard],
                children: []
            },
            {
                path: 'relatorio-caixa/:id',
                component: CashierDetailComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'mapa-comandas',
                component: MapOrderComponent,
                canActivate: [AuthGuard]
            },            
            {
                path: 'comanda',
                component: TicketSingleComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'configuracao',
                component: ConfigurationComponent,
                canActivate: [AuthGuard]
            },
           
        ]
    },
   
    {
        path: '**',
        component: Error404Component,
        canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    // Exportar modulo rota
    exports: [RouterModule]
})
export class AppRoutingModule { }
function newFunction(): string {
    return 'usuario';
}
