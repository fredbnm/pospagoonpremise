import { async } from '@angular/core/testing';

import { Observable } from 'rxjs/Observable';
import { ValidationResult } from './components/model/validationResult.model';
import { HttpModule, Headers, Response, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { LoginModel, TokenModel } from './components/model/login.model';
import { environment } from '../environments/environment.basedev';


@Injectable()
export class AppService<TModel> {
    constructor(private http: HttpClient) {

    }
    private version = environment.versionBase;
    private domain = environment.baseUrl;
    private meepDomain = environment.baseUrlMeep;
    private url: string = this.domain + "/pos/api/" + this.version ;    

    get headers() : HttpHeaders{ 
        var headers = new HttpHeaders();
        var token = localStorage.getItem("token");

      
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        headers.append('Access-Control-Allow-Credentials', '*');
        headers.append('Access-Control-Allow-Origin','*');
        headers.append('Access-Control-Allow-Methods','GET, POST, PATCH, PUT, DELETE, OPTIONS');
        headers.append("Access-Control-Allow-Headers","Origin, X-Requested-With, X-XSRF-TOKEN, Content-Type, Accept, X-Auth-Token");
        

        return headers;
    }

    // get options() : RequestOptions{
    //     var options = new RequestOptions();
    //     options.headers = this.headers;

    //     return options;
    // }
    

    private methodUrl(method: string) {
        return this.url.concat(method);
    }

    protected async getAsync<TModel>(methodName: string): Promise<TModel> {
        var result = await this.http
            .get<ValidationResult<TModel>>(this.methodUrl(methodName),
            {headers: this.headers})
            //.retry(3)
            .toPromise();

        if (result.hasErrors)
            throw new Error(result.message);

        return result.value;
        
    }

    protected async getByFilterAsync<TModel>(methodName: string, model : HttpParams): Promise<TModel> {
        var result = await this.http
            .get<ValidationResult<TModel>>(this.methodUrl(methodName),{
                params: model,
                headers: this.headers
            })
            //.retry(3)
            .toPromise();

        if (result.hasErrors)
            throw new Error(result.message);

        return result.value;
        
    }

    protected async getAllAsync<TModel>(methodName: string): Promise<TModel[]> {
        var result = await this.http
            .get<ValidationResult<TModel[]>>(this.methodUrl(methodName),
            {headers: this.headers})
            //.retry(3)
            .toPromise();

        if (result.hasErrors)
            throw new Error(result.message);

        return result.value;                
    }

    protected async postAsync<TModel>(methodName : string, model : any) : Promise<TModel>
    {
        var result = await this.http
            .post<ValidationResult<TModel>>(this.methodUrl(methodName),
            JSON.stringify(model),
                {headers: this.headers})
            //.retry(3)
            .toPromise();
 
        if (result.hasErrors)
            throw new Error(result.message);
 
        return result.value;
    }

    protected async putAsync<TModel>(methodName : string, model: any = null) : Promise<TModel>
    {
        var result = await this.http
            .put<ValidationResult<TModel>>(this.methodUrl(methodName),
            JSON.stringify(model),
                {headers: this.headers})
            //.retry(3)
            .toPromise();
 
        if (result.hasErrors)
            throw new Error(result.message);
 
        return result.value;
    }

    protected async deleteAsync(methodName : string) : Promise<string>
    {
        var result = await this.http
            .delete<ValidationResult<TModel>>(this.methodUrl(methodName),
                {headers: this.headers})
            //.retry(3)
            .toPromise();
 
        if (result.hasErrors)
            throw new Error(result.message);
        
        return result.message;
    }

    protected async login(loginModel : LoginModel) : Promise<TokenModel>{

        var result = this.http
            .post<TokenModel>(this.domain +"/api/token",
                "username="+loginModel.username+"&password="+ loginModel.password + "&grant_type=password",
                {headers : new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')})
                .toPromise();
        
        return result;

    }

    protected async loginMeepServer(loginModel : LoginModel) : Promise<TokenModel>{
        
        var result = await this.http
            .post<ValidationResult<TokenModel>>(this.methodUrl("gateway/meep/server/login"),
                JSON.stringify(loginModel),
                {headers : new HttpHeaders().set('Content-Type', 'application/json')})
                .toPromise();
        
        if (result.hasErrors)
            throw new Error(result.message);

        return result.value;

    }
}
// function newFunction() {
//     return this;
// }



