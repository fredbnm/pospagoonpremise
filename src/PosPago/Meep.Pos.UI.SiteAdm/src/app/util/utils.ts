export default class Utils{
    
        static roundTo(n, digits) {
            if (digits === undefined) {
                digits = 0;
            }
    
            var multiplicator = Math.pow(10, digits);
            n = parseFloat((n * multiplicator).toFixed(11));
            var final =(Math.round(n) / multiplicator);
            return +(final.toFixed(digits));
        }

        static orderBy(values: any, orderType: any) : any[]{
            return values.sort((a, b) =>{
                if (a[orderType] < b[orderType]) {
                    return -1;
                }
        
                if (a[orderType] > b[orderType]) {
                    return 1;
                }
        
                return 0
            })
        }

        static orderByDesc(values: any, orderType: any) : any[]{
            return values.sort((a, b) =>{
                if (a[orderType] < b[orderType]) {
                    return 1;
                }
        
                if (a[orderType] > b[orderType]) {
                    return -1;
                }
        
                return 0
            })
        }
    }
    