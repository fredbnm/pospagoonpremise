import { ActivatedRoute, Params } from '@angular/router';
import { OnInit } from "@angular/core";



export class AppSingleComponent implements OnInit{
    id: string;

    constructor(private route: ActivatedRoute){
    }

    ngOnInit() {
    }

    private _isNew : boolean = true;

    get isNew() : boolean {

        this.route.params.subscribe((params: Params) => {
            this.id = params['id'];
            if (this.id != undefined && this.id != 'novo') 
                this._isNew = false;
            else
                this._isNew = true;
        });

        return this._isNew;
    }

    isEditPage() : boolean{
        return !this._isNew;
    }

    get className() : string {

        if (this._isNew)
            return "";
        
        return "active";
    }

    ngOnDestroy() {

    }

}