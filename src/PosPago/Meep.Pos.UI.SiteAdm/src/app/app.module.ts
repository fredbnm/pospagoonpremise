import { ConfigurationComponent } from './components/configuration/configuration.component';



import { CreateTicketsComponent } from './components/forms/create-tickets/create-tickets.component';
import { DiscountService } from './components/cruds/discount/discount.service';
import { DiscountSingleComponent } from './components/cruds/discount/discount-single/discount-single.component';
import { DiscountComponent } from './components/cruds/discount/discount.component';
import { SettingsService } from './components/settings/settings.service';
import { CurrencyMaskConfig } from 'ng2-currency-mask/src/currency-mask.config';
import { CURRENCY_MASK_CONFIG } from 'ng2-currency-mask/src/currency-mask.config';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { TicketSingleComponent } from './components/pages/map-order/ticket-single/ticket-single.component';
import { TicketService } from './components/pages/map-order/ticket-single/ticket.service';


import { MessageThanksComponent } from './components/forms/message-thanks/message-thanks.component';
import { UserCreateComponent } from './components/forms/user-create/user-create.component';
import { LoginMeepComponent } from './components/forms/login-meep/login-meep.component';
import { SettingsComponent } from './components/settings/settings.component';
import { TaxSingleComponent } from './components/cruds/tax/tax-single/tax-single.component';
import { TaxComponent } from './components/cruds/tax/tax.component';


import { EventService } from './components/services/event.service';
import { ProductionService } from './components/services/production.service';
import { TaxService } from './components/cruds/tax/tax.service';
import { AttendantService } from './components/cruds/attendant/attendant.service';
import { DeviceManagerService } from './components/pages/device-manager/device-manager.service';
import { MapOrderService } from './components/pages/map-order/map-order.service';
import { CashierDetailService } from './components/report/report-cashier/cashier-detail/cashier-detail.service';
import { CategoryService } from './components/cruds/category/category.service';
import { UserService } from './components/cruds/user/user.service';
import { ProductsService } from './components/cruds/products/products.service';
import { AuthService } from './components/login/auth.service';
import { CashierService } from './components/pages/cashier/cashier.service';
import { AppService } from './app.service';
import { ConfigurationService } from './components/configuration/configuration.service';

// Components
import { AppComponent } from './app.component';

import { ProductionComponent } from './components/pages/production/production.component';
import { AppServiceInterceptor} from './app.service.interceptor';
import { AuthInterceptor } from './app.service.auth.interceptor';
import { BreadcrumbComponent } from './components/shared/breadcrumb/breadcrumb.component';
import { HomeComponent } from './components/pages/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { Error404Component } from './components/pages/error404/error404.component';
import { MenuComponent } from './components/shared/menu/menu.component';
import { PaymentsComponent } from './components/cruds/payments/payments.component';
import { MapOrderComponent } from './components/pages/map-order/map-order.component';
import { CashierDetailComponent } from './components/report/report-cashier/cashier-detail/cashier-detail.component';
import { ReportCashierComponent } from './components/report/report-cashier/report-cashier.component';
import { UserSingleComponent } from './components/cruds/user/user-single/user-single.component';
import { UserComponent } from './components/cruds/user/user.component';
import { DeviceManagerComponent } from './components/pages/device-manager/device-manager.component';
import { CashierComponent } from './components/pages/cashier/cashier.component';
import { AttendantSingleComponent } from './components/cruds/attendant/attendant-single/attendant-single.component';
import { AttendantComponent } from './components/cruds/attendant/attendant.component';
import { ComissionService } from './components/cruds/comission/comission.service';
import { ComissionComponent } from './components/cruds/comission/comission.component';
import { ComissionSingleComponent } from './components/cruds/comission/comission-single/comission-single.component';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthGuard } from './guards/auth.guard';


// importar modulos raiz do angular
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';
import { AppRoutingModule } from './app.routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterializeModule} from 'angular2-materialize';
import { MaterializeDirective } from './../assets/js/materialize-directive';
import { MatTabsModule } from '@angular/material';
// importação dos modulos
import { ProductsModule } from './components/cruds/products/products.module';
import { DatepickerModule } from 'angular2-material-datepicker'
import { CategoryModule } from './components/cruds/category/category.module';
import { HeaderModule } from './components/shared/header/header.module';

import { TextMaskModule } from 'angular2-text-mask';
import { PercentageInputComponent } from './components/forms/percentage-input/percentage-input.component';

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: "right",
  allowNegative: true,
  allowZero: true,
  decimal: ",",
  precision: 2,
  prefix: "R$ ",
  suffix: "",
  thousands: "."
};

@NgModule({  
  imports: [
    BrowserModule,
    DatepickerModule, 
    MaterializeModule,
    ReactiveFormsModule,
    FormsModule,
    HeaderModule,
    ProductsModule,
    CurrencyMaskModule,
    CategoryModule,
    HttpModule,
    MatTabsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    TextMaskModule
  ],
  exports: [
    AttendantComponent,
    AttendantSingleComponent,
    ReportCashierComponent,
    CashierDetailComponent,
    UserComponent,
    UserSingleComponent,
    MapOrderComponent,
    DeviceManagerComponent,
    SettingsComponent,
    LoginMeepComponent,
    UserCreateComponent,
    MessageThanksComponent,
    TicketSingleComponent,
    DiscountComponent,
    DiscountSingleComponent,
    MaterializeDirective,
    ProductionComponent,
    CreateTicketsComponent,
    ComissionComponent,
    ComissionSingleComponent
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    BreadcrumbComponent,
    Error404Component,
    MenuComponent,
    PaymentsComponent,
    HomeComponent,
    AttendantComponent,
    ReportCashierComponent,
    CashierDetailComponent,
    AttendantSingleComponent,
    UserComponent,
    UserSingleComponent, 
    MapOrderComponent,   
    DeviceManagerComponent,
    CashierComponent,
    TaxComponent,
    TaxSingleComponent,
    SettingsComponent,
    LoginMeepComponent,
    UserCreateComponent,
    MessageThanksComponent,
    TicketSingleComponent,
    MaterializeDirective,
    DiscountComponent,
    DiscountSingleComponent,
    ProductionComponent,
    CreateTicketsComponent,
    ConfigurationComponent,
    ComissionComponent,
    ComissionSingleComponent,
    PercentageInputComponent
  ],

  // Para declarar serviços
  providers: [
    AuthService, 
    AuthGuard,
    SettingsService,
    ProductsService,
    CategoryService,
    AttendantService,
    AppService,
    DeviceManagerService,
    UserService,
    CashierService,
    CashierDetailService,
    MapOrderService,
    TaxService,
    TicketService,
    SettingsComponent,
    DiscountService,
    EventService,
    ProductionService,
    ConfigurationService,
    ComissionService,
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    { provide: HTTP_INTERCEPTORS, useClass: AppServiceInterceptor, multi: true},
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
