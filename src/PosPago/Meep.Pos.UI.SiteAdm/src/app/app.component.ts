import { MaterializeDirective } from 'angular2-materialize';
import { AuthService } from './components/login/auth.service';
import { Router, NavigationStart } from '@angular/router';
import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  providers: [Title]
})
export class AppComponent {

    
  getData: string;
  postData: string;

  showContentComponent: boolean = false;

  constructor(private authService: AuthService, private title: Title) {
    let currentTitle = this.title.getTitle();
    this.title.setTitle('Portal Meep pós');
  }

  ngOnInit() {
    this.authService.showMenu.subscribe();
    show => this.showContentComponent = show;


  }
}
