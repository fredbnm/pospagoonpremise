import { ConfigurationModel } from './configuration.model';
import { AppService } from './../../app.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable()
export class ConfigurationService extends AppService<ConfigurationModel> {

  constructor(http: HttpClient) {
    super(http);
  }
  
  method: string = 'configuration';
  
  async getConfiguration() : Promise<ConfigurationModel>{
   var config = await this.getAsync(this.method) ;
   return <ConfigurationModel>config;
  }

  async syncEndpoints() : Promise<ConfigurationModel>{
      var config = await this.getAsync("gateway/endpoint/sync");

      return <ConfigurationModel>config;

  }

  async update(model : ConfigurationModel) : Promise<ConfigurationModel>{
      var config = await this.putAsync(this.method, model);

      return <ConfigurationModel>config;
      
  }
}

