export class ConfigurationModel {
    id : string;
    meepType: MeepTypeEnum;
    integration: boolean;
    isSynchronized: boolean;
}


export enum MeepTypeEnum {
    pre = 1,
    pos = 2,
    both = 3
}

