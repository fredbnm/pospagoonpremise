import swal  from 'sweetalert2';
import { ConfigurationModel } from './configuration.model';
import { Component, OnInit } from '@angular/core';
import { ConfigurationService } from './configuration.service';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.less']
})
export class ConfigurationComponent implements OnInit {

  constructor(private configurationService : ConfigurationService) { }

  configuration : ConfigurationModel = new ConfigurationModel();

  ngOnInit() {
    this.configurationService.getConfiguration().then(c => {
      this.configuration = c;
    });
  }

  setMeepType(meepType){
    this.configuration.meepType = meepType;
  }

  changeIntegration(){
    this.configuration.integration = !this.configuration.integration;
  }

  sync(){
    this.configurationService.syncEndpoints().then(c => {
      if (c.isSynchronized)
        swal("Sincronizado com sucesso!");
      else
        swal("Houve um erro durante a sincronização. Salve as alterações e tente novamente.");
    })
  }

  save() {
    this.configurationService.update(this.configuration).then(c => {
      this.configuration = c;
      swal("Atualizado com sucesso.");
    });
  }
}
