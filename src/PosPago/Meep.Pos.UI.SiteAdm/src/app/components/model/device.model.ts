export class DeviceAttendantModel{
    id : string;
    deviceId : string;
    deviceName : string;
    attendantId : string;
    attendantName : string;
    isActive : boolean;
}