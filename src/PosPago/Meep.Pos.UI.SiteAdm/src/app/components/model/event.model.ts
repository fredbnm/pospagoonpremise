export class EventModel{
    id : string;
    eventType : EventTypeEnum;
    timeStamp : Date;
    message : string;
    isRead : boolean;
    get url() : string {
        switch(this.eventType){
            case EventTypeEnum.ProductionLine:
                return "linha-producao";
            default:
                return "home"
        }
    }
}

export enum EventTypeEnum{
    ProductionLine = 1,
    Device = 2,
    ProductExclude = 3
}