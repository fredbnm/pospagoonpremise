import { InputOptions } from './inputOptions';
export class SettingOptions {
    title: string;
    textOption: string;
    subTitle: string;
    inputs: InputOptions[] = [];
    okButtonEnabled: boolean = true;
    okButtonText: string;
    cancelButtonEnabled: boolean;
    cancelButtonText: string;
}