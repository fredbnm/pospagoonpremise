export class InputOptions {
    constructor(text: string, type : string){
        this.text = text;
        this.type = type;
    }
    text: string;
    //type: InputTypeEnum;
    type: string;
}