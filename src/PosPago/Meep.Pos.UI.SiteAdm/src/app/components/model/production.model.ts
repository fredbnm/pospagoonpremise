import { Subscription } from 'rxjs/RX';
import { Observable } from 'rxjs';

export class ProductionModel {
    productName: string;
    quantity: number;
    observation: string;
    id: string;
    categoryId:string;
    categoryName:string;
    idOrder: string;
    idOrderItem: string;
    isFinalized: boolean;
    productionTime: number;
    ticketNumber: string;
    date: Date;

    //timeLeft: Observable<number>;
    timer : Subscription;
    timeLeft: number = 0; 
    percent: number;
}