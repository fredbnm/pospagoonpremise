import { AttendantModel } from './../cruds/attendant/attendant.model';
import Utils from '../../util/utils';

export class OrderModel {
    id : string;
    products : OrderProduct[] = [];
    taxes : OrderTax[] = [];
    payments : Payment;
    dateOpened : Date;
    _dateClosed : any;
    get dateClosed(){
        if (this._dateClosed === undefined)
            return null;
        if (this._dateClosed.toString() === '0001-01-01T00:00:00')
            return null
        else
            return this._dateClosed;
    }
    set dateClosed(dateClosed : Date){
        this._dateClosed = dateClosed;
    }
    _subTotal : number;
    get subTotal() {
        var total = 0;
        this.products.forEach(product => {
            total += product.total;
        });
        return Utils.roundTo(total,2);
    }
    _total : number;
    get total(){
        return Utils.roundTo(this._total,2);
    }
    set total(total : number){
        this._total = total;
    }

    _totalPayment : number;
    get totalPayment(){
        return Utils.roundTo(this._totalPayment,2);
    }
    set totalPayment(total : number){
        this._totalPayment = total;
    }
    ticketId : string;
    ticketNumber : string;
    isOpen : boolean;
    attendant : AttendantModel = new AttendantModel();
}

export class OrderTax{
    dateOrder : Date;
    count : number;
    value : number;
    total : number;
    taxId : string;
    taxName : string;
    taxCalcType : number;
    required : boolean;
}

export class OrderProduct{
    dateOrder : Date;
    count : number;
    _value : number;
    get value(){
        return Utils.roundTo(this._value,2);
    }
    set value(value : number){
        this._value = value;
    }
    _total : number;
    get total(){
        return Utils.roundTo(this._total,2);
    }
    set total(total : number){
        this._total = total;
    }
    productId : string;
    productName : string;
    description : string;
    attendantId : string;
    attendantName : string;
}

export class Payment{
    id : string;
}