export class LocationModel{
    id : string;
    address : string;
    city : string;
    cnpj : string;
    country : string;
    name : string;
    state : string;
    zipCode : string;
}