import Utils from "../../../util/utils";

export class TaxModel {
    id : string;
    description : string;
    value : number;
    required : boolean;
    calcType : number;
}