import { HttpClient } from '@angular/common/http';
import { TaxModel } from './tax.model';
import { Injectable } from '@angular/core';
import { AppService } from '../../../app.service';
@Injectable()
export class TaxService extends AppService<TaxModel> {
    
    constructor(http: HttpClient){
        super(http);
    }

    method : string = 'tax';

    async getAll() : Promise<TaxModel[]>{
        var taxes = await this.getAllAsync(this.method);

        return <TaxModel[]>taxes;
    }

    async get(id) : Promise<TaxModel>{
        var tax = await this.getAsync(this.method + "/" + id);

        return <TaxModel>tax;
    }

    async save(model : TaxModel) : Promise<TaxModel>{
        var result = await this.postAsync(this.method, model);

        return <TaxModel>result;
    }

    async update(model : TaxModel) : Promise<TaxModel>{
        var result = await this.putAsync(this.method, model);

        return <TaxModel>result;
    }

    async delete(id) : Promise<string>{
        var result = await this.deleteAsync(this.method + "?id="+id);

        return result;
    }
}