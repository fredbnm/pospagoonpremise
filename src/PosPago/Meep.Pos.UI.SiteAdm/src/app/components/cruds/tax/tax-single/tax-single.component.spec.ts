import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxSingleComponent } from './tax-single.component';

describe('TaxSingleComponent', () => {
  let component: TaxSingleComponent;
  let fixture: ComponentFixture<TaxSingleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxSingleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
