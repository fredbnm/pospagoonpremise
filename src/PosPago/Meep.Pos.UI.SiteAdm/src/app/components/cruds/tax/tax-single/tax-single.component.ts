import swal from 'sweetalert2';
import { AppSingleComponent } from './../../../../app-single.component';
import { TaxService } from './../tax.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TaxModel } from './../tax.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tax-single',
  templateUrl: './tax-single.component.html',
  styleUrls: ['./tax-single.component.less']
})
export class TaxSingleComponent extends AppSingleComponent {

  tax : TaxModel = new TaxModel();

  constructor(route: ActivatedRoute, 
    private taxService: TaxService,
    private router : Router) { 
      super(route);
  }

  ngOnInit() {
    if (!this.isNew){
      this.taxService.get(this.id).then( t => this.tax = t );
    }
  }

  required(){
    this.tax.required = !this.tax.required;
  }

  save(){
    if (this.isNew){
      this.taxService.save(this.tax).then(t => {
        this.tax = t;
        swal({
          title: 'Taxa criada com sucesso!',
          showCancelButton: false,
          confirmButtonText: "Ok"
        }).then(() => {
          this.router.navigate(['home/taxa']);
        })
      })
    }
    else{
      this.taxService.update(this.tax).then(t => {
        this.tax = t;
        swal({
          title: 'Taxa atualizada com sucesso!',
          showCancelButton: false,
          confirmButtonText: "Ok"
        }).then(() => {
          this.router.navigate(['home/taxa']);
        })
      })
    }
  }

}
