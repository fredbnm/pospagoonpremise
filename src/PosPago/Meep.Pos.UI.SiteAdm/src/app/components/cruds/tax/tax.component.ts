import { TaxService } from './tax.service';
import { TaxModel } from './tax.model';
import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';

@Component({
  selector: 'app-tax',
  templateUrl: './tax.component.html',
  styleUrls: ['./tax.component.less']
})
export class TaxComponent implements OnInit {

  taxes : TaxModel[] = [];
  constructor(private taxService : TaxService) { 

  } 

  isRequired(required : boolean){
    if (required)
      return "Sim";
    else
      return "Não";
  }

  delete(id){
    swal({
      title: 'Deseja excluir esse desconto?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(() => this.deletedSuccess(id) );  
    //this.discountService.delete(id).then(d => this.discountService.getAll().then(discounts => this.discounts = discounts ));
  }

  deletedSuccess(id){
    this.taxService.delete(id).then(deleted => {
        swal(
          'Deletado!',
          'O produto foi removido',
          'success'
        );
        this.taxService.getAll().then(taxes => {
          this.taxes = taxes
        });
    });
  }

  ngOnInit() {
    this.taxService.getAll().then(taxs => this.taxes = taxs);
  }

}
