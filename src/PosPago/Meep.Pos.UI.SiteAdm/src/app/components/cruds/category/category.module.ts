import { SweetAlert2Module } from '@toverux/ngsweetalert2';
import { CategorySingleComponent } from './category-single/category-single.component';
import { CategoryComponent } from './category.component';
import { AppRoutingModule } from './../../../app.routing.module';
import { FormsModule } from '@angular/forms';
import { MaterializeModule } from 'angular2-materialize';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';




@NgModule({
  imports: [
    BrowserModule,
    MaterializeModule,
    FormsModule,
    CommonModule,
    AppRoutingModule,
    SweetAlert2Module
  ],
 
  exports: [
   CategoryComponent,
   CategorySingleComponent
  ],
  declarations: [
    CategoryComponent,
    CategorySingleComponent
  ],
  providers: []
})
export class CategoryModule { }