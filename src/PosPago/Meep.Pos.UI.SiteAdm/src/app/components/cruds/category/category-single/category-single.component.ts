import { AppSingleComponent } from './../../../../app-single.component';
import { CategoryModel } from './../category.model';
import { Params, ActivatedRoute } from '@angular/router';
import { CategoryService } from './../category.service';
import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';

@Component({
  selector: 'app-category-single',
  templateUrl: './category-single.component.html',
  styleUrls: ['./category-single.component.less']
})
export class CategorySingleComponent extends AppSingleComponent {

  category : CategoryModel = new CategoryModel();

  constructor(route: ActivatedRoute, 
    private categoryService: CategoryService) { 
      super(route);
    }

  ngOnInit() {
    if (!this.isNew){
      this.categoryService.get(this.id).then( c => this.category = c);
    }
  }

  save(){
    if (this.isNew)
      this.categoryService.save(this.category).then( c  => {
        this.category = c
        swal({
          title: 'Categoria criada com sucesso!',
          showCancelButton: false,
          confirmButtonText: 'Ok'
        }).then(function() {
          this.router.navigate(['categorias']);
          
        });
      
      });
    else
      this.categoryService.update(this.category).then( c  => { 
        this.category = c 
        swal({
          title: 'Categoria atualizada com sucesso!',
          showCancelButton: false,
          confirmButtonText: 'Ok'
        }).then(function() {
          this.router.navigate(['categorias']);
          
        });
      });
      
  }

}
