import { CategoryModel } from './category.model';
import { CategoryService } from './category.service';
import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.less']
})
export class CategoryComponent implements OnInit {

  categories : CategoryModel[];

  constructor(private categoryService : CategoryService) { }

  ngOnInit() {
    this.categoryService.getAll()
    .then(c => {
      this.categories = c;
      });
  }

  
  delete(id : string){
    swal({
      title: 'Deseja excluir essa Categoria?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(() => this.deletedSuccess(id), 
    (dismiss) => this.deletedError(dismiss));    
  }

  deletedSuccess(id){
    this.categoryService.delete(id).then(deleted => {
        swal(
          'Deletado!',
          'A Categoria foi removida',
          'success'
        ).then(() => {
          this.categoryService.getAll().then(c => {
            this.categories = c;
            });
        });
    });
  }

  deletedError(dismiss) {
    if (dismiss === 'cancel') {
      swal(
        'Cancelado',
        'Categoria não excluida',
        'error'
      )
    }
  }
}
