import { SweetAlert2Module } from '@toverux/ngsweetalert2';
import { HttpClientModule } from '@angular/common/http';

import { ProductsService } from './products.service';
// import { ProductsRoutingModule } from './products.routing.module';
import { AppRoutingModule } from './../../../app.routing.module';
import { FormsModule } from '@angular/forms';
import { MaterializeModule } from 'angular2-materialize';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsSingleComponent } from './products-single/products-single.component';
import { ProductsComponent } from './products.component';
import { CurrencyMaskModule } from "ng2-currency-mask";
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from "ng2-currency-mask/src/currency-mask.config";
export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: "right",
  allowNegative: true,
  allowZero: true,
  decimal: ",",
  precision: 2,
  prefix: "R$ ",
  suffix: "",
  thousands: "."
};


@NgModule({
  imports: [
    BrowserModule,
    MaterializeModule,
    FormsModule,
    CommonModule,
    MatProgressSpinnerModule,
    CurrencyMaskModule,
    AppRoutingModule,
    HttpClientModule,
    SweetAlert2Module
    // ProductsRoutingModule
  ],
 
  exports: [
    ProductsComponent,
    ProductsSingleComponent
  ],
  declarations: [
    ProductsComponent,
    ProductsSingleComponent
  ],
  providers: [{ provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }]
})
export class ProductsModule { }
