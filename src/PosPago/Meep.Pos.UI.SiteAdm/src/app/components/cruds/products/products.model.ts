export class ProductModel{
    id : string;
    name : string;
    categoryId : string;
    categoryName : string;
    price : number;
    unitEnum : MeasureUnitEnum;
    isActive : boolean;
    isProduction : boolean;
    productionTime : number;
}

enum MeasureUnitEnum {
    Unit = 1,
    Kg = 2
}