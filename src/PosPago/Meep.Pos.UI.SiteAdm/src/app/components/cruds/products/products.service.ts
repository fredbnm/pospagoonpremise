import { ProductModel } from './products.model';
import { ValidationResult } from './../../model/validationResult.model';
import { HttpClient } from '@angular/common/http';
import { AppService } from './../../../app.service';
import { HttpModule, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { ProductsComponent } from './products.component';
import { Injectable } from '@angular/core';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpErrorResponse } from "@angular/common/http";

@Injectable()
export class ProductsService extends AppService<ProductModel> {

  constructor(http: HttpClient) {
    super(http); 
  }

  method : string = 'product';

  async getAll(): Promise<ProductModel[]>{

    var products = await this.getAllAsync(this.method);
    
    return <ProductModel[]>products;
  }

  async get(id : string) : Promise<ProductModel>{
    var result = await this.getAsync(this.method+'/'+id);

    return <ProductModel>result;
  }

  async save(product : ProductModel) : Promise<ProductModel>{

    var result = await this.postAsync(this.method,product);

    return <ProductModel>result;
  }

  async update(product : ProductModel) : Promise<ProductModel>{
    
    var result = await this.putAsync(this.method,product);

    return <ProductModel>result;
  }
  
  async delete(id : string) : Promise<string>{
    var result = await this.deleteAsync(this.method+'/'+id);

    return result;
  }
}
