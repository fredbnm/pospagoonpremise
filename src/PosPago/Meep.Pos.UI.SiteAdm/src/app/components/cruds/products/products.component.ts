
import { ProductModel } from './products.model';
import { Component, OnInit } from '@angular/core';
import { ProductsService } from './products.service';
import { CategoryService } from '../category/category.service';
import swal from 'sweetalert2';

@Component({
 
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.less'],
  providers: [ProductsService]
  
})

export class ProductsComponent implements OnInit {
  loading =  true;
  products : ProductModel[];

  constructor(private productsService: ProductsService,
      private categoryService : CategoryService) {
    
  }

  ngOnInit() {
  
   this.productsService.getAll().then(p => {
    this.products = p;

    this.products.forEach(product => {
      this.loading =  false;
       this.categoryService.get(product.categoryId)
            .then(category => product.categoryName = category.name);
            // var valueProduct = <number>product.price;
      });
    });
  }

  delete(id : string){
    swal({
      title: 'Deseja excluir esse produto?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(() => this.deletedSuccess(id), 
    (dismiss) => this.deletedError(dismiss));    
  }

  updateProduct(product : ProductModel){
    product.isActive = !product.isActive;
    this.productsService.update(product)
    .then(p => {
      this.products[this.products.indexOf(product)] = p;
      if (product.isActive)
        swal("Produto ativado com sucesso!");
      else
        swal("Produto desativado com sucesso!");
    });
  }

  deletedSuccess(id){
    this.productsService.delete(id).then(deleted => {
        swal(
          'Deletado!',
          'O produto foi removido',
          'success'
        );
    });
  }

  deletedError(dismiss) {
    if (dismiss === 'cancel') {
      swal(
        'Cancelado',
        'Produto não excluido',
        'error'
      )
    }
  }
}
