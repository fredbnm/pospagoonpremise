import swal from 'sweetalert2';
import { AppSingleComponent } from './../../../../app-single.component';
import { CategoryService } from './../../category/category.service';
import { CategoryModel } from './../../category/category.model';
import { ProductModel } from './../products.model';
import { ProductsService } from './../products.service';
import { MaterializeAction } from './../../../../../assets/js/materialize-directive';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs/RX';
import { Form, FormBuilder, FormGroup } from "@angular/forms";
@Component({
  selector: 'app-products-single',
  templateUrl: './products-single.component.html',
  styleUrls: ['./products-single.component.less']
})
export class ProductsSingleComponent extends AppSingleComponent {
  
  //isNew: boolean = true;
  form: FormGroup;
  product : ProductModel = new ProductModel();
  categories : CategoryModel[];
  categoryId : string;

  constructor(route: ActivatedRoute, 
    private formBuilder: FormBuilder,
    private productsService: ProductsService,
    private router: Router,
    private categoryService: CategoryService) { 
      super(route);
      
  }

  ngOnInit() {

    if (!this.isNew){
      this.productsService.get(this.id).then( p => {
        this.product = p;
        this.product.categoryId = p.categoryId;
        this.categoryId = p.categoryId;
        this.product.unitEnum = p.unitEnum;
        this.product.isProduction = p.isProduction;
      });
    }
    else
    {
      this.product.unitEnum = 1;
    }
    
    this.fillCategoryDropDown();
  }

  fillCategoryDropDown() {
    this.categoryService.getAll().then(category => {
      this.categories = category;
      var selectCategory = new CategoryModel();
      selectCategory.id = '-1';
      selectCategory.name = 'Selecione';
      this.categories.splice(0, 0, selectCategory);

      if (this.isNew) this.product.categoryId = '-1';
    });
  }

  save(){
    if (this.isNew)
    {
      //this.product.categoryId = this.categoryId;
      this.productsService.save(this.product).then( p => {
        this.product = p;
        this.showSuccess("criado");
      });

    }
    else
      this.productsService.update(this.product)
      .then( p => {
        this.product = p;
        this.showSuccess("atualizado");
       
      });
  }

  production(){
    this.product.isProduction = !this.product.isProduction;
  }

  showSuccess(status){
    swal({
      title: 'Produto '+ status +' com sucesso!',
      showCancelButton: false,
      confirmButtonText: 'Ok'
    }).then(() => {
      this.router.navigate(['/home/produtos']);
      
    })
  }

  change(category : CategoryModel){
      this.product.categoryId = category.id;
  }
  
}
