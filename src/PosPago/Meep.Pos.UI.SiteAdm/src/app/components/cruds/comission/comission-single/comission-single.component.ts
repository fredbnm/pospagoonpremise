import { AppSingleComponent } from './../../../../app-single.component';
import { ComissionModel } from './../comission.model';
import { ComissionService } from './../comission.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, NgModule } from '@angular/core';
import swal from 'sweetalert2';

@Component({
  selector: 'app-comission-single',
  templateUrl: './comission-single.component.html',
  styleUrls: ['./comission-single.component.less']
})

export class ComissionSingleComponent extends AppSingleComponent {

  comission : ComissionModel = new ComissionModel();
  
  constructor(route: ActivatedRoute, private router: Router, private comissionService: ComissionService) { 
    super(route);
  }

  ngOnInit() {
    if (!this.isNew){
      this.comissionService.get(this.id).then(_comission => {
        this.comission.id           = _comission.id;
        this.comission.description  = _comission.description;
        this.comission.value        = _comission.value;
      });
    }
  }

  save() {
    if(this.comission.description && this.comission.value) {
      if (this.isNew) { 
        this.comissionService.save(this.comission)
        .then(this.backendSuccessCallbackResponse)
        .catch(this.backendErrCallbackResponse);
      } else {
        this.comissionService.update(this.comission)
        .then(this.backendSuccessCallbackResponse)
        .catch(this.backendErrCallbackResponse);
      }
    } else {
      swal(
        'Campos inválidos!',
        'Por favor, preencha todos os campos.',
        'error'
      )
    }
  }
  
  private showSuccess(status){
    swal({
      title: 'Comissão '+ status +' com sucesso!',
      showCancelButton: false,
      confirmButtonText: 'Ok',
      allowOutsideClick: false
    }).then(() => {
      this.router.navigate(['/home/comissão']);
    })
  }

  private backendSuccessCallbackResponse = _comission => {
    this.comission = _comission;
    if(this.isNew) {
      this.showSuccess("criada");
    } else {
      this.showSuccess("atualizada");
    }
  }

  private backendErrCallbackResponse = () => {
    swal(
      'Ocorreu um erro!',
      'Por favor, tente novamente.',
      'error'
    )
  }
}
