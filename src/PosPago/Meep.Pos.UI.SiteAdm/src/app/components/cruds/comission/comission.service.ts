import { async } from '@angular/core/testing';
import { ComissionModel } from './comission.model';
import { HttpClient } from '@angular/common/http';
import { AppService } from './../../../app.service';
import { Injectable } from '@angular/core';

@Injectable()
export class ComissionService extends AppService<ComissionModel> {
  method: string = 'comission';

  constructor(http: HttpClient) {
    super(http);
  }

  async getAll(): Promise<ComissionModel[]> {
    let comissions = await this.getAllAsync(this.method);
    return <ComissionModel[]>comissions;
  }

  async get(id: string): Promise<ComissionModel> {
    let comission = await this.getAsync(this.method + '/' + id);
    return <ComissionModel>comission;
  }

  async save(comission: ComissionModel): Promise<ComissionModel> {
    let result = await this.postAsync(this.method, comission);
    return <ComissionModel>comission;
  }

  async update(comission: ComissionModel): Promise<ComissionModel> {
    let result = await this.putAsync(this.method, comission);
    return <ComissionModel>comission;
  }

  async delete(id: string): Promise<string> {
    var result = await this.deleteAsync(this.method + "?id=" + id);
    return result;
  }
}