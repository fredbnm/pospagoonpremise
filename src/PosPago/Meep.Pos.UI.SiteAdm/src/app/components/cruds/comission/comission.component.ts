import { ComissionModel } from './comission.model';
import { Component, OnInit } from '@angular/core';
import { ComissionService } from './comission.service'
import swal from 'sweetalert2';

@Component({
  selector: 'app-comission',
  templateUrl: './comission.component.html',
  styleUrls: ['./comission.component.less']
})

export class ComissionComponent implements OnInit {

  loading = true;
  noRegister = true;

  comissions: ComissionModel[];

  constructor(private comissionService: ComissionService) { }

  ngOnInit() {
    this.comissionService.getAll().then(this.formatComissionListCallback)
    .catch(err => {
      swal(
        'Ocorreu um erro!',
        'Por favor, tente novamente.',
        'error'
      )
    })
    .then(() => {
      this.loading = false;
    });
  }

  delete(id){
    swal({
      title: 'Deseja excluir essa comissão?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    })
    .then(() => this.deletedSuccess(id))
    .catch(swal.noop);
  }

  private deletedSuccess(id){
    this.comissionService.delete(id)
    .then(deleted => {
        swal(
          'Deletado!',
          'O produto foi removido',
          'success'
        );
        this.comissionService.getAll().then(this.formatComissionListCallback);
    });
  }

  private formatComissionListCallback = _comissions => {
    this.comissions = _comissions;
    if(_comissions.length > 0) {
      this.noRegister = false;
    } else {
      this.noRegister = true;
    }
  }
}
