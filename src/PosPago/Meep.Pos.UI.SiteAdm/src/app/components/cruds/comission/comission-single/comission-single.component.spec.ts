import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissionSingleComponent } from './comission-single.component';

describe('ComissionSingleComponent', () => {
  let component: ComissionSingleComponent;
  let fixture: ComponentFixture<ComissionSingleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissionSingleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissionSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
