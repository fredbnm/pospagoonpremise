import { UserModel } from './user.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppService } from './../../../app.service';
import { LocationModel } from '../../model/location.model';

@Injectable()
export class UserService extends AppService<UserModel> {

    constructor(http: HttpClient) {
        super(http);
    }

    method: string = 'user';

    async getAll(): Promise<UserModel[]> {
        var user = await this.getAllAsync(this.method);

        return <UserModel[]>user;
    }

    async get(id: string): Promise<UserModel> {
        var result = await this.getAsync(this.method + '/' + id);

        return <UserModel>result;
    }


    async save(model: UserModel): Promise<UserModel> {

        var result = await this.postAsync(this.method, model);

        return <UserModel>result;
    }

    

    async update(model: UserModel): Promise<UserModel> {

        var result = await this.putAsync(this.method, model);

        return <UserModel>result;
    }

    async delete(id: string): Promise<string> {
        var result = await this.deleteAsync(this.method + '/' + id);

        return result;
    }

    async getLocation(id : string) : Promise<LocationModel> {
        var result = await this.getAsync("location" + '/' + id);

        return <LocationModel>result;
    }


}