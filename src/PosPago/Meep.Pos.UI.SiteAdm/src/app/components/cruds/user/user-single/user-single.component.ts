import { UserService } from './../user.service';
import { UserModel } from './../user.model';
import { ActivatedRoute } from '@angular/router';
import { AppSingleComponent } from './../../../../app-single.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-single',
  templateUrl: './user-single.component.html',
  styleUrls: ['./user-single.component.less']
})
export class UserSingleComponent extends AppSingleComponent {

  user : UserModel = new UserModel();
  
    constructor(route: ActivatedRoute, 
      private UserService: UserService) { 
        super(route);
      }
  
    ngOnInit() {
      if (!this.isNew){
        this.UserService.get(this.id).then( u => {
          this.user = u
        });
      }
    }
  
    save(){
      if (this.isNew)
        this.UserService.save(this.user).then(u => this.user = u);
      else
        this.UserService.update(this.user).then(u => this.user = u);
    }
  

}

