import { UserService } from './user.service';
import { UserModel } from './user.model';
import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.less']
})
export class UserComponent implements OnInit {

  users : UserModel [];

  constructor( private userService : UserService ) { 

  }

  ngOnInit() {
    this.userService.getAll().then(u => {
      this.users = u;
    })
  }
  

  delete(id : string){
    swal({
      title: 'Deseja excluir esse usuário?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(() => this.deletedSuccess(id), 
    (dismiss) => this.deletedError(dismiss));    
  }

  deletedSuccess(id){
    this.userService.delete(id).then(deleted => {
        swal(
          'Deletado!',
          'O usuário foi removido',
          'success'
        );
    });
  }

  deletedError(dismiss) {
    if (dismiss === 'cancel') {
      swal(
        'Cancelado',
        'Usuário não excluido',
        'error'
      )
    }
  }
}
