import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendantSingleComponent } from './attendant-single.component';

describe('AttendantSingleComponent', () => {
  let component: AttendantSingleComponent;
  let fixture: ComponentFixture<AttendantSingleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendantSingleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendantSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
