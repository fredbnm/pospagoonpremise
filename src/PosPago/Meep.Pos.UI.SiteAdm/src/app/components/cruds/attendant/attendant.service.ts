import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppService } from './../../../app.service';
import { AttendantModel } from './attendant.model';

@Injectable()
export class AttendantService extends AppService<AttendantModel> {

  constructor(http: HttpClient) {
    super(http); 
  }

  method : string = 'attendant';

  async getAll() : Promise<AttendantModel[]>{
      var attendants = await this.getAllAsync(this.method);

      return <AttendantModel[]>attendants;
  }

  async get(id : string) : Promise<AttendantModel>{
      var result = await this.getAsync(this.method + '/' + id);

      return <AttendantModel>result;
  }

  
  async save(model : AttendantModel) : Promise<AttendantModel>{
    
        var result = await this.postAsync(this.method,model);
    
        return <AttendantModel>result;
    }

    async update(model : AttendantModel) : Promise<AttendantModel>{
  
        var result = await this.putAsync(this.method,model);

        return <AttendantModel>result;
    }
    
    async delete(id : string) : Promise<string>{
        var result = await this.deleteAsync(this.method+'?id='+id);

        return result;
    }

    async newPin(id : string) : Promise<AttendantModel>{
        var result = await this.putAsync(this.method + '/' + id + "/new/pin");

        return <AttendantModel>result;
    }
}