import { AttendantService } from './attendant.service';
import { Component, OnInit } from '@angular/core';
import { AttendantModel } from './attendant.model';
import swal from 'sweetalert2';

@Component({
  selector: 'app-attendant',
  templateUrl: './attendant.component.html',
  styleUrls: ['./attendant.component.less']
})
export class AttendantComponent implements OnInit {

  attendants : AttendantModel[];

  constructor(private attendantService : AttendantService) { 

  }

  ngOnInit() {
    this.attendantService.getAll().then(a => {
      this.attendants = a;
    });
  }

  delete(id : string){
    swal({
      title: 'Deseja excluir esse atendente?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(() => this.deletedSuccess(id), 
    (dismiss) => this.deletedError(dismiss));    
  }

  deletedSuccess(id){
    this.attendantService.delete(id).then(deleted => {
        swal(
          'Deletado!',
          'O atendente foi removido',
          'success'
        ).then(() => {
          this.attendantService.getAll().then(a => {
            this.attendants = a;
            });
        });
    });
  }

  deletedError(dismiss) {
    if (dismiss === 'cancel') {
      swal(
        'Cancelado',
        'Atendente não excluida',
        'error'
      )
    }
  }
  
  newPin(id : string){
    this.attendantService.newPin(id).then(attendant => {
      this.attendants[this.attendants.indexOf(attendant)] = attendant;
      swal("Novo Pin gerado com sucesso! " + attendant.pinCode);
  });
  }

}
