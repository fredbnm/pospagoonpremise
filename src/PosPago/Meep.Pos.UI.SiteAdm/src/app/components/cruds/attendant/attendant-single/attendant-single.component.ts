import swal from 'sweetalert2';
import { AttendantModel } from './../attendant.model';
import { ActivatedRoute, Router } from '@angular/router';
import { AttendantService } from './../attendant.service';
import { AppSingleComponent } from './../../../../app-single.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-attendant-single',
  templateUrl: './attendant-single.component.html',
  styleUrls: ['./attendant-single.component.less']
})
export class AttendantSingleComponent extends AppSingleComponent {

  attendant : AttendantModel = new AttendantModel();

  constructor(private router :Router,
    route: ActivatedRoute, 
    private attendantService: AttendantService) { 
      super(route);
    }

  ngOnInit() {
    if (!this.isNew){
      this.attendantService.get(this.id).then( a => this.attendant = a);
    }
  }

  save(){
    if (this.isNew)
      this.attendantService.save(this.attendant).then(a => {
        this.attendant = a
        swal({
          title: 'Atendente criado com sucesso!',
          showCancelButton: false,
          confirmButtonText: 'Ok'
        }).then(() => {
          this.router.navigate(['/home/atendente']);
        })      
      });
    else
      this.attendantService.update(this.attendant).then(a => {this.attendant = a
        swal({
          title: 'Atendente atualizado com sucesso!',
          showCancelButton: false,
          confirmButtonText: 'Ok'
        }).then(() => {
          this.router.navigate(['/home/atendente']);
          
        })
      });
  }

}
