import Utils from "../../../util/utils";

export class DiscountModel {
    id : string;
    description : string;
    value : number;
    calcType : number;
}