import { DiscountService } from './../discount.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DiscountModel } from './../discount.model';
import { AppSingleComponent } from './../../../../app-single.component';
import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';

@Component({
  selector: 'app-discount-single',
  templateUrl: './discount-single.component.html',
  styleUrls: ['./discount-single.component.less']
})
export class DiscountSingleComponent extends AppSingleComponent {

  discount : DiscountModel = new DiscountModel();
  
    constructor(route: ActivatedRoute, 
      private discountService: DiscountService,
      private router : Router) { 
        super(route);
    }
  
    ngOnInit() {
      if (!this.isNew){
        this.discountService.get(this.id).then( t => {
          this.discount = t
          this.discount.calcType = t.calcType;
        } );
      }
      else{
        this.discount.calcType = 1;
      }
    }
    save(){
      if (this.isNew){
        this.discountService.save(this.discount).then(t => {
          this.discount = t;
          swal({
            title: 'Desconto criado com sucesso!',
            showCancelButton: false,
            confirmButtonText: "Ok"
          }).then(() => {
            this.router.navigate(['home/desconto']);
          })
        })
      }
      else{
        this.discountService.update(this.discount).then(t => {
          this.discount = t;
          swal({
            title: 'Desconto atualizado com sucesso!',
            showCancelButton: false,
            confirmButtonText: "Ok"
          }).then(() => {
            this.router.navigate(['home/desconto']);
          })
        })
      }
    }
  

}
