import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscountSingleComponent } from './discount-single.component';

describe('DiscountSingleComponent', () => {
  let component: DiscountSingleComponent;
  let fixture: ComponentFixture<DiscountSingleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscountSingleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscountSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
