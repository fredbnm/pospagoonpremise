import { DiscountModel } from './discount.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppService } from '../../../app.service';
@Injectable()
export class DiscountService extends AppService<DiscountModel> {
    
    constructor(http: HttpClient){
        super(http);
    }

    method : string = 'discount';

    async getAll() : Promise<DiscountModel[]>{
        var taxes = await this.getAllAsync(this.method);

        return <DiscountModel[]>taxes;
    }

    async get(id) : Promise<DiscountModel>{
        var tax = await this.getAsync(this.method + "/" + id);

        return <DiscountModel>tax;
    }

    async save(model : DiscountModel) : Promise<DiscountModel>{
        var result = await this.postAsync(this.method, model);

        return <DiscountModel>result;
    }

    async update(model : DiscountModel) : Promise<DiscountModel>{
        var result = await this.putAsync(this.method, model);

        return <DiscountModel>result;
    }

    async delete(id) : Promise<string>{
        var result = await this.deleteAsync(this.method + "?id=" + id);

        return result;
    }
}