import swal from 'sweetalert2';
import { DiscountService } from './discount.service';
import { DiscountModel } from './discount.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-discount',
  templateUrl: './discount.component.html',
  styleUrls: ['./discount.component.less']
})
export class DiscountComponent implements OnInit {
  
  discounts : DiscountModel[] = [];

  constructor(private discountService : DiscountService) { 

  } 

  ngOnInit() {
    this.discountService.getAll().then(discounts => {
      this.discounts = discounts
    });
  }

  delete(id){
    swal({
      title: 'Deseja excluir esse desconto?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(() => this.deletedSuccess(id) );  
    //this.discountService.delete(id).then(d => this.discountService.getAll().then(discounts => this.discounts = discounts ));
  }

  deletedSuccess(id){
    this.discountService.delete(id).then(deleted => {
        swal(
          'Deletado!',
          'O produto foi removido',
          'success'
        );
        this.discountService.getAll().then(discounts => {
          this.discounts = discounts
        });
    });
  }
}
