import { OrderProduct } from './../model/order.model';
import { ProductionModel } from './../model/production.model';
import { AppService } from './../../app.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ProductionService extends AppService<ProductionModel> {

  constructor(http: HttpClient) {
    super(http); 
  }

  method : string = 'production';

  async getAll() : Promise<ProductionModel[]>{
      var productionList = await this.getAllAsync(this.method);

      return <ProductionModel[]>productionList;
  }

  async delete(id) : Promise<string>{
    var productionList = await this.deleteAsync(this.method + '?id=' + id);

    return productionList;
  }

  async finishItem(idOrder, idOrderItem) : Promise<OrderProduct>{
      var orderProduct = await this.putAsync("order/" + idOrder + "/item/" + idOrderItem + "/finalized")

      return <OrderProduct>orderProduct;
  }

//   async get(id : string) : Promise<AttendantModel>{
//       var result = await this.getAsync(this.method + '/' + id);

//       return <AttendantModel>result;
//   }

  
//   async save(model : AttendantModel) : Promise<AttendantModel>{
    
//         var result = await this.postAsync(this.method,model);
    
//         return <AttendantModel>result;
//     }

//     async update(model : AttendantModel) : Promise<AttendantModel>{
  
//         var result = await this.putAsync(this.method,model);

//         return <AttendantModel>result;
//     }
    
//     async delete(id : string) : Promise<string>{
//         var result = await this.deleteAsync(this.method+'?id='+id);

//         return result;
//     }

//     async newPin(id : string) : Promise<AttendantModel>{
//         var result = await this.putAsync(this.method + '/' + id + "/new/pin");

//         return <AttendantModel>result;
//     }
}