import { EventModel } from './../model/event.model';
import { AppService } from './../../app.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class EventService extends AppService<EventModel> {

  constructor(http: HttpClient) {
    super(http); 
  }

  method : string = 'event';

  async getAll() : Promise<EventModel[]>{
      var eventList = await this.getAllAsync(this.method);

      return <EventModel[]>eventList;
  }

}