import { HttpClient } from '@angular/common/http';
import { OrderModel } from './../../../model/order.model';
import { AppService } from './../../../../app.service';
import { Injectable } from '@angular/core';

@Injectable()
export class CashierDetailService extends AppService<OrderModel>{

    constructor(http: HttpClient) {
        super(http);
    }

    async get(id) : Promise<OrderModel>{
        var result = await this.getAsync('order/'+id);

        return <OrderModel>result;
    }
}