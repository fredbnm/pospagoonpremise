import { CashierDetailService } from './cashier-detail.service';
import { ActivatedRoute, Params } from '@angular/router';
import { OrderModel, OrderTax } from './../../../model/order.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cashier-detail',
  templateUrl: './cashier-detail.component.html',
  styleUrls: ['./cashier-detail.component.less']
})
export class CashierDetailComponent implements OnInit {

  order : OrderModel = new OrderModel();
  constructor( private route: ActivatedRoute,
    private cashierDetailService : CashierDetailService) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      var id = params['id'];

      if (id != '' && id != 'undefined') {
        this.cashierDetailService.get(id).then(order => {
          this.order = order;
          console.log(this.order);
        });
      }
    });
  }

  goBack(){
    window.history.back();
  }

  dateFormat(date : Date){
    if (date != null && date != undefined){
      if (date.toString() == '0001-01-01T00:00:00')
        return '';
      else
        return date;
    }
  }

  taxValue(tax : OrderTax){
    if (tax.taxCalcType == 2)
      return (tax.value * 100) +'%';
    else
      return 'R$'+ tax.value.toFixed(2);
  }

}
