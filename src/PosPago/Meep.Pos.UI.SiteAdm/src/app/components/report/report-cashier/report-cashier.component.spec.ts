import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportCashierComponent } from './report-cashier.component';

describe('ReportCashierComponent', () => {
  let component: ReportCashierComponent;
  let fixture: ComponentFixture<ReportCashierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportCashierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportCashierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
