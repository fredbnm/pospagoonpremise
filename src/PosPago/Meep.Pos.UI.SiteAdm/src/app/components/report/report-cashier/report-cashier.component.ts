import { CashierReportModel } from './../../pages/cashier/models/cashierReport.model';
import { CashierService } from './../../pages/cashier/cashier.service';
import { Router } from '@angular/router';
import { CashierModel } from './../../pages/cashier/models/cashier.model';
import { Component, OnInit, Input } from '@angular/core';
import Utils from '../../../util/utils';

@Component({
  selector: 'app-report-cashier',
  templateUrl: './report-cashier.component.html',
  styleUrls: ['./report-cashier.component.less']
})
export class ReportCashierComponent implements OnInit {
 
 cashier : CashierModel;
 ddlCashier : Date[] = [];
 selectedDate : Date;
 cashierFilterModel : CashierReportModel;
 
  constructor(private router : Router,
    private cashierService : CashierService) { 
    this.cashier = new CashierModel();
    this.cashierFilterModel = new CashierReportModel();
    this.selectedDate = new Date();
  } 

  ngOnInit() {
    this.fillDropDown();
  }

  fillDropDown(){
    this.cashierService.getAll().then(cashiers => {

      cashiers = Utils.orderByDesc(cashiers,'dateOpened');
      cashiers.forEach(cashier => {
        this.ddlCashier.push(cashier.dateOpened);
      });

      this.ddlCashier = this.ddlCashier.filter((x, i, a) => x && a.indexOf(x) === i);
      this.cashierFilterModel.dateOpened = this.ddlCashier[0];
    });
  }

  search(){
    this.cashierService
      .getReport(this.cashierFilterModel)
      .then(cashier =>{
        this.cashier = cashier;
      });
  }

  dateFormat(date : Date){
    if (date != null && date != undefined){
      if (date.toString() == '0001-01-01T00:00:00')
        return '';
      else
        return date;
    }
  }

  viewDetail(orderId){
    this.router.navigate(["home/relatorio-caixa/"+orderId])
  }

}
