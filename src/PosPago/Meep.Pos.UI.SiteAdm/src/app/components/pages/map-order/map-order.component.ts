import { Subscription } from 'rxjs/RX';
import { TicketModel } from './../../model/ticket.model';
import { AttendantModel } from './../../cruds/attendant/attendant.model';
import { OrderProduct, OrderModel } from './../../model/order.model';
import { CashierModel } from './../cashier/models/cashier.model';
import { MapOrderService } from './map-order.service';
import { RouterModule, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import Utils from '../../../util/utils';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-map-order',
  templateUrl: './map-order.component.html',
  styleUrls: ['./map-order.component.less']
})
export class MapOrderComponent implements OnInit {

  cashier : CashierModel = new CashierModel();
  tickets : TicketModel[] = [];
  refresher : Subscription;

  constructor(private router : Router,
    private mapOrderService : MapOrderService) { }

  ngOnInit() {    
    this.mapOrderService.getCashier().then(cashier =>{ 
      this.cashier = cashier;
      this.mapOrderService.getAllTickets().then(tickets => {
        
        tickets.forEach(ticket => {
          if (ticket.type != 2)
            this.tickets.push(ticket);
        });       

        this.mergeActiveWithInactives();
      });
    });

    this.refresher = Observable.timer(5000).repeat().subscribe(() => {
      this.mapOrderService.getCashier().then(cashier =>{
        this.cashier = cashier;
        this.mergeActiveWithInactives();        
      });
    });
    
  }

  ngOnDestroy(){
    this.refresher.unsubscribe();
  }

  mergeActiveWithInactives(){
    this.tickets.forEach(ticket => {
      var exists = false;
      this.cashier.orders.forEach(order => {
        if (order.ticketNumber == ticket.number && order.isOpen )
          exists = true;
      });
      
      if (!exists){
        var orderModel = new OrderModel();
        
        orderModel.ticketId = ticket.id;
        orderModel.ticketNumber = ticket.number;
        orderModel.attendant = new AttendantModel();
        orderModel.attendant.name = "";
        orderModel.total = 0;
        orderModel.isOpen = true;

        this.cashier.orders.push(orderModel);
      }
    });
  }

  activeOrders(){
    var orders = [];
    this.cashier.orders = Utils.orderByDesc(this.cashier.orders, "ticketNumber");

    this.cashier.orders.forEach(order => {
      if (order.isOpen)
        orders.push(order);
    });

    return orders.sort((t1, t2) =>{
      if ((+t1.ticketNumber) < (+t2.ticketNumber)) {
          return -1;
      }

      if ((+t1.ticketNumber) > (+t2.ticketNumber)) {
          return 1;
      }

      return 0;
    });;
  }

  timeWithoutOrder(orderProduct : OrderProduct[]){
    var products = <OrderProduct[]>Utils.orderByDesc(orderProduct, "dateOrder");

    if (products != null && products != undefined && products.length > 0){
      var date = new Date(products[0].dateOrder);
      var timeRemaining = new Date().getTime() - date.getTime();
      return (timeRemaining/ 60000).toFixed(0);
    }
    
    return "0";
  }

  viewDetails(orderId){
    if (orderId == undefined)
      return;
    this.router.navigate(["home/relatorio-caixa/"+orderId]);
  }

}
