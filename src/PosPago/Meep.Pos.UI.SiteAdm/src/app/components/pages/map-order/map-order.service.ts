import { TicketModel } from './../../model/ticket.model';
import { CashierModel } from './../cashier/models/cashier.model';
import { AppService } from './../../../app.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OrderModel } from '../../model/order.model';

@Injectable()
export class MapOrderService extends AppService<CashierModel>{

    constructor(http: HttpClient) {
        super(http);
    }

    async getCashier() : Promise<CashierModel>{
        var result = await this.getAsync('cashier/current/order');

        return <CashierModel>result;
    }

    async getAllTickets() : Promise<TicketModel[]>{
        var result = await this.getAllAsync('ticket');

        return <TicketModel[]>result;
    }
}