import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppService } from './../../../../app.service';
import { TicketModel } from "../../../model/ticket.model";


@Injectable()
export class TicketService extends AppService<TicketModel> {

    constructor(http: HttpClient) {
        super(http);
    }

    method: string = 'ticket';

    async getAll(): Promise<TicketModel[]> {
        var cashiers = await this.getAllAsync(this.method);

        return <TicketModel[]>cashiers;
    }

    async insert(model : TicketModel): Promise<TicketModel>{
        var ticket = await this.postAsync(this.method, model);

        return <TicketModel>ticket;
    }

    async insertList(model : TicketModel[]): Promise<TicketModel[]>{
        var tickets = await this.postAsync(this.method + '/list', model);

        return <TicketModel[]>tickets;
    }
}