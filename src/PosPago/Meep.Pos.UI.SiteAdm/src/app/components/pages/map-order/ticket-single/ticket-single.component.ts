import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { AppSingleComponent } from './../../../../app-single.component';
import { TicketService } from './ticket.service';
import { TicketModel } from './../../../model/ticket.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ticket-single',
  templateUrl: './ticket-single.component.html',
  styleUrls: ['./ticket-single.component.less']
})
export class TicketSingleComponent extends AppSingleComponent {

  ticket : TicketModel = new TicketModel();

  constructor(private ticketService : TicketService,
    private router : Router,
    route: ActivatedRoute) { 
      super(route);
    }

  ngOnInit() {
  }

  save(){
    this.ticketService.insert(this.ticket).then(t =>{
      swal("Mesa/Comanda criada com sucesso!")
      this.router.navigate(["home/mapa-comandas"])
    })
  }

}
