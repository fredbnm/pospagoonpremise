import { DeviceAttendantModel } from './../../model/device.model';
import { AppService } from './../../../app.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class DeviceManagerService extends AppService<DeviceAttendantModel> {

    constructor(httpClient : HttpClient) {
        super(httpClient);
    }

    method = "device";

    async getAll() : Promise<DeviceAttendantModel[]>{
        var result = await this.getAllAsync(this.method);

        return <DeviceAttendantModel[]>result;
    }

    async changeStatus(id, isActive) : Promise<DeviceAttendantModel>{
        var result = await this.getAsync(this.method+"/"+id+"/"+isActive);

        return <DeviceAttendantModel>result;
    }

  
}