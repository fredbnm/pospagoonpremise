import swal from 'sweetalert2';
import { DeviceAttendantModel } from './../../model/device.model';
import { DeviceManagerService } from './device-manager.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-device-manager',
  templateUrl: './device-manager.component.html',
  styleUrls: ['./device-manager.component.less']

})
export class DeviceManagerComponent implements OnInit {

  devices : DeviceAttendantModel[] = [];
  constructor(private deviceService : DeviceManagerService) { 

  }

  ngOnInit() {
    this.deviceService.getAll().then(device => { 
      this.devices = device;
    });
  }

  changeStatus(id, status){
    this.deviceService.changeStatus(id, !status).then(device => {
      this.devices.forEach(element => {
        if (element.id == id)
          this.devices[this.devices.indexOf(element)] = device;
      });

      if (device.isActive)
        swal("Dispositivo habilitado com sucesso!");
      else
        swal("Dispositivo desabilitado com sucesso!");
      //this.devices = device;
    })
  }

}
