import { CashierModel } from './models/cashier.model';
import { CloseCashierModel } from './models/closeCashier.model';
import { OpenCashierModel } from './models/openCashier.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppService } from './../../../app.service';
import { CashierReportModel } from './models/cashierReport.model';
import { HttpParams } from '@angular/common/http';

@Injectable()
export class CashierService extends AppService<CashierModel> {

    constructor(http: HttpClient) {
        super(http);
    }

    method: string = 'cashier';

    async getAll(): Promise<CashierModel[]> {
        var cashiers = await this.getAllAsync(this.method);

        return <CashierModel[]>cashiers;
    }
    async getCurrent(): Promise<CashierModel> {
        var cashier = await this.getAsync(this.method + '/current');

        return <CashierModel>cashier;
    }

    async getReport(cashierReport : CashierReportModel){
        var params = this.setParams(cashierReport);
        var cashier = await this.getByFilterAsync(this.method + "/report", params);
        return <CashierModel>cashier;
    }

    private setParams(model : CashierReportModel): HttpParams{
        var params = new HttpParams();
        //params = params.set("DateOpened",model.dateOpened.toString());
        params = params.append("DateOpened",new Date(model.dateOpened).toISOString());

        return params;
    }

    async get(id: string): Promise<CashierModel> {
        var result = await this.getAsync(this.method + '/' + id);

        return <CashierModel>result;
    }


    async openCashier(model: OpenCashierModel): Promise<CashierModel> {

        var result = await this.postAsync(this.method + '/open', model);

        return <CashierModel>result;
    }

    async closeCashier(modelClose: CloseCashierModel): Promise<CashierModel> {

        var result = await this.putAsync(this.method + '/current/close', modelClose);

        return <CashierModel>result;
    }




}