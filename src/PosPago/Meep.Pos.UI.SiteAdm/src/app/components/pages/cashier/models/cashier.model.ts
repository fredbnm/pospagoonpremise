import { OrderModel } from "../../../model/order.model";

export class CashierModel {
    id : string;
    initialValue :number;
    cashierOpenerId:string;
    cashierCloserId: string;
    cashierOpenerName:string;
    cashierCloserName: string;
    ordersId: string;
    orders : OrderModel[] = [];
    total: number;
    isOpen: boolean;
    dateClosed: Date;
    dateOpened: Date;
}

