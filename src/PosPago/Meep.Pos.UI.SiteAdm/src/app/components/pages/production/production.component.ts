import swal from 'sweetalert2';
import { Subscription } from 'rxjs/RX';
import { CategoryService } from './../../cruds/category/category.service';
import { Observable } from 'rxjs';
import { ProductionService } from './../../services/production.service';
import { ProductionModel } from './../../model/production.model';
import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { CategoryModel } from '../../cruds/category/category.model';
import { MatTabsModule } from '@angular/material/tabs';
@Component({
  selector: 'app-production',
  templateUrl: './production.component.html',
  styleUrls: ['./production.component.less'],
  providers: [ProductionService]
})
export class ProductionComponent implements OnInit {

  observer: Subscription;
  categories: CategoryModel[] = [];
  productions: ProductionModel[] = [];

  constructor(private router: Router, private productionService: ProductionService,
    private route: ActivatedRoute, private categoryService: CategoryService) { }
  goToHome() {
    this.router.navigate(['home/caixa']);
  }


  ngOnInit() {
    this.observer = Observable.timer(5000).repeat().subscribe(() => {
      this.productionService.getAll().then(p => {
        var qtAdded = 0;
        //Caso não tenha produções adicionadas, preenche a lista.
        if (this.productions.length == 0) {
          qtAdded = p.length;
          this.productions = p;
        }
        else {
          //Caso contrário, verifica se o item a ser adicionado não existe, para não ficar renovando a lista
          p.forEach(prod => {
            if (!this.productions.find(production => production.id == prod.id)) {
              this.productions.push(prod);
              qtAdded++;
            }
          });
        }
        if (qtAdded > 0) {
          this.categories = []
          this.categoryService.getAll().then(c => {
            this.filterCategory(c);
          });
        }
      });
    });
  }

  


  startTimer(production: ProductionModel) {
    production.timeLeft = 0;
    production.percent = 0;
    production.productionTime = production.productionTime * production.quantity * 60;

    production.timer = Observable.timer(1000).repeat().subscribe(() => {
      if (production.timeLeft < production.productionTime) {
        production.timeLeft = production.timeLeft + 1;
        production.percent = (production.timeLeft * 100) / production.productionTime;
      }
    });
  }

  stopTimer(production: ProductionModel) {
    swal(
      {
        title: "Confirmação",
        text: `A produção de ` + production.productName + ' foi finalizada ? ',
        showCancelButton: true,
        cancelButtonText: "Não",
        confirmButtonText: "Sim"
      }).then(() => {
        production.timer.unsubscribe();
        this.productionService.finishItem(production.idOrder, production.idOrderItem).then(() => {
          this.productions.splice(this.productions.indexOf(this.productions.find(p => p.id == production.id)), 1);

          if (!this.categories.find(cat => cat.id == production.categoryId)) {
            this.categories.splice(this.categories.indexOf(this.categories.find(cat => cat.id == production.categoryId)));
          }

        });
      });
  }

  filterProductions(categoryId) {

    var productions: ProductionModel[] = [];

    this.productions.forEach(production => {
      if (production.categoryId == categoryId)
        productions.push(production);
    });

    return productions

  }
  filterCategory(categories: CategoryModel[]) {
    categories.forEach(category => {
      if (this.productions.find(production => production.categoryId == category.id)) {
        this.categories.push(category);
      }
    });
  }
  print(production){
    var conteudo = document.getElementById('print_div').innerHTML;
    var tela_impressao = window.open(`
    <!DOCTYPE html>
    <html>
    <head>
 
    </head>
    
    <body>
    
    </body>
    </html>
    `);

    tela_impressao.document.write(conteudo);
    
    tela_impressao.window.print();
    tela_impressao.window.close();
  }

  ngOnDestroy() {
    this.observer.unsubscribe();
  }
  
}
