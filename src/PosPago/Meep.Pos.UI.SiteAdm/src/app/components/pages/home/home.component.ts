import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',

  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less'],
   // make fade in animation available to this component
 
})
export class HomeComponent implements OnInit {
  
  
  constructor() { }

  ngOnInit() {
  }

}
