import { WizardController } from "./wizard.controller";

export class WizardStep  {
    private _route : string;
    private _routeParameter : string;

    constructor(route : string, routeParameter : string) {
        this._route = route;
        this._routeParameter = routeParameter;
    }

    getRoute() : string {
        return this._route;

    };

    getRouteParameter() : string {
        return this._routeParameter;
    }

}