import { WizardStep } from './wizard.implementation.step';
import { EventEmitter } from '@angular/core';
import { Router } from '@angular/router';


export abstract class WizardController {

    // Variável local que controla os steps
    protected _wizardSteps: Array<WizardStep>;
    private router: Router;

    // índice do step corrent
    private _currentIndex: number;

    // Event emitter para cuidar da navegação
    static nextStep: EventEmitter<any> = new EventEmitter<any>();
    static prevStep: EventEmitter<any> = new EventEmitter<any>();

    constructor(router: Router) {
        this._wizardSteps = this.getSteps();
        this._currentIndex = 0;

        this.router = router;

        WizardController.nextStep.subscribe(a => this.next());
        WizardController.prevStep.subscribe(a => this.prev());
    }

    protected clearSteps() {
        this._currentIndex = 0;
    }

    protected abstract getSteps(): Array<WizardStep>;

    next(): boolean {
        var countSteps = this._wizardSteps.length;

        if ((this._currentIndex + 1) <= countSteps) {
            this._currentIndex++;

            var nextWizardStep = this._wizardSteps[this._currentIndex];

            this.router.navigate([nextWizardStep.getRoute()]); // TODO: Passar parâmetros para a rota
        }

        return true;
    }

    prev(): boolean {

        if ((this._currentIndex - 1) >= 0) {
            this._currentIndex--;

            var nextWizardStep = this._wizardSteps[this._currentIndex];

            this.router.navigate([nextWizardStep.getRoute()]); // TODO: Passar parâmetros para a rota
        }
        return true;
    }
}