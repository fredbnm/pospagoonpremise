import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.less']
})
export class MenuComponent implements OnInit {
  /* menu responsivo*/

  hideMenuResponsivo =  true;

  isSubMenuCadastroHide = true;
  isSubMenuRelHide = true;
  newFormCrud = true;

  constructor() { }

  openResponsivo(){
    this.hideMenuResponsivo = !this.hideMenuResponsivo;
  }

  openSubMenu(submenu) {
    if (submenu == 1)
      this.isSubMenuCadastroHide = !this.isSubMenuCadastroHide;
    if (submenu == 2)
      this.isSubMenuRelHide = !this.isSubMenuRelHide;
  }

  newItem() {
    this.newFormCrud = !this.newFormCrud;
  }
  ngOnInit() {
  }

}
