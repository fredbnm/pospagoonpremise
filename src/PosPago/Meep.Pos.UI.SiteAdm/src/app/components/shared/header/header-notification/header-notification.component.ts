import { Router } from '@angular/router';
import { EventModel, EventTypeEnum } from './../../../model/event.model';
import { EventService } from './../../../services/event.service';
import { ProductionService } from './../../../services/production.service';
import { ProductionModel } from './../../../model/production.model';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/RX';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header-notification',
  templateUrl: './header-notification.component.html',
  styleUrls: ['./header-notification.component.less']
})
export class HeaderNotificationComponent implements OnInit {
  hiddenNotification = true;

  notificator : Subscription;
  eventList : EventModel[] = [];
  get notificationQuantity() : number{

    return this.eventList.filter(e => !e.isRead).length;
  };

  constructor(private eventService : EventService,
  private router : Router) { }

  ngOnInit() {
    this.notificator = Observable.timer(5000).repeat().subscribe(() => {
      this.eventService.getAll().then(eventList =>{
        if (this.eventList.length == 0)
          this.eventList = eventList;
        else {
          eventList.forEach(event => {
            if (!this.eventList.find(ev => ev.id == event.id)){
              this.eventList.unshift(event);
            }
          });
        }
      });
    });
  }

  showNotification() {
    this.hiddenNotification = !this.hiddenNotification;
    this.eventList.filter(e => !e.isRead).forEach(e => e.isRead = true);
  }

  redirect(eventType : EventTypeEnum) {
    this.hiddenNotification = !this.hiddenNotification;
    switch(eventType){
      case EventTypeEnum.ProductionLine:
        this.router.navigate(["linha-producao"]);   
        break;
      case EventTypeEnum.Device:
        this.router.navigate(["home/gerenciador-dispositivos"]); 
        break;
    }
    
  }  

  ngOnDestroy(){
    this.notificator.unsubscribe();
  }

}
