import { UserModel } from './../../cruds/user/user.model';
import { AuthService } from './../../login/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

  constructor(private authService : AuthService) { }

  user : UserModel;
  ngOnInit() {
    this.user = this.authService.user;
  }

}
