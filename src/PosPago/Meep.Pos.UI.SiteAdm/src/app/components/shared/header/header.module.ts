import { AppRoutingModule } from './../../../app.routing.module';
import { HeaderComponent } from './header.component';
import { HeaderNotificationComponent } from './header-notification/header-notification.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  exports: [
    HeaderComponent,
    HeaderNotificationComponent
  ],
  declarations: [
    HeaderComponent,
    HeaderNotificationComponent
  ]
})
export class HeaderModule { }
