import { LoginModel, TokenModel } from './../model/login.model';
import { HttpClient } from '@angular/common/http';
import { AppService } from './../../app.service';
import { Injectable } from '@angular/core';

@Injectable()
export class SettingsService extends AppService<LoginModel>{
  	
 
  constructor(http: HttpClient) { 
    super(http)
  }

  async loginMeepServer(loginModel: LoginModel) : Promise<TokenModel> {
    var token = await this.loginMeepServer(loginModel);

    return token;
  }

}
