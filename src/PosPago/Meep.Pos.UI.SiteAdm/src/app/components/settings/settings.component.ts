import { Router } from '@angular/router';
import { WizardStep } from './../shared/wizard/wizard.implementation.step';
import { Component, OnInit, Injectable } from '@angular/core';
import { WizardController } from '../shared/wizard/wizard.controller';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.less']
})

export class SettingsComponent extends WizardController {

    constructor(router : Router){
        super(router);
    }
    protected getSteps(): WizardStep[] {
        return [
            new WizardStep('', ''),            
            new WizardStep('/mesas', ''),
            new WizardStep('/criar-usuario', ''),
            new WizardStep('/obrigado', '')
        ];
    }

    clearSteps() {
        this.clearSteps();
    }
    nextPage() {
        // validar
        // seu codigo. enviar mensagem de rro
    
        // se não deu erro, vai para o proximo
          this.next();
        // senão não faz nada
       }
      
    
       prevPage() {
        this.prev();
       }
    /**
     * 
     * Na classe login meep, quando for fazer um next, usar:
     * PaginaController.nextStep.emit();
     * 
     * Em prev, usar
     * PaginaController.prevStep.emit()
     * 
     */
}