
import { ActivatedRoute, Router } from '@angular/router';
import { UserLogin } from './userLogin';
import { AuthService } from './auth.service';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  user: UserLogin = new UserLogin ();
  
  constructor(private authService: AuthService,
    private router : Router) {
      
  //  if (localStorage.getItem("configured"))
  //     this.router.navigate(["login"]);

    this.authService.isConfigured().then(isConfigured => {
      if (!isConfigured)
        this.router.navigate(["login-meep"]);
    })
  }
  
  ngOnInit() {  
      localStorage.clear();
  }

  signIn(){
    this.authService.signIn(this.user).then(token => {
      localStorage.setItem("token", token.access_token);
      localStorage.setItem("user", JSON.stringify(token.user));
      
      this.router.navigate(["home/caixa"]);
    });
  }
}