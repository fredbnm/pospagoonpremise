import { UserModel } from './../cruds/user/user.model';
import { HttpClient } from '@angular/common/http';
import { AppService } from './../../app.service';
import { LoginModel, TokenModel } from './../model/login.model';
import { Router } from '@angular/router';
import { UserLogin } from './userLogin';
import { Injectable, EventEmitter } from '@angular/core';


@Injectable()
export class AuthService extends AppService<UserModel> {

  private authenticatedUser: boolean = true;

  showMenu = new EventEmitter<boolean>();

  get user() : UserModel {
    return JSON.parse(localStorage.getItem("user"));
  } 

  get token(){
    return localStorage.getItem("token");
  }
  constructor(http: HttpClient) {
    super(http);
  }

  async signIn(user: UserLogin) : Promise<TokenModel>{
    var model = new LoginModel();
    model.username = user.nameLogin;
    model.password = user.passwordLogin;
    
    var token = await this.login(model);

    return token;
  }
  
  userIsAuthenticated(){
    return this.token != undefined && this.token != "";
  }

  async signInMeepServer(login :LoginModel){
    var token = await this.loginMeepServer(login);
    return token;
  }

  async isConfigured(){
    var configured = await this.getAsync("gateway/portal/configured")

    return configured;
  }

}
