import { TicketModel } from './../../model/ticket.model';
import { TicketService } from './../../pages/map-order/ticket-single/ticket.service';
import { SettingsComponent } from './../../settings/settings.component';
import { AuthService } from './../../login/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-tickets',
  templateUrl: './create-tickets.component.html',
  styleUrls: ['./create-tickets.component.less']
})
export class CreateTicketsComponent implements OnInit {

  constructor(private settingsComponent : SettingsComponent, 
    private ticketService: TicketService,
    private router : Router) {
  
  }
  tickets : TicketModel[] = [];
  quantity : number;

  ngOnInit() {
  }

  nextPage(){
    if (this.quantity){
      for(let i = 0; i < this.quantity; i++){
        var ticket = new TicketModel();
        ticket.number = (i + 1).toString();
        this.tickets.push(ticket);
      }
      this.ticketService.insertList(this.tickets).then(tickets =>{
        this.settingsComponent.next();
      });
    }
    else
      this.settingsComponent.next();
  }

}
