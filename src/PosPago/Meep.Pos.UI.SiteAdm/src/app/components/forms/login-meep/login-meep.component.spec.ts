import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginMeepComponent } from './login-meep.component';

describe('LoginMeepComponent', () => {
  let component: LoginMeepComponent;
  let fixture: ComponentFixture<LoginMeepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginMeepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginMeepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
