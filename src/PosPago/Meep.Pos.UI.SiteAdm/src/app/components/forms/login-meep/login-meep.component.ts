import { Router } from '@angular/router';
import { AuthService } from './../../login/auth.service';
import swal from 'sweetalert2';
import { LoginModel } from './../../model/login.model';

import { UserCreateComponent } from './../user-create/user-create.component';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { SettingsComponent } from './../../settings/settings.component';
@Component({
  selector: 'app-login-meep',
  templateUrl: './login-meep.component.html',
  styleUrls: ['./login-meep.component.less']
})
export class LoginMeepComponent implements OnInit {
  login : LoginModel = new LoginModel();

  constructor(private settingsComponent : SettingsComponent, 
    private authService: AuthService,
    private router : Router) {
      
    // if (localStorage.getItem("configured"))
    //   this.router.navigate(["login-meep"]);

    // this.authService.isConfigured().then(isConfigured => {
    //   if (!isConfigured)
    //     this.router.navigate(["login-meep"]);
    // })
  }

  ngOnInit() {
    //this.settingsComponent.clearSteps();
    }

   nextPage() {
     this.authService.signInMeepServer(this.login).then( token =>{
      if(token) {
        localStorage.setItem("token",token.access_token);
        localStorage.setItem("locationId", token.locationId);
        this.settingsComponent.next();
      } else {
        swal("Esse usuário não existe, contate o administrador!")
      }
    });

    // validar
    // seu codigo. enviar mensagem de rro
    
    // se não deu erro, vai para o proximo
      
    // senão não faz nada
   }
  

   prevPage() {
    this.settingsComponent.prev();
   }
  


}
