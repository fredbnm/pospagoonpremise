import { LocationModel } from './../../model/location.model';
import { UserModel } from './../../cruds/user/user.model';
import { UserService } from './../../cruds/user/user.service';
import  swal  from 'sweetalert2';
import { SettingsComponent } from './../../settings/settings.component';
import { Component, OnInit } from '@angular/core';
// import { SettingsComponent } from '';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.less']
})
export class UserCreateComponent implements OnInit {
  location : LocationModel = new LocationModel();
  login :string;
  password : string;
  passwordConfirm : string;
  email : string;


  constructor(private settingsComponent : SettingsComponent,
      private userService : UserService) { }

  ngOnInit() {
    this.userService.getLocation(localStorage.getItem("locationId")).then(location => {
      this.location = location;
    })
  }

  save() {
    if (!this.login || !this.password || !this.passwordConfirm || !this.email){
      swal("Para prosseguir, é obrigatório o preenchimento de todos os campos.");
      return;
    }

    if (this.password != this.passwordConfirm){
      swal("A senha digitada não condiz com a confirmação.");
      return;
    }
    
    var user = new UserModel();

    user.Login = this.login;
    user.Password = this.password;
    user.Email = this.email;
    user.Name = this.login;

    this.userService.save(user).then(u => {
      if (u){
        //salvar
        //chamar nextPage
        this.nextPage();
      }
    })
    
  }

  nextPage() {
    // validar
    // seu codigo. enviar mensagem de rro

    // se não deu erro, vai para o proximo
      this.settingsComponent.next();
    // senão não faz nada
   }
  

   prevPage() {
    this.settingsComponent.prev();
   }
  

}
