import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PercentageInputComponent } from './percentage-input.component';

describe('PercentageInputComponent', () => {
  let component: PercentageInputComponent;
  let fixture: ComponentFixture<PercentageInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PercentageInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PercentageInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
