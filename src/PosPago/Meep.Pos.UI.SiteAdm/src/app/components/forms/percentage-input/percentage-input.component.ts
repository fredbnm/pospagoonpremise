import { Component, Input, Output, EventEmitter } from '@angular/core';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';

@Component({
  selector: 'app-percentage-input',
  templateUrl: './percentage-input.component.html',
  styleUrls: ['./percentage-input.component.less']
})

export class PercentageInputComponent {

  @Input() percentage: number;
  @Output() percentageChange: EventEmitter<number> = new EventEmitter();

  mask = createNumberMask({
    allowDecimal: true,
    includeThousandsSeparator: false,
    prefix: '',
    suffix: '%',
    integerLimit: 3
  });

  formatQuantity() {
    if(this.percentage) {
      if (this.formatValue(this.percentage) > 100) {
        this.percentage = 100.00;
      }
    } else {
      this.percentage = 0;
    }
    this.percentageChange.next(this.formatValue(this.percentage));
  }

  private formatValue(value: number): number {
    let string = new String();
    string = value.toString();
    return parseFloat(string.split('%')[0]);
  }
}
