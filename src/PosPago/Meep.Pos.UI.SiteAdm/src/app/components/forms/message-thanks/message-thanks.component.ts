import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-message-thanks',
  templateUrl: './message-thanks.component.html',
  styleUrls: ['./message-thanks.component.less']
})
export class MessageThanksComponent implements OnInit {

  constructor() { }


  
  ngOnInit() {
    localStorage.removeItem("token");
    localStorage.setItem("configured", "true");
  }

}
