import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageThanksComponent } from './message-thanks.component';

describe('MessageThanksComponent', () => {
  let component: MessageThanksComponent;
  let fixture: ComponentFixture<MessageThanksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageThanksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageThanksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
