import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import 'hammerjs';
if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
.then(() => {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('sw.js').then(function (reg) {
      reg.onupdatefound = function () {
        const installingWorker = reg.installing;
        installingWorker.onstatechange = function () {
          switch (installingWorker.state) {
            case 'installed':
              if (navigator.serviceWorker.controller) {
                console.log('O conteúdo novo ou atualizado está disponível.');
              } else {
                console.log('O conteúdo agora está disponível offline!');
              }
              break;

            case 'redundant':
              console.error('O Service Worker instalação tornou-se redundante.');
              break;
          }
        };
      };
    }).catch(function (e) {
      console.error('Erro durante o cadastro do Service Worker:', e);
    });
  }
});
