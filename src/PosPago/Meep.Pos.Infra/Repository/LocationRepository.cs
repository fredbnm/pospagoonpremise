﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Domain.Core.Repository;

namespace Meep.Pos.Infra.Data.Repository
{
    public class LocationRepository : Repository<Location>, ILocationRepository
    {
    }
}
