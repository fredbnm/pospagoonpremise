﻿using Meep.Pos.Domain.Core.Repository;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Infra.Data.Repository
{
    public class UserRepository : Repository<AppUser>, IUserRepository
    {

    }
}
