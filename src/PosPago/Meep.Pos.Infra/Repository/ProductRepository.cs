﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Meep.Pos.Domain.Models;
using Meep.Pos.Domain.Core.Repository;
using Meep.Pos.Infra.Data.Repository.Interfaces;

namespace Meep.Pos.Infra.Data.Repository
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository()
        {
            
        }

        #region [ Methods ]
        

        #endregion
    }
}
