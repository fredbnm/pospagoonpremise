﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Infra.Data.Repository.Base.Interface
{
    public interface IRepository<TEntity>
    {
        TEntity Insert(TEntity item);
        bool Remove(Guid id);
        TEntity Get(Guid id);
        IEnumerable<TEntity> GetAll();
        TEntity Update(TEntity item);
        IEnumerable<TEntity> GetByFilter(Func<TEntity, bool> filter);
    }
}
