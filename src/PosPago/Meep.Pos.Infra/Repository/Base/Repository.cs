﻿using Meep.Pos.Domain.Core.Models;
using Meep.Pos.Infra.Data.Mongo.Repository.Interface;
using Meep.Pos.Infra.Data.Repository.Base.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Infra.Data.Repository.Base
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : BaseModel, new()
    {
        IList<TEntity> _items;
        IMongoDbRepository _repository;

        public Repository(IMongoDbRepository repository)
        {
            _repository = repository;
        }

        public virtual TEntity Insert(TEntity item)
        {
            _items.Add(item);
            return item;
        }

        public virtual bool Remove(Guid id)
        {
            var item = _repository.DeleteOne<TEntity>();
            return _items.Remove(item);
        }

        public virtual TEntity Get(Guid id)
        {
            return _repository.GetOne<TEntity>(p => p.Id == id);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return _repository.GetMany<TEntity>(null);
        }

        public virtual TEntity Update(TEntity item)
        {
            //TODO: Pode dar erro por estar fazendo referencia de memória. Corrigir. (shallow copy?)
            var itemToUpdate = _items.FirstOrDefault(i => i.Id == item.Id);
            itemToUpdate = item;

            return itemToUpdate;

        }

        public virtual IEnumerable<TEntity> GetByFilter(Func<TEntity, bool> filter)
        {
            return _items.Where(filter);
        }
    }
}
