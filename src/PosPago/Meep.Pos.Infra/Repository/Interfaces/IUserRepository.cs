﻿using Meep.Pos.Domain.Core.Repository;
using Meep.Pos.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Infra.Data.Repository.Interfaces
{
    public interface IUserRepository : IRepository<AppUser>
    {

    }
}
