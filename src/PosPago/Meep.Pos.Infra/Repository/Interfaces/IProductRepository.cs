﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Core.Repository;
using Meep.Pos.Domain.Models;

namespace Meep.Pos.Infra.Data.Repository.Interfaces
{
    public interface IProductRepository : IRepository<Product>
    {

    }
}
