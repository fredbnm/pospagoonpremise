﻿using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Dapper.Repository;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Meep.Pos.Infra.Data.Repository
{
    public class DeviceRepository : Repository<Device>, IDeviceRepository
    {
        public DeviceRepository(IDbConnection db) : base(db)
        {

        }
    }
}
