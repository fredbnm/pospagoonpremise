﻿using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Dapper.Repository;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Flepper.QueryBuilder;
using Dapper;
using Meep.Pos.Infra.Data.Dapper.Extensions;

namespace Meep.Pos.Infra.Data.Repository
{
    public class UserRepository : PersonRepository<AppUser>, IUserRepository
    {
        public UserRepository(IDbConnection db) : base(db)
        {
        }

        protected override AppUser InsertSub(AppUser person)
        {

            var sql = FlepperQueryBuilder.Insert()
               .Into("AppUser")
               .Columns("Id, Name, Cpf, Sex, Email, Login, Password")
               .Values(person.Id, person.Name, person.Cpf, person.Sex, person.Email, person.Login, person.Password)
               .BuildWithParameters();

            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return person;
        }

    }
}
