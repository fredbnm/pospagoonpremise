﻿
using Dapper;
using Flepper.QueryBuilder;
using Meep.Pos.Infra.Data.Dapper.Extensions;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Dapper.Repository;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;

namespace Meep.Pos.Infra.Data.Repository
{
    public class DeviceAttendantRepository : Repository<DeviceAttendant>, IDeviceAttendantRepository
    {
        public DeviceAttendantRepository(IDbConnection db) : base(db)
        {

        }

        public override IEnumerable<DeviceAttendant> GetAll()
        {

            var sql = @"SELECT DA.*, D.*, A.*
                FROM DeviceAttendant AS DA
                INNER JOIN Device AS D ON D.Id = DA.DeviceId
                INNER JOIN Attendant as A ON A.Id = DA.AttendantId";

            var deviceAttendants = Db.Query<DeviceAttendant, Device, Attendant, DeviceAttendant>(sql, (da, d, a) =>
                {
                    da.SetFieldValue("_attendant", a);
                    da.SetFieldValue("_device", d);
                    return da;
                });

            return deviceAttendants;
         
        }

        public override DeviceAttendant Get(Guid id)
        {
            var sql = @"SELECT DA.*, D.*, A.*
                FROM DeviceAttendant AS DA
                INNER JOIN Device AS D ON D.Id = DA.DeviceId
                INNER JOIN Attendant as A ON A.Id = DA.AttendantId
                WHERE DA.Id = @Id";

            var deviceAttendant = Db.Query<DeviceAttendant, Device, Attendant, DeviceAttendant>(sql, (da, d, a) =>
            {
                da.SetFieldValue("_attendant", a);
                da.SetFieldValue("_device", d);
                return da;
            }, new { Id = id })
            .FirstOrDefault();

            return deviceAttendant;
        }

        public override DeviceAttendant Update(DeviceAttendant item)
        {
            var sql = FlepperQueryBuilder.Update(TableName)
                        .Set("AttendantId",item.Attendant.Id.ToString())
                        .Set("DeviceId", item.Device.Id)
                        .Set("IsActive", item.IsActive)
                        .Where("Id")
                        .EqualTo(item.Id.ToString())
                        .BuildWithParameters();

            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return item;
        }
    }
}
