﻿using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Core;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Meep.Pos.Infra.Data.Repository
{
    public class EventRepository : CacheRepository<Event>, IEventRepository
    {
        private IMemoryCache _memoryCache;
        private MemoryCacheEntryOptions _cacheEntryOptions;

        public EventRepository(IMemoryCache memoryCache)            
            : base(memoryCache, 
                  new MemoryCacheEntryOptions() { SlidingExpiration = TimeSpan.FromMinutes(30), AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(30) }, 
                  "event")
        {

        }

        public void ExcludeEvent(Guid identifier)
        {
            var @event = GetAll().FirstOrDefault(e => e.Identifier == identifier);

            Delete(@event.Id);
    }
    }
}
