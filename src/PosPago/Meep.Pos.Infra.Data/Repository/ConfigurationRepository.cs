﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Infra.Data.Dapper.Repository;
using System.Data;
using System.Linq;
using Flepper.QueryBuilder;
using Dapper;
using Meep.Pos.Domain.Models.Interface;
using Meep.Pos.Infra.Data.Dapper.Extensions;
using Meep.Pos.Domain.Core.Models;
using System.Collections;

namespace Meep.Pos.Infra.Data.Repository
{
    public class ConfigurationRepository : Repository<Configuration>, IConfigurationRepository
    {
        public ConfigurationRepository(IDbConnection db) : base(db)
        {

        }

        public Configuration Get()
        {
            var sql = FlepperQueryBuilder.Select().From(TableName).Build();

            var configuration =  Db.Query<Configuration>(sql.MySqlAdapter()).FirstOrDefault();

            configuration.Endpoints = GetEndpoints(configuration.Id);

            return configuration;

        }

        private List<ConfigurationEndpoint> GetEndpoints(Guid configId)
        {

            var sql = FlepperQueryBuilder.Select().From("ConfigurationEndpoint").Where("ConfigurationId").EqualTo(configId).BuildWithParameters();

            var endpoints = Db.Query<ConfigurationEndpoint>(sql.Query.MySqlAdapter(), sql.Parameters);

            return endpoints.ToList();

        }

        public override Configuration Insert(Configuration item)
        {
            var sql = FlepperQueryBuilder.Insert()
                .Into(this.TableName)
                .Columns("Id", "MeepType", "Integration", "CreationDate")
                .Values(item.Id, item.MeepType, item.Integration, DateTime.Now)
               .BuildWithParameters();

            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return item;
        }

        public override Configuration Update(Configuration item)
        {
            var sql = FlepperQueryBuilder.Update(this.TableName)
               .Set("MeepType", (int)item.MeepType)
               .Set("Integration", (bool)item.Integration)
               .Where("Id")
               .EqualTo(item.Id.ToString())
               .BuildWithParameters();

            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return item;
        }


        public Configuration InsertEndpoints(Configuration configuration)
        {
            Db.Open();

            using (var trans = Db.BeginTransaction())
            {
                try
                {
                    foreach (var endpoint in configuration.Endpoints)
                    {
                        InsertEndpoint(endpoint, configuration.Id);
                    }

                    trans.Commit();
                }
                catch
                {
                    trans.Rollback();
                    throw;
                }
                finally
                {
                    Db.Close();
                }
            }

            return configuration;
        }

        private void InsertEndpoint(ConfigurationEndpoint configurationEndpoint, Guid configurationId)
        {
            var sql = FlepperQueryBuilder.Insert().Into("ConfigurationEndpoint")
                        .Columns("Id", "Endpoint", "Flow", "Pipeline", "EndpointType", "ContinueOnError", "ConfigurationId")
                        .Values(configurationEndpoint.Id, configurationEndpoint.Endpoint, configurationEndpoint.Flow, configurationEndpoint.Pipeline, configurationEndpoint.EndpointType,
                            configurationEndpoint.ContinueOnError, configurationId)
                        .BuildWithParameters();

            this.Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);
        }

        public List<ConfigurationEndpoint> GetEndpoints()
        {
            var sql = FlepperQueryBuilder.Select().From("ConfigurationEndpoint").Build();

            return Db.Query<ConfigurationEndpoint>(sql.MySqlAdapter()).ToList();
        }
    }
}
