﻿using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Dapper.Repository;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Flepper.QueryBuilder;
using Dapper;
using Meep.Pos.Infra.Data.Dapper.Extensions;

namespace Meep.Pos.Infra.Data.Repository
{
    public abstract class PersonRepository<TPerson> : Repository<TPerson>, IPersonRepository<TPerson> where TPerson : Person
    {
        protected PersonRepository() : base()
        {
        }

        public PersonRepository(IDbConnection db) : base(db)
        {
        }

        public override TPerson Insert(TPerson item)
        {
            Db.Open();
            using (var trans = Db.BeginTransaction())
            {
                try
                {
                    InsertParent(item);

                    InsertSub(item);

                    trans.Commit();
                }
                catch(Exception ex)
                {
                    trans.Rollback();
                    throw;
                }
            }
            Db.Close();

            return item;
        }

        protected abstract TPerson InsertSub(TPerson person);

        private void InsertParent(Person person)
        {
            var sql = FlepperQueryBuilder.Insert()
                .Into("Person")
                .Columns("Id, Name, Sex, Cpf")
                .Values(person.Id, person.Name, person.Sex, person.Cpf)
                .BuildWithParameters();

            this.Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);
        }
    }
}
