﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Infra.Data.Dapper.Repository;
using System.Data;

namespace Meep.Pos.Infra.Data.Repository
{
    public class LocationRepository : Repository<Location>, ILocationRepository
    {
        public LocationRepository(IDbConnection db) : base(db)
        {
        }
    }
}
