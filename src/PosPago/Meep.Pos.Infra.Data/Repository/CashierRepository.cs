﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Infra.Data.Dapper.Repository;
using System.Data;
using Flepper.QueryBuilder;
using Dapper;
using Meep.Pos.Infra.Data.Dapper.Extensions;
using System.Linq;

namespace Meep.Pos.Infra.Data.Repository
{
    public class CashierRepository : Repository<Cashier>, ICashierRepository
    {
        public CashierRepository(IDbConnection db) : base(db)
        {

        }

        public override Cashier Get(Guid id)
        {
            var sqlCashier = @"SELECT C.*, O.*, PO.*, PA.*
                             FROM Cashier C LEFT JOIN Orders O ON O.CashierId = C.ID
                                LEFT JOIN Person PO ON PO.Id = C.CashierOpenerId
                                LEFT JOIN Person PA ON PA.Id = C.CashierCloserId
                                WHERE C.Id = @Id";

            Cashier cashier = Db.Query<Cashier, List<Orders>, Person, Person, Cashier>(sqlCashier, (c, o, po, pa) =>
            {
                c.SetFieldValue("_orders", o);
                c.SetFieldValue("_cashierOpenerAttendant", po);
                c.SetFieldValue("_cashierCloserAttendant", pa);
                return c;

            }, new { Id = id }).FirstOrDefault();


            return cashier;
        }

        public override Cashier Insert(Cashier item)
        {
            
            var sql = FlepperQueryBuilder.Insert()
               .Into(item.GetType().Name)
               .Columns("Id, DateOpened, Total, InitialValue, CashierOpenerId")
               .Values(item.Id, item.DateOpened, item.Total, item.InitialValue, item.CashierOpener.Id)
               .BuildWithParameters();


            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return item;
        }

        public override Cashier Update(Cashier item)
        {
            var sql = FlepperQueryBuilder.Update(item.GetType().Name)
               .Set("DateClosed", item.DateClosed)
               .Set("Total", item.Total)
               .Set("CashierCloserId", item.CashierCloser?.Id)
               .BuildWithParameters();

            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return item;
        }

        public override IEnumerable<Cashier> GetAll()
        {
            var sqlCashier = @"SELECT C.*, O.*, PO.*, PA.*
                             FROM Cashier C 
                                LEFT JOIN Orders O ON O.CashierId = C.ID
                                LEFT JOIN Person PO ON PO.Id = C.CashierOpenerId
                                LEFT JOIN Person PA ON PA.Id = C.CashierCloserId;";

            IEnumerable<Cashier> cashiers = Db.Query<Cashier, List<Orders>, Person, Person, Cashier>(sqlCashier, (c, o, po, pa) =>
            {
                c.SetFieldValue("_orders", o);
                c.SetFieldValue("_cashierOpenerAttendant", po);
                c.SetFieldValue("_cashierCloserAttendant", pa);
                return c;

            }) ;
            
            return cashiers;
        }

        public IEnumerable<Cashier> CashierReport(DateTime dateOpened)
        {
            var sql = @"SELECT C.*, O.*, PO.*, PA.*
                             FROM Cashier C 
                                LEFT JOIN Orders O ON O.CashierId = C.ID
                                LEFT JOIN Person PO ON PO.Id = C.CashierOpenerId
                                LEFT JOIN Person PA ON PA.Id = C.CashierCloserId
                            WHERE C.DateOpened = @DateOpened";


            IEnumerable<Cashier> cashiers = Db.Query<Cashier, List<Orders>, Person, Person, Cashier>(sql, (c, o, po, pa) =>
            {
                c.SetFieldValue("_orders", o);
                c.SetFieldValue("_cashierOpenerAttendant", po);
                c.SetFieldValue("_cashierCloserAttendant", pa);
                return c;

            }, new { DateOpened = dateOpened } );

            return cashiers;
        }

        public Cashier GetCurrent()
        {
            return GetByFilter(c => c.DateOpened <= DateTime.Now && c.DateClosed == DateTime.MinValue)
                .OrderByDescending(c => c.DateOpened)
                .FirstOrDefault();
        }
    }
}
