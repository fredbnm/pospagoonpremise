﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Infra.Data.Dapper.Repository;
using System.Data;

namespace Meep.Pos.Infra.Data.Repository
{
    public class TicketRepository : Repository<Ticket>, ITicketRepository
    {
        public TicketRepository(IDbConnection db) : base(db)
        {
        }
    }
}
