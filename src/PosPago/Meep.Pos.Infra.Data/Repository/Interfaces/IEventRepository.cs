﻿using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Core;
using Meep.Pos.Infra.Data.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Infra.Data.Repository.Interfaces
{
    public interface IEventRepository : ICacheRepository<Event>
    {
        void ExcludeEvent(Guid identifier);
    }
}
