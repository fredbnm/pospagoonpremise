﻿using Meep.Pos.Domain;
using Meep.Pos.Infra.Data.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Infra.Data.Repository.Interfaces
{
    public interface IProductionRepository : ICacheRepository<Production>
    {
        //Production Get(Guid productionId);
        //List<Production> GetAll();
        //Production Insert(Production production);
        //Production Update(Production production);
        //bool Delete(Guid productionId);
    }
}
