﻿using System;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Core.Interfaces;
using System.Collections.Generic;

namespace Meep.Pos.Infra.Data.Repository.Interfaces
{
    public interface IOrderRepository : IRepository<Orders>
    {
        Orders OpenOrder(Orders item, Guid cashierId);

        Orders SaveItems(Orders item);

        Orders GetOrderOnCashierByTicket(Cashier cashier, string TicketNumber);

        IEnumerable<Orders> GetByCashierId(Guid cashierId);

        Payment SavePayment(Payment payment, Guid orderId);

        OrderTax RemoveTax(OrderTax item);

        OrderProduct UpdateOrderProduct(OrderProduct item);
    }
}
