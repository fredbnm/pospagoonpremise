﻿using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Infra.Data.Repository.Interfaces
{
    public interface IDeviceAttendantRepository : IRepository<DeviceAttendant>
    {
    }
}
