﻿using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Core.Interfaces;
using System.Collections.Generic;

namespace Meep.Pos.Infra.Data.Repository.Interfaces
{
    public interface IConfigurationRepository : IRepository<Configuration>
    {
        Configuration InsertEndpoints(Configuration configuration);
        List<ConfigurationEndpoint> GetEndpoints();
        Configuration Get();
    }
}
