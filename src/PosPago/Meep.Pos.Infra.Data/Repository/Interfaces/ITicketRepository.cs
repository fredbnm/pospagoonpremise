﻿using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Core.Interfaces;

namespace Meep.Pos.Infra.Data.Repository.Interfaces
{
    public interface ITicketRepository : IRepository<Ticket>
    {
    }
}
