﻿using System;
using System.Collections.Generic;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Core.Interfaces;

namespace Meep.Pos.Infra.Data.Repository.Interfaces
{
    public interface IDiscountRepository : IRepository<Discount>
    {
    }
}
