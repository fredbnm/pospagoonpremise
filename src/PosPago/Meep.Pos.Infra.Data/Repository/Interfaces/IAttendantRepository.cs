﻿using System;
using System.Collections.Generic;
using System.Text;
//using Meep.Pos.Domain.Core.Repository;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Dapper.Repository.Interface;
using Meep.Pos.Infra.Data.Repository.Core.Interfaces;

namespace Meep.Pos.Infra.Data.Repository.Interfaces
{
    public interface IAttendantRepository : IRepository<Attendant>
    {
        
    }
}
