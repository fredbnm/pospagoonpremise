﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Core.Interfaces;

namespace Meep.Pos.Infra.Data.Repository.Interfaces
{
    public interface ICashierRepository : IRepository<Cashier>
    {
        Cashier GetCurrent();
        IEnumerable<Cashier> CashierReport(DateTime dateOpened);
    }
}
