﻿using Meep.Pos.Infra.Data.Repository.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain;
using Meep.Pos.Infra.Data.Core;
using System.Linq;

namespace Meep.Pos.Infra.Data.Repository
{
    public class ProductionRepository : CacheRepository<Production>, IProductionRepository
    {
        private IMemoryCache _memoryCache;
        private MemoryCacheEntryOptions _cacheEntryOptions;
        private List<Production> _productions;
        private string _key = "production";


        public ProductionRepository(IMemoryCache memoryCache) 
            : base (memoryCache, 
                  new MemoryCacheEntryOptions() { SlidingExpiration = TimeSpan.FromDays(1), AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1) }, 
                  "production")
        {
            _memoryCache = memoryCache;
            var cacheEntryOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromDays(1));
            _productions = new List<Production>();
        }

        //public ProductionRepository() : base()
        //{
        //}

        //public bool Delete(Guid productionId)
        //{
        //    if (_memoryCache.Get(_key) != null)
        //    {
        //        var productionList = _memoryCache.Get<List<Production>>(_key);
        //        var production = productionList.FirstOrDefault(p => p.Id == productionId);
        //        var removed = productionList.Remove(production);

        //        _memoryCache.Set(_key, productionList);
        //        return removed;
        //    }

        //    return false;
        //}

        //public List<Production> GetAll()
        //{
        //    var items = new List<Production>();
        //    if (_memoryCache.TryGetValue(_key, out items))
        //        return items;
        //    else
        //        return new List<Production>();
        //}

        //public Production Get(Guid productionId)
        //{
        //    return _memoryCache.Get<List<Production>>(_key).FirstOrDefault(p => p.Id == productionId);
        //}

        //public Production Insert(Production production)
        //{
        //    var productionList = _memoryCache.Get<List<Production>>(_key);

        //    if (productionList == null)
        //        productionList = new List<Production>();

        //    productionList.Add(production);

        //    _memoryCache.Set(_key, productionList);

        //    return production;
        //}

        //public Production Update(Production production)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
