﻿using Dapper;
using Flepper.QueryBuilder;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Dapper.Extensions;
using Meep.Pos.Infra.Data.Dapper.Repository;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Meep.Pos.Infra.Data.Repository
{
    public class ComissionRepository : Repository<Comission>, IComissionRepository
    {
        private ComissionRepository()
        {

        }
        public ComissionRepository(IDbConnection db) : base(db)
        {

        }

        public override Comission Update(Comission item)
        {
            var sql = FlepperQueryBuilder.Update(TableName)
                        .Set("Description", item.Description)
                        .Set("Value", item.Value)
                        .Where("Id")
                        .EqualTo(item.Id.ToString())
                        .BuildWithParameters();

            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return item;
        }
    }
}
