﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Infra.Data.Dapper.Repository;
using System.Data;
using Flepper.QueryBuilder;
using Dapper;
using Meep.Pos.Infra.Data.Dapper.Extensions;
//using Meep.Pos.Infra.Data.Mongo.Repository.Interface;

namespace Meep.Pos.Infra.Data.Repository
{
    public class AttendantRepository : PersonRepository<Attendant>, IAttendantRepository
    {
        protected AttendantRepository() : base()
        {

        }
        public AttendantRepository(IDbConnection db):base(db)
        {

        }

        protected override Attendant InsertSub(Attendant person)
        {
            var sql = FlepperQueryBuilder.Insert()
               .Into("Attendant")
               .Columns("Id, Name, Cpf, Sex, PinCode")
               .Values(person.Id, person.Name, person.Cpf, person.Sex, person.PinCode)
               .BuildWithParameters();


            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return person;
        }

        public override Attendant Update(Attendant item)
        {
            var sql = FlepperQueryBuilder.Update("Attendant")
              .Set("Name", item.Name)
              .Set("Cpf", item.Cpf)
              .Set("Sex", item.Sex)
              .Set("PinCode", item.PinCode)
              .Where("Id").EqualTo(item.Id.ToString())
              .BuildWithParameters();

            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return item;
        }
    }
}
