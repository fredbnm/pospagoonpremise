﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Infra.Data.Dapper.Repository;
using System.Data;
using Flepper.QueryBuilder;
using Meep.Pos.Infra.Data.Dapper.Extensions;
using Dapper;

namespace Meep.Pos.Infra.Data.Repository
{
    public class TaxRepository : Repository<Tax>, ITaxRepository
    {
        public TaxRepository(IDbConnection db) : base(db)
        {
        }

        public override Tax Update(Tax item)
        {
            var sql = FlepperQueryBuilder.Update("Tax")
                        .Set("Description", item.Description)
                        .Set("Value", item.Value)
                        .Set("CalcType", item.CalcType)
                        .Set("Required", item.Required)
                        .Where("Id").EqualTo(item.Id.ToString())
                        .BuildWithParameters();


            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return item;
        }
    }
}
