﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Infra.Data.Dapper.Repository;
using System.Data;
using System.Linq;
using Flepper.QueryBuilder;
using Dapper;
using Meep.Pos.Domain.Models.Interface;
using Meep.Pos.Infra.Data.Dapper.Extensions;
using Meep.Pos.Domain.Models.Interface;
using Meep.Pos.Domain.Core.Models;
using System.Collections;

namespace Meep.Pos.Infra.Data.Repository
{
    public class OrderRepository : Repository<Orders>, IOrderRepository
    {
        public OrderRepository(IDbConnection db) : base(db)
        {
        }

        const string ORDER_SQL = @"SELECT O.*, P.*, A.*, T.*
                                    FROM Orders O 
                                    LEFT JOIN Payment P ON P.OrderId = O.Id 
                                    INNER JOIN Attendant A ON A.Id = O.AttendantId 
                                    INNER JOIN Ticket T ON T.Id = O.TicketId ";

        const string ORDER_PRODUCTS_SQL = @"SELECT OI.*, OP.*, P.*, A.*
                                        FROM OrderItem OI 
                                        INNER JOIN Attendant A ON OI.AttendantId = A.Id
                                        INNER JOIN OrderProduct OP ON OP.Id = OI.Id
                                        INNER JOIN Product P ON P.Id = OP.ProductId ";

        const string ORDER_TAXES_SQL = @"SELECT OI.*, OD.*, D.* 
                                        FROM OrderItem OI 
                                        INNER JOIN OrderTax OD ON OD.Id = OI.Id
                                        INNER JOIN Tax D ON D.Id = OD.TaxId ";

        const string ORDER_DISCOUNT_SQL = @"SELECT OI.*, OD.*, D.* 
                                            FROM OrderItem OI                                         
                                            INNER JOIN OrderDiscount OD ON OD.Id = OI.Id
                                            INNER JOIN Discount D ON D.Id = OD.DiscountId ";

        const string PAYMENT_SQL = @"SELECT P.*, A.*
                                    FROM Orders O 
                                    INNER JOIN Payment P ON P.OrderId = O.Id
                                    INNER JOIN Attendant A ON P.AttendantId = A.Id ";

        public override Orders Get(Guid id)
        {
            var sqlOrder = ORDER_SQL 
                + "WHERE O.Id = @Id";


            Orders order = Db.Query<Orders, List<Payment>, Attendant, Ticket, Orders>(sqlOrder, (o, p, a, t) =>
             {
                 o.SetFieldValue("_payments", p);
                 o.SetFieldValue("_attendant", a);
                 o.SetFieldValue("_ticket", t);
                 return o;

             }, new
             {
                 Id = id.ToString()
             }).FirstOrDefault();

            if (order != null)
            {
                var sqlOrderProducts = ORDER_PRODUCTS_SQL 
                    + @" WHERE OI.OrderId = @Id";

                var sqlOrderDiscount = ORDER_DISCOUNT_SQL + @" WHERE OI.OrderId = @Id";

                var sqlOrderTax = ORDER_TAXES_SQL + @" WHERE OI.OrderId = @Id";

                var sqlPayments = PAYMENT_SQL + @" WHERE O.Id = @Id";


                var orderProducts = Db.Query<OrderProduct, Product, Attendant, OrderProduct>(sqlOrderProducts, (o, p, a) =>
                {
                    o.SetFieldValue("_product", p);
                    o.SetFieldValue("_attendant", a);
                    return o;
                }, new { Id = id });

                var orderDiscounts = Db.Query<OrderDiscount, Discount, OrderDiscount>(sqlOrderDiscount, (o, d) =>
                {
                    o.SetFieldValue("_discount", d);
                    return o;
                }, new { Id = id });

                var orderTaxes = Db.Query<OrderTax, Tax, OrderTax>(sqlOrderTax, (o, t) =>
                {
                    o.SetFieldValue("_tax", t);
                    return o;
                }, new { Id = id });

                var payments = Db.Query<Payment, Attendant, Payment>(sqlPayments, (p, a) =>
                {
                    p.SetFieldValue("_attendant",a);
                    return p;
                },new { Id = id });

                order.SetFieldValue("_payments", payments);

                List<IOrderItem> orderItems = new List<IOrderItem>();

                foreach (var item in orderProducts)
                    orderItems.Add(item);

                foreach (var item in orderDiscounts)
                    orderItems.Add(item);

                foreach (var item in orderTaxes)
                    orderItems.Add(item);

                if (orderItems.Count > 0)
                    order.SetFieldValue("_orderItems", orderItems);
            }

            return order;
        }

        public virtual IEnumerable<Orders> GetByCashierId(Guid cashierId)
        {
            var sqlOrder = ORDER_SQL + @" WHERE O.CashierId = @Id";


            var orders = Db.Query<Orders, List<Payment>, Attendant, Ticket, Orders>(sqlOrder, (o, p, a, t) =>
            {
                o.SetFieldValue("_payments", p);
                o.SetFieldValue("_attendant", a);
                o.SetFieldValue("_ticket", t);
                return o;

            }, new
            {
                Id = cashierId.ToString()
            });

            foreach (var order in orders)
            {
                var sqlOrderProducts = ORDER_PRODUCTS_SQL + @" WHERE OI.OrderId = @Id";

                var sqlOrderDiscount = ORDER_DISCOUNT_SQL + @" WHERE OI.OrderId = @Id";

                var sqlOrderTax = ORDER_TAXES_SQL + @" WHERE OI.OrderId = @Id";

                var sqlPayments = PAYMENT_SQL + @" WHERE O.Id = @Id";


                var orderProducts = Db.Query<OrderProduct, Product, Attendant, OrderProduct>(sqlOrderProducts, (o, p, a) =>
                {
                    o.SetFieldValue("_product", p);
                    o.SetFieldValue("_attendant", a);
                    return o;
                }, new { Id = order.Id });

                var orderDiscounts = Db.Query<OrderDiscount, Discount, OrderDiscount>(sqlOrderDiscount, (o, d) =>
                {
                    o.SetFieldValue("_discount", d);
                    return o;
                }, new { Id = order.Id });

                var orderTaxes = Db.Query<OrderTax, Tax, OrderTax>(sqlOrderTax, (o, t) =>
                {
                    o.SetFieldValue("_tax", t);
                    return o;
                }, new { Id = order.Id });

                var payments = Db.Query<Payment, Attendant, Payment>(sqlPayments, (p, a) =>
                {
                    p.SetFieldValue("_attendant", a);
                    return p;
                }, new { Id = order.Id });

                order.SetFieldValue("_payments", payments);

                List<IOrderItem> orderItems = new List<IOrderItem>();

                foreach (var item in orderProducts)
                    orderItems.Add(item);

                foreach (var item in orderDiscounts)
                    orderItems.Add(item);

                foreach (var item in orderTaxes)
                    orderItems.Add(item);

                if (orderItems.Count > 0)
                    order.SetFieldValue("_orderItems", orderItems);
            }

            return orders;
        }

        public virtual Payment SavePayment(Payment payment, Guid orderId)
        {
            var sql = FlepperQueryBuilder.Insert()
                    .Into(payment.GetType().Name)
                    .Columns("Id, Date, Value, PaymentState, PaymentType, AttendantId, OrderId")
                    .Values(payment.Id, payment.Date, payment.Value, payment.PaymentState, payment.PaymentType, payment.Attendant.Id, orderId)
                    .BuildWithParameters();

            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return payment;

        }

        public override Orders Update(Orders item)
        {
            var sql = FlepperQueryBuilder.Update(item.GetType().Name)
               .Set("DateClosed", item.DateClosed)
               .Set("Value", item.Value)
               .Set("IsOpen", item.IsOpen)
               .Set("CustomerCpf", item.CustomerCpf)
               .Where("Id")
               .EqualTo(item.Id.ToString())
               //.Set("CashierCloserId", item.clo.Id)
               .BuildWithParameters();



            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return item;
        }

        public override Orders Insert(Orders item)
        {
            var sql = FlepperQueryBuilder.Insert()
                   .Into(item.GetType().Name)
                   .Columns("Id, DateOpened, Value, TicketId, AttendantId")
                   .Values(item.Id, item.DateOpened, item.Value, item.Attendant.Id)
                   .BuildWithParameters();


            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return item;
        }

        public virtual Orders OpenOrder(Orders item, Guid cashierId)
        {
            var sql = FlepperQueryBuilder.Insert()
                   .Into(item.GetType().Name)
                   .Columns("Id, DateOpened, Value, TicketId, AttendantId, CashierId, IsOpen")
                   .Values(item.Id, item.DateOpened, item.Value, item.Ticket.Id, item.Attendant.Id, cashierId, item.IsOpen)
                   .BuildWithParameters();

            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return item;
        }

        public virtual Orders GetOrderOnCashierByTicket(Cashier cashier, string ticketNumber)
        {
            var sql = @"select O.*, T.*
                        from Orders O
                        INNER JOIN Ticket T ON O.TicketId = T.Id
                        WHERE T.Number = @number
                            AND O.CashierId = @cashierId
                            AND O.IsOpen = @isOpen
                        ORDER BY O.DateOpened DESC";

            //var orders = cashier.Orders.Select(o => string.Format($"'{o.Id}'"));

            var order = Db.Query<Orders, Ticket, Orders>(sql, (o, t) =>
            {
                o.SetFieldValue("_ticket", t);

                return o;
            },
            new {
                number = ticketNumber,
                cashierId = cashier.Id,
                isOpen = true
            }).FirstOrDefault();

            return order;
        }

        public virtual OrderTax RemoveTax(OrderTax item)
        {
            var sqlOrderTax = FlepperQueryBuilder.Delete()
                .From("OrderTax")
                .Where("Id")
                .EqualTo(item.Id.ToString())
                .BuildWithParameters();

            Db.Query(sqlOrderTax.Query.MySqlAdapter(), sqlOrderTax.Parameters);

            var sql = FlepperQueryBuilder.Delete()
                .From("OrderItem")
                .Where("Id")
                .EqualTo(item.Id.ToString())
                .BuildWithParameters();

            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return item;
        }

        public virtual OrderProduct GetOrderProduct(Guid id)
        {
            var sqlOrderProduct = FlepperQueryBuilder.Select()
                .From("OrderProduct")
                .Where("Id")
                .EqualTo(id.ToString())
                .BuildWithParameters();

            var orderProduct = Db.Query<OrderProduct>(sqlOrderProduct.Query.MySqlAdapter(), sqlOrderProduct.Parameters);

            return orderProduct.FirstOrDefault();
        }

        public OrderProduct UpdateOrderProduct(OrderProduct item)
        {
            if (item.DateFinalized.HasValue)
            {
                var sql = FlepperQueryBuilder.Update(item.GetType().Name)
                     .Set("DateFinalized", item.DateFinalized.Value)
                     .Where("Id")
                     .EqualTo(item.Id.ToString())
                     .BuildWithParameters();

                Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

                return item;
            }
            else
                return null;
        }


        #region [ Insert ]

        public virtual Orders SaveItems(Orders item)
        {
            Db.Open();
            using (var trans = Db.BeginTransaction())
            {
                try
                {
                    foreach (var orderProduct in item.OrderProducts)
                    {
                        InsertOrderItem(orderProduct, item.Id);

                        InsertOrderProduct(orderProduct);
                    }

                    foreach (var orderDiscount in item.OrderDiscounts)
                    {
                        InsertOrderItem(orderDiscount, item.Id);

                        InsertOrderDiscount(orderDiscount);
                    }

                    foreach (var orderTax in item.OrderTaxes)
                    {
                        InsertOrderItem(orderTax, item.Id);

                        InsertOrderTax(orderTax);
                    }


                    trans.Commit();
                }
                catch
                {
                    trans.Rollback();
                    throw;
                }
            }
            Db.Close();

            return item;
        }
                
        private void InsertOrderItem<TOrderItem>(TOrderItem orderItem, Guid orderId) where TOrderItem : BaseModel, IOrderItem
        {
            var sql = FlepperQueryBuilder.Insert().Into("OrderItem")
                            .Columns("Id", "DateOrder", "Count", "Value", "Total", "OrderId", "AttendantId")
                            .Values(orderItem.Id, orderItem.DateOrder, orderItem.Count, orderItem.Value, orderItem.Total, orderId, orderItem.Attendant.Id)
                            .BuildWithParameters();

            this.Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);
        }

        private void InsertOrderProduct(OrderProduct orderProduct)
        {
            QueryResult sql;

            if (orderProduct.Comission != null)
            {
                sql = FlepperQueryBuilder.Insert().Into("OrderProduct")
                                .Columns("Id", "Description", "ProductId", "ComissionId")
                                .Values(orderProduct.Id,
                                    string.IsNullOrWhiteSpace(orderProduct.Description) ? "" : orderProduct.Description,
                                    orderProduct.Product.Id,
                                    orderProduct.Comission.Id)
                                .BuildWithParameters();
            }
            else
            {
                sql = FlepperQueryBuilder.Insert().Into("OrderProduct")
                                .Columns("Id", "Description", "ProductId")
                                .Values(orderProduct.Id,
                                    string.IsNullOrWhiteSpace(orderProduct.Description) ? "" : orderProduct.Description,
                                    orderProduct.Product.Id)
                                .BuildWithParameters();
            }

            this.Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);
        }

        private void InsertOrderTax(OrderTax orderTax)
        {
            var sql = FlepperQueryBuilder.Insert().Into("OrderTax")
                            .Columns("Id", "TaxId")
                            .Values(orderTax.Id, orderTax.Tax.Id)
                            .BuildWithParameters();

            this.Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);
        }

        private void InsertOrderDiscount(OrderDiscount orderDiscount)
        {
            var sql = FlepperQueryBuilder.Insert().Into("OrderDiscount")
                            .Columns("Id", "DiscountId")
                            .Values(orderDiscount.Id, orderDiscount.Discount.Id)
                            .BuildWithParameters();

            this.Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);
        }

        #endregion
    }
}
