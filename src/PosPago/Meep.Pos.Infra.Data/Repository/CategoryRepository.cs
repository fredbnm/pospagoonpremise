﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Infra.Data.Dapper.Repository;
using System.Data;
using Flepper.QueryBuilder;
using Meep.Pos.Infra.Data.Dapper.Extensions;
using Dapper;

namespace Meep.Pos.Infra.Data.Repository
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(IDbConnection db) : base(db)
        {
        }

        public override Category Update(Category item)
        {
            var sql = FlepperQueryBuilder.Update(this.TableName)
                .Set("Name", item.Name)
                .Where("Id")
                .EqualTo(item.Id.ToString())
                .BuildWithParameters();

            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return item;
        }
    }
}
