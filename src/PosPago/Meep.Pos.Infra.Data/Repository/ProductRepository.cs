﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Infra.Data.Dapper.Repository;
using System.Data;
using Dapper;
using Flepper.QueryBuilder;
using Meep.Pos.Infra.Data.Dapper.Extensions;

namespace Meep.Pos.Infra.Data.Repository
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(IDbConnection db) : base(db)
        {
        }

        public override IEnumerable<Product> GetAll()
        {
            var sql = FlepperQueryBuilder.Select().From(TableName).As("P")
                          .InnerJoin("Category").As("C").On("P", "CategoryId").EqualTo("C", "Id")
                          .Build();

            var products = Db.Query<Product, Category, Product>(sql.MySqlAdapter(), 
                (product, category) => 
                {
                    product.Category = category;
                    return product;
            });

            return products;
        }

        public override Product Get(Guid id)
        {
            var sql = FlepperQueryBuilder.Select().From(TableName).As("P")
                          .InnerJoin("Category").As("C").On("P", "CategoryId").EqualTo("C", "Id")
                          .Where("P.Id").EqualTo(id.ToString())
                          .BuildWithParameters();

            var product = Db.Query<Product, Category, Product>(sql.Query.MySqlAdapter()
                ,(prod, category) =>
                {
                    prod.Category = category;
                    return prod;
                }
                ,sql.Parameters).FirstOrDefault();

            return product;
        }

        public override Product Update(Product item)
        {
            var sql = FlepperQueryBuilder.Update(TableName)
                        .Set("Name", item.Name)
                        .Set("Price", item.Price)
                        .Set("CategoryId", item.Category.Id)
                        .Set("Unit", item.Unit)
                        .Set("IsActive", item.IsActive)
                        .Set("IsProduction", item.IsProduction)
                        .Set("ProductionTime", item.ProductionTime)
                        .Where("Id")
                        .EqualTo(item.Id.ToString())
                        .BuildWithParameters();

            Db.Query(sql.Query.MySqlAdapter(), sql.Parameters);

            return item;
        }
    }
}
