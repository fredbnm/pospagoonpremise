﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Meep.Pos.Infra.Data
{
    internal static class ReflectionExtension
    {
        public static void SetValue<T>(this T obj, string propertyName, object value)
        {
            Type tObject = typeof(T);

            PropertyInfo pInfo = tObject.GetProperty(propertyName);
             
            pInfo?.SetValue(obj, value);
        }
        public static void SetFieldValue<T>(this T obj, string fieldName, object value)
        {
            Type tObject = typeof(T);

            string firstChar = fieldName.Substring(0, 1);

            FieldInfo pInfo = tObject.GetField(fieldName, BindingFlags.NonPublic |
                                                          BindingFlags.Instance);

            pInfo?.SetValue(obj, value);
        }
    }
}

