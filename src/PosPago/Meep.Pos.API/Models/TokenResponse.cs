﻿using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Service.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Models
{
    public class TokenResponse
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public UserValueObject user { get; set; }
        public Guid attendantId { get; set; }
        public string attendantName { get; set; }
        public string locationId { get; set; }
        public MeepTypeEnum meepType { get; set; }
    }
}
