﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Models.Order
{
    /// <summary>
    /// Define o informações sobre o status do pagamento.
    /// </summary>
    public class PaymentStatusModel
    {
        /// <summary>
        /// Status atual do pagamento.
        /// </summary>
        public StatusEnum Status { get; set; }

        /// <summary>
        /// Mensagem de erro ou sucesso.
        /// </summary>
        public string Message { get; set; }
    }

    /// <summary>
    /// Status do pagamento.
    /// </summary>
    public enum StatusEnum
    {
        /// <summary>
        /// Aguardando inserir o cartão
        /// </summary>
        InsertCard = 1,
        /// <summary>
        /// Aguardando a senha ser digitada.
        /// </summary>
        WaitingPassword = 2,
        /// <summary>
        /// Pagamento efetuado com sucesso.
        /// </summary>
        Success = 3,
        /// <summary>
        /// Pagamento efetuado com erro.
        /// </summary>
        Failure = 4,
        /// <summary>
        /// Transação cancelada.
        /// </summary>
        Canceled = 5
    }
}
