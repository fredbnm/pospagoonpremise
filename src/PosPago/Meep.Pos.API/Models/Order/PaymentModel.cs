﻿using Meep.Pos.Domain.Core.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Models
{
    /// <summary>
    /// Model com informações sobre o pagamento.
    /// </summary>
    public class PaymentModel
    {
        /// <summary>
        /// Valor pago.
        /// </summary>
        public decimal Value { get; set; }
        /// <summary>
        /// Estado do pagamento (Final ou parcial).
        /// </summary>
        public PaymentStateEnum PaymentState { get; set; }
        /// <summary>
        /// Tipo de pagamento (débito, dinheiro, crédito).
        /// </summary>
        public PaymentTypeEnum PaymentType { get; set; }
        /// <summary>
        /// Id do atendente responsável pelo recebimento do pagamento.
        /// </summary>
        public Guid AttendantId { get; set; }
    }
}
