﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Models
{
    /// <summary>
    /// Model de item a ser adicionado.
    /// </summary>
    public class OrderItemModel
    {
        /// <summary>
        /// Id do item. Pode ser um produto, uma taxa ou um desconto.
        /// </summary>
        public Guid ItemId { get; set; }

        /// <summary>
        /// Quantidade do item a ser adicionado.
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// Descrição do item adicionado. Não é obrigatório e deve ser utilizado apenas para produtos.
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// Atendente que fez o pedido.
        /// </summary>
        public Guid AttendantId { get; set; }
        /// <summary>
        /// Id da comissão
        /// </summary>
        public Guid? ComissionId { get; set; }
    }
}
