﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Models.Order
{
    /// <summary>
    /// Model para realizar a abertura de order para executar pedidos.
    /// </summary>
    public class OpenExpressOrderModel
    {
            /// <summary>
            /// Número do atendente da order.
            /// </summary>
            public Guid AttendantId { get; set; }
    }
}
