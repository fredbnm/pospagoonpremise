﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Models
{
    /// <summary>
    /// Model para realizar a abertura de order para executar pedidos.
    /// </summary>
    public class OpenOrderModel
    {
        /// <summary>
        /// Número do atendente da order.
        /// </summary>
        public Guid AttendantId { get; set; }
        /// <summary>
        /// Número do Ticket (mesa, comanda) a ser aberto.
        /// </summary>
        public string TicketNumber { get; set; }
    }
}
