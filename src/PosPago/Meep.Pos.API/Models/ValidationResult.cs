﻿using Meep.Pos.Domain.Core.Localization;
using Meep.Pos.Domain.Core.Localization.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Models
{
    public class ValidationResult<T>
    {
        private readonly bool _hasErrors;
        private readonly string _message;
        private readonly string _code;
        private readonly T _value;
        private readonly Exception _exception;
        private readonly object[] _args;

        public ValidationResult(T value)
        {
            _hasErrors = false;
            _value = value;
        }

        public ValidationResult(T value, IResourceMessage resourceMessage)
        {
            _hasErrors = false;
            _exception = null;
            _message = resourceMessage.Message;
            _value = value;
            _code = resourceMessage.Code;
        }

        public ValidationResult(Exception exception, IResourceMessage resourceMessage)
        {
            _hasErrors = true;
            _exception = exception;
            _message = resourceMessage.Message;
            _code = resourceMessage.Code;
            _value = default(T);
        }


        public bool HasErrors
        {
            get { return _hasErrors; }
        }

        //public string ErrorMessage
        //{
        //    get { return _errorMessage; }
        //}

        public string Code
        {
            get { return _code; }
        }

        public Exception Exception
        {
            get { return _exception; }
        }

        public string Message
        {
            get { return _message; }
        }

        public T Value
        {
            get { return _value; }
        }
    }
}
