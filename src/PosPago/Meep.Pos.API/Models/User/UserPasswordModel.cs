﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Models.User
{
    public class UserPasswordModel
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
