﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Models.User
{
    public class UserEmailModel
    {
        public string Password { get; set; }
        public string OldEmail { get; set; }
        public string NewEmail { get; set; }
    }
}
