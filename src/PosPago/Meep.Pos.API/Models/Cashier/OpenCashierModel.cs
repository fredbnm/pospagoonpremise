﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Models
{
    /// <summary>
    /// Modelo de abertura de caixa.
    /// </summary>
    public class OpenCashierModel
    {
        /// <summary>
        /// Id do atendente que irá realizar a abertura do caixa.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Valor de abertura do caixa.
        /// </summary>
        public decimal InitialValue { get; set; }
    }
}
