﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Models
{
    /// <summary>
    /// Model para fechamento de caixa.
    /// </summary>
    public class CloseCashierModel
    {
        /// <summary>
        /// Id do atendente responsável pelo fechamento do caixa.
        /// </summary>
        public Guid UserId { get; set; }
    }
}
