﻿using Meep.Pos.Domain.Core.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Models.Device
{
    public class LoginDeviceModel
    {
        public int PinCode { get; set; }
        public string BrowserFingerprint { get; set; }
        public string Resolution { get; set; }
        public string UserAgent { get; set; }
        public MeepTypeEnum orderType { get; set; }
    }
}
