﻿using Meep.Pos.API.Gateway.Model;
using Meep.Pos.Service.ValueObjects;
using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Gateway.Interface
{
    //[Headers("Authorization: Bearer bF9D1AM6x-9_v6SN8skAMXxey901sS0YeaWT5NCUcMmKt1osOVPFnQ5ruGsYyAwbhEMZVIWhCwb_QAm-ukOsYR5iNHiFdG180bdE1oN54uF3b3dmYyhHk1GdVtjQBMvl6bTxfm9ymUVY4ubTzVD4xmGi4JT8VBWtVyOa0WAx20Wucsr9TOUR6FIuV72FQ07rC7vipKykUaBJAxdPpNR4q4RVimnSzrNIV5MJOXuPI3b6eA4416c1fjukrg4kCeXmQz48ULB1RSjMXZHYjpLnZI7jz_IStZasGRUdx02CTqZMYJX-SePtF1kYXIzDtrFkgMNwTmdF3mG4kwZ8f-zfM63QMgZeElwSE_lh1oe3GSqEZusjnyf6GhqRI162SQBbxNsydBJYFDd0ej0nHk_VdtJ9VknoD5t-Vow1nqL54E-eOEvuWhZ1S0o5zax1dPdHbsN3jd45AAWL0IpeDTN2uC0l3Rs9L3bY8bYVFagN6-PwttE8dJ6jtRl1c1JbLL3p0P2SicxTPOPfWouABZf8csh74_TZJ8qq0V00nth4xyUsZX-a1wcxe-uONuKLIs2u")]
    public interface IMeepApi
    {
        [Post("/api/UsuarioMeep/BuscarUsuarioMeepPorDocumento")]
        [Headers("Content-Type: application/json")]
        Task <UsuarioMeepModel> GetUsuarioMeep([Body]string model, [Header("Authorization")]string token);

        [Post("/api/UsuarioMeep/AtualizarDadosUsuarioMeep")]
        [Headers("Content-Type: application/json")]
        Task<UsuarioMeepModel> UpdateUsuarioMeep([Body]string model, [Header("Authorization")]string token);

        [Post("/api/UsuarioMeep/AdicionarUsuarioMeep")]
        [Headers("Content-Type: application/json")]
        Task<UsuarioMeepModel> InsertUsuarioMeep([Body]string model, [Header("Authorization")]string token);

        [Get("/api/Cidade/BuscarCidadesPorEstadoUF/{estadoUF}")]
        Task<IEnumerable<CidadesModel>> GetCidades(string estadoUF, [Header("Authorization")]string token);

        [Post("/token")]
        [Headers("Content-Type : application/x-www-form-urlencoded")]
        Task<MeepTokenModel> Login([Body]string model);

        [Get("/api/SistemaGestao/ObterEndpoints")]
        [Headers("Content-Type: application/json")]
        Task<IEnumerable<MeepEndpointModel>> SyncEndpoints(Guid localId, int tipoSistema, [Header("Authorization")]string token);


        //[Post("/token")]
        //[Headers("Content-Type : application/x-www-form-urlencoded")]
        //Task<MeepTokenModel> Login([Body]string username, [Body]string password, [Body]string grant_type);


    }
}
