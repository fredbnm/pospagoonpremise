﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Gateway.Model.Exception
{
    public class MeepApiExceptionModel
    {
        public MeepApiExceptionModel()
        {
            Errors = new List<Error>();
        }
        public string Message { get; set; }
        public List<Error> Errors { get; set; }
    }

    public class Error
    {
        public string Key { get; set; }
        public string[] Messages { get; set; }
    }
}
