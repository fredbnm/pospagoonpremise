﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Gateway.Model
{
    public class MeepEndpointModel
    {
        public Guid Id { get; set; }

        public int TipoEvento { get; set; }

        public Guid SistemaGestaoId { get; set; }

        public string Endpoint { get; set; }

        public bool ContinueOnError { get; set; }

        public int TipoFluxo { get; set; }
    }
}
