﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Gateway.Model
{
    public class LocalModel
    {
        public Guid LocalId { get; set; }
        public string CNPJ { get; set; }
        public string NomeLocal { get; set; }
        public string Endereco { get; set; }
        public string Cep { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string Pais { get; set; }
    }
}
