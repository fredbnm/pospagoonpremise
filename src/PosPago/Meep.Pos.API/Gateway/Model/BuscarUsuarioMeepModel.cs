﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Gateway.Model
{
    public class BuscarUsuarioMeepModel
    {
        public Guid? LocalClienteId { get; set; }
        public string CPF { get; set; }
    }
}
