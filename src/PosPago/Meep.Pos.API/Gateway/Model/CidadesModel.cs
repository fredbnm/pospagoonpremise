﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Gateway.Model
{
    public class CidadesModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
    }
}
