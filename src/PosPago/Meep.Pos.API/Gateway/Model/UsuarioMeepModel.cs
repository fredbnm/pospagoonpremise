﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Gateway.Model
{
    public class UsuarioMeepModel
    {
        public string CPF { get; set; }
        public Guid? LocalClienteId { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string DDD { get; set; }
        public string Telefone { get; set; }
        public string RG { get; set; }
        public EnderecoMeepModel Endereco { get; set; }
    }

    public class EnderecoMeepModel
    {
        public string CEP { get; set; }
        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public Guid? CidadeId { get; set; }
        public string CidadeNome { get; set; }
        public Guid? EstadoId { get; set; }
        public string estadoUF { get; set; }
    }
}
