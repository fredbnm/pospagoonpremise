﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Gateway.Model
{
    public class MeepTokenModel
    {        
        public string access_token { get; set; }
        public string token_type { get; set; }
        public long expires_in { get; set; }
        public string roles { get; set; }
        public string userName { get; set; }
        public string issued { get; set; }
        public string expires { get; set; }
        public string usuarioMeep { get; set; }
        public string configuracaoUsuarioMeep { get; set; }
        public string locais { get; set; }
        
    }
}
