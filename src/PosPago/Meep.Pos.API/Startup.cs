﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using Meep.Pos.Infra.CrossCutting.IoC;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using Meep.Pos.API.Filters;
using Microsoft.Extensions.Localization;
using Folke.Localization.Json;
using Meep.Pos.API.Configurations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Meep.Pos.API.Providers;
using Meep.Pos.Service.Interfaces;
//using Meep.Pos.Infra.CrossCutting.IoC;

namespace Meep.Pos.API
{
    public partial class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            _signingKey =
               new SymmetricSecurityKey(
                   Encoding.UTF8.GetBytes(Configuration.GetSection("TokenAuthentication:SecretKey").Value));

            _tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey,
                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = true,
                ValidIssuer = Configuration.GetSection("TokenAuthentication:Issuer").Value,
                // Validate the JWT Audience (aud) claim
                ValidateAudience = true,
                ValidAudience = Configuration.GetSection("TokenAuthentication:Audience").Value,
                // Validate the token expiry
                ValidateLifetime = true,
                // If you want to allow a certain amount of clock drift, set that here:
                ClockSkew = TimeSpan.Zero
            };

            _tokenProviderOptions = new TokenProviderOptions
            {
                Path = Configuration.GetSection("TokenAuthentication:TokenPath").Value,
                Audience = Configuration.GetSection("TokenAuthentication:Audience").Value,
                Issuer = Configuration.GetSection("TokenAuthentication:Issuer").Value,
                SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256),
                IdentityResolver = GetIdentity
            };
        }


        private IUserService _userService;
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Setting culture to dates
            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture("pt-BR");
                //options.SupportedCultures = new List<CultureInfo> { new CultureInfo("en-US"), new CultureInfo("en-NZ") };
            });

            //Automapper
            services.AddAutoMapper(typeof(Startup));

            RegisterServices(services);

            services.AddCors();

            services.AddMvc(config =>
            {
                config.Filters.Add(typeof(CustomExceptionFilter));
            });

            //Resources
            services.AddJsonLocalization(options =>
            {
                options.ResourceFilesDirectory = "Resources";
                //options.DefaultBaseName = "text";
            });
            

            //GZIP
            services.Configure<GzipCompressionProviderOptions>(options => options.Level = System.IO.Compression.CompressionLevel.Optimal);
            services.AddResponseCompression();

            //Cache
            services.AddMemoryCache();

            //Swagger
            services.AddSwaggerGen(
               options =>
               {
                   options.SwaggerDoc("v1", new Info
                   {
                       Version = "v1",
                       Title = "Meep Pos",
                       Description = "Meep Pos Pago Swagger Interface",
                       Contact = new Contact { Name = "Bruno Gouvêa Roldão", Email = "brunoroldao@meep-app.com.br", Url = "http://www.meep-app.com" },
                   });

                   options.IncludeXmlComments(XmlCommentsFilePath);
               });
                        
            services.AddLogging();

            services.AddWebApi(options =>
            {
                options.OutputFormatters.Remove(new XmlDataContractSerializerOutputFormatter());
            });

            var sp = services.BuildServiceProvider();
            _userService = sp.GetService<IUserService>();

            _tokenProviderOptions.UserService = _userService;
            ConfigureAuth(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddFile($"Logs/Meep.Pos.txt");
            loggerFactory.AddDebug();            

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
               builder.AllowAnyHeader()
                       .AllowAnyMethod()
                       .AllowAnyOrigin()
               );

            app.UseStaticFiles();

            app.UseMvcWithDefaultRoute();

            app.UseRequestLocalization();

            app.UseAuthentication();

            app.UseMiddleware<TokenProviderMiddleware>(Options.Create(_tokenProviderOptions));
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "DefaultApi",
                    template: "pos/api/v1/{controller}/{action}/{id?}"
                    );
                routes.MapRoute(
                    name: "Api",
                    template: "pos/api/v1/{controller}/{id?}"
                    );
            });

            app.UseStatusCodePages(async context =>
            {
                context.HttpContext.Response.ContentType = "text/plain";

                await context.HttpContext.Response.WriteAsync("Status code page, status code: " + context.HttpContext.Response.StatusCode);
            });

            app.UseSwagger();
            app.UseSwaggerUI(options => options.SwaggerEndpoint($"/swagger/v1/swagger.json", "Meep Pos API V1"));

            
        }

        static string XmlCommentsFilePath
        {
            get
            {
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var fileName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name + ".xml";
                return Path.Combine(basePath, fileName);
            }
        }



        public void RegisterServices(IServiceCollection services)
        {

            NativeInjectorBootstrapper.RegisterServices(services, Configuration);
        }
    }
}
