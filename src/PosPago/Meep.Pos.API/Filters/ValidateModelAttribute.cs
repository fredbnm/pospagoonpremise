﻿using Meep.Pos.Domain.Core.Exception.Custom;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meep.Pos.API.Filters
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            var modelState = context.ModelState;

            if (!modelState.IsValid)
            {
                var children = modelState.Root.Children.ToList();

                List<string> errors = new List<string>();

                foreach (var child in children)
                {
                    foreach (var error in child.Errors)
                    {
                        errors.Add(error.Exception.Message);
                    }                    
                }

                var final = string.Join("\n ", errors);

                throw new CustomModelNotValidException(final);
            }

        }
    }
}