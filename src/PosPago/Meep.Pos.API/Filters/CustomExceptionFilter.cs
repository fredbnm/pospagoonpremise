﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;
using System.Net.Http;
using Meep.Pos.API.Models;
using Meep.Pos.Domain.Core.Exception;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Meep.Pos.Domain.Core.Localization;
using Meep.Pos.Domain.Core.Localization.Interface;
using Microsoft.AspNetCore.Mvc;
using static Meep.Pos.Domain.Core.Constants.DomainConstants.ErrorCode.Custom;
using Microsoft.Extensions.Logging;
using Meep.Pos.API.Gateway.Model.Exception;
using System.Text;
using System.Dynamic;

namespace Meep.Pos.API.Filters
{
    public class CustomExceptionFilter : IExceptionFilter
    {
        private IResourceMessage _resource;
        private ILogger<CustomExceptionFilter> _logger;

        public CustomExceptionFilter(IResourceMessage resource,
            ILogger<CustomExceptionFilter> logger)
        {
            _resource = resource;
            _logger = logger;
        }

        public void OnException(ExceptionContext context)
        {
            HttpStatusCode status = HttpStatusCode.InternalServerError;
            ValidationResult<string> result = new ValidationResult<string>(context.Exception,
                                        _resource.Instance(Null_Reference.CODE,
                                            Null_Reference.MESSAGE,
                                            context.Exception.Message));

            var contextException = context.Exception;
            var baseException = contextException.GetBaseException();

            if (contextException.GetType().IsSubclassOf(typeof(DomainException)))
            {
                var exception = ((DomainException)context.Exception);
                status = exception.StatusCode.HasValue ? (HttpStatusCode)exception.StatusCode.Value : HttpStatusCode.InternalServerError;
                result = new ValidationResult<string>(context.Exception, _resource.Instance(exception.ErrorCode, exception.Message));
            }

            if (contextException.GetType() == typeof(UnauthorizedAccessException))
            {
                status = HttpStatusCode.Unauthorized;
                result = new ValidationResult<string>(context.Exception, _resource.Instance(Unauthorized_Acess.CODE, Unauthorized_Acess.MESSAGE));
            }

            if (contextException.GetType() == typeof(NullReferenceException))
            {
                status = HttpStatusCode.InternalServerError;
                result = new ValidationResult<string>(context.Exception, _resource.Instance(Null_Reference.CODE, Null_Reference.MESSAGE));
            }

            if (contextException.GetType() == typeof(MySql.Data.MySqlClient.MySqlException))
            {
                status = HttpStatusCode.InternalServerError;
                result = new ValidationResult<string>(context.Exception, _resource.Instance(Database.CODE, Database.MESSAGE));
            }

            if (contextException.GetType() == typeof(Refit.ApiException))
            {
                var exception = ((Refit.ApiException)contextException);
                status = HttpStatusCode.InternalServerError;
                try
                {
                    var error = JsonConvert.DeserializeObject<MeepApiExceptionModel>(exception.Content);

                    if (string.IsNullOrWhiteSpace(error.Message))
                    {
                        var genericError = JsonConvert.DeserializeObject<dynamic>(exception.Content);

                        if (PropertyExists(genericError, "error_description"))
                        {
                            error.Errors.Add(new Error
                            {
                                Key = "Autenticação",
                                Messages = new string[] { genericError.error_description }
                            });
                        }
                    }

                    StringBuilder message = new StringBuilder(error.Message);

                    if (error.Errors != null)
                        foreach (var err in error.Errors)
                        {
                            message.AppendLine(string.Join(" ", err.Messages.Select(a => string.Format(" {0}", a))));
                        }

                    result = new ValidationResult<string>(new Exception(message.ToString()), _resource.Instance("", message.ToString()));
                }
                catch
                {

                }
            }

            HttpResponse response = context.HttpContext.Response;

            response.StatusCode = (int)status;
            response.ContentType = "application/json";
            context.Result = new JsonResult(result);

            //base.OnException(context);
        }

        public static bool PropertyExists(dynamic settings, string name)
        {
            if (settings is ExpandoObject)
                return ((IDictionary<string, object>)settings).ContainsKey(name);

            return settings.GetType().GetProperty(name) != null;
        }
    }
}
