﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Meep.Pos.API.Controllers.Base;
using Meep.Pos.API.Gateway.Interface;
using Microsoft.Extensions.Configuration;
using Meep.Pos.API.Filters;
using Meep.Pos.API.Gateway.Model;
using Meep.Pos.API.Models;
using Newtonsoft.Json;
using Meep.Pos.API.Providers;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.Domain.Core.Enumerators.Configuration;

namespace Meep.Pos.API.Controllers
{
    /// <summary>
    /// Controller que serve como uma ponte para acesso ao Meep Api.
    /// </summary>
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class GatewayController : BaseController
    {
        private IMeepApi _meepApi;
        private IConfiguration _configuration;
        private ILocationService _locationService;
        private IUserService _userService;
        private IConfigurationService _configurationService;
        private string _token;

        public GatewayController(IConfiguration configuration,
                ILocationService locationService,
                IUserService userService,
                IConfigurationService configurationService)
        {
            _meepApi = Refit.RestService.For<IMeepApi>(configuration.GetSection("MeepApi:BaseUrl").Value);
            _configuration = configuration;
            _locationService = locationService;
            _userService = userService;
            _configurationService = configurationService;
            _token = configuration.GetSection("MeepApi:TokenProd").Value;
        }

        /// <summary>
        /// Retorna todas as cidades cadastradas por estado.
        /// </summary>
        /// <returns>Lista de cidades</returns>
        [ValidateModel]
        [HttpGet("city/{uf}")]
        public async Task<ValidationResult<List<CidadesModel>>> GetCities(string uf)
        {
            var cities = await _meepApi.GetCidades(uf,_token);

            var result = new ValidationResult<List<CidadesModel>>(cities.ToList());

            return result;
        }

        [ValidateModel]
        [AllowAnonymous]
        [HttpPost("meep/server/login")]
        public async Task<ValidationResult<TokenResponse>> Login([FromBody]LoginModel login)
        {
            var loginModel = $"username={login.username}&password={login.password}&grant_type=password";
            var meepLogin = await _meepApi.Login(loginModel);

            var local = JsonConvert.DeserializeObject<List<LocalModel>>(meepLogin.locais).FirstOrDefault();

            var location = _locationService.CreateOrGet(new Service.ValueObjects.LocationValueObject()
            {
                Address = local.Endereco,
                City = local.Cidade,
                Cnpj = local.CNPJ,
                Country = local.Pais,
                Name = local.NomeLocal,
                State = local.Estado,
                ZipCode = local.Cep,
                MeepLocationId = local.LocalId
            });

            var token = GenerateToken(login);

            var tokenResponse = new TokenResponse()
            {
                access_token = token.Item1,
                expires_in = token.Item2,
                locationId = location.Id.ToString()
            };

            var result = new ValidationResult<TokenResponse>(tokenResponse);

            return result;
        }

        [ValidateModel]
        [AllowAnonymous]
        [HttpGet("portal/configured")]
        public async Task<ValidationResult<bool>> IsConfigured()
        {
            var location = _locationService.GetAll().Any();
            var user = _userService.GetAll().Any();

            if (location && user)
                return new ValidationResult<bool>(true);
            else
                return new ValidationResult<bool>(false);            
        }

        [ValidateModel]
        [HttpGet("endpoint/sync")]
        public async Task<ValidationResult<ConfigurationValueObject>> SyncEndpoints()
        {
            var location = _locationService.GetAll().FirstOrDefault();

            var endpoints = await _meepApi.SyncEndpoints(location.MeepLocationId.Value, 2, _token);

            ConfigurationValueObject configuration;

            if (endpoints.Any())
            {
                configuration = _configurationService.SyncEndpoints(endpoints.Select(ep => new ConfigurationEndpointValueObject()
                {
                    Endpoint = ep.Endpoint,
                    ContinueOnError = ep.ContinueOnError,
                    EndpointType = Domain.Core.Enumerators.Configuration.EndpointTypeEnum.NF,
                    Flow = (FlowTypeEnum)ep.TipoFluxo,
                    Pipeline = (PipelineEnum)ep.TipoEvento
                }).ToList());

                configuration.IsSynchronized = true;
            }
            else
                configuration = null;


            var result = new ValidationResult<ConfigurationValueObject>(configuration);

            return result;            

        }

        #region [ Util ]

        private Tuple<string,int> GenerateToken(LoginModel login)
        {
            var options = new TokenProviderOptions();

            options.Path = _configuration.GetSection("TokenAuthentication:TokenPath").Value;
            options.Audience = _configuration.GetSection("TokenAuthentication:Audience").Value;
            options.Issuer = _configuration.GetSection("TokenAuthentication:Issuer").Value;
            var signingKey = new SymmetricSecurityKey(
                   Encoding.UTF8.GetBytes(_configuration.GetSection("TokenAuthentication:SecretKey").Value));

            options.SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);

            options.Expiration = TimeSpan.FromHours(1);

            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, login.username),
                new Claim(JwtRegisteredClaimNames.Sub, login.password),
                //new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(now).ToUniversalTime().ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64)
            };

            return new Tuple<string,int>(Util.SecurityToken.GetEncoded(claims, options), (int)options.Expiration.TotalSeconds);
        }

        #endregion
    }
}