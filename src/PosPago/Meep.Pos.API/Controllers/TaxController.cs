﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.API.Models;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.API.Controllers.Base;
using static Meep.Pos.Domain.Core.Constants.DomainConstants.SuccessCode.Tax;
using Meep.Pos.Domain.Core.Localization;
using Meep.Pos.Domain.Core.Localization.Interface;
using Meep.Pos.API.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Meep.Pos.API.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TaxController : BaseController
    {
        private ITaxService _taxService;
        private IResourceMessage _resourceMessage;
        public TaxController(ITaxService taxService, IResourceMessage resourceMessage)
        {
            _taxService = taxService;
            _resourceMessage = resourceMessage;
        }

        /// <summary>
        /// Retorna todas as taxas cadastradas.
        /// </summary>
        /// <returns>Lista de taxas</returns>
        [ValidateModel]
        [HttpGet]
        public ValidationResult<List<TaxValueObject>> List()
        {
            var taxes = _taxService.GetAll();

            var result = new ValidationResult<List<TaxValueObject>>(taxes.ToList());

            return result;
        }

        /// <summary>
        /// Retorna uma taxa pelo id.
        /// </summary>
        /// <param name="id">Id da taxa.</param>
        /// <returns>Taxa selecionada.</returns>
        [ValidateModel]
        [HttpGet("{id}")]
        public ValidationResult<TaxValueObject> Get(Guid id)
        {
            var tax = _taxService.Get(id);

            var result = new ValidationResult<TaxValueObject>(tax);

            return result;
        }

        /// <summary>
        /// Cria uma nova taxa.
        /// </summary>
        /// <param name="value">Taxa a ser criada.</param>
        /// <returns>Taxa criada.</returns>
        [ValidateModel]
        [HttpPost]
        public ValidationResult<TaxValueObject> Post([FromBody]TaxValueObject value)
        {
            var tax = _taxService.Create(value);

            var result = new ValidationResult<TaxValueObject>(tax, _resourceMessage.Instance(Inserted.CODE, Inserted.MESSAGE));

            return result;
        }

        /// <summary>
        /// Atualizar uma taxa.
        /// </summary>
        /// <param name="value">Taxa a ser atualizada.</param>
        /// <returns>Taxa atualizada.</returns>
        [ValidateModel]
        [HttpPut]
        public ValidationResult<TaxValueObject> Put([FromBody]TaxValueObject value)
        {
            var tax = _taxService.Update(value);

            var result = new ValidationResult<TaxValueObject>(tax, _resourceMessage.Instance(Updated.CODE, Updated.MESSAGE));

            return result;
        }

        /// <summary>
        /// Deletar uma taxa.
        /// </summary>
        /// <param name="id">Id da taxa a ser deletada.</param>
        /// <returns>Mensagem de confirmação.</returns>
        [ValidateModel]
        [HttpDelete]
        public ValidationResult<TaxValueObject> Delete(Guid id)
        {
            _taxService.Remove(id);

            var result = new ValidationResult<TaxValueObject>(new TaxValueObject(), _resourceMessage.Instance(Removed.CODE, Removed.MESSAGE));

            return result;
        }
    }
}