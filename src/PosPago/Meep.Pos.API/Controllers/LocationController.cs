﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.API.Models;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.API.Controllers.Base;
using static Meep.Pos.Domain.Core.Constants.DomainConstants.SuccessCode.Location;
using Meep.Pos.Domain.Core.Localization;
using Meep.Pos.Domain.Core.Localization.Interface;
using Meep.Pos.API.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Meep.Pos.API.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class LocationController : BaseController
    {
        private ILocationService _locationService;
        private IResourceMessage _resourceMessage;
        public LocationController(ILocationService locationService, IResourceMessage resourceMessage)
        {
            _locationService = locationService;
            _resourceMessage = resourceMessage;
        }

        /// <summary>
        /// Retorna todos os locais cadastrados.
        /// </summary>
        /// <returns>Lista de locais</returns>
        [ValidateModel]
        [HttpGet]
        public ValidationResult<List<LocationValueObject>> List()
        {
            var locations = _locationService.GetAll();

            var result = new ValidationResult<List<LocationValueObject>>(locations.ToList());

            return result;
        }

        /// <summary>
        /// Retorna um local pelo id.
        /// </summary>
        /// <param name="id">Id do local.</param>
        /// <returns>Local selecionado.</returns>
        [ValidateModel]
        [HttpGet("{id}")]
        public ValidationResult<LocationValueObject> Get(Guid id)
        {
            var location = _locationService.Get(id);

            var result = new ValidationResult<LocationValueObject>(location);

            return result;
        }

        /// <summary>
        /// Cria um novo local.
        /// </summary>
        /// <param name="value">Local a ser criado.</param>
        /// <returns>Local criado.</returns>
        [ValidateModel]
        [HttpPost]
        public ValidationResult<LocationValueObject> Post([FromBody]LocationValueObject value)
        {
            var location = _locationService.Create(value);

            var result = new ValidationResult<LocationValueObject>(location, _resourceMessage.Instance(Inserted.CODE, Inserted.MESSAGE));

            return result;
        }

        /// <summary>
        /// Atualizar um local.
        /// </summary>
        /// <param name="value">Local a ser atualizado.</param>
        /// <returns>Local atualizado.</returns>
        [ValidateModel]
        [HttpPut]
        public ValidationResult<LocationValueObject> Put([FromBody]LocationValueObject value)
        {
            var location = _locationService.Update(value);

            var result = new ValidationResult<LocationValueObject>(location, _resourceMessage.Instance(Updated.CODE, Updated.MESSAGE));

            return result;
        }

        /// <summary>
        /// Deletar um local.
        /// </summary>
        /// <param name="id">Id do local a ser deletado.</param>
        /// <returns>Mensagem de confirmação.</returns>
        [ValidateModel]
        [HttpDelete]
        public ValidationResult<LocationValueObject> Delete(Guid id)
        {
            _locationService.Remove(id);

            var result = new ValidationResult<LocationValueObject>(new LocationValueObject(), _resourceMessage.Instance(Removed.CODE, Removed.MESSAGE));

            return result;
        }
    }
}