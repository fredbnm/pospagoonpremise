﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Meep.Pos.API.Controllers.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.API.Filters;
using Meep.Pos.API.Models;
using Meep.Pos.Service.ValueObjects;

namespace Meep.Pos.API.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ConfigurationController : BaseController
    {
        IConfigurationService _configurationService;

        public ConfigurationController(IConfigurationService configurationService)
        {
            _configurationService = configurationService;
        }

        [ValidateModel]
        [HttpGet]
        public ValidationResult<ConfigurationValueObject> Get()
        {
            var configuration = _configurationService.GetOrCreate();

            var result = new ValidationResult<ConfigurationValueObject>(configuration);

            return result;
        }

        [ValidateModel]
        [HttpPut]
        public ValidationResult<ConfigurationValueObject> Update([FromBody]ConfigurationValueObject config)
        {
            var configuration = _configurationService.Update(config);

            var result = new ValidationResult<ConfigurationValueObject>(configuration);

            return result;
        }
    }
}