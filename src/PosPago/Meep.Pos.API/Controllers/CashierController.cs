﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Meep.Pos.API.Controllers.Base;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.API.Models;
using Meep.Pos.Service.ValueObjects;
using static Meep.Pos.Domain.Core.Constants.DomainConstants.SuccessCode.Cashier;
using Meep.Pos.Domain.Core.Localization;
using Meep.Pos.Domain.Core.Localization.Interface;
using Meep.Pos.API.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Meep.Pos.API.Models.Order;

namespace Meep.Pos.API.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CashierController : BaseController
    {
        private ICashierService _cashierService;
        private IResourceMessage _resourceMessage;

        public CashierController(ICashierService cashierService, IResourceMessage resourceMessage)
        {
            _cashierService = cashierService;
            _resourceMessage = resourceMessage;
        }

        /// <summary>
        /// Retorna todos os caixas abertos.
        /// </summary>
        /// <returns>Lista de caixas no campo Value.</returns>
        [ValidateModel]
        [HttpGet]
        public ValidationResult<List<CashierValueObject>> List()
        {
            var cashiers = _cashierService.GetAll();

            var result = new ValidationResult<List<CashierValueObject>>(cashiers.ToList());

            return result;
        }


        /// <summary>
        /// Retorna todos os caixas abertos.
        /// </summary>
        /// <returns>Lista de caixas no campo Value.</returns>
        [ValidateModel]
        [HttpGet("report")]
        public ValidationResult<CashierValueObject> FilteredList([FromQuery]CashierValueObject filters)
        {
            var cashier = _cashierService.GetByDate(filters);

            var result = new ValidationResult<CashierValueObject>(cashier);

            return result;
        }

        /// <summary>
        /// Retorna um caixa através de seu id.
        /// </summary>
        /// <param name="id">Id do caixa a ser retornada</param>
        /// <returns>Caixa no campo Value.</returns>
        [ValidateModel]
        [HttpGet("{id}")]
        public ValidationResult<CashierValueObject> Get(Guid id)
        {
            var cashier = _cashierService.Get(id);

            var result = new ValidationResult<CashierValueObject>(cashier);

            return result;
        }

        /// <summary>
        /// Retorna o caixa aberto no momento.
        /// </summary>
        /// <returns>Caixa no campo Value.</returns>
        [ValidateModel]
        [HttpGet("current")]
        public ValidationResult<CashierValueObject> GetCurrent()
        {
            var cashier = _cashierService.GetCurrent();

            var result = new ValidationResult<CashierValueObject>(cashier);

            return result;
        }

        /// <summary>
        /// Realiza a abertura de caixa.
        /// </summary>
        /// <param name="value">Valores necessários para abertura do caixa.</param>
        /// <returns>Caixa aberto com seus respectivos valores.</returns>
        [ValidateModel]
        [HttpPost("open")]
        public ValidationResult<CashierValueObject> OpenCashier([FromBody]OpenCashierModel value)
        {
            var cashier = _cashierService.OpenCashier(value.UserId, value.InitialValue);

            var result = new ValidationResult<CashierValueObject>(cashier, _resourceMessage.Instance(Open.CODE, Open.MESSAGE));

            return result;
        }

        /// <summary>
        /// Atualizar o caixa realizando uma abertura de order.
        /// </summary>
        /// <param name="id">Id do caixa para abertura da order.</param>
        /// <param name="value">Campos necessários para abertura de order.</param>
        /// <returns>Caixa com a order adicionada.</returns>
        //[ValidateModel]
        //[HttpPut("{id}/order")]
        //public ValidationResult<CashierValueObject> OpenOrder(Guid id, [FromBody]OpenOrderModel value)
        //{
        //    var cashier = _cashierService.OpenOrder(id, value.AttendantId, value.TicketId);

        //    var result = new ValidationResult<CashierValueObject>(cashier, _resourceMessage.Instance(Open_Order.CODE, Open_Order.MESSAGE));

        //    return result;
        //}

        /// <summary>
        /// Atualizar o caixa realizando uma abertura de order verificando o caixa aberto.
        /// </summary>
        /// <param name="value">Campos necessários para abertura de order.</param>
        /// <returns>Caixa com a order adicionada.</returns>
        [ValidateModel]
        [HttpPut("current/order")]
        public ValidationResult<OrderValueObject> OpenOrder([FromBody]OpenOrderModel value)
        {
            var order = _cashierService.OpenOrder(value.AttendantId, value.TicketNumber);

            var result = new ValidationResult<OrderValueObject>(order, _resourceMessage.Instance(Open_Order.CODE, Open_Order.MESSAGE));

            return result;
        }

        /// <summary>
        /// Cria um novo pedido expresso (com código aleatório)
        /// </summary>
        /// <param name="value">Campos necessários para abertura de order.</param>
        /// <returns>Caixa com a order adicionada.</returns>
        [ValidateModel]
        [HttpPost("current/order/express")]
        public ValidationResult<OrderValueObject> ExpressOrder([FromBody]OpenExpressOrderModel value)
        {
            var order = _cashierService.ExpressOrder(value.AttendantId);

            var result = new ValidationResult<OrderValueObject>(order, _resourceMessage.Instance(Open_Order.CODE, Open_Order.MESSAGE));

            return result;
        }

        /// <summary>
        /// Retorna o caixa corrente com todas as orders abertas até então.
        /// </summary>
        /// <param name="value">Campos necessários para abertura de order.</param>
        /// <returns>Caixa com a order adicionada.</returns>
        [ValidateModel]
        [HttpGet("current/order")]
        public ValidationResult<CashierValueObject> GetCashierAndOrders()
        {
            var order = _cashierService.GetCurrentCashierWithOrders();

            var result = new ValidationResult<CashierValueObject>(order, _resourceMessage.Instance(Open_Order.CODE, Open_Order.MESSAGE));

            return result;
        }



        ///// <summary>
        ///// Fechar um order após efetuar todos os pagamentos.
        ///// </summary>
        ///// <param name="id">Id do caixa.</param>
        ///// <param name="orderId">Id da order.</param>
        ///// <returns>Caixa com as informações atualizadas.</returns>
        //[ValidateModel]
        //[HttpPut("{id}/order/{orderId}/close")]
        //public ValidationResult<CashierValueObject> CloseOrder(Guid id, Guid orderId)
        //{
        //    var cashier = _cashierService.CloseOrder(id, orderId);

        //    var result = new ValidationResult<CashierValueObject>(cashier, _resourceMessage.Instance(Close_Order.CODE, Close_Order.MESSAGE));

        //    return result;
        //}

        /// <summary>
        /// Fechar um order após efetuar todos os pagamentos selecionando a última order aberta.
        /// </summary>
        /// <param name="orderId">Id da order.</param>
        /// <returns>Caixa com as informações atualizadas.</returns>
        [ValidateModel]
        [HttpPut("current/order/{orderId}/close")]
        public ValidationResult<CashierValueObject> CloseOrder(Guid orderId)
        {
            var cashier = _cashierService.CloseOrder(orderId);

            var result = new ValidationResult<CashierValueObject>(cashier, _resourceMessage.Instance(Close_Order.CODE, Close_Order.MESSAGE));

            return result;
        }


        ///// <summary>
        ///// Fechar o caixa após o fim do expediente.
        ///// </summary>
        ///// <param name="id">Id do caixa.</param>
        ///// <param name="value">Campos necessários para efetuar o fechamento do caixa.</param>
        ///// <returns>Caixa atualizado.</returns>
        //[ValidateModel]
        //[HttpPut("{id}/close")]
        //public ValidationResult<CashierValueObject> CloseCashier(Guid id, [FromBody]CloseCashierModel value)
        //{
        //    var category = _cashierService.CloseCashier(id, value.UserId);

        //    var result = new ValidationResult<CashierValueObject>(category, _resourceMessage.Instance(Close.CODE, Close.MESSAGE));

        //    return result;
        //}

        /// <summary>
        /// Fechar o caixa corrente após o fim do expediente.
        /// </summary>
        /// <param name="value">Campos necessários para efetuar o fechamento do caixa.</param>
        /// <returns>Caixa atualizado.</returns>
        [ValidateModel]
        [HttpPut("current/close")]
        public ValidationResult<CashierValueObject> CloseCashier([FromBody]CloseCashierModel value)
        {
            var cashier = _cashierService.CloseCashier(value.UserId);

            var result = new ValidationResult<CashierValueObject>(cashier, _resourceMessage.Instance(Close.CODE, Close.MESSAGE));

            return result;
        }
    }
}