﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Meep.Pos.API.Controllers.Base;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.API.Models;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.API.Filters;
using Meep.Pos.Domain.Core.Localization.Interface;
using static Meep.Pos.Domain.Core.Constants.DomainConstants.SuccessCode.Category;

namespace Meep.Pos.API.Controllers
{
    public class ProductionController : BaseController
    {
        private IProductionService _productionService;
        private IResourceMessage _resourceMessage;
        public ProductionController(IProductionService productionService, IResourceMessage resourceMessage)
        {
            _productionService = productionService;
            _resourceMessage = resourceMessage;
        }

        [ValidateModel]
        [HttpGet]
        public ValidationResult<List<ProductionValueObject>> Get()
        {
            var result = new ValidationResult<List<ProductionValueObject>>(_productionService.GetAll());

            return result;
        }

        [ValidateModel]
        [HttpDelete]
        public ValidationResult<ProductionValueObject> Delete(Guid id)
        {
            _productionService.Delete(id);

            var result = new ValidationResult<ProductionValueObject>(new ProductionValueObject(), _resourceMessage.Instance(Removed.CODE, Removed.MESSAGE));

            return result;
        }
    }
}