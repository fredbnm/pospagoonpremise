﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.API.Models;
using Microsoft.Extensions.Localization;
using Folke.Localization.Json;
using Meep.Pos.Domain.Core.Localization;
using Meep.Pos.API.Controllers.Base;
using static Meep.Pos.Domain.Core.Constants.DomainConstants.SuccessCode.Product;
using Meep.Pos.Domain.Core.Localization.Interface;
using Meep.Pos.API.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Meep.Pos.API.Controllers
{
    /// <summary>
    /// CRUD de Produtos.
    /// </summary>
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ProductController : BaseController
    {
        private IProductService _productService;
        private IResourceMessage _resourceMessage;

        public ProductController(IProductService productService, IResourceMessage resourceMessage)
        {
            _productService = productService;
            _resourceMessage = resourceMessage;
        }

        // GET: api/Product
        /// <summary>
        /// Retorna todos os produtos cadastrados.
        /// </summary>
        /// <returns>Lista de produtos</returns>
        [ValidateModel]
        [AllowAnonymous]
        [HttpGet]
        public ValidationResult<List<ProductValueObject>> List()
        {
            var products = _productService.GetAll();

            var result = new ValidationResult<List<ProductValueObject>>(products.ToList());

            return result;
        }

        // GET: api/Product
        /// <summary>
        /// Retorna todos os produtos cadastrados.
        /// </summary>
        /// <returns>Lista de produtos</returns>
        [ValidateModel]
        [HttpGet("active")]
        public ValidationResult<List<ProductValueObject>> ListActives()
        {
            var products = _productService.GetAll().Where(product => product.IsActive);

            var result = new ValidationResult<List<ProductValueObject>>(products.ToList());

            return result;
        }

        /// <summary>
        /// Retorna um produto pelo id.
        /// </summary>
        /// <param name="id">id do produto</param>
        /// <returns></returns>
        [ValidateModel]
        [AllowAnonymous]
        [HttpGet("{id}")]
        public ValidationResult<ProductValueObject> Get(Guid id)
        {
            var product = _productService.Get(id);

            var result = new ValidationResult<ProductValueObject>(product);

            return result;
        }

        /// <summary>
        /// Cria um novo produto.
        /// </summary>
        /// <param name="value">Produto a ser criado.</param>
        /// <returns>Produto criado.</returns>
        [ValidateModel]
        [AllowAnonymous]
        [HttpPost]
        public ValidationResult<ProductValueObject> Post([FromBody]ProductValueObject value)
        {
            var product = _productService.Create(value);

            var result = new ValidationResult<ProductValueObject>(product, _resourceMessage.Instance(Inserted.CODE, Inserted.MESSAGE));

            return result;
        }

        /// <summary>
        /// Atualizar um produto.
        /// </summary>
        /// <param name="value">Produto a ser atualizado.</param>
        /// <returns>Produto atualizado.</returns>
        [ValidateModel]
        [HttpPut]
        public ValidationResult<ProductValueObject> Put([FromBody]ProductValueObject value)
        {
            var product = _productService.Update(value);

            var result = new ValidationResult<ProductValueObject>(product, _resourceMessage.Instance(Updated.CODE, Updated.MESSAGE));

            return result;
        }

        /// <summary>
        /// Deletar um produto
        /// </summary>
        /// <param name="id">Id do produto a ser deletado.</param>
        /// <returns>Mensagem de confirmação.</returns>
        [ValidateModel]
        [HttpDelete]
        public ValidationResult<ProductValueObject> Delete(Guid id)
        {
            _productService.Remove(id);

            var result = new ValidationResult<ProductValueObject>(new ProductValueObject(), _resourceMessage.Instance(Removed.CODE, Removed.MESSAGE));

            return result;
        }
    }
}
