﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.API.Models;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.API.Controllers.Base;
using static Meep.Pos.Domain.Core.Constants.DomainConstants.SuccessCode.Ticket;
using Meep.Pos.Domain.Core.Localization;
using Meep.Pos.Domain.Core.Localization.Interface;
using Meep.Pos.API.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Meep.Pos.API.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TicketController : BaseController
    {
        private ITicketService _ticketService;
        private IResourceMessage _resourceMessage;
        public TicketController(ITicketService ticketService, IResourceMessage resourceMessage)
        {
            _ticketService = ticketService;
            _resourceMessage = resourceMessage;
        }

        /// <summary>
        /// Retorna todos os tickets cadastrados.
        /// </summary>
        /// <returns>Lista de produtos</returns>
        [ValidateModel]
        [HttpGet]
        public ValidationResult<List<TicketValueObject>> List()
        {
            var tickets = _ticketService.GetAll();

            var result = new ValidationResult<List<TicketValueObject>>(tickets.ToList());

            return result;
        }

        /// <summary>
        /// Retorna um ticket pelo id.
        /// </summary>
        /// <param name="id">id do ticket</param>
        /// <returns></returns>
        [ValidateModel]
        [HttpGet("{id}")]
        public ValidationResult<TicketValueObject> Get(Guid id)
        {
            var ticket = _ticketService.Get(id);

            var result = new ValidationResult<TicketValueObject>(ticket);

            return result;
        }

        /// <summary>
        /// Retorna um ticket pelo seu número.
        /// </summary>
        /// <param name="number">Número do ticket.</param>
        /// <returns>Ticket solicitado.</returns>
        [ValidateModel]
        [HttpGet("{number}")]
        public ValidationResult<TicketValueObject> GetByNumber(string number)
        {
            var ticket = _ticketService.GetByNumber(number);

            var result = new ValidationResult<TicketValueObject>(ticket);

            return result;
        }

        /// <summary>
        /// Cria um novo ticket.
        /// </summary>
        /// <param name="value">Ticket a ser criado.</param>
        /// <returns>Produto criado.</returns>
        [ValidateModel]
        [HttpPost]
        public ValidationResult<TicketValueObject> Post([FromBody]TicketValueObject value)
        {
            var ticket = _ticketService.Create(value);

            var result = new ValidationResult<TicketValueObject>(ticket, _resourceMessage.Instance(Inserted.CODE, Inserted.MESSAGE));

            return result;
        }

        /// <summary>
        /// Cria um novo ticket.
        /// </summary>
        /// <param name="values">Ticket a ser criado.</param>
        /// <returns>Produto criado.</returns>
        [ValidateModel]
        [HttpPost("list")]
        public ValidationResult<List<TicketValueObject>> PostList([FromBody]List<TicketValueObject> values)
        {
            foreach (var value in values)
            {
                var ticket = _ticketService.Create(value);
            }

            var result = new ValidationResult<List<TicketValueObject>>(values, _resourceMessage.Instance(Inserted.CODE, Inserted.MESSAGE));

            return result;
        }

        /// <summary>
        /// Atualizar um ticket.
        /// </summary>
        /// <param name="value">Ticket a ser atualizado.</param>
        /// <returns>Ticket atualizado.</returns>
        [ValidateModel]
        [HttpPut]
        public ValidationResult<TicketValueObject> Put([FromBody]TicketValueObject value)
        {
            var ticket = _ticketService.Update(value);

            var result = new ValidationResult<TicketValueObject>(ticket, _resourceMessage.Instance(Updated.CODE, Updated.MESSAGE));

            return result;
        }

        /// <summary>
        /// Deletar um ticket.
        /// </summary>
        /// <param name="id">Id do ticket a ser deletado.</param>
        /// <returns>Mensagem de confirmação.</returns>
        [ValidateModel]
        [HttpDelete]
        public ValidationResult<TicketValueObject> Delete(Guid id)
        {
            _ticketService.Remove(id);

            var result = new ValidationResult<TicketValueObject>(new TicketValueObject(), _resourceMessage.Instance(Removed.CODE, Removed.MESSAGE));

            return result;
        }

    }
}