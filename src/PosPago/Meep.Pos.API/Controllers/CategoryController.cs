﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Meep.Pos.API.Models;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.API.Controllers.Base;
using static Meep.Pos.Domain.Core.Constants.DomainConstants.SuccessCode.Category;
using Meep.Pos.Domain.Core.Localization;
using Meep.Pos.Domain.Core.Localization.Interface;
using Meep.Pos.API.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Meep.Pos.API.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CategoryController : BaseController
    {
        private ICategoryService _categoryService;
        private IResourceMessage _resourceMessage;

        public CategoryController(ICategoryService categoryService, IResourceMessage resourceMessage)
        {
            _categoryService = categoryService;
            _resourceMessage = resourceMessage;
        }

        /// <summary>
        /// Retorna todas as categorias cadastradas.
        /// </summary>
        /// <returns>Lista de categorias no campo Value.</returns>
        [ValidateModel]
        [HttpGet]
        public ValidationResult<List<CategoryValueObject>> List()
        {
            var categories = _categoryService.GetAll();

            var result = new ValidationResult<List<CategoryValueObject>>(categories.ToList());

            return result;
        }

        /// <summary>
        /// Retorna uma categoria através de seu id.
        /// </summary>
        /// <param name="id">Id da categoria a ser retornada</param>
        /// <returns>Categoria no campo Value.</returns>
        [ValidateModel]
        [HttpGet("{id}")]
        public ValidationResult<CategoryValueObject> Get(Guid id)
        {
            var category = _categoryService.Get(id);

            var result = new ValidationResult<CategoryValueObject>(category);

            return result;
        }

        /// <summary>
        /// Criar uma nova categoria na base.
        /// </summary>
        /// <param name="value">Categoria a ser criada.</param>
        /// <returns>Categoria criada.</returns>
        [ValidateModel]
        [HttpPost]
        public ValidationResult<CategoryValueObject> Post([FromBody]CategoryValueObject value)
        {
            var category = _categoryService.Create(value);

            var result = new ValidationResult<CategoryValueObject>(category, _resourceMessage.Instance(Inserted.CODE, Inserted.MESSAGE));

            return result;
        }

        /// <summary>
        /// Atualizar uma categoria existente.
        /// </summary>
        /// <param name="value">Categoria a ser atualizada.</param>
        /// <returns>Categoria atualizada.</returns>
        [ValidateModel]
        [HttpPut]
        public ValidationResult<CategoryValueObject> Put([FromBody]CategoryValueObject value)
        {
            var category = _categoryService.Update(value);

            var result = new ValidationResult<CategoryValueObject>(category, _resourceMessage.Instance(Updated.CODE, Updated.MESSAGE));

            return result;
        }

        /// <summary>
        /// Deletar uma categoria.
        /// </summary>
        /// <param name="id">Id da categoria a ser deletada.</param>
        /// <returns>Mensagem de sucesso.</returns>
        [ValidateModel]
        [HttpDelete]
        public ValidationResult<CategoryValueObject> Delete(Guid id)
        {
            _categoryService.Remove(id);

            var result = new ValidationResult<CategoryValueObject>(new CategoryValueObject(), _resourceMessage.Instance(Removed.CODE, Removed.MESSAGE));

            return result;
        }
    }
}
