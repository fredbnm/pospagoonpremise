﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Meep.Pos.API.Controllers.Base;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.API.Models;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.API.Models.User;
using static Meep.Pos.Domain.Core.Constants.DomainConstants.SuccessCode.User;
using Meep.Pos.Domain.Core.Localization;
using Meep.Pos.Domain.Core.Localization.Interface;
using Meep.Pos.API.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Meep.Pos.API.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserController : BaseController
    {
        private IUserService _userService;
        private IResourceMessage _resourceMessage;

        public UserController(IUserService userService, IResourceMessage resourceMessage)
        {
            _userService = userService;
            _resourceMessage = resourceMessage;
        }

        /// <summary>
        /// Retorna todos os usuários cadastrados.
        /// </summary>
        /// <returns>Lista de usuários</returns>
        [ValidateModel]
        [HttpGet]
        public ValidationResult<List<UserValueObject>> List()
        {
            var users = _userService.GetAll();

            var result = new ValidationResult<List<UserValueObject>>(users.ToList());

            return result;
        }

        /// <summary>
        /// Retorna um usuário pelo id.
        /// </summary>
        /// <param name="id">Id do usuário.</param>
        /// <returns>Usuário solicitado.</returns>
        [ValidateModel]
        [HttpGet("{id}")]
        public ValidationResult<UserValueObject> Get(Guid id)
        {
            var user = _userService.Get(id);

            var result = new ValidationResult<UserValueObject>(user);

            return result;
        }

        /// <summary>
        /// Cria um novo usuário.
        /// </summary>
        /// <param name="value">Usuário a ser criado.</param>
        /// <returns>Usuário criado.</returns>
        [ValidateModel]
        [HttpPost]
        public ValidationResult<UserValueObject> Post([FromBody]UserModel value)
        {
            var user = _userService
                .Create(new UserValueObject()
                {
                    Cpf = value.Cpf,
                    Email = value.Email,
                    Login = value.Login,
                    Name = value.Name,
                    Sex = value.Sex,
                    Password = value.Password
                });

            var result = new ValidationResult<UserValueObject>(user, _resourceMessage.Instance(Inserted.CODE, Inserted.MESSAGE));

            return result;
        }

        /// <summary>
        /// Atualizar um usuário.
        /// </summary>
        /// <param name="value">Usuário a ser atualizado.</param>
        /// <returns>Usuário atualizado.</returns>
        [ValidateModel]
        [HttpPut]
        public ValidationResult<UserValueObject> Put([FromBody]UserValueObject value)
        {
            var user = _userService.Update(value);

            var result = new ValidationResult<UserValueObject>(user, _resourceMessage.Instance(Updated.CODE, Updated.MESSAGE));

            return result;
        }

        /// <summary>
        /// Deletar um usuário.
        /// </summary>
        /// <param name="id">Id do usuário a ser deletado.</param>
        /// <returns>Mensagem de confirmação.</returns>
        // DELETE: api/ApiWithActions/5
        [ValidateModel]
        [HttpDelete]
        public ValidationResult<UserValueObject> Delete(Guid id)
        {
            _userService.Remove(id);

            var result = new ValidationResult<UserValueObject>(new UserValueObject(), _resourceMessage.Instance(Removed.CODE, Removed.MESSAGE));

            return result;
        }

        /// <summary>
        /// Alterar a senha do usuário através do login.
        /// </summary>
        /// <param name="login">Login do usuário.</param>
        /// <param name="userPasswordModel">Informações para a alteração da senha.</param>
        /// <returns>Usuário solicitado.</returns>
        [ValidateModel]
        [HttpPut("{login}/password")]
        public ValidationResult<UserValueObject> ChangePassword(string login, [FromBody]UserPasswordModel userPasswordModel)
        {
            var user = _userService.ChangePassword(login, userPasswordModel.OldPassword, userPasswordModel.NewPassword);

            var result = new ValidationResult<UserValueObject>(user, _resourceMessage.Instance(Password_Changed.CODE, Password_Changed.MESSAGE));

            return result;
        }

        /// <summary>
        /// Alterar o email do usuário através do login.
        /// </summary>
        /// <param name="login">Login do usuário.</param>
        /// <param name="userEmailModel">Informações para a alteração do email.</param>
        /// <returns>Usuário solicitado.</returns>
        [ValidateModel]
        [HttpPut("{login}/email")]
        public ValidationResult<UserValueObject> ChangeEmail(string login, [FromBody]UserEmailModel userEmailModel)
        {
            var user = _userService.ChangeEmail(login, userEmailModel.Password, userEmailModel.OldEmail, userEmailModel.NewEmail);

            var result = new ValidationResult<UserValueObject>(user, _resourceMessage.Instance(Email_Changed.CODE, Email_Changed.MESSAGE));

            return result;
        }
    }
}