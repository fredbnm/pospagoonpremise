﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Meep.Pos.API.Controllers.Base;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Domain.Core.Localization.Interface;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.API.Models;
using Meep.Pos.API.Filters;

namespace Meep.Pos.API.Controllers
{
    public class EventController : BaseController
    {
        private IEventService _eventService;
        private IResourceMessage _resourceMessage;

        public EventController(IEventService eventService, IResourceMessage resourceMessage)
        {
            _eventService = eventService;
            _resourceMessage = resourceMessage;
        }

        [ValidateModel]
        [HttpGet]
        public ValidationResult<List<EventValueObject>> Get()
        {
            var result = new ValidationResult<List<EventValueObject>>(_eventService.GetAll());

            return result;
        }

    }
}