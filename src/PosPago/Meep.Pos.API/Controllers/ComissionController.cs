﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Meep.Pos.API.Controllers.Base;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Domain.Core.Localization.Interface;
using Meep.Pos.API.Models;
using Meep.Pos.API.Filters;
using Meep.Pos.Service.ValueObjects;
using static Meep.Pos.Domain.Core.Constants.DomainConstants.SuccessCode.Comission;

namespace Meep.Pos.API.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ComissionController : BaseController
    {
        private IComissionService _comissionService;
        private IResourceMessage _resourceMessage;

        public ComissionController(IComissionService categoryService, 
            IResourceMessage resourceMessage)
        {
            _comissionService = categoryService;
            _resourceMessage = resourceMessage;
        }

        /// <summary>
        /// Retorna todas as categorias cadastradas.
        /// </summary>
        /// <returns>Lista de categorias no campo Value.</returns>
        [ValidateModel]
        [HttpGet]
        public ValidationResult<List<ComissionValueObject>> List()
        {
            var categories = _comissionService.GetAll();

            var result = new ValidationResult<List<ComissionValueObject>>(categories.ToList());

            return result;
        }

        /// <summary>
        /// Retorna uma categoria através de seu id.
        /// </summary>
        /// <param name="id">Id da categoria a ser retornada</param>
        /// <returns>Categoria no campo Value.</returns>
        [ValidateModel]
        [HttpGet("{id}")]
        public ValidationResult<ComissionValueObject> Get(Guid id)
        {
            var category = _comissionService.Get(id);

            var result = new ValidationResult<ComissionValueObject>(category);

            return result;
        }

        /// <summary>
        /// Criar uma nova categoria na base.
        /// </summary>
        /// <param name="value">Categoria a ser criada.</param>
        /// <returns>Categoria criada.</returns>
        [ValidateModel]
        [HttpPost]
        public ValidationResult<ComissionValueObject> Post([FromBody]ComissionValueObject value)
        {
            var category = _comissionService.Insert(value);

            var result = new ValidationResult<ComissionValueObject>(category, _resourceMessage.Instance(Inserted.CODE, Inserted.MESSAGE));

            return result;
        }

        /// <summary>
        /// Atualizar uma categoria existente.
        /// </summary>
        /// <param name="value">Categoria a ser atualizada.</param>
        /// <returns>Categoria atualizada.</returns>
        [ValidateModel]
        [HttpPut]
        public ValidationResult<ComissionValueObject> Put([FromBody]ComissionValueObject value)
        {
            var category = _comissionService.Update(value);

            var result = new ValidationResult<ComissionValueObject>(category, _resourceMessage.Instance(Updated.CODE, Updated.MESSAGE));

            return result;
        }

        /// <summary>
        /// Deletar uma categoria.
        /// </summary>
        /// <param name="id">Id da categoria a ser deletada.</param>
        /// <returns>Mensagem de sucesso.</returns>
        [ValidateModel]
        [HttpDelete]
        public ValidationResult<ComissionValueObject> Delete(Guid id)
        {
            _comissionService.Delete(id);

            var result = new ValidationResult<ComissionValueObject>(new ComissionValueObject(), _resourceMessage.Instance(Removed.CODE, Removed.MESSAGE));

            return result;
        }
    }
}