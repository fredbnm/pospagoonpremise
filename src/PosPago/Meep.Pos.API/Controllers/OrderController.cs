﻿using Meep.Pos.API.Controllers.Base;
using Meep.Pos.API.Filters;
using Meep.Pos.API.Gateway.Interface;
using Meep.Pos.API.Gateway.Model;
using Meep.Pos.API.Models;
using Meep.Pos.API.Models.Order;
using Meep.Pos.Domain.Core.Enumerators.Configuration;
using Meep.Pos.Domain.Core.Localization;
using Meep.Pos.Domain.Core.Localization.Interface;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.ValueObjects;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Meep.Pos.Domain.Core.Constants.DomainConstants.SuccessCode.Order;

namespace Meep.Pos.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Microsoft.AspNetCore.Authorization.Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class OrderController : BaseController
    {
        private IOrderService _orderService;
        private IResourceMessage _resourceMessage;
        private IMemoryCache _cache;
        private ILocationService _locationService;
        private IConfigurationService _configurationService;
        private IMeepApi _meepApi;
        private IConfiguration _configuration;
        private string _tokenMeepServer;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderService"></param>
        /// <param name="resourceMessage"></param>
        /// <param name="cache"></param>
        public OrderController(IOrderService orderService, ILocationService locationService, IConfigurationService configurationService, 
            IResourceMessage resourceMessage, IMemoryCache cache, IConfiguration configuration)
        {
            _orderService = orderService;
            _locationService = locationService;
            _resourceMessage = resourceMessage;
            _cache = cache;
            _meepApi = RestService.For<IMeepApi>(configuration.GetSection("MeepApi:BaseUrl").Value,
                        new RefitSettings
                        {
                            JsonSerializerSettings = new JsonSerializerSettings
                            {
                                ContractResolver = new CamelCasePropertyNamesContractResolver()
                            }
                        });

            _configurationService = configurationService;
            _tokenMeepServer = configuration.GetSection("MeepApi:TokenProd").Value;
            
        }

        /// <summary>
        /// Retorna todos os orders adicionados.
        /// </summary>
        /// <returns>Lista de orders</returns>
        [ValidateModel]
        [HttpGet]
        public ValidationResult<List<OrderValueObject>> List()
        {
            var orders = _orderService.GetAll();

            var result = new ValidationResult<List<OrderValueObject>>(orders.ToList());

            return result;
        }

        /// <summary>
        /// Retorna um order pelo id.
        /// </summary>
        /// <param name="id">Id do order.</param>
        /// <returns>Order selecionado.</returns>
        [ValidateModel]
        [HttpGet("{id}")]
        public ValidationResult<OrderValueObject> Get(Guid id)
        {
            var order = _orderService.Get(id);

            var result = new ValidationResult<OrderValueObject>(order);

            return result;
        }

        /// <summary>
        /// Adicionar um item à order. É possível adicionar produtos, taxas e descontos.
        /// </summary>
        /// <param name="id">Id da order.</param>
        /// <param name="value">Id do item e quantidade.</param>
        /// <returns>Order com os itens adicionados.</returns>
        [ValidateModel]
        [HttpPost("{id}/add")]
        public ValidationResult<OrderValueObject> AddItem(Guid id, [FromBody]OrderItemModel value)
        {
            var order = _orderService.AddItem(id,
                new OrderItemValueObject()
                {
                    ItemId = value.ItemId,
                    Quantity = value.Quantity,
                    Description = value.Description
                });

            var result = new ValidationResult<OrderValueObject>(order, _resourceMessage.Instance(Add_Item.CODE, Add_Item.MESSAGE));

            return result;
        }


        /// <summary>
        /// Adicionar varios itens à order. É possível adicionar produtos, taxas e descontos.
        /// </summary>
        /// <param name="id">Id da order.</param>
        /// <param name="values">Lista de items para inclusão .</param>
        /// <returns>Order com os itens adicionados.</returns>
        [ValidateModel]
        [HttpPost("{id}/add/list")]
        public ValidationResult<OrderValueObject> AddItems(Guid id, [FromBody]List<OrderItemModel> values)
        {
            List<OrderItemValueObject> items = new List<OrderItemValueObject>();

            foreach (var value in values)
            {
                items.Add(new OrderItemValueObject()
                {
                    ItemId = value.ItemId,
                    Description = value.Description,
                    Quantity = value.Quantity,
                    AttendantId = value.AttendantId,
                    ComissionId = value.ComissionId
                });
            }

            var order = _orderService.AddItems(id, items);

            var result = new ValidationResult<OrderValueObject>(order, _resourceMessage.Instance(Add_Item.CODE, Add_Item.MESSAGE));

            return result;
        }

        /// <summary>
        /// Remove um item da order. É possível remover produtos, taxas e descontos.
        /// </summary>
        /// <param name="id">Id da order.</param>
        /// <param name="value">Id do item e quantidade.</param>
        /// <returns>Order com os itens removidos.</returns>
        [ValidateModel]
        [HttpPost("{id}/remove")]
        public ValidationResult<OrderValueObject> RemoveItem(Guid id, [FromBody]OrderItemModel value)
        {
            var order = _orderService.RemoveItem(id, value.ItemId, value.Quantity);

            var result = new ValidationResult<OrderValueObject>(order, _resourceMessage.Instance(Remove_Item.CODE, Remove_Item.MESSAGE));

            return result;
        }

        /// <summary>
        /// Efetua um pagamento. O pagamento pode ser parcial ou final.
        /// </summary>
        /// <param name="id">Id da order.</param>
        /// <param name="value">Campos relacionados ao pagamento.</param>
        /// <returns>Order com o pagamento adicionado.</returns>
        [ValidateModel]
        [HttpPost("{id}/pay")]
        public ValidationResult<OrderValueObject> AddPayment(Guid id, [FromBody]PaymentModel value)
        {
            OrderValueObject order;
            if (!_configurationService.IntegrationExists(FlowTypeEnum.Payment))
            {
                order = _orderService.AddPayment(id,
                    new PaymentValueObject()
                    {
                        AttendantId = value.AttendantId,
                        PaymentState = value.PaymentState,
                        PaymentType = value.PaymentType,
                        Value = value.Value
                    });
            }
            else
            {
                order = _orderService.AddPaymentIntegration(id,
                    new PaymentValueObject()
                    {
                        AttendantId = value.AttendantId,
                        PaymentState = value.PaymentState,
                        PaymentType = value.PaymentType,
                        Value = value.Value
                    });

            }

            var result = new ValidationResult<OrderValueObject>(order, _resourceMessage.Instance(Remove_Item.CODE, Remove_Item.MESSAGE));

            return result;
        }

        /// <summary>
        /// Recebe o status do pagamento 
        /// </summary>
        /// <param name="statusModel">Model com os parâmetros necessários para definir o status.</param>
        /// <param name="orderId">Id da order a ser paga</param>
        /// <returns>Status enviado.</returns>
        [ValidateModel]
        [HttpPost("{orderId}/payment/status")]
        public ValidationResult<PaymentStatusModel> SaveStatus(Guid orderId, [FromBody] PaymentStatusModel statusModel)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(5));

            var model = _cache.Set(orderId, statusModel, cacheEntryOptions);

            var result = new ValidationResult<PaymentStatusModel>(model, _resourceMessage.Instance(Payment_Status_Saved.CODE, Payment_Status_Saved.MESSAGE));

            return result;
        }

        /// <summary>
        /// Recebe o status do pagamento 
        /// </summary>
        /// <param name="orderId">Id da order a ser paga</param>
        /// <returns>Status enviado.</returns>
        [ValidateModel]
        [HttpGet("{orderId}/payment/status")]
        public ValidationResult<PaymentStatusModel> GetStatus(Guid orderId)
        {
            var model = _cache.Get<PaymentStatusModel>(orderId);

            var result = new ValidationResult<PaymentStatusModel>(model, _resourceMessage.Instance(Payment_Status_Saved.CODE, Payment_Status_Saved.MESSAGE));

            return result;
        }

        /// <summary>
        /// Recebe o status do pagamento 
        /// </summary>
        /// <param name="orderId">Id da order a ser paga</param>
        /// <returns>Status enviado.</returns>
        [ValidateModel]
        [HttpDelete("{orderId}/payment/status")]
        public ValidationResult<string> DeleteStatus(Guid orderId)
        {
            _cache.Remove(orderId);

            var result = new ValidationResult<string>("Deletado com sucesso");

            return result;
        }

        /// <summary>
        /// Recebe o status do pagamento 
        /// </summary>
        /// <param name="statusModel">Model com os parâmetros necessários para definir o status.</param>
        /// <param name="orderId">Id da order a ser paga</param>
        /// <returns>Status enviado.</returns>
        [ValidateModel]
        [HttpPost("{orderId}/customer")]
        public async Task<ValidationResult<bool>> CustomerToOrder(Guid orderId, [FromBody]UsuarioMeepModel model)
        {
            var location = _locationService.GetAll().FirstOrDefault();
            UsuarioMeepModel user = null;

            model.LocalClienteId = location.MeepLocationId;

            var modelJson = JsonConvert.SerializeObject(model);
            try
            {
                //user = await _meepApi.GetUsuarioMeep(json);
                user = await _meepApi.GetUsuarioMeep(JsonConvert.SerializeObject(new BuscarUsuarioMeepModel() { CPF = model.CPF, LocalClienteId = location.MeepLocationId }), _tokenMeepServer);
            }
            catch (Refit.ApiException ex)
            {
                user = null;
            }
            

            if (user != null)
            {
                await _meepApi.UpdateUsuarioMeep(modelJson, _tokenMeepServer);
            }
            else
            {
                await _meepApi.InsertUsuarioMeep(modelJson, _tokenMeepServer);
            }

            _orderService.AttachCustomerCpf(orderId, model.CPF);

            var result = new ValidationResult<bool>(true, _resourceMessage.Instance(Customer_Cpf_Attached.CODE, Customer_Cpf_Attached.MESSAGE));

            return result;
        }

        [ValidateModel]
        [HttpGet("{orderId}/customer/{cpf}")]
        public async Task<ValidationResult<UsuarioMeepModel>> CustomerExists(Guid orderId, string cpf)
        {
            var location = _locationService.GetAll().FirstOrDefault();

            var json = JsonConvert.SerializeObject(new BuscarUsuarioMeepModel() { CPF = cpf, LocalClienteId = location.MeepLocationId });
            UsuarioMeepModel user = null;

            try
            {
                //user = await _meepApi.GetUsuarioMeep(json);
                user = await _meepApi.GetUsuarioMeep(json, _tokenMeepServer);
            }
            catch(Refit.ApiException ex)
            {
                user = null;
            }

            var result = new ValidationResult<UsuarioMeepModel>(user, _resourceMessage.Instance(Customer_Cpf_Attached.CODE, Customer_Cpf_Attached.MESSAGE));

            return result;
        }
        [ValidateModel]
        [HttpPut("{orderId}/item/{itemId}/finalized")]
        public ValidationResult<OrderProductValueObject> FinalizeItem(Guid orderId, Guid itemId)
        {
            var orderProduct = _orderService.FinalizeOrderProduct(orderId, itemId);

            var result = new ValidationResult<OrderProductValueObject>(orderProduct);

            return result;
        }
    }
}