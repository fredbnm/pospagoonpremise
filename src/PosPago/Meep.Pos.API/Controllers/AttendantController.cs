﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.API.Models;
using Meep.Pos.API.Controllers.Base;
using static Meep.Pos.Domain.Core.Constants.DomainConstants.SuccessCode.Attendant;
using Meep.Pos.Domain.Core.Localization;
using Meep.Pos.Domain.Core.Localization.Interface;
using Meep.Pos.API.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Meep.Pos.API.Controllers
{

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AttendantController : BaseController
    {
        private IAttendantService _attendantService;
        private IResourceMessage _resourceMessage;
        public AttendantController(IAttendantService attendantService, IResourceMessage resourceMessage)
        {
            _attendantService = attendantService;
            _resourceMessage = resourceMessage;
        }


        /// <summary>
        /// Retorna todos os atendentes cadastrados.
        /// </summary>
        /// <returns>Lista de atendentes</returns>
        [ValidateModel]
        [HttpGet]
        public ValidationResult<List<AttendantValueObject>> List()
        {
            var attendants = _attendantService.GetAll();

            var result = new ValidationResult<List<AttendantValueObject>>(attendants.ToList());

            return result;
        }

        /// <summary>
        /// Retorna um atendente pelo id.
        /// </summary>
        /// <param name="id">Id do atendente.</param>
        /// <returns>Atendente solicitado.</returns>
        [HttpGet("{id}")]
        public ValidationResult<AttendantValueObject> Get(Guid id)
        {
            var attendant = _attendantService.Get(id);

            var result = new ValidationResult<AttendantValueObject>(attendant);

            return result;
        }

        /// <summary>
        /// Retorna um atendente pelo pin.
        /// </summary>
        /// <param name="pin">Pin do atendente.</param>
        /// <returns>Atendente solicitado.</returns>
        /// 
        [HttpGet("pin/{pin}")]
        public ValidationResult<AttendantValueObject> Get(int pin)
        {
            var attendant = _attendantService.GetByPin(pin);

            var result = new ValidationResult<AttendantValueObject>(attendant);

            return result;
        }

        /// <summary>
        /// Cria um novo atendente.
        /// </summary>
        /// <param name="value">Atendente a ser criado.</param>
        /// <returns>Atendente criado.</returns>
        [ValidateModel]
        [HttpPost]
        public ValidationResult<AttendantValueObject> Post([FromBody]AttendantValueObject value)
        {
            var attendant = _attendantService.Create(value);

            var result = new ValidationResult<AttendantValueObject>(attendant, _resourceMessage.Instance(Inserted.CODE, Inserted.MESSAGE));

            return result;
        }

        /// <summary>
        /// Atualizar um atendente.
        /// </summary>
        /// <param name="value">Atendente a ser atualizado.</param>
        /// <returns>Atendente atualizado.</returns>
        [ValidateModel]
        [HttpPut]
        public ValidationResult<AttendantValueObject> Put([FromBody]AttendantValueObject value)
        {
            var attendant = _attendantService.Update(value);

            var result = new ValidationResult<AttendantValueObject>(attendant, _resourceMessage.Instance(Updated.CODE, Updated.MESSAGE));

            return result;
        }

        /// <summary>
        /// Deletar um atendente.
        /// </summary>
        /// <param name="id">Id do ticket a ser deletado.</param>
        /// <returns>Mensagem de confirmação.</returns>
        [ValidateModel]
        [HttpDelete]
        public ValidationResult<AttendantValueObject> Delete(Guid id)
        {
            _attendantService.Remove(id);

            var result = new ValidationResult<AttendantValueObject>(new AttendantValueObject(), _resourceMessage.Instance(Removed.CODE, Removed.MESSAGE));

            return result;
        }

        /// <summary>
        /// Gera um novo pin code para o atendente.
        /// </summary>
        /// <param name="value">Id do atendente.</param>
        /// <returns>Atendente com o novo PIN gerado.</returns>
        [ValidateModel]
        [HttpPut("{id}/new/pin")]
        public ValidationResult<AttendantValueObject> Put(Guid id)
        {
            var attendant = _attendantService.NewPinCode(id);

            var result = new ValidationResult<AttendantValueObject>(attendant, _resourceMessage.Instance(Pin_Changed.CODE, Pin_Changed.MESSAGE, attendant.PinCode));

            return result;
        }
    }
}