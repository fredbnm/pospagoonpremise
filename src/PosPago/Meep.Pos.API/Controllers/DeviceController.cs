﻿using Meep.Pos.API.Controllers.Base;
using Meep.Pos.API.Filters;
using Meep.Pos.API.Models;
using Meep.Pos.API.Models.Device;
using Meep.Pos.API.Providers;
using Meep.Pos.API.Util;
using Meep.Pos.Domain.Core.Localization.Interface;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.ValueObjects;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Meep.Pos.API.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DeviceController : BaseController
    {
        private IConfiguration _configuration;
        private IResourceMessage _resourceMessage;
        private IDeviceAttendantService _deviceAttendantService;
        private IAttendantService _attendantService;
        private IConfigurationService _configurationService;

        public DeviceController(IConfiguration configuration,
            IResourceMessage resourceMessage,
            IDeviceAttendantService deviceAttendantService,
            IAttendantService attendantService,
            IConfigurationService configurationService)
        {
            _configuration = configuration;
            _resourceMessage = resourceMessage;
            _deviceAttendantService = deviceAttendantService;
            _attendantService = attendantService;
            _configurationService = configurationService;
        }

        /// <summary>
        /// Retorna todos os dispositivos relacionados a atendentes cadastrados.
        /// </summary>
        /// <returns>Lista de dispositivos relacionados ao atendente no campo Value.</returns>
        [ValidateModel]
        [HttpGet]
        public ValidationResult<List<DeviceAttendantValueObject>> List()
        {
            var deviceAttendants = _deviceAttendantService.GetAll();

            var result = new ValidationResult<List<DeviceAttendantValueObject>>(deviceAttendants.ToList());

            return result;
        }

        /// <summary>
        /// Retorna um dispositivo relacionado ao atendente através de seu id.
        /// </summary>
        /// <param name="id">Id da relação entre atendente e dispostivo a ser retornada</param>
        /// <returns>Dispositivo no campo Value.</returns>
        [ValidateModel]
        [HttpGet("{id}")]
        public ValidationResult<DeviceAttendantValueObject> Get(Guid id)
        {
            var deviceAttendant = _deviceAttendantService.Get(id);

            var result = new ValidationResult<DeviceAttendantValueObject>(deviceAttendant);

            return result;
        }

        /// <summary>
        /// Ativa ou desativa um device.
        /// </summary>
        /// <param name="id">Id da relação entre atendente e dispostivo a ser retornada</param>
        /// <returns>Dispositivo no campo Value.</returns>
        [ValidateModel]
        [HttpGet("{id}/{isActive}")]
        public ValidationResult<DeviceAttendantValueObject> ChangeStatus(Guid id, bool isActive)
        {
            var deviceAttendant = _deviceAttendantService.ChangeDeviceStatus(id, isActive);

            var result = new ValidationResult<DeviceAttendantValueObject>(deviceAttendant);

            return result;
        }

        /// <summary>
        /// Efetua login através do dispositivo e gera um token
        /// </summary>
        /// <param name="id">Id da categoria a ser retornada</param>
        /// <returns>Informações do token no campo Value.</returns>
        [ValidateModel]
        [AllowAnonymous]
        [HttpPost("login")]
        public ValidationResult<TokenResponse> Login([FromBody]LoginDeviceModel login)
        {
            var deviceAttendant = _deviceAttendantService.LoginByDevice(login.PinCode, login.BrowserFingerprint, login.UserAgent, login.Resolution);

            var options = new TokenProviderOptions();

            options.Path = _configuration.GetSection("TokenAuthentication:TokenPath").Value;
            options.Audience = _configuration.GetSection("TokenAuthentication:Audience").Value;
            options.Issuer = _configuration.GetSection("TokenAuthentication:Issuer").Value;
            var signingKey = new SymmetricSecurityKey(
                   Encoding.UTF8.GetBytes(_configuration.GetSection("TokenAuthentication:SecretKey").Value));

            options.SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);

            switch (login.orderType)
            {
                case Domain.Core.Enumerators.MeepTypeEnum.Pos:
                    options.Expiration = TimeSpan.FromMinutes(Convert.ToDouble(_configuration.GetSection("TokenAuthentication:Expires_Order").Value));
                    break;
                case Domain.Core.Enumerators.MeepTypeEnum.Pre:
                    options.Expiration = TimeSpan.FromDays(1);
                    break;
                default:
                    break;
            }
            

            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, login.BrowserFingerprint),
                new Claim(JwtRegisteredClaimNames.Sub, login.PinCode.ToString()),
                //new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(now).ToUniversalTime().ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64)
            };

            var token = Util.SecurityToken.GetEncoded(claims, options);

            var attendant = _attendantService.GetByPin(login.PinCode);

            var configuration = _configurationService.GetOrCreate();

            var tokenResponse = new TokenResponse()
            {
                access_token = token,
                expires_in = (int)options.Expiration.TotalSeconds,
                attendantId = attendant.Id.Value,
                attendantName = attendant.Name,
                meepType = configuration.MeepType
            };

            var result = new ValidationResult<TokenResponse>(tokenResponse);

            return result;
        }
    }
}
