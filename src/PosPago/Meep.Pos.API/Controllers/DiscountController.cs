﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.API.Models;
using Meep.Pos.API.Controllers.Base;
using static Meep.Pos.Domain.Core.Constants.DomainConstants.SuccessCode.Discount;
using Meep.Pos.Domain.Core.Localization;
using Meep.Pos.Domain.Core.Localization.Interface;
using Meep.Pos.API.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Meep.Pos.API.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DiscountController : BaseController
    {
        private IDiscountService _discountService;
        private IResourceMessage _resourceMessage;

        public DiscountController(IDiscountService taxService, IResourceMessage resourceMessage)
        {
            _discountService = taxService;
            _resourceMessage = resourceMessage;
        }


        /// <summary>
        /// Retorna todos os descontos cadastrados.
        /// </summary>
        /// <returns>Lista de descontos</returns>
        [ValidateModel]
        [HttpGet]
        public ValidationResult<List<DiscountValueObject>> List()
        {
            var discounts = _discountService.GetAll();

            var result = new ValidationResult<List<DiscountValueObject>>(discounts.ToList());

            return result;
        }

        /// <summary>
        /// Retorna um desconto pelo id.
        /// </summary>
        /// <param name="id">Id do desconto.</param>
        /// <returns>Desconto selecionado.</returns>
        [ValidateModel]
        [HttpGet("{id}")]
        public ValidationResult<DiscountValueObject> Get(Guid id)
        {
            var discount = _discountService.Get(id);

            var result = new ValidationResult<DiscountValueObject>(discount);

            return result;
        }

        /// <summary>
        /// Cria um novo desconto.
        /// </summary>
        /// <param name="value">Desconto a ser criado.</param>
        /// <returns>Desconto criado.</returns>
        [ValidateModel]
        [HttpPost]
        public ValidationResult<DiscountValueObject> Post([FromBody]DiscountValueObject value)
        {
            var discount = _discountService.Create(value);

            var result = new ValidationResult<DiscountValueObject>(discount, _resourceMessage.Instance(Inserted.CODE, Inserted.MESSAGE));

            return result;
        }

        /// <summary>
        /// Atualizar um desconto.
        /// </summary>
        /// <param name="value">Desconto a ser atualizado.</param>
        /// <returns>Desconto atualizado.</returns>
        [ValidateModel]
        [HttpPut]
        public ValidationResult<DiscountValueObject> Put([FromBody]DiscountValueObject value)
        {
            var discount = _discountService.Update(value);

            var result = new ValidationResult<DiscountValueObject>(discount, _resourceMessage.Instance(Updated.CODE, Updated.MESSAGE));

            return result;
        }

        /// <summary>
        /// Deletar um desconto.
        /// </summary>
        /// <param name="id">Id do desconto a ser deletado.</param>
        /// <returns>Mensagem de confirmação.</returns>
        [ValidateModel]
        [HttpDelete]
        public ValidationResult<DiscountValueObject> Delete(Guid id)
        {
            _discountService.Remove(id);

            var result = new ValidationResult<DiscountValueObject>(new DiscountValueObject(), _resourceMessage.Instance(Removed.CODE, Removed.MESSAGE));

            return result;
        }
    }
}