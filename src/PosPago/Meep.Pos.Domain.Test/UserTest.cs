﻿using Meep.Pos.Domain.Core.Exception.User;
using Meep.Pos.Domain.Models;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Xunit;

namespace Meep.Pos.Domain.Test
{
    public class UserTest : IDisposable
    {
        private AppUser _user;
        public UserTest()
        {
            _user = new AppUser("Bruno@meep-app.com", "b.roldao", "123456", "Bruno", "12345678910", Core.Enumerators.SexEnum.M);
        }
        #region [ Tests ]

        [Fact]
        public void CreateUser()
        {
            Assert.NotNull(_user);
            
            Assert.NotNull(_user.Password);
        }

        [Fact]
        public void ChangePassword()
        {
            var oldPassword = "123456";

            _user.ChangePassword(_user.Login, oldPassword, "teste123");

            Assert.NotEqual(GeneratePasswordHash(oldPassword), _user.Password);

            Assert.Equal(GeneratePasswordHash("teste123"), _user.Password);
        }

        [Fact]
        public void Exception_ChangePassword()
        {
            var oldPassword = "123456";

            Assert.Throws<UserWrongPasswordException>(()=>_user.ChangePassword(_user.Login, "123", oldPassword));

            Assert.Throws<UserNewPasswordSameAsOldException>(() => _user.ChangePassword(_user.Login, "123456", "123456"));
            
        }

        [Fact]
        public void ChangeEmail()
        {
            var oldEmail = _user.Email;

            var pswd = GeneratePasswordHash(_user.Password);

            _user.ChangeEmail(_user.Login, "123456", oldEmail, "bruno-roldao@meep-app.com");

            Assert.NotEqual(oldEmail, _user.Email);

            Assert.Equal("bruno-roldao@meep-app.com", _user.Email);
        }

        [Fact]
        public void Exception_ChangeEmail()
        {
            var oldEmail = _user.Email;

            Assert.Throws<UserWrongEmailException>(() => _user
                                .ChangeEmail(_user.Login, "123456", "bruno-roldao@meep-app.com", "bruno-roldao@meep-app.com"));
        }

        private string GeneratePasswordHash(string senha)
        {
            if (string.IsNullOrWhiteSpace(senha))
                return null;

            byte[] salt = new byte[] { 0x16, 0x63, 0x67, 0x32, 0x72, 0xc4, 0x04, 0x34, 0xb5, 0x84, 0x85, 0xf7, 0xb5, 0x13, 0x92 };
            byte[] plainText = Encoding.UTF8.GetBytes(senha);
            HashAlgorithm algorithm = new SHA256Managed();

            byte[] plainTextWithSaltBytes =
              new byte[plainText.Length + salt.Length];

            for (int i = 0; i < plainText.Length; i++)
            {
                plainTextWithSaltBytes[i] = plainText[i];
            }
            for (int i = 0; i < salt.Length; i++)
            {
                plainTextWithSaltBytes[plainText.Length + i] = salt[i];
            }

            return Convert.ToBase64String(algorithm.ComputeHash(plainTextWithSaltBytes));
        }
        #endregion  

        #region [ Dispose ] 
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
