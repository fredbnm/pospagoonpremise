﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Models;
using Xunit;

namespace Meep.Pos.Domain.Test
{
    public class ProductTest : IDisposable
    {
        #region [ Tests ]

        //[Fact]
        //public void CreateProduct()
        //{
        //    Product product = new Product("Serra Malte", new Category(), (decimal) 8.90);

        //    Assert.NotNull(product);

        //    Assert.Equal(product.Description, "Serra Malte");
        //}

        #endregion  

        #region [ Dispose ] 
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
