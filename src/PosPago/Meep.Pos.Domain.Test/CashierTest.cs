﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Models;
using Xunit;
using Meep.Pos.Domain.Core.Exception.Cashier;
using Meep.Pos.Service.Test.Mock;

namespace Meep.Pos.Domain.Test
{
    public class CashierTest : IDisposable
    {

        private Attendant _attendant;
        public CashierTest()
        {
            _attendant = MockDomain.Attendant("Bruno", "123456543", Core.Enumerators.SexEnum.M);
        }
        #region [ Tests ]
        [Fact]
        public void OpenCashier()
        {
            DateTime currentDate = DateTime.Now;

            Cashier cashier = new Cashier();

            cashier.Open(_attendant, 10 );

            Assert.True(cashier.IsOpen);

            Assert.NotEqual(cashier.DateOpened, DateTime.MinValue);

            Assert.Equal(cashier.DateOpened.Date, currentDate.Date.Date);
        }

        [Fact]
        public void GetExceptionWhenOpenCashierManyTimes()
        {
            Cashier cashier = new Cashier();

            cashier.Open(_attendant, 10);

            Assert.True(cashier.IsOpen);

            Assert.Throws<CashierAlreadyOpenedException>(() => cashier.Open(_attendant, 10));

        }

        [Fact]
        public void CloseCashier()
        {
            DateTime currentDate = DateTime.Now;
            Cashier cashier = new Cashier();
            var attendant = _attendant;

            cashier.Open(attendant, 10);

            cashier.Close(attendant);

            Assert.False(cashier.IsOpen);

            //Verificando se as datas foram preenchidas.
            Assert.NotEqual(cashier.DateOpened, DateTime.MinValue);
            Assert.NotEqual(cashier.DateClosed, DateTime.MinValue);

            //Verificando se a data de fechamento é a data atual.
            Assert.Equal(cashier.DateClosed.Date, currentDate.Date);

        }

        [Fact]
        public void GetExceptionWhenCloseCashierWithoutOpen()
        {
            Cashier cashier = new Cashier();

            Assert.Throws<CashierMustBeOpenedException>(() => cashier.Close(_attendant));
        }

        [Fact]
        public void GetExceptionWhenCloseCashierManyTimes()
        {
            Cashier cashier = new Cashier();

            var attendant = _attendant;

            cashier.Open(attendant, 10);

            cashier.Close(attendant);

            Assert.False(cashier.IsOpen);

            Assert.Throws<CashierMustBeOpenedException>(() => cashier.Close(attendant));
        }

        [Fact]
        public void MakeAnOrder()
        {
            Cashier cashier = new Cashier();

            Orders order = new Orders(new Ticket("1"), _attendant);

            cashier.Open(_attendant, 15);

            cashier.StartOrder(order);

            Assert.Equal(1, cashier.Orders.Count());
        }

        [Fact]
        public void GetExceptionWhenCallingMethodsWithoutCashierOpened()
        {
            Cashier cashier = new Cashier();

            Orders order = new Orders(new Ticket("1"), _attendant);

            Assert.Throws<CashierMustBeOpenedException>(() =>
            {
                cashier.StartOrder(order);
                cashier.ChangeTicket("1", "2");
            });
        }

        [Fact]
        public void ChangeTicket()
        {
            Cashier cashier = new Cashier();

            Orders order = new Orders(new Ticket("1"), _attendant);

            cashier.Open(_attendant, 200);
            cashier.StartOrder(order);

            cashier.ChangeTicket("1", "2");

            Assert.Equal("2", cashier.Orders.FirstOrDefault(o => o.Id == order.Id).Ticket.Number);
        }

        #endregion

        #region [ Dispose ]
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
