﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Models;
using Xunit;
using Meep.Pos.Domain.Core.Util;

namespace Meep.Pos.Domain.Test
{
    public class OrderItemTest : IDisposable
    {
        public OrderItemTest()
        {
            
        }

        #region [ Tests ]

        //[Fact]
        //public void OrderAnItem()
        //{
        //    Product product = new Product("Água", new Category(), (decimal)4.90);
        //    OrderProduct orderItem = new OrderProduct(product);

        //    Assert.NotNull(orderItem.Product);
        //    Assert.NotEqual(orderItem.DateOrder, DateTime.MinValue);

        //}

        //[Fact]
        //public void AddItems()
        //{
            
        //}

        [Fact]
        public void GenerateTicket()
        {
            var t = RandomGenerator.Ticket();

            Assert.Equal(t.Length, 6);
        }

        #endregion

        #region [ Dispose ]

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion  
    }
}
