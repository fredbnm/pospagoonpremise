﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Models;
using Xunit;
using Meep.Pos.Domain.Core.Exception.Order;

namespace Meep.Pos.Domain.Test
{
    public class CashierFlowTest : IDisposable
    {
        public CashierFlowTest()
        {
            
        }

        #region [ Tests ]

        [Fact]
        public void CashierFlow()
        {
            #region [ 1 - Abertura de caixa ]
            Cashier cashier = new Cashier();
            var initialValue = 200;

            Attendant cashierAttendant = new Attendant("Joana", "123456754", SexEnum.M);

            cashier.Open(cashierAttendant, initialValue);

            #endregion

            #region [ 2 - Abertura de comanda ]
            Ticket ticket = new Ticket("1");

            Attendant attendant = new Attendant("Joaquim", "123456789", SexEnum.M);

            Orders order = new Orders(ticket, attendant);

            cashier.StartOrder(order);

            Assert.Equal(1, cashier.Orders.Count());
            #endregion

            #region [ 3 - Comandar ]

            var currentOrder = cashier.GetOrder(order.Id);

            //Produtos
            var skol = new Product("Skol", new Category("Cerveja"), (decimal)7.9);
            var porcao = new Product("Porção de Contra-filé", new Category("Comida"), (decimal)25.9);
            var sobremesa = new Product("Sorvete", new Category("Sobremesa"), 10);
            
            //Taxa
            var taxaGarcom = new Tax("10% do Garçom", PriceCalcTypeEnum.Percent, 10);

            #region [ 3.1 - Adicionar Produto ]

            //Adicionando 4 cervejas
            currentOrder.AddItem(skol, 4);
            //Adicionando 2 Porções de Carne
            currentOrder.AddItem(porcao, 2);
            //Adicionando 1 sobremesa
            currentOrder.AddItem(sobremesa);

            Assert.Equal(3, cashier.GetOrder(order.Id).OrderItems.Count());
            #endregion

            #region [ 3.2 - Remover Produto ]

            //Removendo 1 cerveja
            currentOrder.RemoveItem(skol);
            //Verificando se os produtos ainda estão no pedido.
            Assert.Equal(3, cashier.GetOrder(order.Id).OrderItems.Count());
            //Verificando se agora existem 3 cervejas.
            Assert.Equal(3, cashier.GetOrder(order.Id).GetQuantityByProduct(skol));

            //Removendo sobremesa
            currentOrder.RemoveItem(sobremesa);

            Assert.Equal(2, cashier.GetOrder(order.Id).OrderItems.Count());

            #endregion

            #region [ 3.3 - Inclusão de Taxa ]

            //Adicionando taxa do garçom
            currentOrder.AddItem(taxaGarcom);

            Assert.Equal(1, cashier.GetOrder(order.Id).OrderTaxes.Count());

            #endregion

            #region [ 3.4 - Solicitação de Parcial ]

            //Visualizar o total da comanda
            var parcial = cashier.GetOrder(order.Id).Total;

            var calculoParcial = ((skol.Price * 3) + (porcao.Price * 2));

            calculoParcial += calculoParcial * (decimal) 0.1;

            Assert.Equal(calculoParcial, parcial);

            #endregion

            #region [ 3.5 - Abatimento de Valor ]

            //Cliente paga parte do valor (15 reais)
            var abate = new Payment(15,PaymentStateEnum.Partial, PaymentTypeEnum.Debit, attendant);

            currentOrder.AddPayment(abate);

            var calculoAposAbatimento = calculoParcial - abate.Value;

            Assert.Equal(calculoAposAbatimento, cashier.GetOrder(order.Id).Total - cashier.GetOrder(order.Id).TotalPayment);

            #endregion

            #endregion

            #region [ 4 - Fechamento ]

            //Efetuando fechamento.
            //Deve ocorrer erro ao tentar fechar sem finalizar o pagamento
            Assert.Throws<OrderCannotCloseException>(() => currentOrder.Close());

            //Adicionando pagamento final
            var pagamento = new Payment(calculoAposAbatimento, PaymentStateEnum.Final, PaymentTypeEnum.Credit, attendant);

            currentOrder.AddPayment(pagamento);

            //Validando se o total pago bate com o total da conta
            Assert.Equal(currentOrder.Total, currentOrder.TotalPayment);

            //Fechando a conta após o pagamento
            currentOrder.Close();

            //Conferindo se a conta foi fechada
            Assert.NotEqual(DateTime.MinValue, currentOrder.DateClosed);

            //Fechando o caixa
            cashier.Close(cashierAttendant);

            Assert.Equal(currentOrder.Total + initialValue, cashier.Total);

            #endregion
        }
        
        #endregion

        #region [ Dispose ]
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
