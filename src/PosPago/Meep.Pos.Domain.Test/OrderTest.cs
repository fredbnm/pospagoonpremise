﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Models;
using Xunit;
using Meep.Pos.Domain.Core.Exception.Tax;
using Meep.Pos.Domain.Core.Exception.Order;
using Meep.Pos.Service.Test.Mock;

namespace Meep.Pos.Domain.Test
{
    public class OrderTest : IDisposable
    {

        private Attendant _attendant;
        public OrderTest()
        {
            _attendant = MockDomain.Attendant("Bruno", "10755822222", Core.Enumerators.SexEnum.M);
        }

        #region [ Tests ]

        /// <summary>
        /// Abrindo uma order
        /// </summary>
        [Fact]
        public void MakeAnOrder()
        {
            string productName = "Água";

            Orders order = new Orders(new Ticket("1"), _attendant);

            order.AddItem(new Product(productName, new Category("Bebidas"), 10), 1);

            Assert.NotNull(order.Ticket);

            Assert.NotNull(order.OrderProducts);

            Assert.Collection(order.OrderProducts, oitem => Assert.Contains(productName, oitem.Product.Name));

            Assert.NotEqual(order.DateOpened, DateTime.MinValue);
        }

        /// <summary>
        /// Abrindo e Fechando uma order
        /// </summary>
        [Fact]
        public void MakeAndCloseAnOrder()
        {
            string productName = "Água";

            var attendant = _attendant;

            Orders order = new Orders(new Ticket("1"), attendant);

            order.AddItem(new Product(productName, new Category("Bebidas"), (decimal)5.90), 1);

            Assert.NotEqual(order.DateOpened, DateTime.MinValue);

            Payment payment = new Payment((decimal)5.90, PaymentStateEnum.Final, PaymentTypeEnum.Debit, attendant);

            order.AddPayment(payment);

            order.Close();

            Assert.NotEqual(order.DateClosed, DateTime.MinValue);
        }

        [Fact]
        public void GetExceptionWhenCloseAnOrderMultipleTimes()
        {
            string productName = "Água";

            var attendant = _attendant;

            Orders order = new Orders(new Ticket("1"), attendant);

            order.AddItem(new Product(productName, new Category("Bebidas"), (decimal)5.90), 1);

            Payment payment = new Payment((decimal)5.90, PaymentStateEnum.Final, PaymentTypeEnum.Debit, attendant);

            order.AddPayment(payment);

            order.Close();

            Assert.NotEqual(order.DateClosed, DateTime.MinValue);

            Assert.Throws<OrderAlreadyClosedException>(() => order.Close());

        }

        /// <summary>
        /// Adicionando e removendo produtos ao pedido
        /// </summary>
        [Fact]
        public void AddAndRemoveItem()
        {
            string productName = "Água";

            Orders order = new Orders(new Ticket("1"), _attendant);

            Product product = new Product(productName, new Category("Bebidas"), (decimal)5.90);

            order.AddItem(product, 1);

            Assert.Collection(order.OrderProducts, oitem => Assert.Contains(productName, oitem.Product.Name));

            order.RemoveItem(product, 1);

            Assert.False(order.OrderProducts.Any());

        }

        [Fact]
        public void AddProductByKgAndCalcValue()
        {
            string productName = "Almoço no KG";

            Orders order = new Orders(new Ticket("1"), _attendant);

            Product product = new Product(productName, new Category("Comida"), (decimal)45.90, false, MeasureUnitEnum.Kg);

            order.AddItem(product, (decimal)0.350);

            Assert.Equal(Math.Round((decimal)45.90 * (decimal)0.350,2), order.Total);

            order.RemoveItem(product, 1);

            Assert.False(order.OrderProducts.Any());

        }

        [Fact]
        public void AddMultipleProductByKgAndCalcValue()
        {
            string productName = "Almoço no KG";

            Orders order = new Orders(new Ticket("1"), _attendant);

            Product product = new Product(productName, new Category("Comida"), (decimal)45.90, false, MeasureUnitEnum.Kg);

            order.AddItem(product, (decimal)0.350);

            order.AddItem(product, (decimal)0.500);

            Assert.Equal(Math.Round(Math.Round(((decimal)45.90 * (decimal)0.350),2) + Math.Round(((decimal)45.90 * (decimal)0.500),2),2), order.Total);

            order.RemoveItem(product, 1);

            order.RemoveItem(product, 1);

            Assert.False(order.OrderProducts.Any());

        }

        [Fact]
        public void CheckProductQuantity()
        {
            string productName = "Água";

            Product product = new Product(productName, new Category("Bebidas"), (decimal)5.90);

            Orders order = new Orders(new Ticket("1"), _attendant);

            order.AddItem(product, 1);
            order.AddItem(product, 1);

            Assert.Equal(2, order.GetQuantityByProduct(product));

        }

        [Fact]
        public void RemoveSameItemMultipleTimes()
        {
            var product = new Product("Água", new Category("Bebidas"), (decimal)5.90);

            Orders order = new Orders(new Ticket("1"), _attendant);

            //Adicionando duas águas
            order.AddItem(product, 2);

            //Removendo duas águas
            order.RemoveItem(product, 2);

            var product2 = new Product("Hamburguer", new Category("Comida"), (decimal)25.90);
            //Adicionando dois hamburgueres
            order.AddItem(product2, 2);

            //Removendo um hamburguer
            order.RemoveItem(product2);

            //Removendo uma água
            Assert.Throws<OrderItemDoesntExistsException>(() => order.RemoveItem(product));

            Assert.Equal(0, order.GetQuantityByProduct(product));

            Assert.Equal(1, order.GetQuantityByProduct(product2));
        }

        [Fact]
        public void RemoveSameItemMoreTimesThanIsAdded()
        {
            var product = new Product("Água", new Category("Bebidas"), (decimal)5.90);

            Orders order = new Orders(new Ticket("1"), _attendant);

            order.AddItem(product, 2);

            order.RemoveItem(product, 3);

            Assert.Equal(0, order.OrderProducts.Count());

        }

        [Fact]
        public void GetOrderValue()
        {
            Orders order = new Orders(new Ticket("1"), _attendant);

            var product = new Product("Água", new Category("Bebidas"), (decimal)5.90);

            order.AddItem(product);

            var product2 = new Product("Hamburguer", new Category("Comida"), (decimal)25.90);

            order.AddItem(product2);

            var value = order.Total;

            Assert.Equal(product.Price + product2.Price, value);
        }

        #region [ OrderTax ] 

        [Fact]
        public void ApplyTax()
        {
            Orders order = new Orders(new Ticket("1"), _attendant);

            var tax = new Tax("Couvert", PriceCalcTypeEnum.Value, 5);

            order.AddItem(tax);

            Assert.NotNull(order.OrderTaxes);

            Assert.Equal(1, order.OrderTaxes.Count());
        }

        [Fact]
        public void ApplyTaxAndCalcTotal()
        {
            Orders order = new Orders(new Ticket("1"), _attendant);

            var product = new Product("Água", new Category("Bebidas"), (decimal)5.90);

            order.AddItem(product);

            var tax = new Tax("Couvert", PriceCalcTypeEnum.Value, 5);

            order.AddItem(tax);

            Assert.Equal(product.Price + tax.Value, order.Total);
        }

        [Fact]
        public void ApplyPercentageTax()
        {
            Orders order = new Orders(new Ticket("1"), _attendant);

            var tax = new Tax("Garçom", PriceCalcTypeEnum.Percent, 10);

            order.AddItem(tax);

            Assert.NotNull(order.OrderTaxes);

            Assert.Equal(1, order.OrderTaxes.Count());
        }

        [Fact]
        public void ApplyPercentageTaxAndCalcTotal()
        {
            Orders order = new Orders(new Ticket("1"), _attendant);

            var product = new Product("Água", new Category("Bebidas"), (decimal)5.90);

            order.AddItem(product, 5);

            var tax = new Tax("Garçom", PriceCalcTypeEnum.Percent, 10);

            order.AddItem(tax);

            var productsPrice = (product.Price * 5);

            var percentValue = ((productsPrice * tax.Value) / 100);

            percentValue += productsPrice;

            Assert.Equal(percentValue, order.Total);
        }

        [Fact]
        public void GetExceptionWhenApplyPercentageTaxGreaterOrLower()
        {

            Assert.Throws<TaxPercentageException>(() => new Tax("Garçom", PriceCalcTypeEnum.Percent, 120));

        }

        #endregion

        #region [ OrderDiscount ]

        [Fact]
        public void ApplyDiscount()
        {
            Orders order = new Orders(new Ticket("1"), _attendant);

            var discount = new Discount("Promoção", PriceCalcTypeEnum.Value, 5);

            order.AddItem(discount);

            Assert.NotNull(order.OrderDiscounts);

            Assert.Equal(1, order.OrderDiscounts.Count());
        }

        [Fact]
        public void ApplyDiscountPercentage()
        {
            Orders order = new Orders(new Ticket("1"), _attendant);

            var discount = new Discount("Promoção", PriceCalcTypeEnum.Percent, 15);

            order.AddItem(discount);

            Assert.NotNull(order.OrderDiscounts);

            Assert.Equal(1, order.OrderDiscounts.Count());
        }

        [Fact]
        public void ApplyPercentageDiscountAndCalcTotal()
        {
            Orders order = new Orders(new Ticket("1"), _attendant);

            var product = new Product("Água", new Category("Bebidas"), (decimal)5.90);

            order.AddItem(product, 5);

            var tax = new Tax("Garçom", PriceCalcTypeEnum.Percent, 10);

            order.AddItem(tax);

            var productsPrice = (product.Price * 5);

            var percentValue = ((productsPrice * tax.Value) / 100);

            percentValue += productsPrice;

            Assert.Equal(percentValue, order.Total);
        }

        #endregion

        [Fact]
        public void PayAccount()
        {
            var attendant = _attendant;
            Orders order = new Orders(new Ticket("1"), attendant);

            var product = new Product("Água", new Category("Bebidas"), (decimal)5.90);

            order.AddItem(product, 5);

            var payment = new Payment(20, PaymentStateEnum.Partial, PaymentTypeEnum.Money, attendant);

            order.AddPayment(payment);

            Assert.Equal(1, order.Payments.Count());

            Assert.NotEqual(order.Total, order.TotalPayment);

            Assert.Throws<OrderCannotCloseException>(() => order.Close());

            var finalPayment = new Payment((decimal)9.5, PaymentStateEnum.Final, PaymentTypeEnum.Debit, attendant);

            order.AddPayment(finalPayment);

            Assert.Equal(2, order.Payments.Count());

            Assert.Equal(order.Total, order.TotalPayment);

            order.Close();

            Assert.NotEqual(DateTime.MinValue, order.DateClosed);

        }

        [Fact]
        public void GetExceptionWhenPayingWithoutBuying()
        {
            var attendant = _attendant;
            Orders order = new Orders(new Ticket("1"), attendant);

            var payment = new Payment(20, PaymentStateEnum.Partial, PaymentTypeEnum.Money, attendant);

            Assert.Throws<OrderPaymentWithoutItemsException>( () => order.AddPayment(payment));

        }


        #endregion

        #region [ Dispose ]
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
