﻿using Meep.Pos.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using System.Linq;
using AutoMapper;

namespace Meep.Pos.Service
{
    public class EventService : IEventService
    {
        private IEventRepository _eventRepository;
        private IMapper _mapper;
        public EventService(IEventRepository eventRepository, IMapper mapper)
        {
            _eventRepository = eventRepository;
            _mapper = mapper;
        }

        public List<EventValueObject> GetAll()
        {
            var events = _eventRepository.GetAll();

            events = events.OrderByDescending(e => e.TimeStamp).ToList();

            return _mapper.Map<List<EventValueObject>>(events);
        }
    }
}
