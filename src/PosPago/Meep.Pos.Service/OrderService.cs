﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.Domain.Core.Exception.Order;
using System.Linq;
using Meep.Pos.Domain.Core.Exception.Attendant;
using Meep.Pos.Domain.Command.Interface;
using Meep.Pos.Domain.Command.Production;
using Meep.Pos.Domain.Command.Command.Production;
using Meep.Pos.Domain.Core.Enumerators.Configuration;
using Meep.Pos.Service.ValueObjects.Integration;
using Meep.Pos.Domain.Core.Enumerators;

namespace Meep.Pos.Service
{
    public class OrderService : IOrderService
    {
        private IOrderRepository _orderRepository;
        private IProductRepository _productRepository;
        private ITaxRepository _taxRepository;
        private IDiscountRepository _discountRepository;
        private IAttendantRepository _attendantRepository;
        private IConfigurationRepository _configurationRepository;
        private IIntegrationService _integrationService;
        private IComissionRepository _comissionRepository;
        private ILocationRepository _locationRepository;
        private ICommandHandler _commandHandler;
        private IMapper _mapper;

        public OrderService(IOrderRepository orderRepository,
            IProductRepository productRepository,
            ITaxRepository taxRepository,
            IDiscountRepository discountRepository,
            IAttendantRepository attendantRepository,
            IConfigurationRepository configurationRepository,
            IIntegrationService integrationService,
            IComissionRepository comissionRepository,
            ILocationRepository locationRepository,
            ICommandHandler commandHandler,
            IMapper mapper)
        {
            _orderRepository = orderRepository;
            _productRepository = productRepository;
            _taxRepository = taxRepository;
            _discountRepository = discountRepository;
            _attendantRepository = attendantRepository;
            _configurationRepository = configurationRepository;
            _comissionRepository = comissionRepository;
            _integrationService = integrationService;
            _locationRepository = locationRepository;
            _commandHandler = commandHandler;
            _mapper = mapper;
        }

        #region [ Methods ]

        public OrderValueObject AddItem(Guid orderId, OrderItemValueObject item)
        {
            var order = _orderRepository.Get(orderId);

            order.ClearItems();

            Attendant attendant;

            if (item.AttendantId != Guid.Empty)
            {
                attendant = _attendantRepository.Get(item.AttendantId);

                if (attendant == null)
                    throw new AttendantDoesntExistsException();

            }
            else
                attendant = order.Attendant;

            var product = _productRepository.Get(item.ItemId);
            if (product != null)
            {                
                var orderProduct = order.AddItem(product, item.Quantity, item.Description, attendant);                
                if(product.IsProduction)
                    _commandHandler.Handle(new CreateProductionCommand(order.Ticket.Number, product.Id, product.Name, item.Quantity, 
                        item.Description, DateTime.Now, orderProduct.Id, orderId, attendant.Id));
            }

            var tax = _taxRepository.Get(item.ItemId);
            if (tax != null)
            {
                order.AddItem(tax, item.Quantity, null, attendant);
            }

            var discount = _discountRepository.Get(item.ItemId);
            if (discount != null)
            {
                order.AddItem(discount, item.Quantity, null, attendant);
            }

            if (product == null && tax == null && discount == null)
                throw new OrderItemDoesntExistsException();

            var insertedOrder = _orderRepository.SaveItems(order);

            return _mapper.Map<OrderValueObject>(insertedOrder);

        }

        public OrderValueObject RemoveItem(Guid orderId, Guid itemId, decimal quantity)
        {
            var order = _orderRepository.Get(orderId);

            var product = _productRepository.Get(itemId);
            if (product != null)
            {
                order.RemoveItem(product, quantity);
            }

            var tax = _taxRepository.Get(itemId);
            if (tax != null)
            {
                _orderRepository.RemoveTax(order.OrderTaxes.Where(o => o.Tax.Id == tax.Id).FirstOrDefault());
                order.RemoveItem(tax, quantity);                
            }

            var discount = _discountRepository.Get(itemId);
            if (discount != null)
            {
                order.RemoveItem(discount, quantity);
            }

            if (product == null && tax == null && discount == null)
                throw new OrderItemDoesntExistsException();

            var newOrder = _orderRepository.Update(order);
            
            return _mapper.Map<OrderValueObject>(order);

        }

        public OrderValueObject AddPayment(Guid orderId, PaymentValueObject paymentVO)
        {
            var order = _orderRepository.Get(orderId);

            var attendant = _attendantRepository.Get(paymentVO.AttendantId);

            var payment = new Payment(paymentVO.Value, paymentVO.PaymentState, paymentVO.PaymentType, attendant);

            order.AddPayment(payment);

            _orderRepository.SavePayment(payment, order.Id);

            return _mapper.Map<OrderValueObject>(order);
        }

        public OrderValueObject Get(Guid orderId)
        {
            var order = _orderRepository.Get(orderId);

            if (order == null)
                throw new OrderItemDoesntExistsException();

            return _mapper.Map<OrderValueObject>(order);
        }
        
        public IEnumerable<OrderValueObject> GetAll()
        {
            return _mapper.Map<IEnumerable<OrderValueObject>>(_orderRepository.GetAll());
        }

        public OrderValueObject AddItems(Guid orderId, List<OrderItemValueObject> items)
        {
            var order = _orderRepository.Get(orderId);

            var total = order.Total;

            order.ClearItems();

            foreach (var item in items)
            {
                Attendant attendant;

                total += order.Total;

                if (item.AttendantId != Guid.Empty)
                {
                    attendant = _attendantRepository.Get(item.AttendantId);

                    if (attendant == null)
                        throw new AttendantDoesntExistsException();

                }
                else
                    attendant = order.Attendant;

                var product = _productRepository.Get(item.ItemId);
                if (product != null)
                {
                    var orderProduct = order.AddItem(product, item.Quantity, item.Description, attendant);
                    if (item.ComissionId.HasValue)
                    {
                        var comission = _comissionRepository.Get(item.ComissionId.Value);
                        if (comission != null)
                            ((OrderProduct)orderProduct).AddComission(comission);
                    }

                    if (product.IsProduction)
                        _commandHandler.Handle(new CreateProductionCommand(order.Ticket.Number, product.Id, product.Name, item.Quantity, 
                            item.Description, DateTime.Now, orderProduct.Id, orderId, attendant.Id));
                }

                var tax = _taxRepository.Get(item.ItemId);
                if (tax != null)
                {
                    order.AddItem(tax, item.Quantity,null,attendant);
                }

                var discount = _discountRepository.Get(item.ItemId);
                if (discount != null)
                {
                    if (total < discount.Value)
                        throw new OrderCannotAddDiscountException();

                    order.AddItem(discount, item.Quantity, null, attendant);
                }

                if (product == null && tax == null && discount == null)
                    throw new OrderItemDoesntExistsException();
            }

            var insertedOrder = _orderRepository.SaveItems(order);

            return _mapper.Map<OrderValueObject>(insertedOrder);
        }

        public OrderValueObject AttachCustomerCpf(Guid orderId, string cpf)
        {
            var order = _orderRepository.Get(orderId);

            order.AttachCustomer(cpf);

            _orderRepository.Update(order);

            return _mapper.Map<OrderValueObject>(order);
        }

        public OrderProductValueObject FinalizeOrderProduct(Guid orderId, Guid orderProductId)
        {
            var order = _orderRepository.Get(orderId);

            var orderProduct = order.OrderProducts.FirstOrDefault(op => op.Id == orderProductId);

            orderProduct.FinalizeOrder();

            _commandHandler.Handle(new DeleteProductionCommand(orderProduct.Id));

            _orderRepository.UpdateOrderProduct(orderProduct);

            return _mapper.Map<OrderProductValueObject>(orderProduct);
        }

        public OrderValueObject AddPaymentIntegration(Guid orderId, PaymentValueObject paymentVO)
        {
            var configuration = _configurationRepository.Get();

            var endpointPre = configuration.Endpoints.Where(e => e.Pipeline == PipelineEnum.Pre);
            var endpointPos = configuration.Endpoints.Where(e => e.Pipeline == PipelineEnum.Pos);

            var order = _orderRepository.Get(orderId);

            PaymentIntegration(order, paymentVO.PaymentType, endpointPre.ToList());

            var attendant = _attendantRepository.Get(paymentVO.AttendantId);

            var payment = new Payment(paymentVO.Value, paymentVO.PaymentState, paymentVO.PaymentType, attendant);

            order.AddPayment(payment);

            _orderRepository.SavePayment(payment, order.Id);

            PaymentIntegration(order, paymentVO.PaymentType, endpointPos.ToList());

            return _mapper.Map<OrderValueObject>(order);
        }

        private void PaymentIntegration(Orders order, PaymentTypeEnum paymentType, List<ConfigurationEndpoint> endpointPre)
        {
            foreach (var endpoint in endpointPre)
            {
                List<PaymentIntegrationValueObject> paymentsIntegration = new List<PaymentIntegrationValueObject>();

                List<PaymentIntegrationValueObject.ProdutoIntegrationValueObject> produtos = new List<PaymentIntegrationValueObject.ProdutoIntegrationValueObject>();

                foreach (var product in order.OrderProducts)
                {
                    produtos.Add(new PaymentIntegrationValueObject.ProdutoIntegrationValueObject()
                    {
                        Id = product.Product.Id.ToString(),
                        Descricao = product.Product.Name,
                        Quantidade = Convert.ToInt32(product.Count),
                        Total = product.Total,
                        ValorUnitario = product.Value,
                        DadosComplementares = product.Product.MetaData
                    });
                }
                
                paymentsIntegration.Add(new PaymentIntegrationValueObject()
                {
                    CodigoPedido = order.Id,
                    DataPedido = order.DateOpened,
                    TipoPagamento = (int)paymentType,
                    Acrescimo = 0,
                    ValorTotal = order.Total,
                    Desconto = order.TotalDiscount,
                    AtendenteId = order.Attendant.Id,
                    LocalId = _locationRepository.GetAll().FirstOrDefault().Id,
                    produtos = produtos
                });

                _integrationService.PaymentIntegration(endpoint.Endpoint, endpoint.ContinueOnError, paymentsIntegration);
            }
        }

        #endregion
    }
}
