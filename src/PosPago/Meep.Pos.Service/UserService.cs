﻿using Meep.Pos.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using AutoMapper;
using Meep.Pos.Domain.Core.Exception.User;
using Meep.Pos.Domain.Models;
using System.Linq;

namespace Meep.Pos.Service
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepository;
        private IMapper _mapper;
        public UserService(IUserRepository userRepository,
                            IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }
        public UserValueObject ChangeEmail(string login, string password, string oldEmail, string newEmail)
        {
            if (_userRepository.GetByFilter(u => u.Email == newEmail).Any())
                throw new UserNewEmailAlreadyExistsException();

            var user = _userRepository.GetByFilter(u => u.Login == login).FirstOrDefault();

            if (user == null)
                throw new UserDoesntExistsException();

            user.ChangeEmail(login, password, oldEmail, newEmail);

            return _mapper.Map<UserValueObject>(user);
        }

        public UserValueObject ChangePassword(string login, string oldPassword, string newPassword)
        {
            var user = _userRepository.GetByFilter(u => u.Login == login).FirstOrDefault();

            if (user == null)
                throw new UserDoesntExistsException();

            user.ChangePassword(login, oldPassword, newPassword);

            return _mapper.Map<UserValueObject>(user);
        }

        public UserValueObject Create(UserValueObject userVO)
        {
            if (_userRepository.GetByFilter(c => c.Login == userVO.Login).Any())
                throw new UserAlreadyExistsException();

            var user = new AppUser(userVO.Email, userVO.Login, userVO.Password, userVO.Name, userVO.Cpf, userVO.Sex);

            _userRepository.Insert(user);

            var insertedUserVO = _mapper.Map<UserValueObject>(user);

            return insertedUserVO;
        }

        public UserValueObject Get(Guid userId)
        {
            var user = _userRepository.Get(userId);

            if (user == null)
                throw new UserDoesntExistsException();

            return _mapper.Map<UserValueObject>(user);
        }

        public IEnumerable<UserValueObject> GetAll()
        {
            var users = _userRepository.GetAll();

            return _mapper.Map<List<UserValueObject>>(users);
        }

        public bool PasswordIsCorrect(string login, string password)
        {
            var user = _userRepository.GetByFilter(u => u.Login == login).FirstOrDefault();

            if (user == null)
                throw new UserDoesntExistsException();

            if (user.PasswordIsCorrect(password))
                return true;

            return false;
        }

        public void Remove(Guid userId)
        {
            if (!_userRepository.Remove(userId))
                throw new UserDoesntExistsException();
        }

        public UserValueObject Update(UserValueObject userVO)
        {
            if (!userVO.Id.HasValue)
                throw new UserIdRequiredException();
            //Recupera a categoria pelo Id
            var attendant = _userRepository.Get(userVO.Id.Value);

            //Passa as alterações para a entidade
            _mapper.Map(userVO, attendant);

            //Faz o update
            var updatedAttendant = _userRepository.Update(attendant);

            return _mapper.Map<UserValueObject>(updatedAttendant);
        }

        public UserValueObject GetByLogin(string login)
        {
            var user = _userRepository.GetByFilter(u => u.Login == login).FirstOrDefault();

            if (user == null)
                throw new UserDoesntExistsException();

            return _mapper.Map<UserValueObject>(user);
        }
    }
}
