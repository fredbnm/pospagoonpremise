﻿using Meep.Pos.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using System.Linq;
using AutoMapper;
using Meep.Pos.Service.ValueObjects;

namespace Meep.Pos.Service
{
    public class ProductionService : IProductionService
    {
        IProductionRepository _productionRepository;
        IMapper _mapper;
        public ProductionService(IProductionRepository productionRepository,
            IMapper mapper)
        {
            _productionRepository = productionRepository;
            _mapper = mapper;
        }

        public void Delete(Guid id)
        {
            if (!_productionRepository.Delete(id))
                throw new Exception();
        }

        public List<ProductionValueObject> GetAll()
        {
            var productions = _productionRepository.GetAll();

            productions = productions.OrderByDescending(p => p.Date).ToList();

            var productionsVo = _mapper.Map<List<ProductionValueObject>>(productions);

            return productionsVo;
        }
        
    }
}
