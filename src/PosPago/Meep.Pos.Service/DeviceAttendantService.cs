﻿using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Service.ValueObjects;
using AutoMapper;
using Meep.Pos.Domain.Core.Exception.Device;
using Meep.Pos.Domain.Core.Exception.Attendant;
using System.Linq;
using Meep.Pos.Domain.Command.Interface;
using Meep.Pos.Domain.Models;
using Meep.Pos.Domain.Command.Command.Device;

namespace Meep.Pos.Service
{
    public class DeviceAttendantService : IDeviceAttendantService
    {
        private IDeviceAttendantRepository _deviceAttendantRepository;
        private IAttendantRepository _attendantRepository;
        private IDeviceRepository _deviceRepository;
        private ICommandHandler _commandHandler;
        private IMapper _mapper;

        public DeviceAttendantService(IDeviceAttendantRepository deviceAttendantRepository,
            IAttendantRepository attendantRepository,
            IDeviceRepository deviceRepository,
            ICommandHandler commandHandler,
            IMapper mapper)
        {
            _deviceAttendantRepository = deviceAttendantRepository;
            _deviceRepository = deviceRepository;
            _attendantRepository = attendantRepository;
            _commandHandler = commandHandler;
            _mapper = mapper;
        }

        public DeviceAttendantValueObject ChangeDeviceStatus(Guid id, bool isActive)
        {
            var deviceAttendant = _deviceAttendantRepository.Get(id);

            if (deviceAttendant == null)
                throw new DeviceDoesntExistsException();

            deviceAttendant.SwitchStatus(isActive);

            _deviceAttendantRepository.Update(deviceAttendant);

            return _mapper.Map<DeviceAttendantValueObject>(deviceAttendant);
        }

        public DeviceAttendantValueObject Get(Guid id)
        {
            var device = _deviceAttendantRepository.Get(id);

            if (device == null)
                throw new DeviceDoesntExistsException();

            return _mapper.Map<DeviceAttendantValueObject>(device);
        }

        public List<DeviceAttendantValueObject> GetAll()
        {
            return _mapper.Map<List<DeviceAttendantValueObject>>(_deviceAttendantRepository.GetAll());
        }

        public DeviceAttendantValueObject LoginByDevice(int pinCode, string fingerprint, string userAgent, string resolution)
        {
            DeviceAttendant deviceAttendant = null;

            var attendant = _attendantRepository.GetByFilter(att => att.PinCode == pinCode).FirstOrDefault();

            if (attendant == null)
                throw new AttendantDoesntExistsException();

            var device = _deviceRepository.GetByFilter(dev => dev.Fingerprint == fingerprint).FirstOrDefault();

            if (device == null)
            {
                _commandHandler.Handle(new CreateDeviceAttendantCommand(fingerprint, userAgent, resolution, attendant.Id, attendant.Name));
                throw new DeviceNotAuthorizedException();
            }
            else
            {
                deviceAttendant = _deviceAttendantRepository.GetByFilter(devatt => devatt.Attendant.Id == attendant.Id && devatt.Device.Id == device.Id).FirstOrDefault();

                if (deviceAttendant == null)
                {
                    _commandHandler.Handle(new CreateDeviceAttendantCommand(fingerprint, userAgent, resolution, attendant.Id, attendant.Name, device.Id));
                    throw new DeviceNotAuthorizedException();
                }

                if (!deviceAttendant.IsActive)
                    throw new DeviceNotAuthorizedException();
            }

            return _mapper.Map<DeviceAttendantValueObject>(deviceAttendant);
            
        }

        public DeviceAttendantValueObject GetByDeviceAndAttendant(string fingerprint, int pinCode)
        {
            var deviceAttendant = _deviceAttendantRepository.GetByFilter(devatt => devatt.Device.Fingerprint == fingerprint && devatt.Attendant.PinCode == pinCode).FirstOrDefault();

            if (deviceAttendant == null)
                throw new DeviceDoesntExistsException();

            return _mapper.Map<DeviceAttendantValueObject>(deviceAttendant);
        }

        public void Remove(Guid id)
        {
            if (!_deviceAttendantRepository.Remove(id))
                throw new DeviceDoesntExistsException();
        }
    }
}
