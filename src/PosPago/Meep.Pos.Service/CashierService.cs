﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.Domain.Core.Exception.Cashier;
using Meep.Pos.Domain.Core.Exception.Order;
using Meep.Pos.Domain.Core.Exception.Ticket;
using System.Linq.Expressions;

namespace Meep.Pos.Service
{
    public class CashierService : ICashierService
    {
        private IAttendantRepository _attendantRepository;
        private ITicketRepository _ticketRepository;
        private ICashierRepository _cashierRepository;
        private IOrderRepository _orderRepository;
        private IUserRepository _userRepository;
        private ITaxRepository _taxRepository;
        private ITicketService _ticketService;
        private IMapper _mapper;

        public CashierService(IAttendantRepository attendantRepository,
            ICashierRepository cashierRepository,
            ITicketRepository ticketRepository,

            IOrderRepository orderRepository,
            IUserRepository userRepository,
            ITaxRepository taxRepository,
            ITicketService ticketService,
            IMapper mapper)
        {
            _ticketService = ticketService;
            _attendantRepository = attendantRepository;
            _cashierRepository = cashierRepository;
            _ticketRepository = ticketRepository;
            _orderRepository = orderRepository;
            _userRepository = userRepository;
            _taxRepository = taxRepository;
            _mapper = mapper;
        }

        #region [ Methods ]
        public CashierValueObject OpenCashier(Guid userId, decimal initialValue)
        {
            //TODO: REfatorar esse teste.
            if (_cashierRepository.GetByFilter(c => (c.DateOpened.Date == DateTime.Now.Date || c.DateClosed > DateTime.Now)
                                                        && c.DateClosed == DateTime.MinValue).Any())
                throw new CashierAlreadyOpenedException();

            var user = _userRepository.Get(userId);

            var cashier = new Cashier();

            cashier.Open(user, initialValue);

            var insertedCashier = _cashierRepository.Insert(cashier);

            return _mapper.Map<CashierValueObject>(insertedCashier);
        }

        public CashierValueObject CloseCashier(Guid cashierId, Guid userId)
        {
            var user = _userRepository.Get(userId);

            var cashier = _cashierRepository.Get(cashierId);

            cashier.Orders = _orderRepository.GetByCashierId(cashier.Id);

            cashier.Close(user);

            _cashierRepository.Update(cashier);

            return _mapper.Map<CashierValueObject>(cashier);
        }

        public CashierValueObject CloseCashier(Guid userId)
        {
            var user = _userRepository.Get(userId);

            var cashier = _cashierRepository.GetCurrent();

            cashier.Orders = _orderRepository.GetByCashierId(cashier.Id);

            cashier.Close(user);

            _cashierRepository.Update(cashier);

            return _mapper.Map<CashierValueObject>(cashier);
        }

        public CashierValueObject Get(Guid cashierId)
        {
            var cashier = _cashierRepository.Get(cashierId);

            cashier.Orders = _orderRepository.GetByCashierId(cashier.Id);

            if (cashier == null)
                throw new CashierDoesntExistsException();

            return _mapper.Map<CashierValueObject>(cashier);
        }

        public IEnumerable<CashierValueObject> GetAll()
        {
            return _mapper.Map<IEnumerable<CashierValueObject>>(_cashierRepository.GetAll());
        }

        public CashierValueObject GetByDate(CashierValueObject filter)
        {
            var cashier = _cashierRepository.CashierReport(filter.DateOpened).FirstOrDefault();

            cashier.Orders = _orderRepository.GetByCashierId(cashier.Id);

            return _mapper.Map<CashierValueObject>(cashier);
        }

        public OrderValueObject ExpressOrder(Guid attendantId)
        {
            var cashier = _cashierRepository.GetCurrent();

            if (cashier == null)
                throw new CashierDoesntExistsException();

            var ticketVo = _ticketService.GenerateTicket();
            
            var ticket = _ticketRepository.Get(ticketVo.Id.Value);

            var attendant = _attendantRepository.Get(attendantId);

            var order = new Orders(ticket, attendant);

            _orderRepository.OpenOrder(order, cashier.Id);

            cashier.StartOrder(order);

            return _mapper.Map<OrderValueObject>(order);
        }


        public OrderValueObject OpenOrder(Guid attendantId, string ticketNumber)
        {
            var cashier = _cashierRepository.GetCurrent();

            if (cashier == null)
                throw new CashierMustBeOpenedException();

            var existingOrder = _orderRepository.GetOrderOnCashierByTicket(cashier, ticketNumber);

            if (existingOrder != null)
                return _mapper.Map<OrderValueObject>(existingOrder);

            var attendant = _attendantRepository.Get(attendantId);

            var ticket = _ticketRepository.GetByFilter(t => t.Number == ticketNumber).FirstOrDefault();

            if (ticket == null)
                throw new TicketDoesntExistsException();

            var order = new Orders(ticket, attendant);

            _orderRepository.OpenOrder(order, cashier.Id);

            var requiredTaxes = _taxRepository.GetByFilter(t => t.Required);

            if (requiredTaxes.Any())
            {
                foreach (var tax in requiredTaxes)
                {
                    order.AddItem(tax);
                }

                _orderRepository.SaveItems(order);
            }

            cashier.StartOrder(order);

            return _mapper.Map<OrderValueObject>(order);
        }

        public CashierValueObject CloseOrder(Guid orderId)
        {
            var cashier = _cashierRepository.GetCurrent();

            if (cashier == null)
                throw new CashierMustBeOpenedException();

            var order = _orderRepository.Get(orderId);

            order.Close();

            _orderRepository.Update(order);

            return _mapper.Map<CashierValueObject>(cashier);
        }


        public CashierValueObject OpenOrder(Guid cashierId, Guid attendantId, Guid ticketId)
        {
            var cashier = _cashierRepository.Get(cashierId);

            if (cashier.Orders.Any(o => o.Ticket.Id == ticketId && o.DateClosed == DateTime.MinValue))
                throw new OrderAlreadyOpenedException();

            var attendant = _attendantRepository.Get(attendantId);

            var ticket = _ticketRepository.Get(ticketId);

            var order = new Orders(ticket, attendant);

            _orderRepository.OpenOrder(order, cashier.Id);

            var requiredTaxes = _taxRepository.GetByFilter(t => t.Required);

            if (requiredTaxes.Any())
            {
                foreach (var tax in requiredTaxes)
                {
                    order.AddItem(tax);
                }

                _orderRepository.SaveItems(order);
            }

            cashier.StartOrder(order);

            return _mapper.Map<CashierValueObject>(cashier);
        }

        public CashierValueObject CloseOrder(Guid cashierId, Guid orderId)
        {
            var cashier = _cashierRepository.Get(cashierId);

            var order = _orderRepository.Get(orderId);

            order.Close();

            _orderRepository.Update(order);

            //_cashierRepository.Update(cashier);

            return _mapper.Map<CashierValueObject>(cashier);
        }

        public OrderValueObject GetOrder(Guid cashierId, Guid orderId)
        {
            var cashier = _cashierRepository.Get(cashierId);

            return _mapper.Map<OrderValueObject>(cashier.GetOrder(orderId));
        }

        public CashierValueObject GetCurrent()
        {
            var cashier = _cashierRepository.GetCurrent();

            if (cashier == null)
                throw new CashierDoesntExistsException();

            cashier.Orders = _orderRepository.GetByCashierId(cashier.Id);
            
            return _mapper.Map<CashierValueObject>(cashier);
        }

        public CashierValueObject GetCurrentCashierWithOrders()
        {
            var cashier = _cashierRepository.GetCurrent();

            if (cashier == null)
                return _mapper.Map<CashierValueObject>(null);

            cashier.Orders = _orderRepository.GetByCashierId(cashier.Id);

            return _mapper.Map<CashierValueObject>(cashier);
        }
        #endregion
    }
}