﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.Domain.Core.Exception.Category;

namespace Meep.Pos.Service
{
    public class CategoryService : ICategoryService
    {
        private ICategoryRepository _categoryRepository;
        private IMapper _mapper;

        public CategoryService(ICategoryRepository categoryRepository,
                                IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }

        #region [ Methods ]
        public CategoryValueObject Create(CategoryValueObject categoryVO)
        {
            if (_categoryRepository.GetByFilter(c => c.Name == categoryVO.Name).Any())
                throw new CategoryAlreadyExistsException();

            var category = new Category(categoryVO.Name);

            _categoryRepository.Insert(category);

            var insertedCategoryVO = _mapper.Map<CategoryValueObject>(category);

            return insertedCategoryVO;
        }

        public void Remove(Guid categoryId)
        {
            if (!_categoryRepository.Remove(categoryId))
                throw new CategoryDoesntExistsException();
        }

        public CategoryValueObject Get(Guid categoryId)
        {
            var category = _categoryRepository.Get(categoryId);

            if (category == null)
                throw new CategoryDoesntExistsException();

            return _mapper.Map<CategoryValueObject>(category);
        }

        public CategoryValueObject Update(CategoryValueObject categoryVO)
        {
            if (!categoryVO.Id.HasValue)
                throw new CategoryIdRequiredException();
            //Recupera a categoria pelo Id
            var category = _categoryRepository.Get(categoryVO.Id.Value);

            //Passa as alterações para a entidade
            _mapper.Map(categoryVO, category);

            //Faz o update
            var updatedCategory = _categoryRepository.Update(category);

            return _mapper.Map<CategoryValueObject>(updatedCategory);
        }

        public IEnumerable<CategoryValueObject> GetAll()
        {
            var categories = _categoryRepository.GetAll();

            return _mapper.Map<List<CategoryValueObject>>(categories);
        }

        #endregion
    }
}
