﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.Domain.Core.Exception.Ticket;

namespace Meep.Pos.Service
{
    public class TicketService : ITicketService
    {
        private ITicketRepository _ticketRepository;
        private IMapper _mapper;
        public TicketService(ITicketRepository ticketRepository,
               IMapper mapper)
        {
            _ticketRepository = ticketRepository;
            _mapper = mapper;
        }

        #region [ Methods ]
        public TicketValueObject Create(TicketValueObject ticketVO)
        {
            if (_ticketRepository.GetByFilter(c => c.Number == ticketVO.Number).Any())
                throw new TicketAlreadyExistsException();

            var ticket = _mapper.Map<Ticket>(ticketVO);

            _ticketRepository.Insert(ticket);
            
            return _mapper.Map<TicketValueObject>(ticket); 
        }

        public void Remove(Guid ticketId)
        {
            if (!_ticketRepository.Remove(ticketId))
                throw new TicketDoesntExistsException();
        }

        public TicketValueObject Get(Guid ticketId)
        {
            var ticket = _ticketRepository.Get(ticketId);

            if (ticket == null)
                throw new TicketDoesntExistsException();

            return _mapper.Map<TicketValueObject>(ticket);
        }

        public TicketValueObject Update(TicketValueObject ticketVO)
        {
            if (!ticketVO.Id.HasValue)
                throw new TicketIdRequiredException();
            //Recupera a categoria pelo Id
            var ticket = _ticketRepository.Get(ticketVO.Id.Value);

            //Passa as alterações para a entidade
            _mapper.Map(ticketVO, ticket);

            //Faz o update
            var updatedTicket = _ticketRepository.Update(ticket);

            return _mapper.Map<TicketValueObject>(updatedTicket);
        }

        public IEnumerable<TicketValueObject> GetAll()
        {
            var tickets = _ticketRepository.GetAll();

            return _mapper.Map<List<TicketValueObject>>(tickets);
        }

        public TicketValueObject GetByNumber(string number)
        {
            var ticket = _ticketRepository.GetByFilter(t => t.Number == number);

            if (ticket == null)
                throw new TicketDoesntExistsException();

            return _mapper.Map<TicketValueObject>(ticket);
        }

        public TicketValueObject GenerateTicket()
        {
            var ticket = Ticket.Express();


            while (_ticketRepository.GetByFilter(t => t.Equals(ticket.Number)).Any())
                ticket.NewNumber();

            _ticketRepository.Insert(ticket);

            return _mapper.Map<TicketValueObject>(ticket);
        }

        #endregion


    }
}
