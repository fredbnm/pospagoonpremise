﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;

namespace Meep.Pos.Service.Mapping
{
    public class AutoMapperConfig
    {
        public static MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new DomainToValueObjectProfile());
                cfg.AddProfile(new ValueObjectToDomainProfile());
            });
        }
    }
}
