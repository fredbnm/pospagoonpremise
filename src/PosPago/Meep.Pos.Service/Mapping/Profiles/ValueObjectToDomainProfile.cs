﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Meep.Pos.Domain.Models;
using Meep.Pos.Service.ValueObjects;

namespace Meep.Pos.Service.Mapping
{
    public class ValueObjectToDomainProfile : Profile
    {
        public ValueObjectToDomainProfile()
        {
            CreateMap<ProductValueObject, Product>();
            CreateMap<CategoryValueObject, Category>();
            CreateMap<TaxValueObject, Tax>();
            CreateMap<DiscountValueObject, Discount>();
            CreateMap<AttendantValueObject, Attendant>().ConstructUsing(c =>
                            new Attendant(
                                c.Name, 
                                c.Cpf, 
                                c.Sex
                            ));
            CreateMap<UserValueObject, AppUser>().ConstructUsing(c =>
                            new AppUser(
                                c.Email, 
                                c.Login,
                                c.Password,
                                c.Name,
                                c.Cpf,
                                c.Sex
                                ));
            CreateMap<CashierValueObject, Cashier>();
            CreateMap<TicketValueObject, Ticket>();
            CreateMap<LocationValueObject, Location>();
            CreateMap<OrderValueObject, Orders>();
            CreateMap<PaymentValueObject, Payment>();
            CreateMap<ConfigurationValueObject, Configuration>();
            CreateMap<ConfigurationEndpointValueObject, ConfigurationEndpoint>();
            CreateMap<ComissionValueObject, Comission>();
        }
    }
}
