﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Meep.Pos.Domain.Models;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.Domain;
using Meep.Pos.Service.ValueObjects.Integration;

namespace Meep.Pos.Service.Mapping
{
    public class DomainToValueObjectProfile : Profile
    {
        public DomainToValueObjectProfile()
        {
            CreateMap<Product, ProductValueObject>()
                .ConstructUsing(c =>
                new ProductValueObject()
                {
                    Name = c.Name,
                    CategoryId = c.Category.Id,
                    CategoryName = c.Category.Name,
                    Id = c.Id,
                    Price = c.Price,
                    UnitEnum = c.Unit,
                    IsActive = c.IsActive,
                    IsProduction = c.IsProduction,
                    ProductionTime = c.ProductionTime,
                    MetaData = c.MetaData
                });

            CreateMap<Category, CategoryValueObject>();

            CreateMap<Tax, TaxValueObject>();

            CreateMap<Discount, DiscountValueObject>();
            CreateMap<Attendant, AttendantValueObject>();

            CreateMap<Cashier, CashierValueObject>().ConstructUsing(c =>
            new CashierValueObject()
            {
                CashierOpenerId = c.CashierOpener?.Id,
                CashierCloserId = c.CashierCloser?.Id,
                CashierOpenerName = c.CashierOpener?.Name,
                CashierCloserName = c.CashierCloser?.Name,
                InitialValue = c.InitialValue,
                Id = c.Id,
                Total = c.Total,
                DateOpened = c.DateOpened,
                DateClosed = c.DateClosed,
                OrdersId = c.Orders.Select(o => o.Id).ToArray(),
                IsOpen = (c.DateOpened != DateTime.MinValue && c.DateClosed == DateTime.MinValue),
                Orders = c.Orders.Select<Orders, OrderValueObject>(order =>
                {
                    return ConstructOrder(order);
                }).ToList()
            });

            CreateMap<Ticket, TicketValueObject>();
            CreateMap<Location, LocationValueObject>();

            CreateMap<Orders, OrderValueObject>().ConstructUsing(order =>
                ConstructOrder(order));

            CreateMap<Payment, PaymentValueObject>().ConstructUsing(c =>
            new PaymentValueObject()
            {
                Id = c.Id,
                Value = c.Value,
                AttendantId = c.Attendant.Id,
                PaymentState = c.PaymentState,
                PaymentType = c.PaymentType
            });

            CreateMap<AppUser, UserValueObject>();

            CreateMap<DeviceAttendant, DeviceAttendantValueObject>().ConstructUsing(device =>
            new DeviceAttendantValueObject
            {
                Id = device.Id,
                AttendantId = device.Attendant.Id,
                AttendantName = device.Attendant.Name,
                DeviceName = device.Device.UserAgent,
                DeviceId = device.Device.Id,
                IsActive = device.IsActive
            });

            CreateMap<Production, ProductionValueObject>().ConstructUsing(production =>
            new ProductionValueObject
            {
                Id = production.Id,
                IsFinalized = production.IsFinalized,
                Observation = production.Observation,
                IdOrder = production.OrderId,
                ProductName = production.Product.Name,
                Quantity = production.Quantity,
                TicketNumber = production.Ticket.Number,
                Date = production.Date,
                CategoryId = production.Product.Category.Id,
                CategoryName = production.Product.Category.Name,
                ProductionTime = production.Product.ProductionTime,
                IdOrderItem = production.OrderItemId,
                AttendantId = production.Attendant.Id,
                AttendantName = production.Attendant.Name
            });

            CreateMap<Event, EventValueObject>();

            CreateMap<OrderProduct, OrderProductValueObject>().ConstructUsing(op =>
            new OrderProductValueObject()
            {
                Value = op.Value,
                Count = op.Count,
                Total = op.Total,
                DateOrder = op.DateOrder,
                ProductId = op.Product.Id,
                ProductName = op.Product.Name,
                Description = op.Description,
                AttendantId = op.Attendant != null ? op.Attendant.Id : Guid.Empty,
                AttendantName = op.Attendant != null ? op.Attendant.Name : ""
            });

            CreateMap<Configuration, ConfigurationValueObject>();

            CreateMap<ConfigurationEndpoint, ConfigurationEndpointValueObject>();
            CreateMap<Comission, ComissionValueObject>();

            //CreateMap<OrderProduct, PaymentIntegrationValueObject>().ConstructUsing(o => new PaymentIntegrationValueObject()
            //{
            //    codigoPedido = o.Id,
            //    dataPedido = o.DateOrder,
            //    tipoPagamento = o.Paymen
            //})
        }

        private OrderValueObject ConstructOrder(Orders order)
        {
            return new OrderValueObject()
            {
                Id = order.Id,
                Total = order.Total,

                Attendant = order.Attendant != null ? new AttendantValueObject()
                {
                    Id = order.Attendant.Id,
                    Name = order.Attendant.Name,
                    Cpf = order.Attendant.Cpf,
                    PinCode = order.Attendant.PinCode,
                    Sex = order.Attendant.Sex
                } : null,

                Products = order.OrderProducts.Select(op =>
                    new OrderProductValueObject()
                    {
                        Value = op.Value,
                        Count = op.Count,
                        Total = op.Total,
                        DateOrder = op.DateOrder,
                        ProductId = op.Product.Id,
                        ProductName = op.Product.Name,
                        Description = op.Description,
                        AttendantId = op.Attendant != null ? op.Attendant.Id : Guid.Empty,
                        AttendantName = op.Attendant != null ? op.Attendant.Name : "",
                        ComissionValue = op.ComissionValue
                    }).ToList(),

                Discounts = order.OrderDiscounts.Select(od =>
                    new OrderDiscountValueObject()
                    {
                        Value = od.Value,
                        Count = od.Count,
                        Total = od.Total,
                        DateOrder = od.DateOrder,
                        DiscountName = od.Discount.Description,
                        DiscountId = od.Discount.Id
                    }).ToList(),

                Taxes = order.OrderTaxes.Select(ot =>
                    new OrderTaxValueObject()
                    {
                        Value = ot.Value,
                        Count = ot.Count,
                        Total = ot.Total,
                        DateOrder = ot.DateOrder,
                        TaxName = ot.Tax.Description,
                        TaxId = ot.Tax.Id,
                        TaxCalcType = ot.Tax.CalcType,
                        Required = ot.Tax.Required
                    }).ToList(),

                Payments = order.Payments.Select(p =>
                    new PaymentValueObject()
                    {
                        Id = p.Id,
                        Value = p.Value,
                        PaymentState = p.PaymentState,
                        PaymentType = p.PaymentType,
                        AttendantId = p.Attendant.Id
                    }).ToList(),
                TotalPayment = order.TotalPayment,
                DateOpened = order.DateOpened,
                DateClosed = order.DateClosed,
                TicketId = order.Ticket.Id,
                TicketNumber = order.Ticket?.Number,
                IsOpen = order.IsOpen
            };
        }

    }
}
