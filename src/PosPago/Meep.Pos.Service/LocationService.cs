﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.Domain.Core.Exception.Location;
using Meep.Pos.Domain.Core.Exception.Attendant;

namespace Meep.Pos.Service
{
    public class LocationService : ILocationService
    {
        private ILocationRepository _locationRepository;
        private IMapper _mapper;
        public LocationService(ILocationRepository locationRepository,
            IMapper mapper)
        {
            _locationRepository = locationRepository;
            _mapper = mapper;
        }

        #region [ Methods ]
        public LocationValueObject Create(LocationValueObject locationVO)
        {
            if (_locationRepository.GetByFilter(c => c.ZipCode == locationVO.ZipCode).Any())
                throw new LocationAlreadyExistsException();

            var location = _mapper.Map<Location>(locationVO);

            _locationRepository.Insert(location);
            
            return _mapper.Map<LocationValueObject>(location);
        }

        public void Remove(Guid locationId)
        {
            if (!_locationRepository.Remove(locationId))
                throw new LocationDoesntExistsException();
        }

        public LocationValueObject Get(Guid locationId)
        {
            var location = _locationRepository.Get(locationId);

            if (location == null)
                throw new LocationDoesntExistsException();

            return _mapper.Map<LocationValueObject>(location);
        }

        public LocationValueObject Update(LocationValueObject locationVO)
        {
            if (!locationVO.Id.HasValue)
                throw new LocationIdRequiredException();

            //Recupera a categoria pelo Id
            var location = _locationRepository.Get(locationVO.Id.Value);

            //Passa as alterações para a entidade
            _mapper.Map(locationVO, location);

            //Faz o update
            var updatedLocation = _locationRepository.Update(location);

            return _mapper.Map<LocationValueObject>(updatedLocation);
        }

        public IEnumerable<LocationValueObject> GetAll()
        {
            var locations = _locationRepository.GetAll();

            return _mapper.Map<List<LocationValueObject>>(locations);
        }

        public LocationValueObject CreateOrGet(LocationValueObject locationVO)
        {
            var locationSelected = _locationRepository.GetByFilter(c => c.ZipCode == locationVO.ZipCode).FirstOrDefault();

            if (locationSelected != null)
                return _mapper.Map<LocationValueObject>(locationSelected);

            var location = _mapper.Map<Location>(locationVO);

            _locationRepository.Insert(location);

            return _mapper.Map<LocationValueObject>(location);
        }

        #endregion
    }
}
