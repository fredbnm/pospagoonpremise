﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.Domain.Core.Exception.Attendant;

namespace Meep.Pos.Service
{
    public class AttendantService : IAttendantService
    {
        private IAttendantRepository _attendantRepository;
        private IMapper _mapper;

        public AttendantService(IAttendantRepository attendantRepository, 
                            IMapper mapper)
        {
            _attendantRepository = attendantRepository;
            _mapper = mapper;
        }

        #region [ Methods ]
        
        public AttendantValueObject Create(AttendantValueObject attendantVO)
        {
            if (_attendantRepository.GetByFilter(c => c.Cpf == attendantVO.Cpf).Any())
                throw new AttendantAlreadyExistsException();

            var attendant = _mapper.Map<Attendant>(attendantVO);

            _attendantRepository.Insert(attendant);

            var insertedAttendentVO = _mapper.Map<AttendantValueObject>(attendant);

            return insertedAttendentVO;
        }

        public void Remove(Guid attendantId)
        {
            if (!_attendantRepository.Remove(attendantId))
                throw new AttendantDoesntExistsException();
        }

        public AttendantValueObject Get(Guid attendantId)
        {
            var attendant = _attendantRepository.Get(attendantId);

            if (attendant == null)
                throw new AttendantDoesntExistsException();

            return _mapper.Map<AttendantValueObject>(attendant);
        }

        public AttendantValueObject GetByPin(int pinCode)
        {
            var attendant = _attendantRepository.GetByFilter(a => a.PinCode == pinCode).FirstOrDefault();

            if (attendant == null)
                throw new AttendantDoesntExistsException();

            return _mapper.Map<AttendantValueObject>(attendant);
        }

        public AttendantValueObject Update(AttendantValueObject attendantVO)
        {
            if (!attendantVO.Id.HasValue)
                throw new AttendantIdRequiredException();
            //Recupera a categoria pelo Id
            var attendant = _attendantRepository.Get(attendantVO.Id.Value);

            //Passa as alterações para a entidade
            _mapper.Map(attendantVO, attendant);

            //Faz o update
            var updatedAttendant = _attendantRepository.Update(attendant);

            return _mapper.Map<AttendantValueObject>(updatedAttendant);
        }

        public IEnumerable<AttendantValueObject> GetAll()
        {
            var attendants = _attendantRepository.GetAll();

            return _mapper.Map<List<AttendantValueObject>>(attendants);
        }

        public AttendantValueObject NewPinCode(Guid attendantId)
        {
            var attendant = _attendantRepository.Get(attendantId);

            if (attendant == null)
                throw new AttendantDoesntExistsException();

            while (_attendantRepository.GetByFilter(a => a.PinCode == attendant.PinCode).Any())
            {
                attendant.NewPin();
            }

            attendant = _attendantRepository.Update(attendant);

            return _mapper.Map<AttendantValueObject>(attendant);
        }

        #endregion
    }
}
