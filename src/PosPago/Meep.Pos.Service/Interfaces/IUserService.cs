﻿using Meep.Pos.Service.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.Interfaces
{
    public interface IUserService
    {
        UserValueObject Create(UserValueObject attendantVO);
        void Remove(Guid attendantId);
        UserValueObject Get(Guid attendantId);
        UserValueObject Update(UserValueObject attendantVO);
        IEnumerable<UserValueObject> GetAll();
        UserValueObject ChangePassword(string login, string oldPassword, string newPassword);
        UserValueObject ChangeEmail(string login, string password, string oldEmail, string newEmail);
        bool PasswordIsCorrect(string login, string password);
        UserValueObject GetByLogin(string login);
    }
}
