﻿using Meep.Pos.Service.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.Interfaces
{
    public interface IComissionService
    {
        List<ComissionValueObject> GetAll();
        ComissionValueObject Get(Guid id);
        ComissionValueObject Insert(ComissionValueObject comissionVO);
        ComissionValueObject Update(ComissionValueObject comissionVO);
        bool Delete(Guid id);

    }
}
