﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Service.ValueObjects;

namespace Meep.Pos.Service.Interfaces
{
    public interface ICashierService
    {
        CashierValueObject OpenCashier(Guid attendantId, decimal initialValue);
        CashierValueObject CloseCashier(Guid cashierId, Guid attendantId);
        CashierValueObject CloseCashier(Guid attendantId);
        CashierValueObject Get(Guid cashierId);
        CashierValueObject GetCurrent();
        CashierValueObject GetCurrentCashierWithOrders();
        IEnumerable<CashierValueObject> GetAll();
        OrderValueObject OpenOrder(Guid attendantId, string ticketNumber);
        CashierValueObject CloseOrder(Guid orderId);
        CashierValueObject OpenOrder(Guid cashierId, Guid attendantId, Guid ticketId);
        CashierValueObject CloseOrder(Guid cashierId, Guid orderId);
        OrderValueObject GetOrder(Guid cashierId, Guid orderId);
        CashierValueObject GetByDate(CashierValueObject filter);
        OrderValueObject ExpressOrder(Guid attendantId);
    }
}
