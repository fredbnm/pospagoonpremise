﻿using Meep.Pos.Domain;
using Meep.Pos.Service.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.Interfaces
{
    public interface IProductionService
    {
        List<ProductionValueObject> GetAll();
        void Delete(Guid id);
    }
}
