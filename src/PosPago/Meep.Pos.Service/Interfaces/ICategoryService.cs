﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Service.ValueObjects;

namespace Meep.Pos.Service.Interfaces
{
    public interface ICategoryService
    {
        CategoryValueObject Create(CategoryValueObject categoryVO);
        void Remove(Guid categoryId);
        CategoryValueObject Get(Guid categoryId);
        CategoryValueObject Update(CategoryValueObject categoryVO);
        IEnumerable<CategoryValueObject> GetAll();
    }
}
