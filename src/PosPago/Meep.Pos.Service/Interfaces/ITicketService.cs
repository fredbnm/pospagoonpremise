﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Service.ValueObjects;

namespace Meep.Pos.Service.Interfaces
{
    public interface ITicketService
    {
        TicketValueObject Create(TicketValueObject ticketVO);
        void Remove(Guid ticketId);
        TicketValueObject Get(Guid ticketId);
        TicketValueObject Update(TicketValueObject ticketVO);
        IEnumerable<TicketValueObject> GetAll();
        TicketValueObject GetByNumber(string number);
        TicketValueObject GenerateTicket();
    }
}
