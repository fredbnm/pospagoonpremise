﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Service.ValueObjects;

namespace Meep.Pos.Service.Interfaces
{
    public interface IDiscountService
    {
        DiscountValueObject Create(DiscountValueObject discountVO);
        void Remove(Guid discountId);
        DiscountValueObject Get(Guid discountId);
        DiscountValueObject Update(DiscountValueObject discountVO);
        IEnumerable<DiscountValueObject> GetAll();
    }
}
