﻿using Meep.Pos.Service.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.Interfaces
{
    public interface IDeviceAttendantService
    {
        DeviceAttendantValueObject Get(Guid id);
        DeviceAttendantValueObject ChangeDeviceStatus(Guid id, bool isActive);
        List<DeviceAttendantValueObject> GetAll();
        DeviceAttendantValueObject LoginByDevice(int pinCode, string fingerprint, string userAgent, string resolution);
        DeviceAttendantValueObject GetByDeviceAndAttendant(string fingerprint, int pinCode);
        void Remove(Guid id);
    }
}
