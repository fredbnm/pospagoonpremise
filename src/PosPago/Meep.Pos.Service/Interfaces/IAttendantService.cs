﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Service.ValueObjects;

namespace Meep.Pos.Service.Interfaces
{
    public interface IAttendantService
    {
        AttendantValueObject Create(AttendantValueObject attendantVO);
        void Remove(Guid attendantId);
        AttendantValueObject Get(Guid attendantId);
        AttendantValueObject Update(AttendantValueObject attendantVO);
        IEnumerable<AttendantValueObject> GetAll();
        AttendantValueObject NewPinCode(Guid attendantId);
        AttendantValueObject GetByPin(int pinCode);
    }
}
