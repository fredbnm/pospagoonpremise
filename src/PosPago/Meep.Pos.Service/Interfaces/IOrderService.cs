﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Service.ValueObjects;

namespace Meep.Pos.Service.Interfaces
{
    public interface IOrderService
    {
        OrderValueObject AddItem(Guid orderId, OrderItemValueObject item);
        OrderValueObject AddItems(Guid orderId, List<OrderItemValueObject> items);
        OrderValueObject RemoveItem(Guid orderId, Guid itemId, decimal quantity);
        OrderValueObject AddPayment(Guid orderId, PaymentValueObject paymentVO);
        OrderValueObject AddPaymentIntegration(Guid orderId, PaymentValueObject paymentVO);
        OrderValueObject Get(Guid orderId);
        OrderValueObject AttachCustomerCpf(Guid orderId, string cpf);
        IEnumerable<OrderValueObject> GetAll();
        OrderProductValueObject FinalizeOrderProduct(Guid orderId, Guid orderProductId);

    }
}
