﻿using Meep.Pos.Service.ValueObjects.Integration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.Interfaces
{
    public interface IIntegrationService
    {
        void PaymentIntegration(string endpoint, bool continueOnError, List<PaymentIntegrationValueObject> paymentIntegration);
    }
}
