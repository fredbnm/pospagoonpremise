﻿using Meep.Pos.Service.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.Interfaces
{
    public interface IEventService
    {
        List<EventValueObject> GetAll();
    }
}
