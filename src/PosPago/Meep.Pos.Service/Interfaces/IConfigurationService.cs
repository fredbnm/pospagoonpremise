﻿using Meep.Pos.Domain.Core.Enumerators.Configuration;
using Meep.Pos.Service.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.Interfaces
{
    public interface IConfigurationService
    {
        ConfigurationValueObject GetOrCreate();
        ConfigurationValueObject Create(ConfigurationValueObject configuration);
        ConfigurationValueObject Update(ConfigurationValueObject configuration);
        ConfigurationValueObject SyncEndpoints(List<ConfigurationEndpointValueObject> endpoints);
        bool IntegrationExists(FlowTypeEnum flow);

    }
}
