﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Service.ValueObjects;

namespace Meep.Pos.Service.Interfaces
{
    public interface ITaxService
    {
        TaxValueObject Create(TaxValueObject taxVO);
        void Remove(Guid taxId);
        TaxValueObject Get(Guid taxId);
        TaxValueObject Update(TaxValueObject taxVO);
        IEnumerable<TaxValueObject> GetAll();

    }
}
