﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Service.ValueObjects;

namespace Meep.Pos.Service.Interfaces
{
    public interface IProductService
    {
        ProductValueObject Create(ProductValueObject productVO);
        void Remove(Guid productId);
        ProductValueObject Get(Guid productId);
        ProductValueObject Update(ProductValueObject productVO);
        IEnumerable<ProductValueObject> GetAll();
    }
}
