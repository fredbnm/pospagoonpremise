﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Service.ValueObjects;

namespace Meep.Pos.Service.Interfaces
{
    public interface ILocationService
    {
        LocationValueObject Create(LocationValueObject locationVO);
        LocationValueObject CreateOrGet(LocationValueObject locationVO);
        void Remove(Guid locationId);
        LocationValueObject Get(Guid locationId);
        LocationValueObject Update(LocationValueObject locationVO);
        IEnumerable<LocationValueObject> GetAll();
    }
}
