﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.Domain.Core.Exception.Tax;

namespace Meep.Pos.Service
{
    public class TaxService : ITaxService
    {
        private ITaxRepository _taxRepository;
        private IMapper _mapper;

        public TaxService(ITaxRepository taxRepository,
               IMapper mapper)
        {
            _taxRepository = taxRepository;
            _mapper = mapper;
        }

        #region [ Methods ]
        
        public TaxValueObject Create(TaxValueObject taxVO)
        {
            if (_taxRepository.GetByFilter(t=>t.Description == taxVO.Description).Any())
                throw new TaxAlreadyExistsException(taxVO.Description);

            var tax = _mapper.Map<Tax>(taxVO);

            _taxRepository.Insert(tax);
            
            return _mapper.Map<TaxValueObject>(tax); ;
        }

        public void Remove(Guid taxId)
        {
            if (!_taxRepository.Remove(taxId))
                throw new TaxDoesntExistsException();
        }

        public TaxValueObject Get(Guid taxId)
        {
            var tax = _taxRepository.Get(taxId);

            if (tax == null)
                throw new TaxDoesntExistsException();

            return _mapper.Map<TaxValueObject>(tax);
        }

        public TaxValueObject Update(TaxValueObject taxVO)
        {
            if (!taxVO.Id.HasValue)
                throw new TaxIdRequiredException();

            var tax = _taxRepository.Get(taxVO.Id.Value);

            _mapper.Map(taxVO, tax);

            var updatedTax = _taxRepository.Update(tax);

            return _mapper.Map<TaxValueObject>(updatedTax);

        }

        public IEnumerable<TaxValueObject> GetAll()
        {
            return _mapper.Map<List<TaxValueObject>>(_taxRepository.GetAll());
        }

        #endregion
    }
}
