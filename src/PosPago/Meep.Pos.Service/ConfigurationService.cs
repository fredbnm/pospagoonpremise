﻿using Meep.Pos.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using AutoMapper;
using System.Linq;
using Meep.Pos.Domain.Models;
using Meep.Pos.Domain.Core.Enumerators.Configuration;

namespace Meep.Pos.Service
{
    public class ConfigurationService : IConfigurationService
    {
        IConfigurationRepository _configurationRepository;
        IMapper _mapper;

        public ConfigurationService(IConfigurationRepository configurationRepository,
            IMapper mapper)
        {
            _configurationRepository = configurationRepository;
            _mapper = mapper;
        }

        public ConfigurationValueObject Create(ConfigurationValueObject configuration)
        {
            var conf = _mapper.Map<Configuration>(configuration);

            _configurationRepository.Insert(conf);

            return _mapper.Map<ConfigurationValueObject>(conf);
        }

        public bool IntegrationExists(FlowTypeEnum flow)
        {
            var configuration = _configurationRepository.Get();

            if (configuration.Integration)
            {
                return configuration.Endpoints.Any(e => e.Flow == flow);
            }

            return false;
        }

        public ConfigurationValueObject GetOrCreate()
        {
            var configurations = _configurationRepository.GetAll().ToList();

            if (configurations.Any())
            {
                return _mapper.Map<ConfigurationValueObject>(configurations.FirstOrDefault());
            }

            var configuracao = _configurationRepository.Insert(new Configuration());

            return _mapper.Map<ConfigurationValueObject>(configuracao);
        }

        public ConfigurationValueObject SyncEndpoints(List<ConfigurationEndpointValueObject> endpoints)
        {
            var configuration = _configurationRepository.GetAll().FirstOrDefault();
            var configurationEndpoints = _configurationRepository.GetEndpoints();
            
            foreach (var endpoint in endpoints)
            {
                if (configurationEndpoints.Any(c => c.Endpoint == endpoint.Endpoint))
                    break;

                configuration.AddEndpoint(_mapper.Map<ConfigurationEndpoint>(endpoint));
            }

            _configurationRepository.InsertEndpoints(configuration);
            
            return _mapper.Map<ConfigurationValueObject>(configuration);
        }

        public ConfigurationValueObject Update(ConfigurationValueObject configuration)
        {
            var configurationModel = _configurationRepository.GetAll().FirstOrDefault();

            _mapper.Map(configuration, configurationModel);

            var updatedConfig = _configurationRepository.Update(configurationModel);

            return _mapper.Map<ConfigurationValueObject>(updatedConfig);
        }
        
    }
}
