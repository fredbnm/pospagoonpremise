﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.Domain.Core.Exception.Discount;

namespace Meep.Pos.Service
{
    public class DiscountService : IDiscountService
    {
        private readonly IDiscountRepository _discountRepository;
        private readonly IMapper _mapper;
        public DiscountService(IDiscountRepository discountRepository,
                        IMapper mapper)
        {
            _discountRepository = discountRepository;
            _mapper = mapper;
        }

        public DiscountValueObject Create(DiscountValueObject discountVO)
        {
            if (_discountRepository.GetByFilter(t => t.Description == discountVO.Description).Any())
                throw new DiscountAlreadyExistsException(discountVO.Description);

            var discount = _mapper.Map<Discount>(discountVO);

            _discountRepository.Insert(discount);

            return _mapper.Map<DiscountValueObject>(discount);
        }

        public void Remove(Guid discountId)
        {
            if (!_discountRepository.Remove(discountId))
                throw new DiscountDoesntExistsException();
        }

        public DiscountValueObject Get(Guid discountId)
        {
            var discount = _discountRepository.Get(discountId);

            if (discount == null)
                throw new DiscountDoesntExistsException();

            return _mapper.Map<DiscountValueObject>(discount);
        }

        public DiscountValueObject Update(DiscountValueObject discountVO)
        {
            if (!discountVO.Id.HasValue)
                throw new DiscountIdRequiredException();

            var discount = _discountRepository.Get(discountVO.Id.Value);

            _mapper.Map(discountVO, discount);

            var updatedTax = _discountRepository.Update(discount);

            return _mapper.Map<DiscountValueObject>(updatedTax);
        }

        public IEnumerable<DiscountValueObject> GetAll()
        {
            return _mapper.Map<List<DiscountValueObject>>(_discountRepository.GetAll());
        }
    }
}
