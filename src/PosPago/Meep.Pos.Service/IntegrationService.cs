﻿using Meep.Pos.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Service.ValueObjects.Integration;
using RestSharp;
using Flurl.Http;

namespace Meep.Pos.Service
{
    public class IntegrationService : IIntegrationService
    {
        public async void PaymentIntegration(string endpoint, bool continueOnError, List<PaymentIntegrationValueObject> paymentIntegration)
        {

            //var client = new RestClient();

            //var request = new RestRequest(endpoint, Method.POST);

            //request.AddJsonBody(paymentIntegration);

            try
            {
                var response = await endpoint
                    .PostJsonAsync(paymentIntegration)
                    .ReceiveString();
            }
            catch
            {
                if (continueOnError)
                    return;

                throw;
            }
        }
    }
}
