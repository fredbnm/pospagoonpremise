﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.ValueObjects;
using Microsoft.Extensions.Localization;
using Meep.Pos.Domain.Core.Localization;
using Meep.Pos.Domain.Core.Constants;
using Meep.Pos.Domain.Core.Exception.Product;
using Meep.Pos.Domain.Core.Exception.Location;
using Meep.Pos.Domain.Core.Exception.Category;

namespace Meep.Pos.Service
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IMapper _mapper;

        public ProductService(IProductRepository productRepository,
                            ICategoryRepository categoryRepository,
                            IMapper mapper)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }

        #region [ Methods ]

        public ProductValueObject Create(ProductValueObject productVO)
        {
            ValidateCreate(productVO);

            var category = _categoryRepository.Get(productVO.CategoryId.Value);

            if (category == null)
                throw new ProductCategoryDoesntExistsException();

            var insertedProduct = new Product(productVO.Name, category, productVO.Price, productVO.IsProduction, productVO.UnitEnum, productVO.ProductionTime);

            _productRepository.Insert(insertedProduct);

            var insertedProductVO = _mapper.Map<ProductValueObject>(insertedProduct);

            return insertedProductVO;
        }

        private void ValidateCreate(ProductValueObject productVO)
        {
            if (!productVO.CategoryId.HasValue)
                throw new CategoryDoesntExistsException();

            if (_productRepository.GetByFilter(p => p.Name == productVO.Name).Any())
                throw new ProductAlreadyExistsException(productVO.Name);
        }

        public void Remove(Guid productId)
        {
            if (!_productRepository.Remove(productId))
                throw new ProductDoesntExistsException();
        }

        public ProductValueObject Get(Guid productId)
        {
            var product = _productRepository.Get(productId);

            if (product == null)
                throw new ProductDoesntExistsException();

            return _mapper.Map<ProductValueObject>(product);
        }

        public ProductValueObject Update(ProductValueObject productVO)
        {
            if (!productVO.Id.HasValue)
                throw new ProductIdRequiredException();

            var product = _productRepository.Get(productVO.Id.Value);
            
            _mapper.Map(productVO, product);

            product.Category = _categoryRepository.Get(productVO.CategoryId.Value);

            var updatedProduct = _productRepository.Update(product);
            
            return _mapper.Map<ProductValueObject>(updatedProduct);
        }

        public IEnumerable<ProductValueObject> GetAll()
        {
            var products = _productRepository.GetAll();
            
            return _mapper.Map<List<ProductValueObject>>(products);
        }

        #endregion
    }
}
