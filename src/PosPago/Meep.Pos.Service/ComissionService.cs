﻿using Meep.Pos.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Service.ValueObjects;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Domain.Core.Exception.Comission;
using AutoMapper;
using Meep.Pos.Domain.Models;
using System.Linq;

namespace Meep.Pos.Service
{
    public class ComissionService : IComissionService
    {
        IComissionRepository _comissionRepository;
        IMapper _mapper;

        public ComissionService(IComissionRepository comissionRepository,
            IMapper mapper)
        {
            _comissionRepository = comissionRepository;
            _mapper = mapper;
        }

        public bool Delete(Guid id)
        {
            return _comissionRepository.Remove(id);
        }

        public ComissionValueObject Get(Guid id)
        {
            var comission = _comissionRepository.Get(id);

            if (comission == null)
                throw new ComissionDoesntExistsException();

            return _mapper.Map<ComissionValueObject>(comission);
        }

        public List<ComissionValueObject> GetAll()
        {
            return _mapper.Map<List<ComissionValueObject>>(_comissionRepository.GetAll());
        }

        public ComissionValueObject Insert(ComissionValueObject comissionVO)
        {
            if (_comissionRepository.GetByFilter(c => c.Description == comissionVO.Description).Any())
                throw new ComissionAlreadyExistsException();

            var comission = new Comission(comissionVO.Description, comissionVO.Value);

            _comissionRepository.Insert(comission);

            return _mapper.Map<ComissionValueObject>(comission);
        }

        public ComissionValueObject Update(ComissionValueObject comissionVO)
        {
            if (!comissionVO.Id.HasValue)
                throw new ComissionIdRequiredException();

            var comission = _comissionRepository.Get(comissionVO.Id.Value);

            _mapper.Map(comissionVO, comission);

            comission = _comissionRepository.Update(comission);

            return _mapper.Map<ComissionValueObject>(comission);
        }
    }
}
