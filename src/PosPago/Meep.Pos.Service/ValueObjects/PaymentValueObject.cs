﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Core.Enumerators;

namespace Meep.Pos.Service.ValueObjects
{
    public class PaymentValueObject
    {
        public Guid Id { get; set; }
        public decimal Value { get; set; }
        public PaymentStateEnum PaymentState { get; set; }
        public PaymentTypeEnum PaymentType { get; set; }
        public Guid AttendantId { get; set; }
    }
}
