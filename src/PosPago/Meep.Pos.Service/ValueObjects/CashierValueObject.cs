﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.ValueObjects
{
    public class CashierValueObject
    {
        public Guid Id { get; set; }
        public decimal InitialValue { get; set; }
        public Guid? CashierOpenerId { get; set; }
        public string CashierOpenerName { get; set; }
        public Guid? CashierCloserId { get; set; }
        public string CashierCloserName { get; set; }
        public Guid[] OrdersId { get; set; }
        public List<OrderValueObject> Orders { get; set; }
        public decimal Total { get; set; }
        public DateTime DateOpened { get; set; }
        public DateTime DateClosed { get; set; }
        public bool? IsOpen { get; set; }
    }
}
