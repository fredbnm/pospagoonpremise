﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.ValueObjects
{
    public class OrderValueObject
    {
        public Guid? Id { get; set; }
        public Guid? CashierId { get; set; }
        public AttendantValueObject Attendant { get; set; }
        public List<OrderProductValueObject> Products { get; set; }
        public List<OrderTaxValueObject> Taxes { get; set; }
        public List<OrderDiscountValueObject> Discounts { get; set; }
        public List<PaymentValueObject> Payments { get; set; }
        public DateTime DateOpened { get; set; }
        public DateTime DateClosed { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPayment { get; set; }
        public Guid TicketId { get; set; }
        public string TicketNumber { get; set; }
        public bool IsOpen { get; set; }
    }
}
