﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.ValueObjects
{
    public class ComissionValueObject
    {
        public Guid? Id { get; set; }
        public string Description { get; set; }
        public decimal Value { get; set; }
    }
}
