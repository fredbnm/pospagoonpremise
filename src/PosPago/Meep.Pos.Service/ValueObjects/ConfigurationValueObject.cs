﻿using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Core.Enumerators.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.ValueObjects
{
    public class ConfigurationValueObject
    {
        public MeepTypeEnum MeepType;
        public bool Integration;
        public bool IsSynchronized;
    }

    public class ConfigurationEndpointValueObject
    {
        public string Endpoint;
        public FlowTypeEnum Flow;
        public PipelineEnum Pipeline;
        public EndpointTypeEnum EndpointType;
        public bool ContinueOnError;
    }
}
