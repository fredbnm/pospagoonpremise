﻿using Meep.Pos.Domain.Core.Enumerators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.ValueObjects
{
    public class OrderTaxValueObject
    {
        public DateTime DateOrder { get; set; }
        public decimal Count { get; set; }
        public decimal Value { get; set; }
        public decimal Total { get; set; }
        public Guid TaxId { get; set; }
        public string TaxName { get; set; }
        public PriceCalcTypeEnum TaxCalcType { get; set; }
        public bool Required { get; set; }
    }
}
