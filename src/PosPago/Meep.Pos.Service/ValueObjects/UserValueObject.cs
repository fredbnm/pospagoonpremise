﻿using Meep.Pos.Domain.Core.Enumerators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.ValueObjects
{
    public class UserValueObject
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public string Cpf { get; set; }
        public SexEnum Sex { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
