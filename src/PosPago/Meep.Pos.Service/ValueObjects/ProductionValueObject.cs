﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.ValueObjects
{
    public class ProductionValueObject
    {
        public ProductionValueObject()
        {

        }

        public Guid Id { get; set; }
        public string ProductName { get; set; }
        public decimal Quantity { get; set; }
        public string Observation { get; set; }
        public Guid IdOrder { get; set; }
        public Guid IdOrderItem { get; set; }
        public bool IsFinalized { get; set; }
        public string TicketNumber { get; set; }
        public int? ProductionTime { get; set; }
        public DateTime Date { get; set; }
        public Guid CategoryId { get; set; }
        public string CategoryName { get; set; }
        public Guid AttendantId { get; set; }
        public string AttendantName { get; set; }
    }
}
