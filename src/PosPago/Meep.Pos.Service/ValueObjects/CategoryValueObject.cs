﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.ValueObjects
{
    public class CategoryValueObject
    {
        public Guid? Id { get; set; }

        public string Name { get; set; }

    }
}
