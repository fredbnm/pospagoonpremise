﻿using Meep.Pos.Domain.Core.Enumerators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.ValueObjects
{
    public class ProductValueObject
    {
        public ProductValueObject()
        {
            
        }
        
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public Guid? CategoryId { get; set; }
        public string CategoryName { get; set; }
        public decimal Price { get; set; }
        public MeasureUnitEnum UnitEnum { get; set; }
        public bool IsActive { get; set; }
        public bool IsProduction { get; set; }
        public int? ProductionTime { get; set; }
        public string MetaData { get; set; }
    }

}
