﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.ValueObjects
{
    public class OrderItemValueObject
    {
        public Guid ItemId { get; set; }
        public decimal Quantity { get; set; }
        public string Description { get; set; }
        public Guid AttendantId { get; set; }
        public Guid? ComissionId { get; set; }
    }
}
