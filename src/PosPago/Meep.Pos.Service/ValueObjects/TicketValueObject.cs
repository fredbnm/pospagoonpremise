﻿using Meep.Pos.Domain.Core.Enumerators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.ValueObjects
{
    public class TicketValueObject
    {
        /// <summary>
        /// Id do Ticket. Não deve ser preenchido ao se criar novo ticket.
        /// </summary>
        public Guid? Id { get; set; }
        /// <summary>
        /// Número do Ticket.
        /// </summary>
        public string Number { get; set; }
        public TicketTypeEnum Type { get; set; }
    }
}
