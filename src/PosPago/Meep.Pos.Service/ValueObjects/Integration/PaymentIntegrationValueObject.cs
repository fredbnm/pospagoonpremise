﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.ValueObjects.Integration
{
    public class PaymentIntegrationValueObject
    {
        public PaymentIntegrationValueObject()
        {
            produtos = new List<ProdutoIntegrationValueObject>();
        }
        public Guid CodigoPedido { get; set; }
        public DateTime DataPedido { get; set; }
        public int TipoPagamento { get; set; }
        public decimal ValorTotal { get; set; }
        public decimal Desconto { get; set; }
        public decimal Acrescimo { get; set; }
        public Guid LocalId { get; set; }
        public Guid AtendenteId { get; set; }
        public List<ProdutoIntegrationValueObject> produtos { get; set; }

        public class ProdutoIntegrationValueObject
        {
            public string Id { get; set; }
            public string Descricao { get; set; }
            public decimal ValorUnitario { get; set; }
            public int Quantidade { get; set; }
            public decimal Total { get; set; }
            public string DadosComplementares { get; set; }
            public int Tipo { get; set; } = 0;
        }

    }
}
