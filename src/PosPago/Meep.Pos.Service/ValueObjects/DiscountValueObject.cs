﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Core.Enumerators;

namespace Meep.Pos.Service.ValueObjects
{
    public class DiscountValueObject
    {
        public DiscountValueObject()
        {
            
        }
        public Guid? Id { get; set; }
        public string Description { get; set; }
        public decimal Value { get; set; }
        public PriceCalcTypeEnum CalcType { get; set; }
    }
}
