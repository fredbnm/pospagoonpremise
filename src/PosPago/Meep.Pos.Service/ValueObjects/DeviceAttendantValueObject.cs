﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.ValueObjects
{
    public class DeviceAttendantValueObject
    {
        public Guid Id { get; set; }
        public Guid DeviceId { get; set; }
        public string DeviceName { get; set; }
        public Guid AttendantId { get; set; }
        public string AttendantName { get; set; }
        public bool IsActive { get; set; }
    }
}
