﻿using Meep.Pos.Domain.Core.Enumerators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.ValueObjects
{
    public class EventValueObject
    {
        public Guid Id { get; set; }
        public EventTypeEnum EventType { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Message { get; set; }
        public bool IsRead { get; set; }
    }
}
