﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.ValueObjects
{
    public class OrderProductValueObject
    {
        public DateTime DateOrder { get; set; }
        public decimal Count { get; set; }
        public decimal Value { get; set; }
        public decimal Total { get; set; }
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public Guid AttendantId { get; set; }
        public string AttendantName { get; set; }
        public decimal ComissionValue { get; set; }
    }
}
