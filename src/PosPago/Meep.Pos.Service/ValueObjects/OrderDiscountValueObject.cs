﻿using Meep.Pos.Domain.Core.Enumerators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Service.ValueObjects
{
    public class OrderDiscountValueObject
    {
        public DateTime DateOrder { get; set; }
        public decimal Count { get; set; }
        public decimal Value { get; set; }
        public decimal Total { get; set; }
        public Guid DiscountId { get; set; }
        public string DiscountName { get; set; }
        public PriceCalcTypeEnum DiscountCalcType { get; set; }
    }
}
