const swBuild = require('workbox-build');

swBuild.generateSW({
  navigateFallback: 'index.html',
  globDirectory: './dist',
  globPatterns: [
    'index.html',
    '**.js',
    '**.css',
    'assets/images/**.jpg',
    'assets/images/**.png',
    'manifest.json'
  ],
  swDest: 'dist/sw.js',
  templatedUrls: {
    '?utm_source=pwa': ['index.html'],
  }
}).then(() => console.log('Service Worker gerado')).catch(err => console.error(err, 'Falha ao gerar o service Worker'));
