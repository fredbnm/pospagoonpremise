export const environment = {
  production: false,
  //baseUrl: 'http://192.168.15.246:5000',
  baseUrl: 'https://api-meep-2-0.azurewebsites.net',
  baseUrlMeep: 'http://meepserver-dev.azurewebsites.net',
  versionBase:'v1/'
};
