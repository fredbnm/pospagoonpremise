import { ComissionService } from './services/comission.service';
//import { ModalBaseComponent } from './components/modal/modal.component';
//import { ComissionComponent } from './components/modal/comission/comission.component';

import { UserInfoComponent } from './components/user-info/user-info.component';
import { UserInfoService } from './components/user-info/user-info.service';
import { DiscountService } from './components/category-list/discount.service';
import { AuthInterceptor } from './app.service.auth.interceptor';
import { CategoryListModel } from './components/category-list/category-list.module';
import { OrderService } from './services/order.service';
import { CategoryListService } from './components/category-list/category-list.service';
import { CategoryService } from './services/category.service';
import { ProductsService } from './services/products.service';
import { OrderOpenService } from './components/order-open/order-open.service';
import {HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { OrderProductService } from './components/category-list/orderproduct.service';
import { BrowserModule, disableDebugTools } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from "@angular/http";
import { LoginService } from './components/login/login.service';

import { AppRoutingModule } from "./app.routing.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';

// Componentss
import { AppComponent } from './app.component';
import { SharedComponent } from './shared/shared.component';
import { LoginComponent } from './components/login/login.component';
import { OrderOpenComponent } from './components/order-open/order-open.component';
import { HomeComponent } from './components/home/home.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ResumeComponent } from './components/resume/resume.component';
import { PaymentTypeComponent } from './components/resume/payment-type/payment-type.component';
import { SettingsModalComponent } from './components/login/settings-modal/settings-modal.component';
import { Page404Component } from './page404/page404.component';



import {AppServiceInterceptor} from './app.service.interceptor';
import { AppService } from './app.service';
import { ProductListService } from './components/product-list/product-list.service';

import { CurrencyMaskModule } from 'ng2-currency-mask';
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from 'ng2-currency-mask/src/currency-mask.config';
import { NoConflictStyleCompatibilityMode } from '@angular/material'
import { OrderOpenPreComponent } from './components/order-open-pre/order-open-pre.component';
import { OrderOpenPreService } from './components/order-open-pre/order-open-pre.service';
import { ConfigurationService } from './services/configuration.service';
import { DialogComponent } from './components/modal/dialog/dialog.component';
//import { ModalService } from './components/modal/modal.service';
export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: "right",
  allowNegative: true,
  allowZero: true,
  decimal: ",",
  precision: 2,
  prefix: "R$ ",
  suffix: "",
  thousands: "."
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent, 
    SharedComponent,
    LoginComponent,
    OrderOpenComponent,
    OrderOpenPreComponent,
    ProductListComponent,
    ResumeComponent,
    Page404Component,
    PaymentTypeComponent,
    SettingsModalComponent,
    UserInfoComponent,
    DialogComponent,
    // ComissionComponent,
    // ModalBaseComponent
  ],
  imports: [
    NoConflictStyleCompatibilityMode,
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    CategoryListModel,    
    HttpClientModule,
    TextMaskModule,
    ReactiveFormsModule,
    FormsModule,
    CurrencyMaskModule
  ],
  providers: [
    OrderProductService,
    OrderService,
    AppService,
    ProductListService,
    ConfigurationService,
    LoginService,
    DiscountService,
    OrderOpenService,
    OrderOpenPreService,
    ProductsService,
    UserInfoService,
    CategoryService,
    ComissionService,
    CategoryListService,
    //ModalService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    { provide: HTTP_INTERCEPTORS, useClass: AppServiceInterceptor, multi: true},
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
