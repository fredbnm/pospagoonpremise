
import { LoginDeviceModel } from './../../models/loginDevice.model';
import { TokenResponse } from './../../models/token.response';
import { AttendantModel } from './../../models/attendant.model';
import { AppService } from './../../app.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class LoginService extends AppService<AttendantModel> {


  constructor(http: HttpClient) {
    super(http);
  }

  method: string = 'attendant';

  async sendPin(pin): Promise<AttendantModel> {
    var attendant = await this.getAsync(this.method + '/pin/' + pin);
    return <AttendantModel>attendant;
  }

  async deviceLogin(loginDevice : LoginDeviceModel): Promise<TokenResponse> {
    
    var login = await this.postAsync('device/login',loginDevice);
    return <TokenResponse>login;
  }

}

