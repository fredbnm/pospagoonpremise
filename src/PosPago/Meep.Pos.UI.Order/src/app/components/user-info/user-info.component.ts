import { OrderService } from './../../services/order.service';
import { AppService } from './../../app.service';
import { CityModel } from './../../models/cityModel';
import swal from 'sweetalert2';
import { UserInfoService } from './user-info.service';
import { UserInfo } from './../../models/order/userInfo.model';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ElementRef } from '@angular/core';
import { MatInputModule } from '@angular/material';


@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {
  userInfo: UserInfo = new UserInfo();
  cities: CityModel[] = [];
  orderId = localStorage.getItem("orderId");
  exists = true;
  edit = false;
  linkUser = false;
  public maskPhone = [/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  // public maskDDD = ['(', /[1-9]/, /\d/, /\d/, ')'];
  public maskCpf = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]

  constructor(private router: Router,
    private route: ActivatedRoute,
    private userInfoService: UserInfoService,
    private orderService: OrderService,
    readonly element: ElementRef
   
  ) { }

  ngOnInit() {
    
  }


  searchCPF() {
    this.orderService.getCpfUser(this.orderId, this.userInfo.cpf).then(user => {
      if (user) {
        this.userInfo = user;
        this.exists = true;
        this.edit = false;
        this.linkUser = true;
        this.userInfoService.searchCities(user.endereco.estadoUF).then(cities => {
          this.cities = cities;
        });
      }
      else {
        var cpf = this.userInfo.cpf;
        this.userInfo = new UserInfo();
        this.userInfo.cpf = cpf;
        this.edit = true;
        this.exists = false;
        this.linkUser = false;
      }
    })

  }

  editUser() {
    this.edit = true;
  }
  cancelInfo() {
    this.router.navigate(['/login']);
  }
  searchCepUser() {
    this.userInfoService.searchCep(this.userInfo.endereco.cep)
      .then((userInfo: UserInfo) => {
        this.userInfo.endereco.cep = userInfo.endereco.cep;
        this.userInfo.endereco.logradouro = userInfo.endereco.logradouro;
        this.userInfo.endereco.complemento = userInfo.endereco.complemento;
        this.userInfo.endereco.bairro = userInfo.endereco.bairro;
        this.userInfo.endereco.estadoUF = userInfo.endereco.estado;

        this.userInfoService.searchCities(userInfo.endereco.estado).then(cities => {
          var cityId = cities.find(c => c.nome == userInfo.endereco.cidadeNome).id;
          this.userInfo.endereco.cidadeId = cityId != null || cityId != undefined ? cityId : cities[0].id;
          this.cities = cities;
        });
      })
      .catch(() => {
        this.userInfo = new UserInfo();
        swal(
          'Não foi possível continuar a busca'
        )
      })
  }
  saveInfos() {
    if (this.userInfo.telefone)
      this.userInfo.telefone = this.userInfo.telefone.replace('-', '');
    
    if (this.userInfo.endereco.cep)
      this.userInfo.endereco.cep = this.userInfo.endereco.cep.replace('-', '');

    this.orderService.addCustomerToOrder(this.orderId, this.userInfo)
      .then(result => {
        if (result) {
          swal("Usuário vinculado com sucesso!").then(() => this.router.navigate(["/comandar-pre"]))
        }
      });
  }
  cancelInfos() {
    this.router.navigate(['/comandar-pre'])
  }
}
