import { CityModel } from './../../models/cityModel';
import { AppService } from './../../app.service';
import { UserInfo } from './../../models/order/userInfo.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpModule, Headers } from '@angular/http';
import { HttpErrorResponse } from "@angular/common/http";
import {Http} from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import 'rxjs/add/operator/toPromise';
@Injectable()
export class UserInfoService extends AppService<UserInfo>{

  constructor(private httpClient : HttpClient, private httpC : Http) {
    super(httpClient);
    
   }

  searchCep(cep:string){
   return this.httpC.get(`https://viacep.com.br/ws/${cep}/json/`)
      .toPromise()
      .then(response =>{
        return this.convertResponseCep(response.json());
      })
  }

  private convertResponseCep(cepReturn):UserInfo{
   let userInfo = new UserInfo();
  
    userInfo.endereco.cep = cepReturn.cep;
    userInfo.endereco.logradouro = cepReturn.logradouro;
    userInfo.endereco.complemento = cepReturn.complemento;
    userInfo.endereco.bairro = cepReturn.bairro;
    userInfo.endereco.cidadeNome = cepReturn.localidade;

    //userInfo.cidadeId = cepReturn.cidade;
    userInfo.endereco.estado = cepReturn.uf;
    return userInfo;
  }

  async searchCities(uf : string) : Promise<CityModel[]>{
    var cities = await this.getAllAsync(`gateway/city/${uf}`);
    return <CityModel[]>cities;
  }

}
