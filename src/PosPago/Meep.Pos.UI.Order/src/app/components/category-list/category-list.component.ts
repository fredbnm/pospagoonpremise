
import { ConfigurationService } from './../../services/configuration.service';
import { DiscountService } from './discount.service';
import { DiscountModel } from './../../models/discounts.model';
import { CategoryListModel } from './category-list.module';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { LoginDeviceModel, OrderTypeEnum } from './../../models/loginDevice.model';
import { CategoryListService } from './category-list.service';
import { CategoryService } from './../../services/category.service';
import { ProductsService } from './../../services/products.service';
import { CategoryModel } from './../../models/category/category.model';
import { OrderItemModel } from './../../models/orderItem.model';
import { OrderProductService } from './orderproduct.service';
import { ProductModel, MeasureUnitEnum } from './../../models/product.model';
import { Component, OnInit } from '@angular/core';
import { MeepTypeEnum } from './../../models/configuration.model';
import { MatTabsModule } from '@angular/material';
import Utils from '../../util/utils';
import { ComissionModel } from '../../models/comission.model';
@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

  categories: CategoryModel[];
  discounts: DiscountModel[] = [];
  comissions: ComissionModel[] = [];
  products: ProductModel[] = [];
  orderItems: OrderItemModel[] = [];
  orderId: string = localStorage.getItem("orderId");
  meepType: string = localStorage.getItem("meepType");
  term: string;

  constructor(private orderProductService: OrderProductService,
    private discountService: DiscountService,
    private productService: ProductsService,
    private categoryService: CategoryService,
    private categoryListService: CategoryListService,
    private configurationService: ConfigurationService,
    private router: Router) { }

  ngOnInit() {
    this.categories = [];

    this.discountService.getAll().then(d => {
      this.discounts = d;
    });


    this.productService.getAllActive().then(p => {
      this.products = Utils.orderBy(p, "name");
      this.categoryService.getAll().then(c => {
        this.filterCategory(c);
      });
    });

    this.orderProductService.emitRemoveProduct.subscribe(
      item => {
        var index = this.orderItems.indexOf(item);
        this.orderItems.splice(index, 1);
      }
    );
    
    this.orderProductService.emitClearItems.subscribe(
      clear => {
        if (clear) {
          this.orderItems = [];
        }
      });

    this.orderProductService.emitProduct.subscribe(item => this.orderItems.push(item));
  }

  fillProductQuantity(productId) {
    var quantity = 0;
    this.orderItems.forEach(item => {
      if (item.itemId == productId)
        quantity = item.quantity;
    });
    return quantity;
  }


  filterProducts(categoryId) {
    var products: ProductModel[] = [];

    this.products.forEach(product => {
      if (product.categoryId == categoryId)
        products.push(product);
    });

    return products
  }

  filterCategory(categories: CategoryModel[]) {
    categories.forEach(category => {
      if (this.products.find(product => product.categoryId == category.id)) {
        this.categories.push(category);
      }
    });
  }

  addProduct(product: ProductModel) {
    if (product.unitEnum == MeasureUnitEnum.Kg) {
      swal({
        title: 'Informe a quantidade em kg',
        input: "text",
        inputAttributes: { currencyMask: "", class: "input-value ng-untouched ng-pristine ng-valid", 'ng-reflect-options': "[object Object]", pattern: "[0-9]*" },
        //html: "<input type='text' currencyMask [options]='{ thousands: '.', decimal: ',' }' pattern='[0-9]*' >",
        showCancelButton: true,
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar',
        allowOutsideClick: false,
        preConfirm: quantity => {
          return new Promise((resolve, reject) => {
            this.addKgProduct(product, quantity, resolve, reject);
          })

        }
      });
    }
    else
      this.orderProductService.addProduct(product);
  }

  addKgProduct(product, quantity, resolve, reject) {

    if (quantity === '' && quantity === undefined) {
      reject('A quantidade deve ser maior que zero.')
    } else {
      this.orderProductService.addKgProduct(product, quantity);
      resolve();
    }
  }


  addDiscount(discount: DiscountModel) {
    if (this.orderItems.length <= 0) {
      swal(
        'Você precisa adicionar produtos'
      )
    } else {
      this.orderProductService.addDiscount(discount);
    }
  }

  // addComission(comission: ComissionModel) {
  //   if (this.orderItems.length <= 0) {
  //     swal(
  //       'Você precisa adicionar produtos'
  //     )
  //   } else {
  //     this.orderProductService.addDiscount(comission);
  //   }
  // }

  sendOrder() {
    if (this.orderItems.length > 0) {
      swal({
        title: 'Enviando pedido...',
        onOpen: () =>{
          swal.showLoading();
          this.send();
        }
      })
     
    }
    else
      swal({ title: 'Não há produtos a serem enviados.' })
  }

  send(){
    this.categoryListService.addItems(this.orderId, this.orderItems).then(o => {
      this.orderProductService.clearOrderItems();
      swal({ title: 'Pedido enviado com sucesso!', allowOutsideClick: false }).then(
        () => {
          if (this.meepType == "2")
            this.router.navigate(['..'])
        });
    });
  }
}
