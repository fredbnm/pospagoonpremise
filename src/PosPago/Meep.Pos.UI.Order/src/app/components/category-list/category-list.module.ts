

import { MatTabsModule } from '@angular/material';
import { CategoryListComponent } from './category-list.component';
import { AppRoutingModule } from './../../app.routing.module';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    MatTabsModule,
    Ng2SearchPipeModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  exports: [
    CategoryListComponent
  ],
  declarations: [
    CategoryListComponent
  ]
})
export class CategoryListModel { }
