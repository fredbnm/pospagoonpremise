import { DiscountModel } from './../../models/discounts.model';
import { OrderItemModel } from './../../models/orderItem.model';
import { ProductModel, MeasureUnitEnum } from './../../models/product.model';
import { Injectable, EventEmitter } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { ComissionModel } from '../../models/comission.model';

@Injectable()
export class OrderProductService {

  listOrderItems: OrderItemModel[] = [];
  comission : ComissionModel;

  emitProduct = new EventEmitter<OrderItemModel>();
  emitRemoveProduct = new EventEmitter<OrderItemModel>();
  emitOrderItems = new EventEmitter<OrderItemModel[]>();
  emitClearItems = new EventEmitter<Boolean>();

  partialView(listOrderItems: OrderItemModel[]) {
    this.emitOrderItems.emit(listOrderItems);
  }

  constructor() { }

  clearOrderItems() {
    this.listOrderItems = [];
    this.comission = new ComissionModel();
    this.emitClearItems.emit(true);
  }

  getOrderItems() {
    return this.listOrderItems;
  }

  removeProduct(product: OrderItemModel) {
    var index = this.listOrderItems.indexOf(product);
    this.listOrderItems.splice(index, 1);

    this.emitRemoveProduct.emit(product);
  }

  addProduct(productModel: ProductModel) {

    var orderItem = new OrderItemModel();
    var exists = false;

    this.listOrderItems.forEach(item => {

      if (item.itemId == productModel.id && productModel.unitEnum != MeasureUnitEnum.Kg) {
        item.quantity = item.quantity + 1;
        exists = true;
      }
    });

    if (!exists) {
      orderItem.product = productModel;
      orderItem.quantity = 1;
      orderItem.itemId = productModel.id;
      orderItem.attendantId = localStorage.getItem("attendantId");

      if (this.comission.id != undefined)
        orderItem.comissionId = this.comission.id;

      this.listOrderItems.push(orderItem);
      this.emitProduct.emit(orderItem);
    }
  }

  addComission(comission : ComissionModel){
    this.comission = comission;

    this.listOrderItems.forEach(orderItem => {
      orderItem.comissionId = comission.id;
    });

    this.emitOrderItems.emit(this.listOrderItems);
  }

  addDiscount(discountModel: DiscountModel) {

    var orderItem = new OrderItemModel();
    var exists = false;

    this.listOrderItems.forEach(item => {
      if (item.itemId == discountModel.id) {
        item.quantity = item.quantity + 1;
        exists = true;
      }
    });

    if (!exists) {
      orderItem.discount = discountModel;
      orderItem.quantity = 1;
      orderItem.itemId = discountModel.id;
      orderItem.attendantId = localStorage.getItem("attendantId");

      this.listOrderItems.push(orderItem);
      this.emitProduct.emit(orderItem);
    }
  }


  
  updateProduct(orderItem: OrderItemModel) {
    this.listOrderItems[this.listOrderItems.indexOf(orderItem)] = orderItem;
  }


  addKgProduct(productModel: ProductModel, quantity: number) {

    var orderItem = new OrderItemModel();

    orderItem.product = productModel;
    orderItem.quantity = quantity;
    orderItem.itemId = productModel.id;
    orderItem.attendantId = localStorage.getItem("attendantId");

    this.listOrderItems.push(orderItem);
    this.emitProduct.emit(orderItem);

  }

}
