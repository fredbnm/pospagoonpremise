import { PaymentStatusModel } from './../../models/paymentStatus.model';
import { PaymentModel } from './../../models/order/payment.model';
import swal from 'sweetalert2';
import { OrderModel, OrderTax } from './../../models/order.model';
import { OrderService } from './../../services/order.service';
import { OrderItemModel } from './../../models/orderItem.model';
import { OrderProductService } from './../category-list/orderproduct.service';
import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { MeepTypeEnum } from './../../models/configuration.model';
import Utils from '../../util/utils';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.css']
})

export class ResumeComponent implements OnInit {
  public mask = [/[1-9]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
  public modelWithValue: string
  printDate: string = <string>(new Date().toLocaleDateString() + ' ' +new Date().toLocaleTimeString());
  listOrderItems : OrderItemModel[] = [];
  order : OrderModel = new OrderModel();
  orderId : string = localStorage.getItem("orderId");
  attendantId : string = localStorage.getItem("attendantId");
  usePinpad : boolean = localStorage.getItem("pinpad") === "0";
  meepType: string = localStorage.getItem("meepType");
  
  _valueToPay : number;
  get valueToPay(){
    return Utils.roundTo(parseFloat(this._valueToPay.toLocaleString().replace(',','.')),2);
  }
  set valueToPay(valueToPay : number){
    this._valueToPay = valueToPay;
  }

  showValueToPay(){
    return this.valueToPay.toFixed(2);
  }
  
  isFinal : boolean = false;
  paymentStatusModel : PaymentStatusModel = new PaymentStatusModel();
  payment : PaymentModel;

  constructor(private router: Router, 
    private orderProductService : OrderProductService,
    private orderService : OrderService,
    private route: ActivatedRoute ) { }

  ngOnInit() {    
    
    this.orderProductService.emitOrderItems.subscribe(orderItems => this.listOrderItems = orderItems);
    
      this.orderService.get(this.orderId).then(o => {
        this.order = o;
            
        this.route.params.subscribe((params: Params) => {
          var id = params['id'];
  
          if (id == 'total') {
              this._valueToPay = this.valueRemaining;
              this.isFinal = true;
          }
        });
      });    
  }


  async print(){
    var conteudo = document.getElementById('print_div').innerHTML;
    var tela_impressao = window.open(`
    <!DOCTYPE html>
    <html>
    <head>
 
    </head>
    
    <body>
    Content of the document......
    </body>
    </html>
    `);

    await tela_impressao.document.write(conteudo);
    
    await tela_impressao.window.print();
    await tela_impressao.window.close();
  }


  get taxTotal(){
    var total = 0;

    this.order.taxes.forEach(tax => {
      if (tax.taxCalcType == 2){
        total += this.subTotal * tax.value;
      }
      else{
        total += tax.value;
      }
    });

    return total;
  }

  get subTotal(){
    var subtotal = 0;

    this.order.products.forEach(product =>{
      subtotal += product.total;
    })

    return subtotal;
  }

  taxValue(tax : OrderTax){
    if (tax.taxCalcType == 2)
      return (tax.value * 100) +'%';
    else
      return 'R$'+ tax.value.toFixed(2);
  }

  get valueRemaining(){
    return Utils.roundTo(this.order.total - this.order.totalPayment,2);
  }

  get totalDiscount() {
    var subtotal = 0;
    
        this.order.discounts.forEach(discount =>{
          subtotal += discount.total;
        })
    
        return Utils.roundTo(subtotal,2);
  }
  
  removeTax(tax : OrderTax){
    swal({
      title: 'Confirmação',
      text: 'Tem certeza que deseja excluir esta taxa? Não será possível incluí-la novamente.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    })
    .then(() => {
      var orderItem = new OrderItemModel()
      orderItem.itemId = tax.taxId;
      orderItem.quantity = tax.count;

      this.orderService.removeItem(this.orderId, orderItem)
      .then(order => {
        swal('Deletado', 'A taxa foi excluída com sucesso.', 'error');
        this.order = new OrderModel();
        this.order = order; 
      });
    });
  }

  pay(){    
      if (this._valueToPay == 0 || this._valueToPay == undefined){
        swal("Favor entrar com um valor para efetuar o pagamento.");
      }
      else{
        swal({
          title: 'Forma de pagamento:',
          input: 'radio',
          customClass: 'modalOrder',
          inputClass:'inputOrder',
          inputOptions: {
            '1': 'Crédito',
            '2': 'Débito',
            '3': 'Dinheiro'
          },
          inputPlaceholder: 'Selecione',
          showCancelButton: true,
          cancelButtonText: 'Cancelar',
          confirmButtonText: 'Confirmar',
          inputValidator: function (value) {
            return new Promise<void>((resolve, reject) => {
              if (value !== '') {
                resolve();
              } else {
                swal('Favor escolher uma forma de pagamento.');
              }
            });
          },
          allowOutsideClick: false
        }).then((result) => {
          this.createPayment(result);
        });
      }
  }

  createPayment(paymentType) {
    var valueToPay = parseFloat(this._valueToPay.toString().replace(',','.'));

    if (valueToPay > this.valueRemaining) {
      swal("O valor a ser pago não pode ser maior que o valor restante.");
    }
    else{
      this.payment = new PaymentModel();
      this.payment.attendantId = this.attendantId;
      this.payment.paymentState = this.isFinal ? 2 : 1;
      this.payment.paymentType = paymentType;
      this.payment.value = valueToPay.toString();

      if (this.payment.paymentType != 3 && this.isMobile && this.usePinpad){
        this.paymentStatus(this.orderService);
      }
      else{
        this.registerPayment();
      }
    }
  }

  async paymentStatus(orderService : OrderService){
    this.paymentStatusModel.status = 0;
    this.paymentStatusModel.message = "Iniciando pagamento.";
    swal({
      title: 'Efetuando pagamento.',
      text: this.paymentStatusModel.message,
      timer: 60000,
      onOpen: async () : Promise<boolean> => {

        swal.showLoading();
        
        var result = await this.onOpenOrder();
        if (this.paymentStatusModel.status == 3){
          this.registerPayment();          
        }
        else
        {
          this.orderService.deleteStatus(this.orderId);
          swal(this.paymentStatusModel.message);
        }
        return result;
      },
      allowOutsideClick: false
      }).then(
      function () {
          },
      function (dismiss) {
        if (dismiss === 'timer') {
        }
      }
    )
    
    return true;
  }

   registerPayment(){
    this.orderService.pay(this.orderId, this.payment).then(o => {
      if (!this.isMobile)
        this.print();
      //this.order = new OrderModel();
      this.order.total = o.total;
      this.order.totalPayment = o.totalPayment;
      this._valueToPay = null;

      this.orderService.deleteStatus(this.orderId);
      
      swal({ title: "Pagamento efetuado com sucesso.", allowOutsideClick: false }).then(() => {
       

      });
      // 
    });     
  }

  async onOpenOrder() : Promise<boolean>{
    var endpoint = this.orderService.getPaymentStatusEndPoint(this.orderId);
    
    window.location.href = "intent://#Intent;scheme=meep;S.value="+this.valueToPay.toLocaleString()+";S.paymentType="+this.payment.paymentType+";S.endpoint="+ endpoint +";package=app.meep.meepcheckout;end";    

    while(this.paymentStatusModel.status != 3 && this.paymentStatusModel.status != 4 && this.paymentStatusModel.status != 5){

      this.orderService.getPaymentStatus(this.orderId).then(status => {       
        if (status) {   
            this.paymentStatusModel.message = status.message;
            this.paymentStatusModel.status = status.status;
            swal.setDefaults({
              title: "Efetuando pagamento.", 
              text: this.paymentStatusModel.message
            })
           
        }        
      });

    await this.delay(2000);

  };
  
  return true;
  }

  delay(ms: number){
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  closeOrder(){
    this.orderService.closeOrder(this.orderId).then(o => {   
      if (this.meepType == "1") {        
        swal({title:"Conta do fechada com sucesso.", allowOutsideClick: false}).then( () => 
        this.router.navigate(['/registro-usuario']));
      } else if (this.meepType == "2") {
      swal({title:"Conta fechada com sucesso.", allowOutsideClick: false}).then( () => 
      this.router.navigate(['']));
      }
    })
  }
  
  viewDescription(orderItem : OrderItemModel){
    swal("Descrição do produto", orderItem.description, "info");
  }

  get isMobile() : boolean{
    if(navigator.userAgent.match(/Android/i)
    || navigator.userAgent.match(/webOS/i)
    || navigator.userAgent.match(/iPhone/i)
    || navigator.userAgent.match(/iPad/i)
    || navigator.userAgent.match(/iPod/i)
    || navigator.userAgent.match(/BlackBerry/i)
    || navigator.userAgent.match(/Windows Phone/i)){
       return true;
    }
    else {
       return false;
    }
  }
  
}
