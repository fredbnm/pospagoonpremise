import { OrderOpenService } from './order-open.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrderOpenModel } from '../../models/order/orderOpen.model';

@Component({
  selector: 'app-order-open',
  templateUrl: './order-open.component.html',
  styleUrls: ['./order-open.component.css']
})
export class OrderOpenComponent implements OnInit {

  private attendantId: string = localStorage.getItem("attendantId");
  private _ticketNumber : number[] = []; 
  
  get ticketNumber() : string {
    var code = this._ticketNumber.toString().replace(/,/g,'');
    return code;
  }

  constructor(private router: Router, private orderOpenService : OrderOpenService) { }

  ngOnInit() {
    

  }

  fillNumber(number) {
    
      this._ticketNumber.push(number);
  }
  
  removeNumber(){
    this._ticketNumber.pop();
  }

  orderList(){
    this.router.navigate(['/home']);
    // var model = new OrderOpenModel();
    // model.attendantId = this.attendantId;
    // model.ticketNumber = this.ticketNumber;
    
    // this.orderOpenService.openOrder(model).then(o => {
    //   localStorage.setItem("orderId", o.id)
    //   this.router.navigate(['/home']);
    // })
  }
}
