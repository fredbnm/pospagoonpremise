import { ComissionService } from './../../services/comission.service';
import { ComissionModel } from './../../models/comission.model';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { OrderProductService } from './../category-list/orderproduct.service';
import { OrderItemModel } from './../../models/orderItem.model';
import { ProductModel } from './../../models/product.model';
import { Component, OnInit } from '@angular/core';
import Utils from '../../util/utils';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  orderNumber: string = localStorage.getItem("ticketNumber");
  orderItems: OrderItemModel[] = [];
  meepType: string = localStorage.getItem("meepType");
  showDialog : boolean = false;
  comissions : ComissionModel[] = [];

  constructor(private orderProductService: OrderProductService,
    private comissionService : ComissionService,
    private router: Router) { }

  ngOnInit() {

    this.comissionService.getAll().then(c =>{
      this.comissions = c;
    });

    this.orderProductService.emitProduct.subscribe(
      orderItem => this.orderItems.push(orderItem)
    );

    this.orderProductService.emitClearItems.subscribe(
      clear => {
        if (clear) {
          this.orderItems = [];
        }
      });

    this.orderProductService.emitRemoveProduct.subscribe(
      item => {
        var index = this.orderItems.indexOf(item);
        this.orderItems.splice(index, 1);
      }
    )

    this.orderProductService.clearOrderItems();
  }

  addComission(comission : ComissionModel) {
    this.orderProductService.addComission(comission);
    this.showDialog = false;
  }

  addDescription(orderItem: OrderItemModel) {
    swal({
      title: 'Insira a observação',
      input: "textarea",
      showCancelButton: true,
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar',
      allowOutsideClick: false,
      onOpen: (result) => {
        var text = result.querySelectorAll(".swal2-textarea");
        (<HTMLInputElement>text[0]).value = orderItem.description ? orderItem.description : '';
      },
      preConfirm: observation => {
        return new Promise((resolve, reject) => {
          if (observation === '')
            reject("Favor entrar com uma observação válida.")
          else {
            orderItem.description = observation;
            this.orderProductService.updateProduct(orderItem);
            resolve();
          }
        });
      }
    });

  }

  addQuantity(orderItem: OrderItemModel) {
    orderItem.quantity = orderItem.quantity + 1;
  }
  
  removeQuantity(orderItem: OrderItemModel) {
    if (orderItem.quantity > 1) {
      orderItem.quantity = orderItem.quantity - 1;
    }
  }

  // Método para adicionar desconto a lista
  get desconto() {
    var total = 0.00;
    this.orderItems.forEach(item => {
      if (item.discount.id)
        total += item.total;
    });

    return Utils.roundTo(total, 2);
  }


// Método para calcular valor sem extras
  get subtotal() {
    var total = 0.00;
    this.orderItems.forEach(item => {
      if (item.product.id)
        total += item.total;
    });

    return Utils.roundTo(total, 2);
  }

  //Método para exibir valor final
  get total() {
    var total = 0.00;
    this.orderItems.forEach(item => {
      total += item.total;
    });

    return Utils.roundTo(total, 2);
  }

  removeItem(orderItem: OrderItemModel) {
    //swal('Deseja excluir esse produto?')
    this.orderProductService.removeProduct(orderItem);
  }

  orderResume() {
    this.router.navigate(['resumo']);
  }

  orderResumeTotal() {
    if(this.orderItems == [] || this.orderItems == undefined) {
      swal('Você precisa adicionar itens ao pedido');
    }
    this.router.navigate(['resumo/total']);
  }
}
