import { OrderOpenPreService } from './order-open-pre.service';
import { AttendantModel } from './../../models/attendant.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrderOpenModel } from '../../models/order/orderOpen.model';

@Component({
  selector: 'app-order-open-pre',
  templateUrl: './order-open-pre.component.html',
  styleUrls: ['./order-open-pre.component.css']
})
export class OrderOpenPreComponent implements OnInit {
  attendantName: string = localStorage.getItem("attendantName");
  private attendantId: string = localStorage.getItem("attendantId");
  private _ticketNumber : number[] = []; 
  private _attendantModel : AttendantModel;
  
  get attendantNameMtds() {
    return this._attendantModel.name;
  }

  get ticketNumber() : string {
    var code = this._ticketNumber.toString().replace(/,/g,'');
    return code;
  }

  constructor(private router: Router, private orderOpenPreService : OrderOpenPreService) { }

  ngOnInit() {
    
    this._attendantModel = new AttendantModel();
    // Preencher as propriedades vindas do local storage
    this._attendantModel.name = localStorage.getItem("userName");
  }

  fillNumber(number) {
      this._ticketNumber.push(number);
  }
  
  removeNumber(){
    this._ticketNumber.pop();
  }

  // orderList(){
  //   var model = new OrderOpenModel();
  //   model.attendantId = this.attendantId;
  //   model.ticketNumber = this.ticketNumber;
    
  //   this.orderOpenService.openOrder(model).then(o => {
  //     localStorage.setItem("orderId", o.id)
  //     this.router.navigate(['/home']);
  //   })
  //   openOrderExpress
  // }

  sellOrder(){
    var model = new OrderOpenModel();
    model.attendantId = this.attendantId;
    //model.ticketNumber = this.ticketNumber;
    
    this.orderOpenPreService.openOrderExpress(model).then(o => {
      localStorage.setItem("orderId", o.id)
      localStorage.setItem("ticketNumber", o.ticketNumber);
      this.router.navigate(['/home']);
    })
    
  }
}
