export class PaymentStatusModel{
        /// <summary>
        /// Status atual do pagamento.
        /// </summary>
        status : StatusEnum;
        
        /// <summary>
        /// Mensagem de erro ou sucesso.
        /// </summary>
        message : string;
}

export enum StatusEnum{
     /// <summary>
        /// Aguardando inserir o cartão
        /// </summary>
        InsertCard = 1,
        /// <summary>
        /// Aguardando a senha ser digitada.
        /// </summary>
        WaitingPassword = 2,
        /// <summary>
        /// Pagamento efetuado com sucesso.
        /// </summary>
        Success = 3,
        /// <summary>
        /// Pagamento efetuado com erro.
        /// </summary>
        Failure = 4
}