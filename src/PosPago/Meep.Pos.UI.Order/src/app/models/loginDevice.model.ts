import { MeepTypeEnum } from "./configuration.model";

export class LoginDeviceModel{
    pinCode : string;
    browserFingerprint : string;
    resolution : string;
    userAgent : string;
    orderType : MeepTypeEnum;
}

export enum OrderTypeEnum{
    pos = 1,
    pre = 2
}