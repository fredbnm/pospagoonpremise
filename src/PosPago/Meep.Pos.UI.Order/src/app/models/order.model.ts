import Utils from "../util/utils";

export class OrderModel {
    id : string;
    products : OrderProduct[] = [];
    taxes : OrderTax[] = [];
    discounts : OrderDiscount[] = [];
    payments : Payment;
    dateOpened : Date;
    dateClosed : Date;
    _subTotal : number;
    get subTotal() {
        var total = 0;
        this.products.forEach(product => {
            total += product.total;
        });
        return Utils.roundTo(total,2);
    }
    _total : number;
    get total(){
        return Utils.roundTo(this._total,2);
    }
    set total(total : number){
        this._total = total;
    }

    _totalPayment : number;
    get totalPayment(){
        return Utils.roundTo(this._totalPayment,2);
    }
    set totalPayment(total : number){
        this._totalPayment = total;
    }
    ticketId : string;
    ticketNumber : string;
}

export class OrderTax{
    dateOrder : Date;
    count : number;
    value : number;
    total : number;
    taxId : string;
    taxName : string;
    taxCalcType : number;
    required : boolean;
}
export class OrderDiscount{
    dateOrder : Date;
    count : number;
    value : number;
    total : number;
    discountId : string;
    discountName : string;
    discountCalcType : number;
}
export class OrderProduct{
    dateOrder : Date;
    count : number;
    _value : number;
    get value(){
        return Utils.roundTo(this._value,2);
    }
    set value(value : number){
        this._value = value;
    }
    _total : number;
    get total(){
        return Utils.roundTo(this._total,2);
    }
    set total(total : number){
        this._total = total;
    }
    productId : string;
    productName : string;
    description : string;
}

export class Payment{
    id : string;
}