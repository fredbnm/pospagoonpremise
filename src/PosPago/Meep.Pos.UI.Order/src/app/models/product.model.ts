import Utils from "../util/utils";

export class ProductModel{
    id : string;
    name : string;
    categoryId : string;
    categoryName : string;
    _price : number;
    unitEnum : MeasureUnitEnum;
    get price(){
        return Utils.roundTo(this._price,2);
    }
    set price(price : number){
        this._price = price;
    }

  
}
export enum MeasureUnitEnum {
    Unit = 1,
    Kg = 2
}