export class PaymentModel{
    value : string;
    paymentState : number;
    paymentType : number;
    attendantId : string;
}