export class UserInfo {
    constructor() {
        this.endereco = new AddressMeep();
    }
    cpf: string;
    nome: string;
    email: string;
    ddd: string;
    telefone: string;
    rg: string;
    endereco: AddressMeep;
}

export class AddressMeep {
    cep: string;
    numero: string;
    logradouro: string;
    complemento: string;
    bairro: string;
    cidadeId: string;
    cidadeNome: string;
    estado: string;    
    estadoId: string;
    estadoUF: string;
}
