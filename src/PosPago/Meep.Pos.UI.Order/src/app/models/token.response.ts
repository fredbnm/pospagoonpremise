import { MeepTypeEnum } from "./configuration.model";

export class TokenResponse{
    access_token : string;
    expires_in : number;
    attendantId : string;
    attendantName : string;
    meepType : MeepTypeEnum;
}