export class ConfigurationModel {
    meepType: MeepTypeEnum;
}


export enum MeepTypeEnum {
    pre = 1,
    pos = 2,
    both = 3
}

