import { ValidationResult } from './models/validationResult.model';
import { HttpModule, Headers, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment.basedev';

@Injectable()
export class AppService<TModel> {
    
    constructor(private http: HttpClient) {

    }

    private version = environment.versionBase;
    private domain = environment.baseUrl;
    private url: string = this.domain + "/pos/api/" + this.version ;    
    
    headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
         'Access-Control-Allow-Credentials': '*',
        'Access-Control-Allow-Origin':'*',
         'Access-Control-Allow-Methods':'GET, POST, PATCH, PUT, DELETE, OPTIONS',
         "Access-Control-Allow-Headers":"Origin, X-Requested-With, X-XSRF-TOKEN, Content-Type, Accept, X-Auth-Token"
    });

    protected methodUrl(method: string) {
        return this.url.concat(method);
    }

    protected async getAsync<TModel>(methodName: string): Promise<TModel> {
        var result = await this.http.get<ValidationResult<TModel>>(this.methodUrl(methodName),
        {headers: this.headers})
        .toPromise();

        if (result.hasErrors)
            throw new Error(result.message);

        return result.value;
        
    }

    protected async getAllAsync<TModel>(methodName: string): Promise<TModel[]> {
        var result = await this.http.get<ValidationResult<TModel[]>>(this.methodUrl(methodName),
        {headers: this.headers})
        .toPromise();

        if (result.hasErrors)
            throw new Error(result.message);

        return result.value;
    }

    protected async postAsync<TModel>(methodName : string, model : any) : Promise<TModel>
    {
        //console.log("Enviou o post");
        var result = await this.http
                        .post<ValidationResult<TModel>>(this.methodUrl(methodName),
                        JSON.stringify(model),
                            {headers: this.headers})
                        .toPromise();
        //console.log("Já enviou o post");

        if (result.hasErrors)
            throw new Error(result.message);
 
        return result.value;
    }

    protected async putAsync<TModel>(methodName : string, model: any = null) : Promise<TModel>
    {
        var result = await this.http
                        .put<ValidationResult<TModel>>(this.methodUrl(methodName),
                        JSON.stringify(model),
                            {headers: this.headers})
                        .toPromise();
 
        if (result.hasErrors)
            throw new Error(result.message);
 
        return result.value;
    }

    protected async deleteAsync(methodName : string) : Promise<string>
    {
        var result = await this.http
                        .delete<ValidationResult<TModel>>(this.methodUrl(methodName),
                            {headers: this.headers})
                        .toPromise();
 
        if (result.hasErrors)
            throw new Error(result.message);
        
        return result.message;
    }
}
