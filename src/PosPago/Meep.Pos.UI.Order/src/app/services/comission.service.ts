import { ComissionModel } from './../models/comission.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppService } from './../app.service';

@Injectable()
export class ComissionService extends AppService<ComissionModel> {

  constructor(http: HttpClient) {
    super(http); 
  }

  method : string = 'comission';

  async getAll(): Promise<ComissionModel[]>{

    var orders = await this.getAllAsync(this.method);
    
    return <ComissionModel[]>orders;
  }
}