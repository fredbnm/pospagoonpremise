import { CategoryModel } from './../models/category/category.model';
import { AppService } from './../app.service';
import { HttpClient } from '@angular/common/http';
import { HttpModule, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpErrorResponse } from "@angular/common/http";

@Injectable()
export class CategoryService extends AppService<CategoryModel> {

  constructor(http: HttpClient) {
    super(http); 
  }
  
  method : string = 'category';

  async getAll(): Promise<CategoryModel[]>{

    var categories = await this.getAllAsync(this.method);
    
    return <CategoryModel[]>categories;
  }

  async get(id : string) : Promise<CategoryModel>{
    var result = await this.getAsync(this.method+'/'+id);

    return <CategoryModel>result;
  }

  async save(model : CategoryModel) : Promise<CategoryModel>{

    var result = await this.postAsync(this.method,model);

    return <CategoryModel>result;
  }

  async update(model : CategoryModel) : Promise<CategoryModel>{
    
    var result = await this.putAsync(this.method,model);

    return <CategoryModel>result;
  }
  
  async delete(id : string) : Promise<string>{
    var result = await this.deleteAsync(this.method+'/'+id);

    return result;
  }
}
