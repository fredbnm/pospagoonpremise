import { AppService } from './../app.service';
import { ConfigurationModel } from './../models/configuration.model'; 
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpModule, Headers } from '@angular/http';
import { HttpErrorResponse } from "@angular/common/http";
import { BrowserModule } from '@angular/platform-browser';
@Injectable()
export class ConfigurationService extends AppService<ConfigurationModel> {
  constructor(http: HttpClient) {
    super(http);
  }

  
  method: string = 'configuration';
  
  async getConfiguration() : Promise<ConfigurationModel>{
   var config = await this.getAsync(this.method) ;
   return <ConfigurationModel>config;
  }

  async update(model : ConfigurationModel) : Promise<ConfigurationModel>{
    var config = await this.putAsync(this.method,model);
    return <ConfigurationModel>config;
  }

}

