import { ProductModel } from './../models/product.model';
import { AppService } from './../app.service';

import { HttpClient } from '@angular/common/http';
import { HttpModule, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpErrorResponse } from "@angular/common/http";

@Injectable()
export class ProductsService extends AppService<ProductModel> {

  constructor(http: HttpClient) {
    super(http); 
  }

  method : string = 'product';

  async getAll(): Promise<ProductModel[]>{

    var products = await this.getAllAsync(this.method);
    
    return <ProductModel[]>products;
  }

  
  async getAllActive(): Promise<ProductModel[]>{
    
      var products = await this.getAllAsync(this.method + "/active");
      
      return <ProductModel[]>products;
  }

  async get(id : string) : Promise<ProductModel>{
    var result = await this.getAsync(this.method+'/'+id);

    return <ProductModel>result;
  }

  async save(product : ProductModel) : Promise<ProductModel>{

    var result = await this.postAsync(this.method,product);

    return <ProductModel>result;
  }

  async update(product : ProductModel) : Promise<ProductModel>{
    
    var result = await this.putAsync(this.method,product);

    return <ProductModel>result;
  }
  
  async delete(id : string) : Promise<string>{
    var result = await this.deleteAsync(this.method+'/'+id);

    return result;
  }
}
