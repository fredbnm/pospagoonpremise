//import { writeFile } from 'fs';
// import { dotenv } from 'dotenv';
// import { argv } from 'yargs';
// // import { writeFile } from 'fs';

// // This is good for local dev environments, when it's better to
// // store a projects environment variables in a .gitignore'd file
// //require('dotenv').config();
// dotenv.config();

// // Would be passed to script like this:
// // `ts-node set-env.ts --environment=dev`
// // we get it from yargs's argv object
// const environment = argv.environment;
// const isProd = environment === 'prod';

// const targetPath = `./src/environments/environment.${environment}.ts`;
// const envConfigFile = `
// export const environment = {
//   production: ${isProd},
//   baseUrl: "http://${process.env.API_IP}:${process.env.API_PORT}",
//   baseUrlMeep: 'http://meepserver-dev.azurewebsites.net',
//   versionBase:'v1/'
// };
// `
// writeFile(targetPath, envConfigFile, function (err) {
//   if (err) {
//     console.log(err);
//   }

//   console.log(`Output generated at ${targetPath}`);
// });