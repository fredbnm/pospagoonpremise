﻿using Meep.Pos.Domain.Core.Models.Event;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Command.Interface
{
    public interface ICommand
    {
        IEvent Event { get; }
    }
}
