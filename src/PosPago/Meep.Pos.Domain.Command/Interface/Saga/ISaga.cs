﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Command.Interface
{
    public interface ISaga 
    {
        void Execute<TCommand> (TCommand command) where TCommand : ICommand;
        //void RaiseEvent<TEvent> (TEvent @event) where TEvent : IEvent;
    }
}
