﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Command.Interface
{
    public interface ICommandHandler
    {
        void Handle<TCommand>(TCommand command) where TCommand : ICommand;
    }
    public interface ICommandHandler<TCommand> where TCommand : ICommand
    {
        //void Handle(TCommand command);
    }
}
