﻿using Meep.Pos.Domain.Command.Command.Production;
using Meep.Pos.Domain.Command.Interface;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Meep.Pos.Domain.Command.Production
{
    public class ProductionSaga : IProductionSaga
    {
        IProductionRepository _productionRepository;
        IProductRepository _productRepository;
        ITicketRepository _ticketRepository;
        IAttendantRepository _attendantRepository;

        public ProductionSaga(IProductionRepository productionRepository,
            IProductRepository productRepository,
            ITicketRepository ticketRepository,
            IAttendantRepository attendantRepository)
        {
            _productionRepository = productionRepository;
            _productRepository = productRepository;
            _ticketRepository = ticketRepository;
            _attendantRepository = attendantRepository;
        }

        public void Execute<TCommand>(TCommand command) where TCommand : ICommand
        {
            if (command.GetType() == typeof(CreateProductionCommand))
                CreateProduction(command as CreateProductionCommand);

            if (command.GetType() == typeof(DeleteProductionCommand))
                DeleteProduction(command as DeleteProductionCommand);
            
            
        }

        private void CreateProduction(CreateProductionCommand productionCommand)
        {
            var product = _productRepository.Get(productionCommand.ProductId);

            if (!product.IsProduction)
                return;

            var ticket = _ticketRepository.GetByFilter(t => t.Number == productionCommand.TicketNumber).FirstOrDefault();

            var attendant = _attendantRepository.Get(productionCommand.AttendantId);

            var production = new Domain.Production(ticket, product, attendant, productionCommand.Quantity, productionCommand.Date, productionCommand.Observation);

            production.AttachOrder(productionCommand.OrderId, productionCommand.OrderProductId);

            _productionRepository.Insert(production);
        }

        private void DeleteProduction(DeleteProductionCommand productionCommand)
        {
            var command = _productionRepository.GetAll().FirstOrDefault(p => p.OrderItemId == productionCommand.IdOrderProduct);
            _productionRepository.Delete(command.Id);
        }
    }
}
