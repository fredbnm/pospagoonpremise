﻿using Meep.Pos.Domain.Command.Base;
using Meep.Pos.Domain.Command.Interface;
using Meep.Pos.Domain.Core.Models.Event;
using Meep.Pos.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Command.Production
{
    public class CreateProductionCommand : ICommand
    {
        public CreateProductionCommand(string ticketNumber, Guid productId, string productName,
            decimal quantity, string observation, DateTime date, Guid orderProductId, Guid orderId, Guid attendantId)
        {
            TicketNumber = ticketNumber;
            ProductId = productId;
            ProductName = productName;
            Quantity = quantity;
            Observation = observation;
            OrderId = orderId;
            Date = date;
            OrderProductId = orderProductId;
            AttendantId = attendantId;
        }

        public string TicketNumber { get; set; }
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal Quantity { get; set; }
        public string Observation { get; set; }
        public Guid OrderId { get; set; }
        public Guid OrderProductId { get; set; }
        public DateTime Date { get; set; }
        public Guid AttendantId { get; set; }

        public IEvent Event => new Event(Core.Enumerators.EventTypeEnum.ProductionLine, $"Produto {this.ProductName} enviado para produção na mesa {this.TicketNumber}.", OrderProductId);
    }
}
