﻿using Meep.Pos.Domain.Command.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Core.Models.Event;
using Meep.Pos.Domain.Models;

namespace Meep.Pos.Domain.Command.Command.Production
{
    public class DeleteProductionCommand : ICommand
    {
        public DeleteProductionCommand(Guid id)
        {
            IdOrderProduct = id;
        }

        public Guid IdOrderProduct;

        public IEvent Event => new Event(Core.Enumerators.EventTypeEnum.Delete, null, IdOrderProduct);
    }
}
