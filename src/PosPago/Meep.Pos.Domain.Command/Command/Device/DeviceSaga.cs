﻿using Meep.Pos.Domain.Command.Interface.Saga;
using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Command.Interface;
using Meep.Pos.Infra.Data.Repository.Interfaces;

namespace Meep.Pos.Domain.Command.Command.Device
{
    public class DeviceSaga : IDeviceSaga
    {
        IDeviceRepository _deviceRepository;
        IAttendantRepository _attendantRepository;
        IDeviceAttendantRepository _deviceAttendantRepository;

        public DeviceSaga(IDeviceRepository deviceRepository,
            IAttendantRepository attendantRepository,
            IDeviceAttendantRepository deviceAttendantRepository)
        {
            _deviceRepository = deviceRepository;
            _attendantRepository = attendantRepository;
            _deviceAttendantRepository = deviceAttendantRepository;
        }
        public void Execute<TCommand>(TCommand command) where TCommand : ICommand
        {
            var deviceCommand = command as CreateDeviceAttendantCommand;
            Models.Device device;

            if (!deviceCommand.DeviceId.HasValue)
            {
                device = new Models.Device(deviceCommand.FingerPrint, deviceCommand.UserAgent, deviceCommand.Resolution);

                _deviceRepository.Insert(device);
            }
            else
            {
                device = _deviceRepository.Get(deviceCommand.DeviceId.Value);
            }

            var attendant = _attendantRepository.Get(deviceCommand.AttendantId);

            var deviceAttendant = new Models.DeviceAttendant(attendant, device);

            _deviceAttendantRepository.Insert(deviceAttendant);
        }
    }
}
