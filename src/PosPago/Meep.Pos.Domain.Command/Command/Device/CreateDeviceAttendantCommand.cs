﻿using Meep.Pos.Domain.Command.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Core.Models.Event;
using Meep.Pos.Domain.Models;

namespace Meep.Pos.Domain.Command.Command.Device
{
    public class CreateDeviceAttendantCommand : ICommand
    {
        public CreateDeviceAttendantCommand(string fingerprint, string userAgent, string resolution, Guid attendantId, string attendantName, Guid? deviceId = null)
        {
            FingerPrint = fingerprint;
            UserAgent = userAgent;
            Resolution = resolution;
            AttendantId = attendantId;
            AttendantName = attendantName;
            DeviceId = deviceId;
        }

        public string FingerPrint;
        public string UserAgent;
        public string Resolution;
        public Guid AttendantId;
        public string AttendantName;
        public Guid? DeviceId;

        public IEvent Event => new Event(Core.Enumerators.EventTypeEnum.Device, $"Atendente {AttendantName} solicitou login no order através de um novo dispositivo.", AttendantId);
    }
}
