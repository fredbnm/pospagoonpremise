﻿using Meep.Pos.Domain.Command.Command.Device;
using Meep.Pos.Domain.Command.Command.Production;
using Meep.Pos.Domain.Command.Interface;
using Meep.Pos.Domain.Command.Interface.Saga;
using Meep.Pos.Domain.Command.Production;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Meep.Pos.Domain.Command.Base
{
    public class CommandHandler : ICommandHandler
    {
        IServiceProvider _serviceProvider;
        IEventRepository _eventRepository;
        
        public CommandHandler(IServiceProvider serviceProvider,
            IEventRepository eventRepository)
        {
            _serviceProvider = serviceProvider;
            _eventRepository = eventRepository;
        }

        public void Handle<TCommand>(TCommand command) where TCommand : ICommand
        {
            var saga = Instantiate(command);

            saga.Execute(command);

            if (command.Event != null)
            {
                if (command.Event.EventType == Core.Enumerators.EventTypeEnum.Delete)
                    _eventRepository.ExcludeEvent(command.Event.Identifier);
                else
                    _eventRepository.Insert(command.Event as Event);
            }
        }

        private ISaga Instantiate<T>(T command) where T : ICommand
        {
            if (command.GetType().Equals(typeof(CreateProductionCommand)))
                return _serviceProvider.GetService(typeof(IProductionSaga)) as ProductionSaga;

            if (command.GetType().Equals(typeof(CreateDeviceAttendantCommand)))
                return _serviceProvider.GetService(typeof(IDeviceSaga)) as DeviceSaga;

            if (command.GetType().Equals(typeof(DeleteProductionCommand)))
                return _serviceProvider.GetService(typeof(IProductionSaga)) as ProductionSaga;

            throw new DomainException("Commando não implementado na fabrica do CommandHandler.");

            //switch (command.GetType().ToString().Split('.').LastOrDefault())
            //{
            //    case "CreateProductionCommand":
            //        var productionCommandHandler = _serviceProvider.GetService(typeof(IProductionSaga));
            //        return productionCommandHandler as ProductionSaga;
            //    default:
            //        return null;
            //}
        }
    }
}
