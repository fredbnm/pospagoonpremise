﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bogus;
using Bogus.Extensions.Brazil;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.Mapping;
using Meep.Pos.Service.Test.Mock;
using Meep.Pos.Service.ValueObjects;
using Xunit;
using Meep.Pos.Domain.Core.Exception.Ticket;

namespace Meep.Pos.Service.Test
{
    public class TicketServiceTest : IDisposable
    {
        private ITicketService _ticketService;
        public TicketServiceTest()
        {
            var ticketList = new List<Ticket> { MockDomain.Ticket("1"), MockDomain.Ticket("2"), MockDomain.Ticket("3") };

            var mock = FakeRepository<Ticket>.GetMock<TicketRepository>(ticketList);

            var automapper = AutoMapperConfig.RegisterMappings().CreateMapper();

            _ticketService = new TicketService(mock.Object, automapper);
        }

        #region [ Tests ]

        [Fact]
        public void Get()
        {
            var tickets = _ticketService.GetAll();

            Assert.Equal(3, tickets.Count());

            var ticket = tickets.FirstOrDefault(d => d.Number == "1");

            var ticketFound = _ticketService.Get(ticket.Id.Value);

            Assert.NotNull(ticketFound);
            Assert.Equal(ticket.Number, ticketFound.Number);
        }

        [Fact]
        public void Exception_CreateAttendantWithSameName()
        {
            var attendant = new TicketValueObject().Mock();
            attendant.Number = "1";

            Assert.Throws<TicketAlreadyExistsException>(() => _ticketService.Create(attendant));
        }

        [Fact]
        public void Exception_GetNonExistingTicket()
        {
            Assert.Throws<TicketDoesntExistsException>(() => _ticketService.Get(Guid.NewGuid()));
        }

        [Fact]
        public void CreateAndGet()
        {
            var ticketValueObject = new TicketValueObject().Mock();

            var savedticket = _ticketService.Create(ticketValueObject);

            Assert.NotNull(savedticket);

            Assert.NotEqual(Guid.Empty, savedticket.Id);

            var ticket = _ticketService.Get(savedticket.Id.Value);

            Assert.NotNull(ticket);

            Assert.Equal(savedticket.Number, ticket.Number);
        }
        
        [Fact]
        public void Update()
        {
            var tickets = _ticketService.GetAll();

            Assert.Equal(3, tickets.Count());

            var ticket = tickets.FirstOrDefault(t => t.Number == "1");

            Assert.NotNull(ticket);

            ticket.Number = new Faker<string>().CustomInstantiator(f => f.Random.Number().ToString());

            _ticketService.Update(ticket);

            var updatedTicket = _ticketService.Get(ticket.Id.Value);

            Assert.Equal(ticket.Number, updatedTicket.Number);
        }

        [Fact]
        public void Exception_UpdateTicketWithoutId()
        {
            var ticket = new TicketValueObject().Mock();

            Assert.Throws<TicketIdRequiredException>(() => _ticketService.Update(ticket));
        }

        [Fact]
        public void Remove()
        {
            var tickets = _ticketService.GetAll();

            _ticketService.Remove(tickets.FirstOrDefault().Id.Value);

            tickets = _ticketService.GetAll();

            Assert.Equal(2, tickets.Count());
        }

        [Fact]
        public void Exception_RemoveNonExistingTicket()
        {
            var ticketId = _ticketService.GetAll().FirstOrDefault().Id;

            _ticketService.Remove(ticketId.Value);

            Assert.Throws<TicketDoesntExistsException>(() => _ticketService.Remove(ticketId.Value));

        }

        #endregion

        #region [ Dispose ]
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
