﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Bogus;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository;
using Meep.Pos.Service.Mapping;
using Meep.Pos.Service.Test.Mock;
using Meep.Pos.Service.ValueObjects;
using Xunit;
using Meep.Pos.Domain.Core.Localization;
using Folke.Localization.Json;
using Microsoft.Extensions.Options;
using Moq;
using Meep.Pos.Domain.Core.Exception.Product;
using System.Data;

namespace Meep.Pos.Service.Test
{
    public class ProductServiceTest : IDisposable
    {
        private readonly ProductService _productService;
        private readonly CategoryService _categoryService;

        public ProductServiceTest()
        {
            var db = new Mock<IDbConnection>();
            var autoMapper = AutoMapperConfig.RegisterMappings().CreateMapper();
            var fakeCategoryRepository = FakeRepository<Category>.GetMock<CategoryRepository>(db);
            var fakeProductRepository = FakeRepository<Product>.GetMock<ProductRepository>(db);
            var localization = new Mock<ICustomJsonLocalization>();

            _productService = new ProductService(fakeProductRepository.Object,
                                                fakeCategoryRepository.Object,
                                                autoMapper);

            _categoryService = new CategoryService(fakeCategoryRepository.Object, autoMapper);
        }

        #region [ Tests ]

        [Fact]
        public void CreateAndGet()
        {
            var productVO = new ProductValueObject().Mock();

            var categoryVO = new CategoryValueObject().Mock();

            var savedCategory = _categoryService.Create(categoryVO);

            productVO.CategoryId = savedCategory.Id;

            var savedProduct = _productService.Create(productVO);

            Assert.NotNull(savedProduct);
            Assert.NotEqual(Guid.Empty, savedProduct.Id);

            var product = _productService.Get(savedProduct.Id.Value);

            //Validando se o produto recuperado é o produto salvo.
            Assert.NotNull(product);
            Assert.Equal(product.Name, savedProduct.Name);
        }

        [Fact]
        public void Exception_GetNonExistingProduct()
        {
            var id = Guid.NewGuid();

            Assert.Throws<ProductDoesntExistsException>(() => _productService.Get(id));
            
        }

        [Fact]
        public void Update()
        {
            var categoryVO = new CategoryValueObject().Mock();

            var savedCategory = _categoryService.Create(categoryVO);

            var productVO = new ProductValueObject().Mock();

            productVO.CategoryId = savedCategory.Id;

            var insertedProduct = _productService.Create(productVO);

            insertedProduct.Name = "Changed";

            var updatedProduct = _productService.Update(insertedProduct);

            Assert.Equal("Changed", updatedProduct.Name);

            Assert.Equal("Changed", _productService.Get(updatedProduct.Id.Value).Name);
        }

        [Fact]
        public void Exception_UpdateProductWithoutId()
        {
            var product = new ProductValueObject().Mock();

            Assert.Throws<ProductIdRequiredException>(() => _productService.Update(product));
        }

        [Fact]
        public void Remove()
        {
            var categoryVO = new CategoryValueObject().Mock();

            var savedCategory = _categoryService.Create(categoryVO);

            var productVO = new ProductValueObject().Mock();

            productVO.CategoryId = savedCategory.Id;

            var savedProduct = _productService.Create(productVO);

            _productService.Remove(savedProduct.Id.Value);

            Assert.Throws<ProductDoesntExistsException>(() => _productService.Get(savedProduct.Id.Value));

        }

        [Fact]
        public void Exception_CreateTwoProductsWithSameName()
        {
            var categoryVO = new CategoryValueObject().Mock();

            var savedCategory = _categoryService.Create(categoryVO);

            var productVO = new ProductValueObject().Mock();

            productVO.CategoryId = savedCategory.Id;
            var productVO2 = productVO;

            _productService.Create(productVO);

            Assert.Throws<ProductAlreadyExistsException>(() => _productService.Create(productVO2));
        }

        [Fact]
        public void GetAll()
        {
            var categoryVO = new CategoryValueObject().Mock();

            var savedCategory = _categoryService.Create(categoryVO);

            var productVO = new ProductValueObject().Mock();

            productVO.CategoryId = savedCategory.Id;

            _productService.Create(productVO);

            var productVO2 = new ProductValueObject().Mock();

            productVO2.CategoryId = savedCategory.Id;

            _productService.Create(productVO2);

            var products = _productService.GetAll();

            Assert.Equal(2, products.Count());

        }
        

        #endregion

        #region [ Dispose ]
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
