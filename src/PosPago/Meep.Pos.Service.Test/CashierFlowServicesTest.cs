﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.Mapping;
using Meep.Pos.Service.Test.Mock;
using Meep.Pos.Service.ValueObjects;
using Xunit;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Core.Exception.Order;
using System.Data;
using Moq;
using Meep.Pos.Service.Test.Mock.Repository;
using Meep.Pos.Domain.Command.Interface;

namespace Meep.Pos.Service.Test
{
    /// <summary>
    /// Fluxo completo de caixa passando pelos services.
    /// </summary>
    public class CashierFlowServicesTest : IDisposable
    {
        private IOrderService _orderService;
        private ICashierService _cashierService;
        private List<Orders> _orders;
        private List<Product> _products;
        private List<Tax> _taxes;
        private List<Discount> _discounts;
        private Attendant _attendant;
        private AppUser _user;
        private Ticket _ticketMocked;
        private IMapper _mapper;
        private Comission _comission;

        public CashierFlowServicesTest()
        {
            var autoMapper = AutoMapperConfig.RegisterMappings().CreateMapper();

            _orders = MockDomain.OrderList(3);
            _products = new List<Product>() { MockDomain.Product("Cerveja",5), MockDomain.Product("Espeto",8) };
            _taxes = new List<Tax>() { MockDomain.Tax("10% Garçom", PriceCalcTypeEnum.Percent, 10) };
            _discounts = new List<Discount>() { MockDomain.Discount("5% Off", PriceCalcTypeEnum.Percent, 5) };
            _attendant = MockDomain.Attendant();
            _ticketMocked = MockDomain.Ticket();
            _user = MockDomain.User();
            _comission = MockDomain.Comission();

            var db = new Mock<IDbConnection>();

            var orderRepository = CustomRepository.MockOrderRepository(_orders);

            //orderRepository.Setup()

            var productRepository = FakeRepository<Product>.GetMock<ProductRepository>(_products);
            var taxRepository = FakeRepository<Tax>.GetMock<TaxRepository>(_taxes);
            var discountRepository = FakeRepository<Discount>.GetMock<DiscountRepository>(_discounts);
            var attendantRepository = FakeRepository<Attendant>.GetMock<AttendantRepository>(new List<Attendant> { _attendant });
            var cashierRepository = FakeRepository<Cashier>.GetMock<CashierRepository>(db);
            var ticketRepository = FakeRepository<Ticket>.GetMock<TicketRepository>(new List<Ticket> { _ticketMocked });
            var userRepository = FakeRepository<AppUser>.GetMock<UserRepository>(new List<AppUser>{ _user });
            var commandHandler = new Mock<ICommandHandler>();
            var comissionRepository = FakeRepository<Comission>.GetMock<ComissionRepository>(new List<Comission> { _comission });
            var configurationRepository = FakeRepository<Configuration>.GetMock<ConfigurationRepository>();
            var integrationService = new Mock<IIntegrationService>();

            var ticketService = new TicketService(ticketRepository.Object, autoMapper);

            _orderService = new OrderService(orderRepository.Object, productRepository.Object, taxRepository.Object,
                discountRepository.Object, attendantRepository.Object, configurationRepository.Object,
                integrationService.Object, comissionRepository.Object, commandHandler.Object, autoMapper);

            _cashierService = new CashierService(attendantRepository.Object, cashierRepository.Object,
                ticketRepository.Object, orderRepository.Object, userRepository.Object, taxRepository.Object, ticketService, autoMapper);
        }


        #region [ Tests ]
        [Fact]
        public void CashierFlow()
        {
            //abrindo o caixa
            var cashier = _cashierService.OpenCashier(_user.Id, 100);

            Assert.NotEqual(DateTime.MinValue, cashier.DateOpened);

            //abrindo uma order
            cashier = _cashierService.OpenOrder(cashier.Id, _attendant.Id, _ticketMocked.Id);

            Assert.Equal(1, cashier.OrdersId.Length);

            var order = _orderService.AddItem(cashier.OrdersId.FirstOrDefault(),
                new OrderItemValueObject() { ItemId = _products.FirstOrDefault(p => p.Name == "Cerveja").Id, Quantity = 12 });

            order = _orderService.AddItem(cashier.OrdersId.FirstOrDefault(),
                new OrderItemValueObject() { ItemId = _taxes.FirstOrDefault().Id, Quantity = 1 });

            //Cervejas + 10% = 66
            Assert.Equal(66, order.Total);

            order = _orderService.AddPayment(order.Id.Value,
                new PaymentValueObject().Mock(33, PaymentStateEnum.Partial, PaymentTypeEnum.Money, _attendant.Id));

            //após pagamento parcial de 33, sobram 33.
            Assert.Equal(33, order.Total - order.TotalPayment);

            order = _orderService.AddItem(order.Id.Value,
                new OrderItemValueObject() { ItemId = _products.FirstOrDefault(p => p.Name == "Espeto").Id, Quantity = 3 });
            
            //Valor total 92,4, menos parcial = 59,4.
            Assert.Equal((decimal)59.4, order.Total - order.TotalPayment);

            //Adicionando 5% de desconto ao valor total
            order = _orderService.AddItem(order.Id.Value,
                new OrderItemValueObject() { ItemId = _discounts.FirstOrDefault().Id, Quantity = 1 });

            //5% de desconto sobre 92,4 = 87,78 - parcial = 54,78
            Assert.Equal((decimal)54.78, order.Total - order.TotalPayment);

            order = _orderService.AddPayment(order.Id.Value,
                new PaymentValueObject().Mock(25, PaymentStateEnum.Final, PaymentTypeEnum.Credit, _attendant.Id));

            order = _orderService.AddPayment(order.Id.Value,
                new PaymentValueObject().Mock((decimal)29.78, PaymentStateEnum.Final, PaymentTypeEnum.Credit, _attendant.Id));

            Assert.Equal(3, order.Payments.Count());

            Assert.Equal(order.TotalPayment, order.Total);

            //Fechando mesa/pedidos
            cashier = _cashierService.CloseOrder(cashier.Id, order.Id.Value);

            //abrindo nova mesa/pedido
            cashier = _cashierService.OpenOrder(cashier.Id, _attendant.Id, _ticketMocked.Id);

            Assert.Equal(2, cashier.OrdersId.Count());

            var secondOrder = _orderService.AddItem(cashier.OrdersId.LastOrDefault(),
                new OrderItemValueObject() { ItemId = _products.FirstOrDefault(p => p.Name == "Cerveja").Id, Quantity = 4 });

            secondOrder = _orderService.AddPayment(secondOrder.Id.Value,
                new PaymentValueObject().Mock(20, PaymentStateEnum.Final, PaymentTypeEnum.Debit, _attendant.Id));

            cashier = _cashierService.CloseOrder(cashier.Id, secondOrder.Id.Value);

            cashier = _cashierService.Get(cashier.Id);

            //Total do caixa deve ser a soma das orders abertas, mais o valor que ele foi aberto inicialmente.
            var total = cashier.InitialValue + order.Total + secondOrder.Total;
            Assert.Equal(total, cashier.Total);

            order = _cashierService.GetOrder(cashier.Id, order.Id.Value);

            Assert.NotNull(order.DateClosed);
            Assert.NotEqual(DateTime.MinValue, order.DateClosed);

            cashier = _cashierService.CloseCashier(cashier.Id, _user.Id);

            cashier = _cashierService.Get(cashier.Id);

            Assert.NotEqual(DateTime.MinValue, cashier.DateClosed);

        }


        [Fact]
        public void Exception_AddItemOnClosedOrder()
        {
            //abrindo o caixa
            var cashier = _cashierService.OpenCashier(_attendant.Id, 100);

            Assert.NotEqual(DateTime.MinValue, cashier.DateOpened);

            //abrindo uma order
            cashier = _cashierService.OpenOrder(cashier.Id, _attendant.Id, _ticketMocked.Id);

            cashier = _cashierService.CloseOrder(cashier.Id, cashier.OrdersId.First());

            Assert.Throws<OrderAlreadyClosedException>(() => _orderService.AddItem(cashier.OrdersId.First(),
                new OrderItemValueObject() { ItemId = _products.FirstOrDefault(p => p.Name == "Cerveja").Id, Quantity = 4 }));
        }


        #endregion

        #region [ Dispose ]
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
