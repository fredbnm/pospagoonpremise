﻿using Bogus;
using Bogus.Extensions.Brazil;
using Meep.Pos.Domain.Core.Exception.User;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.Mapping;
using Meep.Pos.Service.Test.Mock;
using Meep.Pos.Service.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Meep.Pos.Service.Test
{
    public class UserServiceTest : IDisposable
    {
        private IUserService _userService;
        private List<AppUser> _userList;
        public UserServiceTest()
        {
            _userList = new List<AppUser> { MockDomain.User("José"), MockDomain.User(), MockDomain.User() };

            var mock = FakeRepository<AppUser>.GetMock<UserRepository>(_userList);

            var automapper = AutoMapperConfig.RegisterMappings().CreateMapper();

            _userService = new UserService(mock.Object, automapper);
        }

        #region [ Tests ]

        [Fact]
        public void Get()
        {
            var users = _userService.GetAll();

            Assert.Equal(3, users.Count());

            var user = users.FirstOrDefault(d => d.Name == "José");

            var userFound = _userService.Get(user.Id.Value);

            Assert.NotNull(userFound);
            Assert.Equal(user.Cpf, userFound.Cpf);
        }

        [Fact]
        public void Exception_GetNonExistingUser()
        {
            Assert.Throws<UserDoesntExistsException>(() => _userService.Get(Guid.NewGuid()));
        }

        [Fact]
        public void CreateAndGet()
        {
            var userVO = new UserValueObject().Mock();

            var savedUser = _userService.Create(userVO);

            Assert.NotNull(savedUser);

            Assert.NotEqual(Guid.Empty, savedUser.Id);

            var attendant = _userService.Get(savedUser.Id.Value);

            Assert.NotNull(attendant);

            Assert.Equal(savedUser.Cpf, attendant.Cpf);
        }

        [Fact]
        public void Exception_CreateUserWithSameName()
        {
            var user = new UserValueObject().Mock();
            user.Login = _userService.GetAll().FirstOrDefault(a => a.Name == "José").Login;

            Assert.Throws<UserAlreadyExistsException>(() => _userService.Create(user));
        }

        [Fact]
        public void Update()
        {
            var users = _userService.GetAll();

            Assert.Equal(3, users.Count());

            var userToUpdate = users.FirstOrDefault(t => t.Name == "José");

            Assert.NotNull(userToUpdate);

            userToUpdate.Cpf = new Faker<string>().CustomInstantiator(f => f.Person.Cpf());
            userToUpdate.Name = "Bruce Wayne";

            _userService.Update(userToUpdate);

            var updatedUser = _userService.Get(userToUpdate.Id.Value);

            Assert.Equal(userToUpdate.Cpf, updatedUser.Cpf);
            Assert.Equal("Bruce Wayne", updatedUser.Name);
        }

        [Fact]
        public void Exception_UpdateUserWithoutId()
        {
            var user = new UserValueObject().Mock();

            Assert.Throws<UserIdRequiredException>(() => _userService.Update(user));
        }

        [Fact]
        public void Remove()
        {
            var users = _userService.GetAll();

            _userService.Remove(users.FirstOrDefault().Id.Value);

            users = _userService.GetAll();

            Assert.Equal(2, users.Count());
        }

        [Fact]
        public void Exception_RemoveNonExistingUser()
        {
            var userId = _userService.GetAll().FirstOrDefault().Id;

            _userService.Remove(userId.Value);

            Assert.Throws<UserDoesntExistsException>(() => _userService.Remove(userId.Value));

        }

        [Fact]
        public void ChangePassword()
        {
            var user = _userList.FirstOrDefault();

            var userVO = _userService.ChangePassword(user.Login, user.Password, "teste123");

            Assert.Equal("teste123", userVO.Password);
        }

        [Fact]
        public void Exception_ChangePassword()
        {
            var user = _userList.FirstOrDefault();

            Assert.Throws<UserDoesntExistsException>(() => _userService.ChangePassword("xpto", user.Password, "teste123"));

            Assert.Throws<UserWrongPasswordException>(() => _userService.ChangePassword(user.Login, "bolinha", "teste123"));

            Assert.Throws<UserNewPasswordSameAsOldException>(() => _userService.ChangePassword(user.Login, user.Password, user.Password));
        }

        [Fact]
        public void ChangeEmail()
        {
            var user = _userList.FirstOrDefault();

            var userVO = _userService.ChangeEmail(user.Login, user.Password, user.Email, "Brunoteste@teste.com");

            Assert.Equal("Brunoteste@teste.com", userVO.Email);
        }

        [Fact]
        public void Exception_ChangeEmail()
        {
            var user = _userList.FirstOrDefault();

            var email = _userList.LastOrDefault().Email;

            Assert.Throws<UserNewEmailAlreadyExistsException>(() => _userService.ChangeEmail(user.Login, user.Password, user.Email, email));

            Assert.Throws<UserWrongEmailException>(() => _userService.ChangeEmail(user.Login, user.Password, "teste@teste.com", "teste2@teste.com"));
        }
        #endregion

        #region [ Dispose ]
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
