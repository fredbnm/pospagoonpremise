﻿using Meep.Pos.Domain;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository;
using Microsoft.Extensions.Caching.Memory;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Meep.Pos.Service.Test.Mock.Repository
{
    public static class CustomRepository
    {
        public static Mock<OrderRepository> MockOrderRepository(List<Orders> orders)
        {
            var orderRepository = FakeRepository<Orders>.GetMock<OrderRepository>(orders);

            orderRepository.Setup(mr => mr.OpenOrder(It.IsAny<Orders>(), It.IsAny<Guid>())).Returns(
                (Orders order, Guid cashierId) => order);

            orderRepository.Setup(mr => mr.SaveItems(It.IsAny<Orders>())).Returns(
                (Orders order) => order);

            orderRepository.Setup(mr => mr.SavePayment(It.IsAny<Payment>(), It.IsAny<Guid>())).Returns(
                (Payment payment, Guid orderId) => payment);

            //orderRepository.Setup(mr => mr.GetByCashierId(It.IsAny<Guid>())).Returns(
            //    (Guid id) => retu)


            return orderRepository;
        }

        public static Mock<ProductionRepository> MockProductionRepository(List<Production> productions)
        {

            List<Production> items = productions;

            var productionRepository = new Mock<ProductionRepository>();

            productionRepository.Setup(p => p.Get(It.IsAny<Guid>())).Returns((Guid id) => items.Where(i => i.Id.Equals(id)).FirstOrDefault());

            productionRepository.Setup(p => p.Insert(It.IsAny<Production>()))
                .Returns((Production prod) => 
                {
                    items.Add(prod);
                    return prod;
                });

            productionRepository.Setup(p => p.Delete(It.IsAny<Guid>()))
                .Returns((Guid id) =>
                {
                    var item = items.FirstOrDefault(i => i.Id == id);
                    return items.Remove(item);
                });

            return productionRepository;
        }

        public static Mock<ProductionRepository> MockProductionRepository()
        {

            List<Production> items = new List<Production>();

            var productionRepository = new Mock<ProductionRepository>();

            productionRepository.Setup(p => p.Get(It.IsAny<Guid>())).Returns((Guid id) => items.Where(i => i.Id.Equals(id)).FirstOrDefault());

            productionRepository.Setup(p => p.Insert(It.IsAny<Production>()))
                .Returns((Production prod) =>
                {
                    items.Add(prod);
                    return prod;
                });

            productionRepository.Setup(p => p.Delete(It.IsAny<Guid>()))
                .Returns((Guid id) =>
                {
                    var item = items.FirstOrDefault(i => i.Id == id);
                    return items.Remove(item);
                });

            return productionRepository;
        }
    }
}
