﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Meep.Pos.Domain.Core.Models;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Moq;
using Meep.Pos.Infra.Data.Dapper.Repository;
using Meep.Pos.Infra.Data.Repository.Core.Interfaces;
using System.Data;

namespace Meep.Pos.Service.Test.Mock
{
    public static class FakeRepository<TEntity> where TEntity : BaseModel
    {
        public static Mock<TRepository> GetMock<TRepository>() where TRepository : Repository<TEntity>, IRepository<TEntity>
        {
            var items = new List<TEntity>();

            var db = new Mock<IDbConnection>();

            var mock = new Mock<TRepository>(MockBehavior.Strict, db.Object);

            mock.Setup(mr => mr.Insert(It.IsAny<TEntity>())).Returns(
                (TEntity target) =>
                {
                    items.Add(target);
                    return target;
                });

            mock.Setup(mr => mr.Get(It.IsAny<Guid>())).Returns(
                (Guid id) => items.FirstOrDefault(c => c.Id == id));

            mock.Setup(mr => mr.GetAll()).Returns(() => items);

            mock.Setup(mr => mr.GetByFilter(It.IsAny<Func<TEntity, bool>>())).Returns(
                (Func<TEntity, bool> filter) => items.Where(filter));

            mock.Setup(mr => mr.Update(It.IsAny<TEntity>())).Returns(
                (TEntity item) =>
                {
                    var itemToUpdate = items.FirstOrDefault(i => i.Id == item.Id);
                    itemToUpdate = item;
                    return itemToUpdate;
                });

            mock.Setup(mr => mr.Remove(It.IsAny<Guid>())).Returns(
                (Guid id) =>
                {
                    var item = items.FirstOrDefault(i => i.Id == id);
                    return items.Remove(item);
                }
            );

            return mock;
        }

        public static Mock<TRepository> GetMock<TRepository>(Mock<IDbConnection> db) where TRepository : Repository<TEntity>, IRepository<TEntity>
        {
            var items = new List<TEntity>();

            var mock = new Mock<TRepository>(MockBehavior.Strict, db.Object);

            mock.Setup(mr => mr.Insert(It.IsAny<TEntity>())).Returns(
                (TEntity target) =>
                {
                    items.Add(target);
                    return target;
                });

            mock.Setup(mr => mr.Get(It.IsAny<Guid>())).Returns(
                (Guid id) => items.FirstOrDefault(c => c.Id == id));

            mock.Setup(mr => mr.GetAll()).Returns(() => items);

            mock.Setup(mr => mr.GetByFilter(It.IsAny<Func<TEntity, bool>>())).Returns(
                (Func<TEntity, bool> filter) => items.Where(filter));

            mock.Setup(mr => mr.Update(It.IsAny<TEntity>())).Returns(
                (TEntity item) =>
                {
                    var itemToUpdate = items.FirstOrDefault(i => i.Id == item.Id);
                    itemToUpdate = item;
                    return itemToUpdate;
                });

            mock.Setup(mr => mr.Remove(It.IsAny<Guid>())).Returns(
                (Guid id) =>
                {
                    var item = items.FirstOrDefault(i => i.Id == id);
                    return items.Remove(item);
                }
            );

            return mock;
        }

        public static Mock<TRepository> GetMock<TRepository>(List<TEntity> itemsList) where TRepository : Repository<TEntity>, IRepository<TEntity>
        {
            var items = new List<TEntity>();

            items.AddRange(itemsList);

            var db = new Mock<IDbConnection>();

            var mock = new Mock<TRepository>(MockBehavior.Strict, db.Object);

            mock.Setup(mr => mr.Insert(It.IsAny<TEntity>())).Returns(
                (TEntity target) =>
                {
                    items.Add(target);
                    return target;
                });

            mock.Setup(mr => mr.Get(It.IsAny<Guid>())).Returns(
                (Guid id) => items.FirstOrDefault(c => c.Id == id));

            mock.Setup(mr => mr.GetAll()).Returns(() => items);

            mock.Setup(mr => mr.GetByFilter(It.IsAny<Func<TEntity, bool>>())).Returns(
                (Func<TEntity, bool> filter) => items.Where(filter));

            mock.Setup(mr => mr.Update(It.IsAny<TEntity>())).Returns(
                (TEntity item) =>
                {
                    var itemToUpdate = items.FirstOrDefault(i => i.Id == item.Id);
                    itemToUpdate = item;
                    return itemToUpdate;
                });

            mock.Setup(mr => mr.Remove(It.IsAny<Guid>())).Returns(
                (Guid id) =>
                {
                    var item = items.FirstOrDefault(i => i.Id == id);
                    return items.Remove(item);
                }
            );

            return mock;
        }

        public static Mock<TRepository> GetMock<TRepository>(Mock<IDbConnection> db, List<TEntity> itemsList) where TRepository : Repository<TEntity>, IRepository<TEntity>
        {
            var items = new List<TEntity>();

            items.AddRange(itemsList);

            var mock = new Mock<TRepository>(MockBehavior.Strict, db.Object);
            //var mock = Moq.Mock.Of<TRepository>();            

            mock.Setup(mr => mr.Insert(It.IsAny<TEntity>())).Returns(
                (TEntity target) =>
                {
                    items.Add(target);
                    return target;
                });

            mock.Setup(mr => mr.Get(It.IsAny<Guid>())).Returns(
                (Guid id) => items.FirstOrDefault(c => c.Id == id));

            mock.Setup(mr => mr.GetAll()).Returns(() => items);

            mock.Setup(mr => mr.GetByFilter(It.IsAny<Func<TEntity, bool>>())).Returns(
                (Func<TEntity, bool> filter) => items.Where(filter));

            mock.Setup(mr => mr.Update(It.IsAny<TEntity>())).Returns(
                (TEntity item) =>
                {
                    var itemToUpdate = items.FirstOrDefault(i => i.Id == item.Id);
                    itemToUpdate = item;
                    return itemToUpdate;
                });

            mock.Setup(mr => mr.Remove(It.IsAny<Guid>())).Returns(
                (Guid id) =>
                {
                    var item = items.FirstOrDefault(i => i.Id == id);
                    return items.Remove(item);
                }
            );

            return mock;
        }

    }
}
