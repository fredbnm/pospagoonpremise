﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bogus;
using Bogus.DataSets;
using Bogus.Extensions.Brazil;
using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Models;
using Xunit.Sdk;

namespace Meep.Pos.Service.Test.Mock
{
    public static class MockDomain
    {
        public static Tax Tax(string description = null, PriceCalcTypeEnum? calcType = null, decimal? value = null, bool required = false)
        {
            return new Faker<Tax>()
                .CustomInstantiator(f =>
                    new Tax(
                        !string.IsNullOrEmpty(description) ?
                            description :
                            f.Random.Word(),
                        calcType.HasValue ? calcType.Value : f.PickRandom<PriceCalcTypeEnum>(),
                        value.HasValue ? value.Value : f.Random.Decimal(0, 100),
                        required));
        }

        public static Discount Discount(string description = null, PriceCalcTypeEnum? calcType = null, decimal? value = null)
        {
            return new Faker<Discount>()
                .CustomInstantiator(f =>
                    new Discount(
                        !string.IsNullOrEmpty(description) ?
                            description :
                            f.Random.Word(),
                        calcType.HasValue ? calcType.Value : f.PickRandom<PriceCalcTypeEnum>(),
                        value.HasValue ? value.Value : f.Random.Decimal(0, 100)));
        }

        public static Comission Comission(string description = null, decimal? value = null)
        {
            return new Faker<Comission>()
                .CustomInstantiator(f =>
                    new Comission(
                        !string.IsNullOrEmpty(description) ?
                            description :
                            f.Random.Word(),
                        value.HasValue ? value.Value : f.Random.Decimal(0, 100)));
        }

        public static Attendant Attendant(string name = null, string cpf = null, SexEnum? sex = null)
        {
            return new Faker<Attendant>()
                .CustomInstantiator(f => 
                    new Attendant(
                        !string.IsNullOrWhiteSpace(name) ? 
                            name : 
                            f.Name.FullName(), 
                        !string.IsNullOrWhiteSpace(cpf) ?
                            cpf :
                            f.Person.Cpf(),
                        sex.HasValue ?
                            sex.Value :
                            f.PickRandom<SexEnum>()));
        }

        public static Ticket Ticket(string number = null)
        {
            return new Faker<Ticket>()
                .CustomInstantiator(f =>
                    new Ticket(!string.IsNullOrWhiteSpace(number) ? number : f.Random.Number(1000).ToString()));
        }

        public static Location Location(string cnpj = null)
        {
            return new Faker<Location>()
                .CustomInstantiator(f =>
                    new Location(cnpj, 
                        f.Name.JobArea(), 
                        f.Address.Country(), 
                        f.Address.State(), 
                        f.Address.City(),
                        f.Address.FullAddress(), 
                        f.Address.ZipCode()));
        }

        public static Product Product(string name = null, decimal? value = null)
        {
            return new Faker<Product>()
                .CustomInstantiator(f =>
                    new Product(!string.IsNullOrWhiteSpace(name) ? name : f.Name.Random.Word(), 
                        new Category(f.Name.Random.Word()), 
                        value ?? f.Random.Number(0,500)));
        }

        public static Orders Order(string name = null)
        {
            return new Faker<Orders>()
                .CustomInstantiator(f =>
                    new Orders(new Ticket(f.Random.Int(0, 500).ToString()),
                        new Attendant($"{f.Person.FirstName} {f.Person.LastName}",
                            f.Person.Cpf(),
                            f.PickRandom<SexEnum>())));
        }

        public static List<Orders> OrderList(int count)
        {
            return new Faker<Orders>()
                .CustomInstantiator(f =>
                    new Orders(new Ticket(f.Random.Int(0, 500).ToString()),
                        new Attendant($"{f.Person.FirstName} {f.Person.LastName}",
                            f.Person.Cpf(),
                            f.PickRandom<SexEnum>()))).Generate(count).ToList();
        }

        public static AppUser User(string name = null)
        {
            return new Faker<AppUser>()
                .CustomInstantiator(f =>
                new AppUser(
                    f.Person.Email,
                    f.Person.UserName,
                    f.Random.AlphaNumeric(6),
                    !string.IsNullOrWhiteSpace(name) ? name : f.Person.FirstName,
                    f.Person.Cpf(),
                    f.PickRandom<SexEnum>()));
        }

        //public static Payment Payment()
        //{
        //    return new Faker<Payment>
        //}
    }
}
