﻿using System;
using System.Collections.Generic;
using System.Text;
using Bogus;
using Bogus.Extensions.Brazil;
using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Models;
using Meep.Pos.Service.ValueObjects;

namespace Meep.Pos.Service.Test.Mock
{
    public static class MockExtension
    {
        public static ProductValueObject Mock(this ProductValueObject product)
        {
            product = new Faker<ProductValueObject>()
                .CustomInstantiator(f => new ProductValueObject()
                {
                    Name = f.Name.Random.Word(),
                    Price = f.Random.Decimal(0, 500)
                });

            return product;
        }

        public static CategoryValueObject Mock(this CategoryValueObject category)
        {
            category = new Faker<CategoryValueObject>()
                .CustomInstantiator(f => new CategoryValueObject()
                    {
                        Name = f.Name.Random.Word()
                    }
                );

            return category;
        }

        public static TaxValueObject Mock(this TaxValueObject tax)
        {
            tax = new Faker<TaxValueObject>()
                .CustomInstantiator(f => new TaxValueObject()
                {
                    Value = f.Random.Decimal(0, 100),
                    CalcType = f.PickRandom<PriceCalcTypeEnum>(),
                    Description = f.Random.Word()
                });

            return tax;
        }

        public static TaxValueObject Mock(this TaxValueObject tax, string description)
        {
            tax = new Faker<TaxValueObject>()
                .CustomInstantiator(f => new TaxValueObject()
                {
                    Value = f.Random.Decimal(0, 100),
                    CalcType = f.PickRandom<PriceCalcTypeEnum>(),
                    Description = description
                });

            return tax;
        }

        public static DiscountValueObject Mock(this DiscountValueObject tax)
        {
            tax = new Faker<DiscountValueObject>()
                .CustomInstantiator(f => new DiscountValueObject()
                {
                    Value = f.Random.Decimal(0, 100),
                    CalcType = f.PickRandom<PriceCalcTypeEnum>(),
                    Description = f.Random.Word()
                });

            return tax;
        }

        public static DiscountValueObject Mock(this DiscountValueObject tax, string description)
        {
            tax = new Faker<DiscountValueObject>()
                .CustomInstantiator(f => new DiscountValueObject()
                {
                    Value = f.Random.Decimal(0, 100),
                    CalcType = f.PickRandom<PriceCalcTypeEnum>(),
                    Description = description
                });

            return tax;
        }

        public static AttendantValueObject Mock(this AttendantValueObject tax)
        {
            tax = new Faker<AttendantValueObject>()
                .CustomInstantiator(f => new AttendantValueObject()
                {
                    Name = f.Name.FullName(),
                    Cpf = f.Person.Cpf(),
                    Sex = f.PickRandom<SexEnum>()
                    // PinCode = f.
                });

            return tax;
        }

        public static AttendantValueObject Mock(this AttendantValueObject tax, string name)
        {
            tax = new Faker<AttendantValueObject>()
                .CustomInstantiator(f => new AttendantValueObject()
                {
                    Name = name,
                    Cpf = f.Person.Cpf()
                });

            return tax;
        }

        public static TicketValueObject Mock(this TicketValueObject ticket)
        {
            ticket = new Faker<TicketValueObject>()
                .CustomInstantiator(f => new TicketValueObject()
                {
                    Number = f.Random.Number(1000).ToString()
                });

            return ticket;
        }

        public static LocationValueObject Mock(this LocationValueObject location, string cnpj)
        {
            location = new Faker<LocationValueObject>()
                .CustomInstantiator(f => new LocationValueObject()
                {
                    Address = f.Address.FullAddress(),
                    City = f.Address.City(),
                    Cnpj = cnpj,
                    Country = f.Address.Country(),
                    Name = f.Name.JobArea(),
                    State = f.Address.State(),
                    ZipCode = f.Address.ZipCode()

                });

            return location;
        }

        public static PaymentValueObject Mock(this PaymentValueObject payment, decimal? value = null,
            PaymentStateEnum? paymentState = null, PaymentTypeEnum? paymentType = null, Guid? attendantId = null)
        {
            return new Faker<PaymentValueObject>()
            .CustomInstantiator(f => new PaymentValueObject()
            {
                Value = value ?? f.Random.Number(0,100),
                PaymentState = paymentState ?? f.PickRandom<PaymentStateEnum>(),
                PaymentType = paymentType ?? f.PickRandom<PaymentTypeEnum>(),
                AttendantId = attendantId ?? Guid.NewGuid()
            });
        }

        public static UserValueObject Mock(this UserValueObject user, string cpf = null, string name = null,
            string login = null, string email = null, string password = null, SexEnum? sex = null)
        {
            return new Faker<UserValueObject>()
                .CustomInstantiator(f => new UserValueObject()
                {
                    Cpf = cpf ?? f.Person.Cpf(),
                    Name = name ?? f.Person.FirstName,
                    Email = email ?? f.Person.Email,
                    Login = login ?? f.Person.UserName,
                    Sex = sex ?? f.PickRandom<SexEnum>()
                });
        }
    }
}
