﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.Mapping;
using Meep.Pos.Service.Test.Mock;
using Meep.Pos.Service.ValueObjects;
using Xunit;
using Meep.Pos.Domain.Core.Exception.Discount;

namespace Meep.Pos.Service.Test
{
    public class DiscountServiceTest : IDisposable
    {
        public IDiscountService _discountService;
        public DiscountServiceTest()
        {
            var discountList = new List<Discount> { MockDomain.Discount("Promoção"), MockDomain.Discount(), MockDomain.Discount() };

            var mock = FakeRepository<Discount>.GetMock<DiscountRepository>(discountList);

            var automapper = AutoMapperConfig.RegisterMappings().CreateMapper();

            _discountService = new DiscountService(mock.Object, automapper);
        }

        #region [ Tests ]

        [Fact]
        public void Get()
        {
            var discounts = _discountService.GetAll();

            Assert.Equal(3, discounts.Count());

            var discount = discounts.FirstOrDefault(d => d.Description == "Promoção");

            var discountFound = _discountService.Get(discount.Id.Value);

            Assert.NotNull(discountFound);
            Assert.Equal(discount.Description, discountFound.Description);
        }

        [Fact]
        public void Exception_GetNonExistingDiscount()
        {
            Assert.Throws<DiscountDoesntExistsException>(() => _discountService.Get(Guid.NewGuid()));
        }

        [Fact]
        public void CreateAndGet()
        {
            var discountValueObject = new DiscountValueObject().Mock();

            var savedDiscount = _discountService.Create(discountValueObject);

            Assert.NotNull(savedDiscount);

            Assert.NotEqual(Guid.Empty, savedDiscount.Id);

            var discount = _discountService.Get(savedDiscount.Id.Value);

            Assert.NotNull(discount);

            Assert.Equal(savedDiscount.Description, discount.Description);
        }

        [Fact]
        public void Exception_CreateDiscountWithSameDescription()
        {
            Assert.Throws<DiscountAlreadyExistsException>(() => _discountService.Create(new DiscountValueObject().Mock("Promoção")));
        }

        [Fact]
        public void Update()
        {
            var discounts = _discountService.GetAll();

            Assert.Equal(3, discounts.Count());

            var discountToUpdate = discounts.FirstOrDefault(t => t.Description == "Promoção");

            Assert.NotNull(discountToUpdate);

            discountToUpdate.Value = 10;
            discountToUpdate.CalcType = PriceCalcTypeEnum.Percent;
            discountToUpdate.Description = "10% Off";

            _discountService.Update(discountToUpdate);

            var updatedDiscount = _discountService.Get(discountToUpdate.Id.Value);

            Assert.Equal(10, updatedDiscount.Value);
            Assert.Equal(PriceCalcTypeEnum.Percent, updatedDiscount.CalcType);
            Assert.Equal("10% Off", updatedDiscount.Description);
        }

        [Fact]
        public void Exception_UpdateDiscountWithoutId()
        {
            var discount = new DiscountValueObject().Mock();

            Assert.Throws<DiscountIdRequiredException>(() => _discountService.Update(discount));
        }

        [Fact]
        public void Remove()
        {
            var discount = _discountService.GetAll();

            _discountService.Remove(discount.FirstOrDefault().Id.Value);

            discount = _discountService.GetAll();

            Assert.Equal(2, discount.Count());
        }

        [Fact]
        public void Exception_RemoveNonExistingDiscount()
        {
            var discountId = _discountService.GetAll().FirstOrDefault().Id;

            _discountService.Remove(discountId.Value);

            Assert.Throws<DiscountDoesntExistsException>(() => _discountService.Remove(discountId.Value));

        }

        #endregion

        #region [ Dispose ]
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
