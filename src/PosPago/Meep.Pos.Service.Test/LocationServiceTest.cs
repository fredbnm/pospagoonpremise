﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bogus;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.Mapping;
using Meep.Pos.Service.Test.Mock;
using Meep.Pos.Service.ValueObjects;
using Xunit;
using Meep.Pos.Domain.Core.Exception.Location;

namespace Meep.Pos.Service.Test
{
    public class LocationServiceTest : IDisposable
    {
        private ILocationService _locationService;
        public LocationServiceTest()
        {
            var locationList = new List<Location> {
                MockDomain.Location("47.800.611/0001-88"),
                MockDomain.Location("77.403.813/0001-57"),
                MockDomain.Location("39.593.874/0001-35")
            };

            var mock = FakeRepository<Location>.GetMock<LocationRepository>(locationList);

            var automapper = AutoMapperConfig.RegisterMappings().CreateMapper();

            _locationService = new LocationService(mock.Object, automapper);
        }

        #region [ Methods ]

        [Fact]
        public void Get()
        {
            var locations = _locationService.GetAll();

            Assert.Equal(3, locations.Count());

            var location = locations.FirstOrDefault(d => d.Cnpj == "39.593.874/0001-35");

            var locatonFound = _locationService.Get(location.Id.Value);

            Assert.NotNull(locatonFound);
            Assert.Equal(location.ZipCode, locatonFound.ZipCode);
        }

        [Fact]
        public void Exception_CreateLocationWithSameZipCode()
        {
            var attendant = new LocationValueObject().Mock("39.593.874/0001-35");

            attendant.ZipCode = _locationService.GetAll().FirstOrDefault().ZipCode;

            Assert.Throws<LocationAlreadyExistsException>(() => _locationService.Create(attendant));
        }

        [Fact]
        public void Exception_GetNonExistinglocation()
        {
            Assert.Throws<LocationDoesntExistsException>(() => _locationService.Get(Guid.NewGuid()));
        }

        [Fact]
        public void CreateAndGet()
        {
            var LocationValueObject = new LocationValueObject().Mock("39.593.874/0001-35");

            var savedLocation = _locationService.Create(LocationValueObject);

            Assert.NotNull(savedLocation);

            Assert.NotEqual(Guid.Empty, savedLocation.Id);

            var location = _locationService.Get(savedLocation.Id.Value);

            Assert.NotNull(location);

            Assert.Equal(savedLocation.ZipCode, location.ZipCode);
        }

        [Fact]
        public void Update()
        {
            var locations = _locationService.GetAll();

            Assert.Equal(3, locations.Count());

            var location = locations.FirstOrDefault(t => t.Cnpj == "39.593.874/0001-35");

            Assert.NotNull(location);

            location.ZipCode = new Faker<string>().CustomInstantiator(f => f.Address.ZipCode());

            _locationService.Update(location);

            var updatedlocation = _locationService.Get(location.Id.Value);

            Assert.Equal(location.ZipCode, updatedlocation.ZipCode);
        }

        [Fact]
        public void Exception_UpdatelocationWithoutId()
        {
            var location = new LocationValueObject().Mock("39.593.874/0001-35");

            Assert.Throws<LocationIdRequiredException>(() => _locationService.Update(location));
        }

        [Fact]
        public void Remove()
        {
            var locations = _locationService.GetAll();

            _locationService.Remove(locations.FirstOrDefault().Id.Value);

            locations = _locationService.GetAll();

            Assert.Equal(2, locations.Count());
        }

        [Fact]
        public void Exception_RemoveNonExistingLocation()
        {
            var locationId = _locationService.GetAll().FirstOrDefault().Id;

            _locationService.Remove(locationId.Value);

            Assert.Throws<LocationDoesntExistsException>(() => _locationService.Remove(locationId.Value));

        }


        #endregion

        #region [ Dispose ]
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
