﻿using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Service.Test.Mock;
using Moq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;
using Xunit;
using Meep.Pos.Domain.Core.Exception.Device;
using Meep.Pos.Service.Mapping;
using Meep.Pos.Domain.Command.Interface;

namespace Meep.Pos.Service.Test
{
    public class DeviceAttendantServiceTest : IDisposable
    {
        private DeviceAttendantService _deviceAttendantService;

        private List<Device> _devices = new List<Device>() { new Device("7dd253524b4decd013f62d4797e57aa4", "Chrome", "1920x1080") };
        private List<Attendant> _attendants = new List<Attendant>() { MockDomain.Attendant("Bruno") };
        private List<DeviceAttendant> _deviceAttendants;
        public DeviceAttendantServiceTest()
        {
            var db = new Mock<IDbConnection>();

            _deviceAttendants = new List<DeviceAttendant>() { new DeviceAttendant(_attendants.First(a => a.Name == "Bruno"), _devices.First(a => a.Fingerprint == "7dd253524b4decd013f62d4797e57aa4")) };

            var deviceRepository = FakeRepository<Device>.GetMock<DeviceRepository>(db, _devices);
            var attendantRepository = FakeRepository<Attendant>.GetMock<AttendantRepository>(db, _attendants);
            var deviceAttendantRepository = FakeRepository<DeviceAttendant>.GetMock<DeviceAttendantRepository>(db, _deviceAttendants);
            var commandHandler = new Mock<ICommandHandler>();

            _deviceAttendantService = new DeviceAttendantService(deviceAttendantRepository.Object, 
                attendantRepository.Object, 
                deviceRepository.Object,
                commandHandler.Object,
                AutoMapperConfig.RegisterMappings().CreateMapper());
            
        }

        #region [ Tests ]

        [Fact]
        public void GetAll()
        {
            var deviceAttendant = _deviceAttendantService.GetAll();

            Assert.Equal(1, deviceAttendant.Count());

        }

        [Fact]
        public void Get()
        {
            var deviceAttendant = _deviceAttendantService.Get(_deviceAttendants.FirstOrDefault().Id);

            Assert.Equal(_deviceAttendants.FirstOrDefault().Attendant.Name, deviceAttendant.AttendantName);

        }


        [Fact]
        public void Exception_DeviceDoesntExists()
        {
            var deviceAttendant = Assert.Throws<DeviceDoesntExistsException>(() => _deviceAttendantService.Get(Guid.NewGuid()));

        }

        [Fact]
        public void LoginByDevice()
        {
            var attendant = _attendants.FirstOrDefault();
            var device = _devices.FirstOrDefault();
            var deviceAttendant = _deviceAttendants.FirstOrDefault();

            _deviceAttendantService.ChangeDeviceStatus(deviceAttendant.Id, true);

            var login = _deviceAttendantService.LoginByDevice(attendant.PinCode, device.Fingerprint, device.UserAgent, device.Resolution);

            Assert.Equal(attendant.Name, login.AttendantName);
            Assert.Equal(device.UserAgent, login.DeviceName);
        }

        [Fact]
        public void Exception_DeviceNotAuthorizedException()
        {
            var attendant = _attendants.FirstOrDefault();
            var device = _devices.FirstOrDefault();
            var deviceAttendant = _deviceAttendants.FirstOrDefault();

            Assert.Throws<DeviceNotAuthorizedException>(() => _deviceAttendantService.LoginByDevice(attendant.PinCode, "123456478794", "Mozzila", "1920x1080"));

            var devatt = _deviceAttendantService.GetByDeviceAndAttendant("123456478794", attendant.PinCode);

            Assert.Equal(attendant.Name, devatt.AttendantName);
            Assert.Equal("Mozzila", devatt.DeviceName);
            Assert.False(devatt.IsActive);
        }


        [Fact]
        public void Exception_DeviceNotAuthorizedException_AfterStatusSetToFalse()
        {
            var attendant = _attendants.FirstOrDefault();
            var device = _devices.FirstOrDefault();
            var deviceAttendant = _deviceAttendants.FirstOrDefault();

            _deviceAttendantService.ChangeDeviceStatus(deviceAttendant.Id, false);

            Assert.Throws<DeviceNotAuthorizedException>(() => _deviceAttendantService.LoginByDevice(attendant.PinCode, device.Fingerprint, "Mozzila", "1920x1080"));
            
        }


        #endregion

        #region [ Dispose ]
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
