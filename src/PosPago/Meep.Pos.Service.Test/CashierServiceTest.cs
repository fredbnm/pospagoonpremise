﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.Mapping;
using Meep.Pos.Service.Test.Mock;
using Meep.Pos.Service.ValueObjects;
using Xunit;
using Meep.Pos.Domain.Core.Exception.Cashier;
using Meep.Pos.Domain.Core.Exception.Order;
using System.Data;
using Moq;
using Meep.Pos.Service.Test.Mock.Repository;

namespace Meep.Pos.Service.Test
{
    public class CashierServiceTest : IDisposable
    {
        private ICashierService _cashierService;
        private Attendant _attendantOpenerMocked;
        private Attendant _attendantCloserMocked;
        private AppUser _userOpenerMocked;
        private AppUser _userCloserMocked;
        private Ticket _ticketMocked;
        private List<Orders> _orders;

        public CashierServiceTest()
        {
            var autoMapper = AutoMapperConfig.RegisterMappings().CreateMapper();

            _attendantOpenerMocked = MockDomain.Attendant();
            _attendantCloserMocked = MockDomain.Attendant();

            _userOpenerMocked = MockDomain.User();
            _userCloserMocked = MockDomain.User();

            _ticketMocked = MockDomain.Ticket();
            _orders = MockDomain.OrderList(3);
            var dbConnection = new Mock<IDbConnection>();

            var attendantRepository = FakeRepository<Attendant>.GetMock<AttendantRepository>(dbConnection,
            new List<Attendant>()
            {
                _attendantOpenerMocked,
                _attendantCloserMocked
            });

            var ticketRepository = FakeRepository<Ticket>.GetMock<TicketRepository>(new List<Ticket>()
            {
                _ticketMocked
            });

            var orderRepository = CustomRepository.MockOrderRepository(_orders);

            var cashierRepository = FakeRepository<Cashier>.GetMock<CashierRepository>(dbConnection);
            var userRepository = FakeRepository<AppUser>.GetMock<UserRepository>(new List<AppUser>() { _userOpenerMocked, _userCloserMocked });
            var taxRepository = FakeRepository<Tax>.GetMock<TaxRepository>(new List<Tax>() { MockDomain.Tax(required: true) });
            var ticketService = new TicketService(ticketRepository.Object, autoMapper);

            _cashierService = new CashierService(attendantRepository.Object, 
                cashierRepository.Object, ticketRepository.Object, orderRepository.Object,
                userRepository.Object, taxRepository.Object, ticketService, autoMapper);
        }

        #region [ Tests ]

        [Fact]
        public void OpenCashier()
        {
            var cashier = _cashierService.OpenCashier(_userOpenerMocked.Id, 100);

            Assert.NotEqual(Guid.Empty, cashier.Id);

            Assert.Equal(_userOpenerMocked.Id, cashier.CashierOpenerId);
        }

        [Fact]
        public void Exception_OpenCashierTwiceADay()
        {
            var cashier = _cashierService.OpenCashier(_userOpenerMocked.Id, 100);

            Assert.Throws<CashierAlreadyOpenedException>(() => _cashierService.OpenCashier(_userOpenerMocked.Id, 200));
        }

        [Fact]
        public void CloseCashier()
        {
            var cashier = _cashierService.OpenCashier(_userOpenerMocked.Id, 100);

            cashier = _cashierService.CloseCashier(cashier.Id, _userCloserMocked.Id);

            Assert.NotEqual(DateTime.MinValue, cashier.DateOpened);
            Assert.NotEqual(DateTime.MinValue, cashier.DateClosed);

        }
        
        [Fact]
        public void OpenOrder()
        {
            var cashier = _cashierService.OpenCashier(_userOpenerMocked.Id, 100);

            var cashierWithOrder = _cashierService.OpenOrder(cashier.Id, _attendantOpenerMocked.Id, _ticketMocked.Id);

            Assert.Equal(1, cashierWithOrder.OrdersId.Length);
        }

        [Fact]
        public void Exception_OpenSameOrderTwoTimes()
        {
            var cashier = _cashierService.OpenCashier(_userOpenerMocked.Id, 100);

            var cashierWithOrder = _cashierService.OpenOrder(cashier.Id, _attendantOpenerMocked.Id, _ticketMocked.Id);

            Assert.Throws<OrderAlreadyOpenedException>(
                () => _cashierService.OpenOrder(cashier.Id, _attendantOpenerMocked.Id, _ticketMocked.Id));
        }

        [Fact]
        public void CloseOrder()
        {
            var cashier = _cashierService.OpenCashier(_userOpenerMocked.Id, 100);

            var cashierWithOrder = _cashierService.OpenOrder(cashier.Id, _attendantOpenerMocked.Id, _ticketMocked.Id);

            Assert.Equal(1, cashierWithOrder.OrdersId.Length);

            var orderId = cashierWithOrder.OrdersId.FirstOrDefault();

            _cashierService.CloseOrder(cashier.Id, orderId);

            var order = _cashierService.GetOrder(cashier.Id, orderId);

            Assert.NotEqual(DateTime.MinValue, order.DateClosed);
        }
        
        [Fact]
        public void Exception_CloseCashierWithOpenOrders()
        {
            var cashier = _cashierService.OpenCashier(_userOpenerMocked.Id, 100);

            var cashierWithOrder = _cashierService.OpenOrder(cashier.Id, _attendantOpenerMocked.Id, _ticketMocked.Id);

            Assert.Throws<CashierCloseOrdersException>(
                () => _cashierService.CloseCashier(cashierWithOrder.Id, _userOpenerMocked.Id));
        }

        [Fact]
        public void GetAll()
        {
            _cashierService.OpenCashier(_userOpenerMocked.Id, 100);

            var cashiers = _cashierService.GetAll();

            Assert.Equal(1, cashiers.Count());

        }

        [Fact]
        public void OpenOrderWithRequiredTax()
        {
            var cashier = _cashierService.OpenCashier(_userOpenerMocked.Id, 100);

            var cashierWithOrder = _cashierService.OpenOrder(cashier.Id, _attendantOpenerMocked.Id, _ticketMocked.Id);

            Assert.Equal(1, cashierWithOrder.OrdersId.Length);

            var orderId = cashierWithOrder.OrdersId.FirstOrDefault();

            var order = _cashierService.GetOrder(cashier.Id, orderId);

            Assert.Equal(1, order.Taxes.Count());

        }

        [Fact]
        public void GetCurrentCashierWithOrders()
        {
            var cashier = _cashierService.OpenCashier(_userOpenerMocked.Id, 200);

            var cashierWithOrders = _cashierService.GetCurrentCashierWithOrders();

            Assert.Equal(cashier.Id, cashierWithOrders.Id);

            Assert.Equal(3, cashierWithOrders.Orders.Count());
        }

        #endregion

        #region [ Dispose ]
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
