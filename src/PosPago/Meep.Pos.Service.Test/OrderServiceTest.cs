﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.Mapping;
using Meep.Pos.Service.Test.Mock;
using Meep.Pos.Service.ValueObjects;
using Xunit;
using Meep.Pos.Domain.Core.Exception.Attendant;
using Meep.Pos.Domain.Core.Exception.Order;
using Meep.Pos.Service.Test.Mock.Repository;
using Moq;
using Meep.Pos.Domain.Command.Interface;

namespace Meep.Pos.Service.Test
{
    public class OrderServiceTest : IDisposable
    {
        private IOrderService _orderService;
        private List<Orders> _orders;
        private List<Product> _products;
        private List<Tax> _taxes;
        private List<Discount> _discounts;
        private Attendant _attendant;
        private Comission _comission;
        private IMapper _mapper;

        public OrderServiceTest()
        {
            var autoMapper = AutoMapperConfig.RegisterMappings().CreateMapper();

            _orders = MockDomain.OrderList(3);
            _products = new List<Product>() {MockDomain.Product("Cerveja", 5), MockDomain.Product("Espeto")};
            _taxes = new List<Tax>() {MockDomain.Tax("10% Garçom", PriceCalcTypeEnum.Percent, 10)};
            _discounts = new List<Discount>() {MockDomain.Discount("5% Off", PriceCalcTypeEnum.Percent, 5)};
            _attendant = MockDomain.Attendant();
            _comission = MockDomain.Comission("Estevan", 5);

            var orderRepository = CustomRepository.MockOrderRepository(_orders);
            var productRepository = FakeRepository<Product>.GetMock<ProductRepository>(_products);
            var taxRepository = FakeRepository<Tax>.GetMock<TaxRepository>(_taxes);
            var discountRepository = FakeRepository<Discount>.GetMock<DiscountRepository>(_discounts);
            var attendantRepository = FakeRepository<Attendant>.GetMock<AttendantRepository>(new List<Attendant> {_attendant});
            var comissionRepository = FakeRepository<Comission>.GetMock<ComissionRepository>(new List<Comission> { _comission });
            var configurationRepository = FakeRepository<Configuration>.GetMock<ConfigurationRepository>();
            var integrationService = new Mock<IIntegrationService>();
            var commandHandler = new Mock<ICommandHandler>();

            _orderService = new OrderService(orderRepository.Object, productRepository.Object, taxRepository.Object,
                discountRepository.Object, attendantRepository.Object, configurationRepository.Object, 
                integrationService.Object, comissionRepository.Object, commandHandler.Object, autoMapper);
        }

        #region [ Tests ]

        [Fact]
        public void AddProduct()
        { 
            var order = _orderService.AddItem(_orders.FirstOrDefault().Id,
                new OrderItemValueObject()
                {
                    ItemId = _products.FirstOrDefault(a => a.Name == "Cerveja").Id,
                    Quantity = 5
                });

            Assert.Equal(1, order.Products.Count());
            Assert.Equal(5, order.Products.FirstOrDefault().Count);
        }

        [Fact]
        public void RemoveProduct()
        {
            var order = _orderService.AddItem(_orders.FirstOrDefault().Id,
                new OrderItemValueObject() { ItemId = _products.FirstOrDefault(a => a.Name == "Cerveja").Id, Quantity = 5 });

            order = _orderService.RemoveItem(order.Id.Value, order.Products.FirstOrDefault().ProductId, 3);

            Assert.Equal(2, order.Products.FirstOrDefault().Count);

            order = _orderService.RemoveItem(order.Id.Value, order.Products.FirstOrDefault().ProductId, 3);

            Assert.Equal(0, order.Products.Count());
        }

        [Fact]
        public void AddTax()
        {
            var order = _orderService.AddItem(_orders.FirstOrDefault().Id,
                new OrderItemValueObject()
                {
                    ItemId = _taxes.FirstOrDefault(t => t.Description == "10% Garçom").Id,
                    Quantity = 1
                });

            Assert.Equal(1, order.Taxes.Count());
            Assert.Equal(1, order.Taxes.FirstOrDefault().Count);
        }

        [Fact]
        public void RemoveTax()
        {
            var order = _orderService.AddItem(_orders.FirstOrDefault().Id,
                new OrderItemValueObject()
                {
                    ItemId = _taxes.FirstOrDefault(t => t.Description == "10% Garçom").Id,
                    Quantity = 2
                });

            order = _orderService.RemoveItem(_orders.FirstOrDefault().Id,
                _taxes.FirstOrDefault(t => t.Description == "10% Garçom").Id,
                1);
            
            Assert.Equal(1, order.Taxes.FirstOrDefault().Count);

            order = _orderService.RemoveItem(_orders.FirstOrDefault().Id,
                _taxes.FirstOrDefault(t => t.Description == "10% Garçom").Id,
                1);

            Assert.Equal(0, order.Taxes.Count);
        }

        [Fact]
        public void AddDiscount()
        {
            var order = _orderService.AddItem(_orders.FirstOrDefault().Id,
                new OrderItemValueObject()
                {
                    ItemId = _discounts.FirstOrDefault(t => t.Description == "5% Off").Id,
                    Quantity = 1
                });

            Assert.Equal(1, order.Discounts.Count());
            Assert.Equal(1, order.Discounts.FirstOrDefault().Count);
        }

        [Fact]
        public void RemoveDiscount()
        {
            var order = _orderService.AddItem(_orders.FirstOrDefault().Id,
                new OrderItemValueObject()
                {
                    ItemId = _discounts.FirstOrDefault(t => t.Description == "5% Off").Id,
                    Quantity = 2
                });

            order = _orderService.RemoveItem(_orders.FirstOrDefault().Id,
                _discounts.FirstOrDefault(t => t.Description == "5% Off").Id,
                1);

            Assert.Equal(1, order.Discounts.FirstOrDefault().Count);

            order = _orderService.RemoveItem(_orders.FirstOrDefault().Id,
                _discounts.FirstOrDefault(t => t.Description == "5% Off").Id,
                1);

            Assert.Equal(0, order.Discounts.Count);
        }

        [Fact]
        public void Get()
        {
            var order = _orderService.Get(_orders.FirstOrDefault().Id);

            Assert.Equal(order.Id, _orders.FirstOrDefault().Id);
        }

        [Fact]
        public void GetAll()
        {
            var orders = _orderService.GetAll();

            Assert.Equal(_orders.Count, orders.Count());
        }

        [Fact]
        public void AddPayment()
        {
            var payment = new PaymentValueObject().Mock(10, PaymentStateEnum.Final, PaymentTypeEnum.Debit, _attendant.Id);
            
            var order = _orderService.AddItem(_orders.FirstOrDefault().Id,
                new OrderItemValueObject() { ItemId = _products.FirstOrDefault(a => a.Name == "Cerveja").Id, Quantity = 5 });

            order = _orderService.AddPayment(_orders.FirstOrDefault().Id, payment);
            
            Assert.Equal(1, order.Payments.Count);
        }

        [Fact]
        public void Exception_AddPaymentWithoutAttendant()
        {
            var payment = new PaymentValueObject().Mock(10, PaymentStateEnum.Final, PaymentTypeEnum.Debit, null);

            var order = _orderService.AddItem(_orders.FirstOrDefault().Id,
                new OrderItemValueObject() { ItemId = _products.FirstOrDefault(a => a.Name == "Cerveja").Id, Quantity = 5 });

            Assert.Throws<AttendantRequiredForPaymentException>(() => _orderService.AddPayment(_orders.FirstOrDefault().Id, payment));
            
        }

        [Fact]
        public void Exception_AddPaymentWithoutProducts()
        {
            var payment = new PaymentValueObject().Mock(10, PaymentStateEnum.Final, PaymentTypeEnum.Debit, _attendant.Id);
            
            Assert.Throws<OrderPaymentWithoutItemsException>(() => _orderService.AddPayment(_orders.FirstOrDefault().Id, payment));

        }

        [Fact]
        public void AddComission_Calc()
        {
            var order = _orderService.AddItems(_orders.FirstOrDefault().Id, new List<OrderItemValueObject>()
                {
                    new OrderItemValueObject()
                    {
                        ItemId = _products.FirstOrDefault(a => a.Name == "Cerveja").Id,
                        Quantity = 7,
                        AttendantId = _attendant.Id,
                        ComissionId = _comission.Id
                    }
                });

            Assert.Equal(Convert.ToDecimal((5*7) * 0.05), order.Products.FirstOrDefault().ComissionValue, 2);


        }
        
        //[Fact]
        //public void Close_OrderProduct

        #endregion

        #region [ Dispose ]
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
