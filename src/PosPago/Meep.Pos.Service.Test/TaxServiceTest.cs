﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.Mapping;
using Meep.Pos.Service.Test.Mock;
using Meep.Pos.Service.ValueObjects;
using Xunit;
using Meep.Pos.Domain.Core.Exception.Tax;

namespace Meep.Pos.Service.Test
{
    public class TaxServiceTest : IDisposable
    {
        private ITaxService _taxService;
        public TaxServiceTest()
        {
            var taxList = new List<Tax>{MockDomain.Tax("Rolha"), MockDomain.Tax(), MockDomain.Tax()};

            var mock = FakeRepository<Tax>.GetMock<TaxRepository>(taxList);

            var automapper = AutoMapperConfig.RegisterMappings().CreateMapper();

            _taxService = new TaxService(mock.Object, automapper);
        }

        #region [ Tests ]

        [Fact]
        public void CreateAndGet()
        {
            var taxVO = new TaxValueObject().Mock();

            var savedTax = _taxService.Create(taxVO);

            Assert.NotNull(savedTax);

            Assert.NotEqual(Guid.Empty, savedTax.Id);

            var tax = _taxService.Get(savedTax.Id.Value);

            Assert.NotNull(tax);

            Assert.Equal(savedTax.Description, tax.Description);
        }

        [Fact]
        public void Exception_CreateTaxWithSameDescription()
        {
            Assert.Throws<TaxAlreadyExistsException>(() => _taxService.Create(new TaxValueObject().Mock("Rolha")));
        }

        [Fact]
        public void Update()
        {
            var taxes = _taxService.GetAll();

            Assert.Equal(3, taxes.Count());

            var taxToUpdate = taxes.FirstOrDefault(t => t.Description == "Rolha");

            Assert.NotNull(taxToUpdate);

            taxToUpdate.Value = 10;
            taxToUpdate.CalcType = PriceCalcTypeEnum.Value;
            taxToUpdate.Description = "10% garçom";

            _taxService.Update(taxToUpdate);

            var updatedTax = _taxService.Get(taxToUpdate.Id.Value);

            Assert.Equal(10, updatedTax.Value);
            Assert.Equal(PriceCalcTypeEnum.Value, updatedTax.CalcType);
            Assert.Equal("10% garçom", updatedTax.Description);
        }

        [Fact]
        public void Exception_UpdateTaxWithoutId()
        {
            var tax = new TaxValueObject().Mock();

            Assert.Throws<TaxIdRequiredException>(() => _taxService.Update(tax));
        }

        [Fact]
        public void Remove()
        {
            var taxes = _taxService.GetAll();

            _taxService.Remove(taxes.FirstOrDefault().Id.Value);

            taxes = _taxService.GetAll();

            Assert.Equal(2, taxes.Count());
        }

        [Fact]
        public void Exception_RemoveNonExistingTax()
        {
            var taxId = _taxService.GetAll().FirstOrDefault().Id;

            _taxService.Remove(taxId.Value);

            Assert.Throws<TaxDoesntExistsException>(() => _taxService.Remove(taxId.Value));

        }

        [Fact]
        public void Get()
        {
            var taxes = _taxService.GetAll();

            Assert.Equal(3, taxes.Count());

            var tax = taxes.FirstOrDefault(d => d.Description == "Rolha");

            var taxFound = _taxService.Get(tax.Id.Value);

            Assert.NotNull(taxFound);
            Assert.Equal(tax.Description, taxFound.Description);
        }

        [Fact]
        public void Exception_GetNonExistingTax()
        {
            Assert.Throws<TaxDoesntExistsException>(() => _taxService.Get(Guid.NewGuid()));
        }

        #endregion

        #region [ Dispose ]

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
