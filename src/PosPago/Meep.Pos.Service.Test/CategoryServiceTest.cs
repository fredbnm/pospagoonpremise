﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bogus;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.Mapping;
using Meep.Pos.Service.Test.Mock;
using Meep.Pos.Service.ValueObjects;
using Xunit;
using Moq;
using Meep.Pos.Domain.Core.Exception.Category;
using System.Data;

namespace Meep.Pos.Service.Test
{
    public class CategoryServiceTest : IDisposable
    {
        private ICategoryService _categoryService;
        public CategoryServiceTest()
        {
            var db = new Mock<IDbConnection>();
            var mock = FakeRepository<Category>.GetMock<CategoryRepository>(db);

            _categoryService = new CategoryService(mock.Object, 
                AutoMapperConfig.RegisterMappings().CreateMapper());
        }

        #region [ Tests ]

        [Fact]
        public void CreateAndGet()
        {
            var categoryVO = new CategoryValueObject().Mock(); ;

            var savedCategory = _categoryService.Create(categoryVO);

            Assert.NotNull(savedCategory);
            Assert.NotEqual(Guid.Empty, savedCategory.Id);

            var category = _categoryService.Get(savedCategory.Id.Value);

            Assert.Equal(categoryVO.Name, category.Name);

        }

        [Fact]
        public void Exception_GetNonExistingCategory()
        {
            var id = Guid.NewGuid();

            Assert.Throws<CategoryDoesntExistsException>(() => _categoryService.Get(id));

        }

        [Fact]
        public void Update()
        {
            var categoryVO = new CategoryValueObject().Mock();

            var savedCategory = _categoryService.Create(categoryVO);

            savedCategory.Name = "Changed";

            var updatedCategory = _categoryService.Update(savedCategory);

            Assert.Equal(savedCategory.Id, updatedCategory.Id);

            Assert.Equal("Changed", updatedCategory.Name);

            Assert.Equal("Changed", _categoryService.Get(updatedCategory.Id.Value).Name);
        }

        [Fact]
        public void Exception_UpdateCategoryWithoutId()
        {
            var category = new CategoryValueObject().Mock();

            Assert.Throws<CategoryIdRequiredException>(() => _categoryService.Update(category));
        }

        [Fact]
        public void Remove()
        {
            var categoryVO = new CategoryValueObject().Mock();

            var savedCategory = _categoryService.Create(categoryVO);

            _categoryService.Remove(savedCategory.Id.Value);

            Assert.Throws<CategoryDoesntExistsException>(() => _categoryService.Get(savedCategory.Id.Value));

        }

        [Fact]
        public void Exception_CreateTwoCategoriesWithSameName()
        {
            var categoryVO = new CategoryValueObject().Mock();

            _categoryService.Create(categoryVO);
            
            var categoryVO2 = categoryVO;

            Assert.Throws<CategoryAlreadyExistsException>(() => _categoryService.Create(categoryVO2));
        }

        [Fact]
        public void GetAll()
        {
            var categoryVO = new CategoryValueObject().Mock();
            _categoryService.Create(categoryVO);

            categoryVO = new CategoryValueObject().Mock();
            _categoryService.Create(categoryVO);

            var categories = _categoryService.GetAll();

            Assert.Equal(2, categories.Count());
            
        }
        
        #endregion

        #region [ Dispose ]
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
