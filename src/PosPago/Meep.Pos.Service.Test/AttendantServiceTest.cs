﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bogus;
using Bogus.Extensions.Brazil;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository;
using Meep.Pos.Infra.Data.Repository.Interfaces;
using Meep.Pos.Service.Interfaces;
using Meep.Pos.Service.Mapping;
using Meep.Pos.Service.Test.Mock;
using Meep.Pos.Service.ValueObjects;
using Xunit;
using Meep.Pos.Domain.Core.Exception.Attendant;

namespace Meep.Pos.Service.Test
{
    public class AttendantServiceTest : IDisposable
    {
        private IAttendantService _attendantService;
        private List<Attendant> _attendantList;
        public AttendantServiceTest()
        {
            _attendantList = new List<Attendant> { MockDomain.Attendant("José"), MockDomain.Attendant(), MockDomain.Attendant() };

            var mock = FakeRepository<Attendant>.GetMock<AttendantRepository>(_attendantList);

            var automapper = AutoMapperConfig.RegisterMappings().CreateMapper();

            _attendantService = new AttendantService(mock.Object, automapper);
        }

        #region [ Tests ]

        [Fact]
        public void Get()
        {
            var attendants = _attendantService.GetAll();

            Assert.Equal(3, attendants.Count());

            var attendant = attendants.FirstOrDefault(d => d.Name == "José");

            var attendantFound = _attendantService.Get(attendant.Id.Value);

            Assert.NotNull(attendantFound);
            Assert.Equal(attendant.Cpf, attendantFound.Cpf);
        }

        [Fact]
        public void Exception_GetNonExistingAttendant()
        {
            Assert.Throws<AttendantDoesntExistsException>(() => _attendantService.Get(Guid.NewGuid()));
        }

        [Fact]
        public void CreateAndGet()
        {
            var attendantValueObject = new AttendantValueObject().Mock();

            var savedAttendant = _attendantService.Create(attendantValueObject);

            Assert.NotNull(savedAttendant);

            Assert.NotEqual(Guid.Empty, savedAttendant.Id);

            var attendant = _attendantService.Get(savedAttendant.Id.Value);

            Assert.NotNull(attendant);

            Assert.Equal(savedAttendant.Cpf, attendant.Cpf);
        }

        [Fact]
        public void Exception_CreateAttendantWithSameName()
        {
            var attendant = new AttendantValueObject().Mock();
            attendant.Cpf = _attendantService.GetAll().FirstOrDefault(a => a.Name == "José").Cpf;

            Assert.Throws<AttendantAlreadyExistsException>(() => _attendantService.Create(attendant));
        }

        [Fact]
        public void Update()
        {
            var attendants = _attendantService.GetAll();

            Assert.Equal(3, attendants.Count());

            var attendantToUpdate = attendants.FirstOrDefault(t => t.Name == "José");

            Assert.NotNull(attendantToUpdate);

            attendantToUpdate.Cpf = new Faker<string>().CustomInstantiator(f => f.Person.Cpf());
            attendantToUpdate.Name = "Bruce Wayne";

            _attendantService.Update(attendantToUpdate);

            var updatedAttendant = _attendantService.Get(attendantToUpdate.Id.Value);

            Assert.Equal(attendantToUpdate.Cpf, updatedAttendant.Cpf);
            Assert.Equal("Bruce Wayne", updatedAttendant.Name);
        }

        [Fact]
        public void Exception_UpdateAttendantWithoutId()
        {
            var attendant = new AttendantValueObject().Mock();

            Assert.Throws<AttendantIdRequiredException>(() => _attendantService.Update(attendant));
        }

        [Fact]
        public void Remove()
        {
            var attendants = _attendantService.GetAll();

            _attendantService.Remove(attendants.FirstOrDefault().Id.Value);

            attendants = _attendantService.GetAll();

            Assert.Equal(2, attendants.Count());
        }

        [Fact]
        public void Exception_RemoveNonExistingAttendant()
        {
            var discountId = _attendantService.GetAll().FirstOrDefault().Id;

            _attendantService.Remove(discountId.Value);

            Assert.Throws<AttendantDoesntExistsException>(() => _attendantService.Remove(discountId.Value));

        }

        //[Fact]
        //public void UpdatePinCode()
        //{
        //    var attendant = _attendantList.FirstOrDefault();

        //    var oldPin = attendant.PinCode;

        //    var attendantVO = _attendantService.NewPinCode(attendant.Id);

        //    Assert.NotEqual(oldPin, attendantVO.PinCode);
        //}

        #endregion

        #region [ Dispose ]
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
