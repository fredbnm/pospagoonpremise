using Meep.Pos.Domain.Command.Interface;
using Meep.Pos.Domain.Command.Production;
using Meep.Pos.Domain.Models;
using Meep.Pos.Infra.Data.Repository;
using Meep.Pos.Service.Test.Mock;
using Meep.Pos.Service.Test.Mock.Repository;
using Microsoft.Extensions.Caching.Memory;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace Meep.Pos.Domain.Command.Test
{
    public class ProductionSagaTest
    {
        private IProductionSaga _productionSaga;
        private ProductionRepository _productionRepository;
        private Product product;
        private Ticket ticket;
        private Attendant attendant;
        public ProductionSagaTest()
        {
            product = MockDomain.Product();
            ticket = MockDomain.Ticket();
            attendant = MockDomain.Attendant();

            _productionRepository = CustomRepository.MockProductionRepository().Object;
            var fakeProductRepository = FakeRepository<Product>.GetMock<ProductRepository>(new List<Product>() { product });
            var fakeTicketRepository = FakeRepository<Ticket>.GetMock<TicketRepository>(new List<Ticket>() { ticket });
            var fakeAttendantRepository = FakeRepository<Attendant>.GetMock<AttendantRepository>(new List<Attendant>() { attendant });

            _productionSaga = new ProductionSaga(_productionRepository, fakeProductRepository.Object, fakeTicketRepository.Object, fakeAttendantRepository.Object);
        }

        [Fact]
        public void Create()
        {
            var insertCommand = new CreateProductionCommand(ticket.Number, product.Id, product.Name, 1, "sem carne", DateTime.Now, Guid.NewGuid(), Guid.NewGuid(), attendant.Id);
            _productionSaga.Execute(insertCommand);
            //_productionRepository.Get()
        }
    }
}
