﻿using Meep.Pos.Infra.Data.Core.Interfaces;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace Meep.Pos.Infra.Data.Mongo.ConnectionProvider
{
    public class MongoDbConnectionProvider : IConnectionProvider
    {
        public const string CONNECTION_STRING_NAME = "MongoDb";
        public const string DATABASE_NAME = "MeepPos";

        private static readonly IMongoClient _client;
        private static readonly IMongoDatabase _database;

        static MongoDbConnectionProvider()
        {
            var connectionString = "mongodb://localhost";//ConfigurationManager.ConnectionStrings[CONNECTION_STRING_NAME].ConnectionString;
            _client = new MongoClient(connectionString);
            _database = _client.GetDatabase(DATABASE_NAME);
        }

        /// <summary>
        /// The private GetCollection method
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public IMongoCollection<TEntity> GetCollection<TEntity>()
        {
            return _database.GetCollection<TEntity>(typeof(TEntity).Name.ToLower() + "s");
        }
    }
}
