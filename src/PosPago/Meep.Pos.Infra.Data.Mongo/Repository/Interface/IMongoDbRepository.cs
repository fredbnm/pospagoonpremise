﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Meep.Pos.Infra.Data.Mongo.Repository.Interface
{
    public interface IMongoDbRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="filter"></param>
        /// <returns></returns>
        GetOneResult<TEntity> GetOne<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class;

        Result<TEntity> AddOne<TEntity>(TEntity item) where TEntity : class;

        Result<TEntity> DeleteOne<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class;

        GetManyResult<TEntity> GetMany<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class;

        GetManyResult<TEntity> GetAll<TEntity>() where TEntity : class;
    }
}
