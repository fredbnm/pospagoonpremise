﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Core.Models;
using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Core.Exception.Attendant;

namespace Meep.Pos.Domain.Models
{
    public class Payment : BaseModel
    {
        private Attendant _attendant;
        private decimal _value;
        private DateTime _date;
        private PaymentStateEnum _paymentState;
        private PaymentTypeEnum _paymentType;

        protected Payment()
        {

        }

        public Payment(decimal value, PaymentStateEnum paymentState, PaymentTypeEnum paymentType, Attendant attendant)
        {
            if (attendant == null)
                throw new AttendantRequiredForPaymentException();

            _value = value;
            _attendant = attendant;
            _paymentType = paymentType;
            _paymentState = paymentState;

            _date = DateTime.Now;
        }

        #region [ Properties ]
        public Attendant Attendant
        {
            get { return _attendant; }
        }

        public decimal Value
        {
            get { return _value; }
            private set { _value = value; }
        }

        public PaymentTypeEnum PaymentType
        {
            get { return _paymentType; }
            private set { _paymentType = value; }
        }

        public PaymentStateEnum PaymentState
        {
            get { return _paymentState; }
            private set { _paymentState = value; }
        }

        public DateTime Date
        {
            get { return _date; }
            private set { _date = value; }
        }

        #endregion
    }
}
