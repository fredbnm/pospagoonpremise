﻿using Meep.Pos.Domain.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Models
{
    public class DeviceAttendant : BaseModel
    {
        private Attendant _attendant;
        private Device _device;
        private bool _isActive;

        protected DeviceAttendant()
        {

        }

        public DeviceAttendant(Attendant attendant, Device device)
        {
            _attendant = attendant;
            _device = device;
            _isActive = false;
        }

        #region [ Properties ]

        public Attendant Attendant { get { return _attendant; } private set { _attendant = value; } }

        public Device Device { get { return _device; } private set { _device = value; } }

        public bool IsActive { get { return _isActive; } private set { _isActive = value; } }
        #endregion

        #region [ Methods ]

        public void SwitchStatus(bool active)
        {
            _isActive = active;
        }
        #endregion
    }
}
