﻿using Meep.Pos.Domain.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Models
{
    public class Device : BaseModel
    {
        private string _fingerprint;
        private string _userAgent;
        private string _resolution;

        protected Device()
        {

        }

        public Device(string fingerprint, string userAgent, string resolution)
        {
            _fingerprint = fingerprint;
            _userAgent = userAgent;
            _resolution = resolution;
        }

        #region [ Properties ]

        public string Fingerprint { get { return _fingerprint; } private set { _fingerprint = value; } }
        public string UserAgent { get { return _userAgent; } private set { _userAgent = value; } }
        public string Resolution { get { return _resolution; } private set { _resolution = value; } }

        #endregion
    }
}
