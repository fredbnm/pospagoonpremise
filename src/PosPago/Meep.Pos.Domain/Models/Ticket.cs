﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Core.Models;
using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Core.Util;

namespace Meep.Pos.Domain.Models
{
    /// <summary>
    /// Comanda
    /// </summary>
    public class Ticket : BaseModel
    {
        private string _number;
        private TicketTypeEnum _type;

        protected Ticket()
        {

        }

        public Ticket(string number)
        {
            _number = number;
            _type = TicketTypeEnum.Constant;
        }

        #region [ Properties ]

        public string Number
        {
            get { return _number; }
            set { _number = value; }
        }

        public TicketTypeEnum Type
        {
            get { return _type; }
            set { _type = value; }
        }

        #endregion

        public void Change(string number)
        {
            _number = number;
        }

        public void NewNumber()
        {
            _number = RandomGenerator.Ticket();
        }

        public static Ticket Express()
        {
            var ticket = new Ticket(RandomGenerator.Ticket());
            ticket._type = TicketTypeEnum.Express;

            return ticket;
        }
    }
}
