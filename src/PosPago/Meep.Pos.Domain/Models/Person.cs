﻿using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Models
{
    public class Person : BaseModel
    {
        private string _name;
        private string _cpf;
        private SexEnum _sex;

        protected Person()
        {

        }
        public Person(string name = "", string cpf = "", SexEnum sex = SexEnum.M)
        {
            _name = name;
            _cpf = cpf;
            _sex = sex;
        }

        public string Name
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(_name))
                    return _name;
                else
                    return "";
            }
            set { _name = value; }
        }

        public string Cpf
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(_cpf))
                    return _cpf;
                else
                    return "";
            }
            set { _cpf = value; }
        }

        public SexEnum Sex
        {
            get
            {
                if (_sex != 0)
                    return _sex;
                else
                    return SexEnum.M;
            }

            set { _sex = value; }
        }

    }
}
