﻿using Meep.Pos.Domain.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Models
{
    public class Comission : BaseModel
    {
        string _description;
        decimal _value;

        private Comission()
        {

        }
        public Comission(string name, decimal value)
        {
            _description = name;
            _value = value;
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public decimal Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
