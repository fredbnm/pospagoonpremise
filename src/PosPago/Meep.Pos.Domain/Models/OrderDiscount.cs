﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Meep.Pos.Domain.Core.Models;
using Meep.Pos.Domain.Models.Interface;

namespace Meep.Pos.Domain.Models
{
    public class OrderDiscount : BaseModel, IOrderItem
    {
        private readonly Discount _discount;
        private DateTime _dateOrder;
        private Attendant _attendant;
        private decimal _count;
        protected OrderDiscount()
        {

        }
        internal OrderDiscount(Discount discount, decimal count, Attendant attendant )
        {
            _discount = discount;
            _dateOrder = DateTime.Now;
            _count = count;
            _attendant = attendant;
        }

        #region [ Properties ]
        public Discount Discount
        {
            get { return _discount; }
        }

        public DateTime DateOrder
        {
            get { return _dateOrder; }
            protected set { _dateOrder = value; }
        }

        public decimal Count
        {
            get { return _count; }
            protected set { _count = value; }
        }

        public decimal Value
        {
            get
            {
                switch (Discount.CalcType)
                {
                    case Core.Enumerators.PriceCalcTypeEnum.Value:
                    default:
                        return Discount.Value;
                        break;
                    case Core.Enumerators.PriceCalcTypeEnum.Percent:
                        return Discount.Value / 100;
                        break;
                }
            }
        }

        public decimal Total
        {
            get
            {
                return (_discount.Value * _count);
            }
        }

        public Attendant Attendant
        {
            get
            {
                return _attendant;
            }
        }

        #endregion

        #region [ Methods ]

        public void AddDiscounts(decimal count)
        {
            _count += count;
        }

        public void RemoveDiscounts(decimal count)
        {
            _count -= count;
        }

        #endregion

    }
}
