﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Core.Models;
using Meep.Pos.Domain.Core.Exception.Cashier;

namespace Meep.Pos.Domain.Models
{
    public class Cashier : BaseModel
    {
        private IList<Orders> _orders;
        private decimal _initialValue;
        private bool _isOpen;
        private DateTime _dateOpened;
        private DateTime _dateClosed;
        private Person _cashierOpenerAttendant;
        private Person _cashierCloserAttendant;

        public Cashier()
        {
            _orders = new List<Orders>();
        }

        #region [ Properties ]

        public IEnumerable<Orders> Orders
        {
            get { return _orders; }
            set { _orders = value.ToList(); }
        }

        public bool IsOpen
        {
            get { return DateOpened != DateTime.MinValue && DateClosed == DateTime.MinValue; }
            set { _isOpen = DateOpened != DateTime.MinValue; }
        }

        public DateTime DateOpened
        {
            get { return _dateOpened; }
            private set { _dateOpened = value == DateTime.MinValue ? _dateOpened : value; } 
        }

        public DateTime DateClosed
        {
            get { return _dateClosed; }
            private set { _dateClosed = value == DateTime.MinValue ? _dateClosed : value; }
        }

        public decimal InitialValue
        {
            get { return _initialValue; }
            private set { _initialValue = value; }
        }

        public Person CashierOpener
        {
            get { return _cashierOpenerAttendant; }
            private set { _cashierOpenerAttendant = value; }
        }

        public Person CashierCloser
        {
            get { return _cashierCloserAttendant; }
            private set { _cashierCloserAttendant = value; }
        }

        public decimal Total
        {
            get { return _orders.Sum(o => o.TotalPayment) + InitialValue; }
            //set { Total = value == decimal.MinValue ? _orders.Sum(o => o.TotalPayment) + InitialValue : value ; }
        }

        #endregion

        #region  [ Methods ]

        public void Open(Person attendant, decimal initialValue)
        {
            if (this.IsOpen)
                throw new CashierAlreadyOpenedException();

            if (attendant == null)
                throw new CashierUserRequiredException();

            _initialValue = initialValue;
            _cashierOpenerAttendant = attendant;
            _isOpen = true;
            _dateOpened = DateTime.Now;
        }

        public void Close(Person attendant)
        {
            IsCashierOpen();

            if (attendant == null)
                throw new CashierUserRequiredException();

            if (Orders.Any(o => o.DateClosed == DateTime.MinValue))
                throw new CashierCloseOrdersException();

            _cashierCloserAttendant = attendant;
            _isOpen = false;
            _dateClosed = DateTime.Now;
        }

        protected void IsCashierOpen()
        {
            if (!this.IsOpen)
                throw new CashierMustBeOpenedException();
        }

        /// <summary>
        /// Trocar "mesa"
        /// </summary>
        /// <param name="ticketNumber"></param>
        /// <param name="newTicketNumber"></param>
        public void ChangeTicket(string ticketNumber, string newTicketNumber)
        {
            IsCashierOpen();

            Orders order = _orders.FirstOrDefault(o => o.Ticket.Number == ticketNumber);

            order.ChangeTicket(newTicketNumber);
        }

        public void StartOrder(Orders order)
        {
            IsCashierOpen();

            _orders.Add(order);
        }

        public Orders GetOrder(Guid orderId)
        {
            return _orders.FirstOrDefault(o => o.Id == orderId);
        }

        #endregion
    }
}
