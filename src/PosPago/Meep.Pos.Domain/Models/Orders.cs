﻿using System;
using System.Collections.Generic;
using System.Linq;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Core.Models;
using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Models.Interface;
using Meep.Pos.Domain.Core.Exception.Order;

namespace Meep.Pos.Domain.Models
{
    public class Orders : BaseModel
    {
        private Ticket _ticket;
        private DateTime _dateClosed;
        private DateTime _dateOpened;

        private IList<IOrderItem> _orderItems;

        private Attendant _attendant;
        private IList<Payment> _payments;
        private decimal _value;
        private bool _isOpen;
        private string _customerCpf;

        protected Orders()
        {
            _orderItems = new List<IOrderItem>();
            _payments = new List<Payment>();
        }

        public Orders(Ticket ticket, Attendant attendant)
        {
            _ticket = ticket;
            _attendant = attendant;
            _dateOpened = DateTime.Now;
            _isOpen = true;

            _orderItems = new List<IOrderItem>();
            _payments = new List<Payment>();
        }

        #region [ Properties ]

        public Ticket Ticket { get { return _ticket; } }

        public IEnumerable<OrderProduct> OrderProducts
        {
            get
            {
                return _orderItems?.OfType<OrderProduct>().ToList();
            }
        }

        public IEnumerable<OrderTax> OrderTaxes
        {
            get
            {
                return _orderItems?.OfType<OrderTax>().ToList();
            }
        }

        public IEnumerable<OrderDiscount> OrderDiscounts
        {
            get
            {
                return _orderItems?.OfType<OrderDiscount>().ToList();
                ;
            }
        }

        public Attendant Attendant
        {
            get { return _attendant; }
            protected set { _attendant = value; }
        }

        public IEnumerable<IOrderItem> OrderItems
        {
            get { return _orderItems; }
        }

        public DateTime DateOpened
        {
            get { return _dateOpened; }
            protected set { _dateOpened = value; }
        }

        public DateTime DateClosed
        {
            get { return _dateClosed; }
            protected set { _dateClosed = value; }
        }

        public bool IsOpen
        {
            get { return _isOpen; }
            set { _isOpen = value; }
        }

        public string CustomerCpf
        {
            get { return string.IsNullOrWhiteSpace(_customerCpf) ? "" : _customerCpf; }
            set { _customerCpf = value; }
        }

        public decimal Value
        {
            get { return _value; }
            protected set { _value = value; }
        }

        public decimal Total
        {
            get { return Math.Round(TotalAccount(), 2); }
        }

        public decimal TotalPayment
        {
            get { return _payments != null ? _payments.Sum(pay => pay.Value) : 0; }
        }

        public IEnumerable<Payment> Payments
        {
            get { return _payments; }
        }

        public decimal TotalDiscount
        {
            get {
                return (OrderDiscounts != null || OrderDiscounts.Any()) ? 
                    Math.Round(OrderDiscounts.Sum(d => d.Total), 2) : 
                    0;
            }
        }
        
        #endregion

        #region [ Methods ]

        private decimal TotalAccount()
        {
            decimal total = decimal.MinValue;

            if (OrderProducts != null && OrderDiscounts != null && OrderTaxes != null)
            {
                //Gasto total em produtos.
                var totalProducts = OrderProducts.Sum(item => item.Total);

                //Gasto total em taxas fixas.
                var totalTaxes = OrderTaxes.Where(tax => tax.Tax.CalcType == PriceCalcTypeEnum.Value)
                    .Sum(tax => tax.Total);

                //Desconto total em dinheiro.
                var totalDiscount = OrderDiscounts.Where(disc => disc.Discount.CalcType == PriceCalcTypeEnum.Value)
                    .Sum(disc => disc.Total);

                total = (totalProducts + totalTaxes);

                //Aplicando calculo de porcentagem.
                if (OrderTaxes.Any(tax => tax.Tax.CalcType == PriceCalcTypeEnum.Percent))
                {
                    decimal taxPercent = OrderTaxes.Where(tax => tax.Tax.CalcType == PriceCalcTypeEnum.Percent)
                        .Sum(tax => tax.Value);

                    total += total * taxPercent;
                }

                //Aplicando descontos.
                if (OrderDiscounts.Any(disc => disc.Discount.CalcType == PriceCalcTypeEnum.Percent))
                {
                    decimal discountPercent = OrderDiscounts.Where(disc => disc.Discount.CalcType == PriceCalcTypeEnum.Percent)
                        .Sum(disc => disc.Value);

                    total -= total * discountPercent;
                }

                total -= totalDiscount;
            }
            return total;
        }

        public IOrderItem AddItem<T>(T item, decimal count = 1,  string description = null, Attendant attendant = null, Comission comission = null) where T : BaseModel
        {
            IsOrderOpened();

            if (attendant == null)
                attendant = this.Attendant;

            if (item is Product)
            {
                OrderProduct orderProduct;
                if (OrderProducts != null && OrderProducts.Any(p => p.Product.Id == item.Id && p.Product.Unit != MeasureUnitEnum.Kg))
                {
                    orderProduct = OrderProducts.FirstOrDefault(p => p.Product.Id == item.Id);

                    if (comission != null)
                        orderProduct.AddComission(comission);

                    orderProduct.AddItems(count);
                }
                else
                {
                    orderProduct = new OrderProduct(item as Product, count, attendant, description);

                    if (comission != null)
                        orderProduct.AddComission(comission);

                    _orderItems.Add(orderProduct);
                }

                return orderProduct;
            }

            if (item is Tax)
            {
                OrderTax orderTax;
                if (OrderTaxes != null && OrderTaxes.Any(t => t.Tax.Id == item.Id))
                {
                    orderTax = OrderTaxes.FirstOrDefault(t => t.Tax.Id == item.Id);
                    orderTax.AddTaxes(count);
                }
                else
                {
                    orderTax = new OrderTax(item as Tax, count, attendant);
                    _orderItems.Add(orderTax);
                }

                return orderTax;
            }

            if (item is Discount)
            {
                OrderDiscount orderDiscount;
                if (OrderDiscounts != null && OrderDiscounts.Any(d => d.Discount.Id == item.Id))
                {

                    orderDiscount = OrderDiscounts.FirstOrDefault(d => d.Discount.Id == item.Id);
                    orderDiscount.AddDiscounts(count);
                }
                else
                {
                    orderDiscount = new OrderDiscount(item as Discount, count, attendant);
                    _orderItems.Add(orderDiscount);
                }
                return orderDiscount;
            }

            return null;
        }

        public void RemoveItem<T>(T item, decimal count = 1) where T : BaseModel
        {
            IsOrderOpened();

            if (item is Product)
            {

                if (OrderProducts.Any(p => p.Product.Id == item.Id))
                {
                    OrderProduct orderProduct = OrderProducts
                        .FirstOrDefault(p => p.Product.Id == item.Id);

                    if (orderProduct.Count <= count || orderProduct.Product.Unit == MeasureUnitEnum.Kg)
                        _orderItems.Remove(orderProduct);
                    else
                        orderProduct.RemoveItems(count);
                }
                else
                    throw new OrderItemDoesntExistsException();
            }

            if (item is Tax)
            {
                if (OrderTaxes.Any(p => p.Tax.Id == item.Id))
                {
                    OrderTax orderTax = OrderTaxes
                        .FirstOrDefault(p => p.Tax.Id == item.Id);

                    if (orderTax.Count <= count)
                        _orderItems.Remove(orderTax);
                    else
                        orderTax.RemoveTaxes(count);
                }
                else
                    throw new OrderItemDoesntExistsException();
            }

            if (item is Discount)
            {
                if (OrderDiscounts.Any(p => p.Discount.Id == item.Id))
                {
                    OrderDiscount orderDiscount = OrderDiscounts
                        .FirstOrDefault(p => p.Discount.Id == item.Id);

                    if (orderDiscount.Count <= count)
                        _orderItems.Remove(orderDiscount);
                    else
                        orderDiscount.RemoveDiscounts(count);
                }
                else
                    throw new OrderItemDoesntExistsException();
            }
        }

        public void ClearItems()
        {
            _orderItems = new List<IOrderItem>();
        }

        public void AddPayment(Payment payment)
        {
            IsOrderOpened();

            if (!OrderItems.Any())
                throw new OrderPaymentWithoutItemsException();

            _payments.Add(payment);
        }
        
        private bool IsOrderOpened()
        {
            if (_dateClosed != DateTime.MinValue)
                throw new OrderAlreadyClosedException();
            else
                return true;
        }

        public int GetQuantityByProduct(Product product)
        {
            return (int)OrderProducts.Where(item => item.Product.Id == product.Id).Select(item => (int)item.Count).Sum();
        }

        public void Close()
        {
            if (_dateClosed != DateTime.MinValue)
                throw new OrderAlreadyClosedException();

            if (TotalPayment < Total)
                throw new OrderCannotCloseException(Total - TotalPayment);

            _dateClosed = DateTime.Now;

            _isOpen = false;
        }

        public void ChangeTicket(string number)
        {
            _ticket.Change(number);
        }

        public void AttachCustomer(string cpf)
        {
            this._customerCpf = cpf;
        }

        //public OrderProduct GetOrderProduct(Attendant attendant, Product product)
        //{
        //    return this.OrderProducts.FirstOrDefault(o => o.Attendant.Id == attendant.Id && o.Product.Id)
        //}
        #endregion
    }
}
