﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Meep.Pos.Domain.Core.Models;
using Meep.Pos.Domain.Models.Interface;
using Meep.Pos.Domain.Core.Enumerators;

namespace Meep.Pos.Domain.Models
{
    public class OrderTax : BaseModel, IOrderItem
    {
        private DateTime _dateOrder;
        private decimal _count;
        private Tax _tax;
        private Attendant _attendant;

        protected OrderTax()
        {

        }

        internal OrderTax(Tax tax, decimal quantidade, Attendant attendant)
        {
            _dateOrder = DateTime.Now;
            _count = quantidade;
            _tax = tax;
            _attendant = attendant;
        }

        #region [ Properties ]

        public DateTime DateOrder
        {
            get { return _dateOrder; }
        }

        public decimal Count
        {
            get { return _count; }
        }

        public Tax Tax
        {
            get { return _tax; }
        }

        public decimal Value {
            get
            {
                switch (Tax.CalcType)
                {
                    case PriceCalcTypeEnum.Value:
                    default:
                        return Tax.Value;
                        break;
                    case PriceCalcTypeEnum.Percent:
                        return Tax.Value / 100;
                        break;
                }

            }
        }

        public decimal Total
        {
            get
            {
                return Tax.Value * Count; 
                
            } 
            
        }

        public Attendant Attendant
        {
            get
            {
                return _attendant;
            }
        }

        public void AddTaxes(decimal count)
        {
            _count += count;
        }

        public void RemoveTaxes(decimal count)
        {
            _count -= count;
        }

        #endregion
    }
}
