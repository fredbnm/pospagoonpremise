﻿using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Core.Models;
using Meep.Pos.Domain.Core.Models.Event;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Models
{
    public class Event : BaseModel, IEvent
    {
        private EventTypeEnum _eventType;
        private string _description;
        private DateTime _timeStamp;
        private bool _isRead;
        private Guid _identifier;

        public Event(EventTypeEnum eventType, string description, Guid identifier)
        {
            _eventType = eventType;
            _description = description;
            _identifier = identifier;
            _timeStamp = DateTime.Now;
        }

        public Guid Identifier { get => _identifier; set => _identifier = value; }
        public EventTypeEnum EventType { get => _eventType; set => _eventType = value; }
        public DateTime TimeStamp { get => _timeStamp; set => _timeStamp = value; }
        public string Message { get => _description; set => _description = value; }
        public bool IsRead { get => _isRead; set => _isRead = value; }
    }
}
