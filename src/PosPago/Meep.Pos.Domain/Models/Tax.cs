﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Core.Models;
using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Models.Interface;
using Meep.Pos.Domain.Core.Exception.Tax;

namespace Meep.Pos.Domain.Models
{
    public class Tax : BaseModel, ITaxDiscount
    {
        private string _description;
        private decimal _value;
        private PriceCalcTypeEnum _calcType;
        private bool _required;

        protected Tax()
        {

        }

        public Tax(string description, PriceCalcTypeEnum calcType, decimal value, bool required = false)
        {
            ValidateValue(calcType, value);

            _description = description;
            _calcType = calcType;
            _value = value;
            _required = required;
        }

        #region [ Properties ] 
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public decimal Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public PriceCalcTypeEnum CalcType
        {
            get { return _calcType; }
            set { _calcType = value; }
        }

        public bool Required
        {
            get { return _required; }
            set { _required = value; }
        }

        #endregion

        #region [ Methods ]

        private static void ValidateValue(PriceCalcTypeEnum calcType, decimal value)
        {
            if (calcType == PriceCalcTypeEnum.Percent && (value > 100 || value < 0))
                throw new TaxPercentageException();
        }

        #endregion
    }
}
