﻿using Meep.Pos.Domain.Core.Enumerators.Configuration;
using Meep.Pos.Domain.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Models
{
    public class ConfigurationEndpoint : BaseModel
    {
        private string _endpoint;
        private FlowTypeEnum _flow;
        private PipelineEnum _pipeline;
        private bool _continueOnError;
        private EndpointTypeEnum _endpointType;

        private ConfigurationEndpoint()
        {

        }

        public ConfigurationEndpoint(string endpoint, FlowTypeEnum flow, PipelineEnum pipeline, EndpointTypeEnum type, bool continueOnError)
        {
            _endpoint = endpoint;
            _flow = flow;
            _pipeline = pipeline;
            _endpointType = type;
            _continueOnError = continueOnError;
        }

        public string Endpoint
        {
            get { return _endpoint; }
            set { _endpoint = value; }
        }

        public FlowTypeEnum Flow
        {
            get { return _flow; }
            set { _flow = value; }
        }

        public PipelineEnum Pipeline
        {
            get { return _pipeline; }
            set { _pipeline = value; }
        }

        public EndpointTypeEnum EndpointType
        {
            get { return _endpointType; }
            set { _endpointType = value; }
        }

        public bool ContinueOnError
        {
            get { return _continueOnError; }
            set { _continueOnError = value; }
        }
    }
}
