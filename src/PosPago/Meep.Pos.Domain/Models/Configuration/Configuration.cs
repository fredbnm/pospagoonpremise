﻿using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Models
{
    public class Configuration : BaseModel
    {
        private MeepTypeEnum _meepType;
        private DateTime _creationDate;
        private bool _integration;
        private List<ConfigurationEndpoint> _endpoints;

        private Configuration()
        {
            _creationDate = DateTime.Now;
            _endpoints = new List<ConfigurationEndpoint>();
        }

        public Configuration(MeepTypeEnum meepType = MeepTypeEnum.Pos, bool nfIntegration = false)
        {
            _meepType = meepType;
            _integration = nfIntegration;
            _creationDate = DateTime.Now;
            _endpoints = new List<ConfigurationEndpoint>();
        }

        #region [ Propriedades ]

        public MeepTypeEnum MeepType
        {
            get { return _meepType; }
            set { _meepType = value; }
        }

        public bool Integration
        {
            get { return _integration; }
            set { _integration = value; }
        }

        public DateTime CreationDate
        {
            get { return _creationDate; }
            set { _creationDate = value; }
        }

        public List<ConfigurationEndpoint> Endpoints
        {
            get { return _endpoints; }
            set { _endpoints = value; }
        }

        #endregion

        #region [ Métodos ]

        public void AddEndpoint(ConfigurationEndpoint endpoint)
        {
            _endpoints.Add(endpoint);
        }

        #endregion
    }
}
