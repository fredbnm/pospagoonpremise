﻿using Meep.Pos.Domain.Core.Models;
using Meep.Pos.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain
{
    public class Production : BaseModel
    {
        private Ticket _ticket;
        private Product _product;
        private decimal _quantity;
        private bool _isFinalized;
        private DateTime _date;
        private Guid _idOrder;
        private Guid _idOrderItem;
        private string _observation;
        private Attendant _attendant;

        public Production(Ticket ticket, Product product, Attendant attendant, decimal quantity, DateTime date, string observation)
        {
            _ticket = ticket;
            _product = product;
            _quantity = quantity;
            _attendant = attendant;
            _date = date;
            _observation = observation;
            _isFinalized = false;            
        }

        #region [ Properties ]

        public Ticket Ticket
        {
            get { return _ticket; }
            private set { _ticket = value; }
        }
        public Product Product
        {
            get { return _product; }
            private set { _product = value; }
        }

        public decimal Quantity
        {
            get { return _quantity; }
            private set { _quantity = value; }
        }

        public Guid OrderId
        {
            get { return _idOrder; }
            private set { _idOrder = value; }
        }

        public Guid OrderItemId
        {
            get { return _idOrderItem; }
            private set { _idOrderItem = value; }
        }

        public bool IsFinalized
        {
            get { return _isFinalized; }
            private set { _isFinalized = value; }
        }

        public DateTime Date
        {
            get { return _date; }
            private set { _date = value; }
        }

        public string Observation
        {
            get { return _observation; }
            private set { _observation = value; }
        }

        public Attendant Attendant
        {
            get { return _attendant; }
            private set { _attendant = value; }
        }
        #endregion

        #region [ Methods ]

        public void Finalize()
        {
            _isFinalized = true;
        }

        public void AttachOrder(Guid id, Guid idOrderItem)
        {
            _idOrder = id;
            _idOrderItem = idOrderItem;
        }

        #endregion
    }
}
