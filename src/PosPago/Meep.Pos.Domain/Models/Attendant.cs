﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Core.Models;
using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Core.Util;

namespace Meep.Pos.Domain.Models
{
    public class Attendant : Person
    {
        private static readonly int _min = 1000;
        private static readonly int _max = 9999;

        private int _pinCode;

        protected Attendant()
        {

        }

        public Attendant(string name, string cpf, SexEnum sex) : base(name, cpf, sex)
        {
            _pinCode = RandomGenerator.Int(_min, _max);
        }

        public int PinCode
        {
            get { return _pinCode; }
            set { _pinCode = value == 0 ? _pinCode : value; }
        }

        public void NewPin()
        {
            this._pinCode = RandomGenerator.Int(_min, _max);
        }
     
    }
}
