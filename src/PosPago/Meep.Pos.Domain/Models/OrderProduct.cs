﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Core.Models;
using Meep.Pos.Domain.Models.Interface;
using Meep.Pos.Domain.Core.Exception.Order;

namespace Meep.Pos.Domain.Models
{
    public class OrderProduct : BaseModel, IOrderItem
    {
        private readonly Product _product;
        private DateTime _dateOrder;
        private decimal _count;
        private string _description;
        private Attendant _attendant;
        private DateTime? _dateFinalized;
        private Comission _comission;

        protected OrderProduct()
        {
            //_comission = new Comission();
        }

        internal OrderProduct(Product product, decimal quantity, Attendant attendant)
        {
            _product = product;
            _dateOrder = DateTime.Now;
            _count = quantity;
            _attendant = attendant;
        }

        internal OrderProduct(Product product, decimal quantity, Attendant attendant, string description = null)
        {
            _product = product;
            _dateOrder = DateTime.Now;
            _count = quantity;
            _attendant = attendant;
            _description = description;
        }
        
        #region [ Properties ]

        public Product Product
        {
            get { return _product; }
        }

        public DateTime DateOrder
        {
            get { return _dateOrder; }
            protected set { _dateOrder = value; }
        }

        public decimal Count
        {
            get { return _count; }
            protected set { _count = value; }
        }

        public decimal Value
        {
            get
            {
                return _product.Price;
            }            
        }

        public decimal Total
        {
            get
            {
                return Math.Round(_product.Price * _count, 2);
            }            
        }

        public decimal ComissionValue
        {
            get
            {
                decimal comission = 0;

                if (_comission != null)
                    comission = _comission.Value / 100;

                return Math.Round((_product.Price * _count) * comission,2);
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            protected set { _description = value; }
        }

        public Attendant Attendant
        {
            get
            {
                return _attendant;
            }
        }

        public DateTime? DateFinalized
        {
            get { return _dateFinalized; }
            private set { _dateFinalized = value; }
        }

        public Comission Comission
        {
            get { return _comission; }
            private set { _comission = value; }
        }

        #endregion

        #region [ Methods ]

        public void AddComission(Comission comission)
        {
            _comission = comission;
        }

        public void AddItems(decimal count)
        {
            _count += count;
        }

        public void RemoveItems(decimal count)
        {
            _count -= count;
        }

        public void FinalizeOrder()
        {
            if (_dateFinalized.HasValue)
                throw new OrderProductAlreadyFinalizedException();

            _dateFinalized = DateTime.Now;
        }

        #endregion

    }
}
