﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Core.Enumerators;

namespace Meep.Pos.Domain.Models.Interface
{
    public interface ITaxDiscount
    {
        string Description { get; }
        decimal Value { get; }
        PriceCalcTypeEnum CalcType { get; }
    }
}
