﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Models.Interface
{
    public interface IOrderItem
    {
        Guid Id { get; }
        DateTime DateOrder { get; }
        decimal Count { get; }
        decimal Value { get; }
        decimal Total { get; }
        Attendant Attendant { get; }
    }
}
