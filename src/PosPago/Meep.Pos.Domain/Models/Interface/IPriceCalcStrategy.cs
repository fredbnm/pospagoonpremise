﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Pos.Domain.Models.Interface
{
    public interface IPriceCalcStrategy
    {
        decimal GetTotal();
        
    }
}
