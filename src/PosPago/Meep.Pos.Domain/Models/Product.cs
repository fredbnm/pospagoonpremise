﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Core.Models;
using Meep.Pos.Domain.Core.Enumerators;

namespace Meep.Pos.Domain.Models
{
    public class Product : BaseModel
    {
        private string _name;
        private Category _category;
        private decimal _price;
        private MeasureUnitEnum _unit;
        private bool _isActive;
        private bool _isProduction;
        private int? _productionTime;
        private string _metadata;

        protected Product()
        {
        }

        public Product(string name, Category category, decimal price, bool isProduction = false, MeasureUnitEnum unit = MeasureUnitEnum.Unit, int? productionTime = null, string metadata = null)
        {
            _name = name;
            _category = category;
            _price = price;
            _unit = unit;
            _isProduction = isProduction;
            _productionTime = productionTime;
            _metadata = metadata;
            _isActive = true;
        }

        #region [ Properties ] 
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Category Category
        {
            get { return _category; }
            set { _category = value; }
        }

        public decimal Price
        {
            get { return _price; }
            set { _price = value; }
        }

        public MeasureUnitEnum Unit
        {
            get { return _unit; }
            set { _unit = value; }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public bool IsProduction
        {
            get { return _isProduction; }
            set { _isProduction = value; }
        }

        public string MetaData
        {
            get { return !string.IsNullOrWhiteSpace(_metadata) ? _metadata : ""; }
            set { _metadata = value; }
        }

        public int? ProductionTime
        {
            get { return _productionTime == null ? 0 : _productionTime; }
            set { _productionTime = value; }
        }

        #endregion
    }
}
