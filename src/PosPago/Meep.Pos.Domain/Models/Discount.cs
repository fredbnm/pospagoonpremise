﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Core.Models;
using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Models.Interface;
using Meep.Pos.Domain.Core.Exception.Discount;

namespace Meep.Pos.Domain.Models
{
    public class Discount: BaseModel, ITaxDiscount
    {
        private string _description;
        private decimal _value;
        private PriceCalcTypeEnum _calcType;

        protected Discount()
        {

        }

        public Discount(string description, PriceCalcTypeEnum calcType, decimal value)
        {
            ValidateValue(calcType, value);

            _description = description;
            _calcType = calcType;
            _value = value;
        }

        #region [ Properties ] 
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public decimal Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public PriceCalcTypeEnum CalcType
        {
            get { return _calcType; }
            set { _calcType = value; }
        }

        #endregion

        #region [ Methods ]

        private static void ValidateValue(PriceCalcTypeEnum calcType, decimal value)
        {
            if (calcType == PriceCalcTypeEnum.Percent && (value > 100 || value < 0))
                throw new DiscountPercentageException();
        }

        #endregion
    }
}
