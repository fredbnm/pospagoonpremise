﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Core.Models;

namespace Meep.Pos.Domain.Models
{
    public class Location : BaseModel
    {
        private string _cnpj;
        private string _name;
        private string _address;
        private string _zipCode;
        private string _country;
        private string _state;
        private string _city;
        private Guid? _meepLocationId;

        protected Location()
        {

        }

        public Location(string cnpj, string name, string country, string state, string city, string address, string zipcode, Guid? meepLocationId = null)
        {
            _cnpj = cnpj;
            _name = name;
            _address = address;
            _zipCode = zipcode;
            _country = country;
            _state = state;
            _city = city;
            _meepLocationId = meepLocationId;
        }

        #region [ Properties ]

        public string Cnpj
        {
            get { return _cnpj; }
            set { _cnpj = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }

        public Guid? MeepLocationId
        {
            get { return _meepLocationId; }
            set { _meepLocationId = value; }
        }

        #endregion
    }
}
