﻿using System;
using System.Collections.Generic;
using System.Text;
using Meep.Pos.Domain.Core.Models;

namespace Meep.Pos.Domain.Models
{
    public class Category : BaseModel
    {
        private string _name;

        protected Category()
        {

        }
        public Category(string name)
        {
            _name = name;
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}
