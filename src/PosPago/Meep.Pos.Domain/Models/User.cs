﻿using Meep.Pos.Domain.Core.Enumerators;
using Meep.Pos.Domain.Core.Exception;
using Meep.Pos.Domain.Core.Exception.User;
using Meep.Pos.Domain.Core.Util;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Meep.Pos.Domain.Models
{
    public class AppUser : Person
    {
        private string _email { get; set; }
        private string _login { get; set; }
        private string _password { get; set; }

        protected AppUser()
        {

        }
        public AppUser(string email, string login, string pasword, string name = "", string cpf = "", SexEnum sex = SexEnum.M) : base(name, cpf, sex)
        {
            _email = email;
            _login = login;
            _password = GeneratePasswordHash(pasword);
        }

        public string Email { get { return _email; } set { _email = value; } }
        public string Login { get { return _login; } set { _login = value; } }
        public string Password { get { return _password; } set { _password = string.IsNullOrWhiteSpace(value) ? _password : value; } }

        public void ChangePassword(string login, string oldPassword, string newPassword)
        {
            oldPassword = GeneratePasswordHash(oldPassword);
            ValidateFields(login, oldPassword);

            var newPasswordHash = GeneratePasswordHash(newPassword);

            if (this.Password == newPasswordHash)
                throw new UserNewPasswordSameAsOldException();

            _password = newPasswordHash;

        }

        public void ChangeEmail(string login, string password, string oldEmail, string newEmail)
        {
            password = GeneratePasswordHash(password);

            ValidateFields(login, password, oldEmail);

            _email = newEmail;
        }

        private void ValidateFields(string login = "", string password = "", string email = "")
        {
            if (!string.IsNullOrWhiteSpace(login))
                if (login != this.Login)
                    throw new UserWrongLoginException();

            if (!string.IsNullOrWhiteSpace(password))
                if (password != this.Password)
                    throw new UserWrongPasswordException();

            if (!string.IsNullOrWhiteSpace(email))
                if (email != this.Email)
                    throw new UserWrongEmailException();
        }

        public bool PasswordIsCorrect(string password)
        {
            if (string.IsNullOrWhiteSpace(password))
                return false;

            string actualHashPassword = this.Password;
            string passwordToCompare = GeneratePasswordHash(password);

            if (string.IsNullOrWhiteSpace(passwordToCompare))
                return false;

            return actualHashPassword == passwordToCompare;
        }

        private string GeneratePasswordHash(string senha)
        {
            if (string.IsNullOrWhiteSpace(senha))
                return null;

            byte[] salt = new byte[] { 0x16, 0x63, 0x67, 0x32, 0x72, 0xc4, 0x04, 0x34, 0xb5, 0x84, 0x85, 0xf7, 0xb5, 0x13, 0x92 };
            byte[] plainText = Encoding.UTF8.GetBytes(senha);
            HashAlgorithm algorithm = new SHA256Managed();

            byte[] plainTextWithSaltBytes =
              new byte[plainText.Length + salt.Length];

            for (int i = 0; i < plainText.Length; i++)
            {
                plainTextWithSaltBytes[i] = plainText[i];
            }
            for (int i = 0; i < salt.Length; i++)
            {
                plainTextWithSaltBytes[plainText.Length + i] = salt[i];
            }

            return Convert.ToBase64String(algorithm.ComputeHash(plainTextWithSaltBytes));
        }
    }
}
